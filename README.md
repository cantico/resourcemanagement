## Gestionnaire de réservations et ressources ##

Module de gestion et Réservations de Ressources : salles, véhicules, matériels ..

* Accessible depuis un navigateur
* Gestion de **différents types de ressources** (salles, voitures,matériels, facturable...)
* Attribution de **plusieurs types à une ressource** (Exemples : Salle et Facturable)
* Gestion d'une **arborescence de domaines**
* **Droits d'accès,** réservation ou notication par domaine et/ou par ressource
* **Processus d'approbation** par ressource,
* Possibilité de **rendre indisponible une ressource** (pour maintenance par exemple)
* Définition de **fi
ches de présentation** par ressource (description,champs spécique(s) au(x) type(s)..)
* Définition des informations à saisir lors de la réservation d'une ressource
* **Planning des réservation** des ressources
* **Personnalisation des jours et heures de disponibilité** pour chaque ressource
* **Réservations périodiques** possibles
* Réservations de plusieurs ressources simultanément possible (Exemple : Une salle, un ordinateur et un vidéoprojecteur ...)
* **Gestion des pré-réservations,** avec délai max. de confirmation après date de demande et/ou un délai min. de conrmation avant la date de la réservation planifiée
* **Recherche de ressources,** (par domaine, type, nom,champ spécifique, description et disponibilité)
* **Export** des données de réservation
* Possibilité de **réserver « au nom de »** pour les utilisateurs habilités
* Possibilité de consulter le planning des ressources **depuis la fonction Agenda d'Ovidentia**