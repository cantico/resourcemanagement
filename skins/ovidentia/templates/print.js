jQuery(document).ready(function(){
    jQuery(document).on('eventAfterAllRender', function() {
        jQuery('.fc-event-container').css('width', '100%');

        var table = jQuery('.fc-border-separate');
        var ratio = 100.0 / table.width();

        jQuery('.fc-day-header, .fc-widget-header, .fc-resourceName').each(function() {
            var header = jQuery(this);
            header.css('width', header.width() * ratio + '%');
            header.addClass('perciento');
        });

        contenttable = jQuery('.fc-view > div > div');
        contenttable.css('overflow-x', 'visible');
        contenttable.css('overflow-y', 'visible');
        contenttable.css('height', 'auto');

        jQuery('.fc-agenda-days tbody .fc-col0 > div').height(contenttable.height());

        jQuery('.fc-content .fc-event').each(function() {
            var event = jQuery(this);
            event.css('width', event.width() * ratio + '%');
            event.css('left', event.position().left * ratio + '%');
        //	event.css('height', event.height() * ratio + '%');
        //	event.css('top', event.position().top * ratio + '%');
        });

        return true;
    });
});