jQuery(document).ready(function(){
	jQuery("#calendar").on('calendarEventAfterAllRender', function(){
		redrawActionToolbar();
	});
});

function redrawActionToolbar(){
	jQuery('.fc-event').mouseover(function(){
		let container = jQuery(this).find('.actions');
		let containerOuterWidth = jQuery(container).outerWidth();
		let nbA = jQuery(container).children().length;

		let aOuterWidth = jQuery(container).find('a').outerWidth();
		let nbPerRow  = (containerOuterWidth - (containerOuterWidth % aOuterWidth)) / aOuterWidth;
		let aLeft = (nbA- (nbA % nbPerRow)) / nbPerRow;
		
		if(aLeft > 0)
		{
			let rowNeeded = Math.ceil(nbA / nbPerRow);
			let height = rowNeeded * aOuterWidth;
			let containerOuterHeight = jQuery(container).outerHeight();
			jQuery(container).css('min-height', height+'px');
		}
	});
}