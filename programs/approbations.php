<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

require_once dirname(__FILE__) . '/set/resource.class.php';
require_once dirname(__FILE__) . '/set/reservation.class.php';
require_once dirname(__FILE__) . '/ui/reservation.ui.php';



/**
 * This function is called when the bab_eventBeforeWaitingItemsDisplayed event is fired.
 *
 * @param bab_eventBeforeWaitingItemsDisplayed $event
 */
function resourcemanagement_onApprobationListDisplay(bab_eventBeforeWaitingItemsDisplayed $event)
{
	include_once $GLOBALS['babInstallPath'] . 'utilit/wfincl.php';
	
	$instances = bab_WFGetWaitingInstances(bab_getUserId());

	if (count($instances) === 0) {
		return;
	}

	
	$reservationSet = new resourcemanagement_ReservationSet();
	$reservationSet->resource();
	$reservations = $reservationSet->select(
		$reservationSet->approbation->in($instances)
	);
	
	$reservations->orderDesc($reservationSet->start);
	
	$reservationsWaitingValidation = array();
	
	foreach ($reservations as $reservation) {
	
		$periodDateStart = bab_shortDate(bab_mktime($reservation->start), false);
		$periodDateEnd = bab_shortDate(bab_mktime($reservation->end), false);
		
		if ($periodDateStart == $periodDateEnd) {
			$periodText = sprintf(
			    resourcemanagement_translate('On %s, from %s to %s'),
			    bab_shortDate(bab_mktime($reservation->start), false),
			    bab_time(bab_mktime($reservation->start)),
			    bab_time(bab_mktime($reservation->end))
			);
		} else {
			$periodText = sprintf(
			    resourcemanagement_translate('From %s to %s'),
			    bab_shortDate(bab_mktime($reservation->start), true),
			    bab_shortDate(bab_mktime($reservation->end), true)
			    );
		}
		
		$author = bab_getUserName($reservation->reservedBy);

		$reservationsWaitingValidation[] = array(
			'text' =>  			$reservation->resource->name,
			'description' =>  	$reservation->description . ' ' . $periodText . ' ' . $author,
			'url' =>  			resourcemanagement_Controller()->Reservation()->editApprobation($reservation->id)->url(),
			'popup' =>  		1,
			'idschi' =>  		$reservation->approbation
		);
// 		$l = array(
// 			'text' 			=> bab_getUserName($row['id_user']),
// 				'description' 	=> sprintf(absences_translate('Request %s from %s to %s'), absences_vacEntryQuantity($row['id']), absences_shortDate(bab_mktime($row['date_begin'])), absences_shortDate(bab_mktime($row['date_end']))),
// 				'url'			=> absences_addon()->getUrl()."approb&idx=confvac&idvac=".$row['id'],
// 				'popup'			=> 1,
// 				'idschi'		=> $row['idfai']
// 		);
	
	
// 		$arr[] = $l;
	}
	

	
	if (!empty($reservationsWaitingValidation)) {
		$event->addObject(resourcemanagement_translate('Reservations requests waiting to be validated'), $reservationsWaitingValidation);
	}
}




function resourcemanagement_saveApprobations()
{
	$form = bab_pp('approbation');
	$reservationsSet = new resourcemanagement_reservationsSet();
	$reservation = $reservationsSet->get($form['id']);
	if(bab_isAccessValid('resourcemanagement_manager_groups', $reservation->ressource)){
		$reservation->description = $form['description'];
		foreach($form['status'] as $k => $val){
			$reservation->status = $k;
		}
		$reservation->validator = $GLOBALS['BAB_SESS_USERID'];
		$reservation->save();
	}
	$GLOBALS['babBody']->addError(resourcemanagement_translate('Approbation succeed.'));
	resourcemanagement_closeApprobations();
}

function resourcemanagement_closeApprobations()
{
	echo '
		<script type="text/javascript">
		<!--
		window.focus();
		//-->
		function ShutdownPopup()
		{
			top.opener.location.reload();
			top.close();
		}
		setTimeout(\'ShutdownPopup()\', 1500);
		</script>
	';
}


