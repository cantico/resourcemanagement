<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';

class resourcemanagement_Controller extends bab_Controller
{
	protected function getControllerTg()
	{
		return 'addon/resourcemanagement/main';
	}


	/**
	 * Get object name to use in URL from the controller classname
	 * @param string $classname
	 * @return string
	 */
	protected function getObjectName($classname)
	{
		$prefix = strlen('resourcemanagement_Ctrl');
		return strtolower(substr($classname, $prefix));
	}


	/**
	 * Can be used in a controller to check if the current request was made by ajax.
	 * @return bool
	 */
	public function isAjaxRequest()
	{
		if (!isset($this->isAjaxRequest)) {
			$this->isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
		}
		return $this->isAjaxRequest;
	}



	/**
	 * Adds the default items menu to the page.
	 *
	 * @param Widget_BabPage $page
	 */
	protected function addDefaultItemMenu($page)
	{
	    $page->addItemMenu('calendarall', resourcemanagement_translate('CSN Calendar'), resourcemanagement_Controller()->Calendar()->displayAll()->url());
		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
		if (resourcemanagement_canManageResources() || resourcemanagement_isManager()) {
			$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
		}
		$page->addItemMenu('search', resourcemanagement_translate('Search availability'), resourcemanagement_Controller()->Search()->displayList()->url());
	}


	/**
	 * @return resourcemanagement_CtrlSearch
	 */
	public function Search($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/search.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlSearch', $proxy);
	}



	/**
	 * @return resourcemanagement_CtrlAdmin
	 */
	public function Admin($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/admin.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlAdmin', $proxy);
	}



	/**
	 * @return resourcemanagement_CtrlResource
	 */
	public function Resource($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/resource.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlResource', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlEquipment
	 */
	public function Equipment($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/equipment.ctrl.php';
	    return bab_Controller::ControllerProxy('resourcemanagement_CtrlEquipment', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlType
	 */
	public function Type($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/type.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlType', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlDomain
	 */
	public function Domain($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/domain.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlDomain', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlCalendar
	 */
	public function Calendar($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/calendar.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlCalendar', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlReservation
	 */
	public function Reservation($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/reservation.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlReservation', $proxy);
	}


	/**
	 * @return resourcemanagement_CtrlReservationType
	 */
	public function ReservationType($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/reservationtype.ctrl.php';
		return bab_Controller::ControllerProxy('resourcemanagement_CtrlReservationType', $proxy);
	}

}