<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
resourcemanagement_loadOrm();



/**
 * This is the link between resourcemanagement_Resource and resourcemanagement_Type.
 *
 * @property ORM_PkField						$id
 * @property resourcemanagement_ResourceSet		$resource
 * @property resourcemanagement_TypeSet			$type
 */
class resourcemanagement_ResourceTypeSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setPrimaryKey('id');
	
// 		$this->addFields(
// 			ORM_StringField('name')
// 		);
		
		
		$this->hasOne('resource', 'resourcemanagement_ResourceSet');
		$this->hasOne('type', 'resourcemanagement_TypeSet');
		
	}
}


/**
 * Resources can be associated to one ore more resource types.
 * Each resource type can be composed of several additional fields.
 *
 * @property int				$id
 * @property resourcemanagement_Resource		$resource
 * @property resourcemanagement_Type			$type
 */
class resourcemanagement_ResourceType extends ORM_MySqlRecord
{
	
	
}