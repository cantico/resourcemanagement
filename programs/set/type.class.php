<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once dirname(__FILE__).'/typefield.class.php';
resourcemanagement_loadOrm();





/**
 * Resources can be associated to one ore more resource types.
 * Each resource type can be composed of several additional fields.
 *
 * @property ORM_PkField		$id
 * @property ORM_StringField	$name
 */
class resourcemanagement_TypeSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
		);

	}
}





/**
 * Resources can be associated to one ore more resource types.
 * Each resource type can be composed of several additional fields.
 *
 * @property int				$id
 * @property string				$name
 */
class resourcemanagement_Type extends ORM_MySqlRecord
{

	/**
	 * Creates or updates the recordSets associated with this type.
	 *
	 * Each type is associated with two record sets one joined to the
	 * resource set and one joined to the reservation set.
	 *
	 */
	public function updateDynamicRecordSets()
	{
		require_once dirname(__FILE__).'/dynamicrecord.class.php';
		require_once dirname(__FILE__).'/typefield.class.php';

		$descriptionDynamicSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($this, resourcemanagement_TypeField::NATURE_DESCRIPTION);
		$reservationDynamicSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($this, resourcemanagement_TypeField::NATURE_RESERVATION);
		$synchronize = new bab_synchronizeSql();
		$synchronize->addOrmSet($descriptionDynamicSet);
		$synchronize->addOrmSet($reservationDynamicSet);
		$synchronize->updateDatabase();

	}





	/**
	 * Saves the record.
	 *
	 * @return boolean True on success, false otherwise
	 */
	public function save()
	{
		if (!parent::save()) {
			return false;
		}

		$this->updateDynamicRecordSets();

		return true;
	}




	public function getDynamicRecordSetTableName($nature)
	{
		return 'resourcemanagement__type' . strtolower(resourcemanagement_TypeField::$fieldNatures[$nature]) . '_' . $this->id;
	}


	public function getDynamicFieldName()
	{
		return 'type' . $this->id;
	}



	public function delete()
	{

		// Delete associations with types.
		$typeFieldSet = new resourcemanagement_TypeFieldSet();

		$typeFieldSet->delete($typeFieldSet->type->is($this->id));




		// Remove extended field values.
// 		$set = $this->getParentSet();
// 		$resourceTypeSet = new resourcemanagement_ResourceTypeSet();
// 		$resourceTypeSet->type();
// 		$resourceTypes = $resourceTypeSet->select(
// 				$resourceTypeSet->resource->is($this->id)
// 		);

// 		foreach ($resourceTypes as $resourceType) {
// 			$type = $resourceType->type();
// 			if (!$type->id) {
// 				continue;
// 			}
// 			$resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
// 			//			$set->addFields($resourceExtSet);

// 			$resourceExt = $resourceExtSet->delete($resourceExtSet->id->is($this->id));
// 		}



		$typeSet = new resourcemanagement_TypeSet();
		$typeSet->delete($typeSet->id->is($this->id));

	}

}

