<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';
require_once dirname(__FILE__) . '/dynamicrecord.class.php';
require_once dirname(__FILE__) . '/resourcetype.class.php';
require_once dirname(__FILE__) . '/resourceavailability.class.php';
require_once dirname(__FILE__) . '/domain.class.php';
require_once dirname(__FILE__) . '/reservation.class.php';
resourcemanagement_loadOrm();



/**
 * Resources.
 *
 * @property ORM_PkField							$id
 * @property ORM_StringField						$name
 * @property ORM_TextField							$description
 * @property ORM_BoolField							$allowConditionalBooking
 * @property ORM_IntField							$confirmationDelayMin
 * @property ORM_IntField							$confirmationDelayMax
 * @property ORM_IntField							$approbation
 * @property ORM_IntField							$reservationDelayMin
 * @property ORM_IntField							$reservationDelayMax
 * @property ORM_BoolField							$unavailable
 * @property ORM_BoolField							$ownAccessRights
 * @property resourcemanagement_DomainSet			$domain
 * @property resourcemanagement_ReservationTypeSet	$defaultReservationType
 */
class resourcemanagement_ResourceSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_TextField('description'),
            ORM_BoolField('allowConditionalBooking'),
            ORM_IntField('confirmationDelayMin'), // Minimum number of days before reservation start date to confirm.
            ORM_IntField('confirmationDelayMax'), // Maximum number of days after reservation is done to confirm.
            ORM_IntField('approbation'),
            ORM_IntField('reservationDelayMin'), // Number of minutes before reservation start after which reservation/modification is not allowed.
            ORM_IntField('reservationDelayMax'), // Number of days before reservation start before which reservation/modification is not allowed.
            ORM_BoolField('unavailable')
                ->setDescription(resourcemanagement_translate('Temporarily unavailable')),
            ORM_BoolField('ownAccessRights'),
            ORM_BoolField('surbooking')
        );

        $this->hasOne('domain', 'resourcemanagement_DomainSet');

        $this->hasOne('defaultReservationType', 'resourcemanagement_ReservationTypeSet');

        $this->hasMany('types', 'resourcemanagement_TypeSet', 'resource',
                'resourcemanagement_ResourceTypeSet');

    }


    public function getExtended($id)
    {
        $resource = $this->get($id);
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($id)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            $resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
            $this->addFields($resourceExtSet);

            $resourceExt = $resourceExtSet->get($id);
            if (!isset($resourceExt)) {
                $resourceExt = $resourceExtSet->newRecord();
            }

            $resourceExtSetName = $resourceExtSet->getName();
            $resource->setValue($resourceExtSetName, $resourceExt);
        }

        return $resource;
    }


    /**
     * Criterion matching resources for which the specified user (or the current user if not specified) can view reservations.
     *
     * @param int	$userId
     *
     * @return ORM_Criterion
     */
    public function hasVisibleReservations($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        $visibleResourceIds = bab_getAccessibleObjects('resourcemanagement_resourceviewer_groups', $userId);

        $hasVisibleReservations = $this->id->in($visibleResourceIds);

        $visibleDomainIds = bab_getAccessibleObjects('resourcemanagement_domainresourceviewer_groups', $userId);

        $hasVisibleReservations = $hasVisibleReservations->_OR_($this->domain->in($visibleDomainIds));

        return $hasVisibleReservations;
    }


    /**
     * Criterion matching resources that the specified user (or the current user if not specified) can manage.
     *
     * @param int	$userId
     *
     * @return ORM_Criterion
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return new ORM_TrueCriterion();
        }

        $manageableResourceIds = bab_getAccessibleObjects('resourcemanagement_resourcemanager_groups', $userId);
//		$manageableDomainIds = bab_getAccessibleObjects('resourcemanagement_domainresourcemanager_groups', $userId);
        $manageableDomainIds = array();
        $domainSet = new resourcemanagement_DomainSet();
        $manageableDomains = $domainSet->select($domainSet->isManaged());
        foreach ($manageableDomains as $manageableDomain) {
            $manageableDomainIds[] = $manageableDomain->id;
        }

        $isManageable = $this->ownAccessRights->is(true)->_AND_($this->id->in($manageableResourceIds));
        $isManageable = $isManageable->_OR_(
            $this->ownAccessRights->is(false)->_AND_($this->domain->in($manageableDomainIds))
        );

        return $isManageable;
    }




    /**
     * Criterion matching resources that the specified user (or the current user if not specified) can book.
     *
     * @param int	$userId
     *
     * @return ORM_Criterion
     */
    public function isBookable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        $resourceIds = bab_getAccessibleObjects('resourcemanagement_resourcebooker_groups', $userId);
        $domainIds = array();
        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isBookable());
        foreach ($domains as $domain) {
            $domainIds[] = $domain->id;
        }

        $accessible = $this->ownAccessRights->is(true)->_AND_($this->id->in($resourceIds));
        $accessible = $accessible->_OR_(
            $this->ownAccessRights->is(false)->_AND_($this->domain->in($domainIds))
        );

        return $accessible;
    }




    /**
     * Criterion matching resources that the specified user (or the current user if not specified) can view.
     *
     * @param int	$userId
     *
     * @return ORM_Criterion
     */
    public function isViewable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        $resourceIds = bab_getAccessibleObjects('resourcemanagement_resourceviewer_groups', $userId);
        $domainIds = array();
        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isViewable());
        foreach ($domains as $domain) {
            $domainIds[] = $domain->id;
        }


        $accessible = $this->ownAccessRights->is(true)->_AND_($this->id->in($resourceIds));
        $accessible = $accessible->_OR_(
            $this->ownAccessRights->is(false)->_AND_($this->domain->in($domainIds))
        );

        return $accessible;
    }



    /**
     * Criterion matching resources being part of the domain or of a sub-domain.
     *
     * @param int	$domainId
     *
     * @return ORM_Criterion
     */
    public function isInDomain($domainId)
    {
        $domainSet = new resourcemanagement_DomainSet();

        $domainIds = array($domainId => $domainId);

        while (true) {
            $domains = $domainSet->select(
                $domainSet->parent->in($domainIds)
                    ->_AND_($domainSet->id->notIn($domainIds))
            );

            if ($domains->count() == 0) {
                break;
            }
            foreach ($domains as $domain) {
                $domainIds[$domain->id] = $domain->id;
            }
        }

        return $this->domain->in($domainIds);
    }
}


/**
 * Resource.
 *
 * @property int								$id
 * @property string								$name
 * @property string								$description
 * @property bool								$allowConditionalBooking
 * @property int								$confirmationDelayMin
 * @property int								$confirmationDelayMax
 * @property int								$approbation
 * @property int                                $reservationDelayMin
 * @property int                                $reservationDelayMax
 * @property bool								$unavailable
 * @property bool								$ownAccessRights
 * @property resourcemanagement_Domain			$domain
 * @property resourcemanagement_ReservationType	$defaultReservationType
 */
class resourcemanagement_Resource extends ORM_MySqlRecord
{

    const SUBFOLDER = 'resource';
    const IMAGESUBFOLDER = 'image';
    const MAINIMAGESUBFOLDER = 'mainimage';


    /**
     * @obsolete See resourcemanagement_Resource::isManageable()
     * @param string $userId
     * @return boolean
     */
    public function isManaged($userId = null)
    {
        return $this->isManageable($userId);
    }

    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return true;
        }


        if (!$this->ownAccessRights) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->domain;
            if ($domainId instanceof resourcemanagement_Domain) {
                 $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isManageable($userId);
            }
            //
        }

        return bab_isAccessValid('resourcemanagement_resourcemanager_groups', $this->id, $userId);
    }

    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isResaManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return true;
        }


        if (!$this->ownAccessRights) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->domain;
            if ($domainId instanceof resourcemanagement_Domain) {
                 $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isResaManageable($userId);
            }
            //
        }

        return bab_isAccessValid('resourcemanagement_resourceresamanager_groups', $this->id, $userId);
    }


    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isViewable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->domain;
            if ($domainId instanceof resourcemanagement_Domain) {
                 $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isViewable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_resourceviewer_groups', $this->id, $userId);
    }


    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isBookable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->domain;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isBookable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_resourcebooker_groups', $this->id, $userId);
    }





    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isBookableForOthers($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->domain;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isBookableForOthers($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_resourceextendedbooker_groups', $this->id, $userId);
    }



    /**
     * Return an array of user ids of people that should be notified when a resource reservation is made / deleted.
     *
     * @return array
     */
    public function getNotifiedUsers()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        if (!$this->ownAccessRights) {
            $domain = $this->domain;
            if (!($domain instanceof resourcemanagement_Domain)) {
                $domainSet = new resourcemanagement_DomainSet();
                $domain = $domainSet->get($domain);
            }
            if ($domain) {
                return $domain->getNotifiedUsers();
            }
        }

        $notifiedUsers = array();

        $users = aclGetAccessUsers('resourcemanagement_resourcenotified_groups', $this->id);
        foreach ($users as $userId => $user) {
            $notifiedUsers[$userId] = $userId;
        }

        return $notifiedUsers;
    }




    /**
     * Get the upload path for folder related to this resource.
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        $path = new bab_Path(bab_getAddonInfosInstance('resourcemanagement')->getUploadPath());
        $path->push(self::SUBFOLDER);
        $path->push($this->id);

        return $path;
    }





    /**
     * Get the folder path for the associated images.
     *
     * @return bab_Path
     */
    public function imagePath()
    {
        $path = $this->uploadPath();
        if (!isset($path)) {
            return null;
        }
        $path->push(self::IMAGESUBFOLDER);
        return $path;
    }




    /**
     * Get the folder path for the associated images.
     *
     * @return bab_Path
     */
    public function mainImagePath()
    {
        $path = $this->uploadPath();
        if (!isset($path)) {
            return null;
        }
        $path->push(self::MAINIMAGESUBFOLDER);
        return $path;
    }


    /**
     * The image respresenting the resource.
     * Main image if it exists or one of the gallery.
     *
     * @return bab_Path
     */
    public function getImage()
    {
        $imagePath = $this->mainImagePath();
        foreach ($imagePath as $image) {
            return $image;
        }
        $imagePath = $this->imagePath();
        foreach ($imagePath as $image) {
            return $image;
        }
        return null;
    }


    /**
     * Move image to image upload path.
     *
     * @param	Widget_FilePickerItem 	$sourcefile
     * @return 	bool
     */
    public function importImage(Widget_FilePickerItem $sourceFile)
    {
        $W = bab_Widgets();
        $imagePath = $this->imagePath();

        if (!isset($imagePath)) {
            return false;
        }

        $imagePath->createDir();

        return rename($sourceFile->getFilePath()->toString(), $imagePath->toString() . '/' . $sourceFile->toString());

        //return $W->imagePicker()->setFolder($imagePath)->importFile($sourceFile, 'UTF-8');
    }


    /**
     * Move image to image upload path.
     *
     * @param	Widget_FilePickerItem 	$sourcefile
     * @return 	bool
     */
    public function importMainImage(Widget_FilePickerItem $sourceFile)
    {
        $W = bab_Widgets();
        $imagePath = $this->mainImagePath();

        if (!isset($imagePath)) {
            return false;
        }

        $imagePath->createDir();

        return rename($sourceFile->getFilePath()->toString(), $imagePath->toString() . '/' . $sourceFile->toString());

        //return $W->imagePicker()->setFolder($imagePath)->importFile($sourceFile, 'UTF-8');
    }



    /**
     * Returns the types associated to the resource in an array
     * @return multitype:resourcemanagement_Type
     */
    public function getTypes()
    {
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($this->id)
        );

        $types = array();
        /* @var $resourceType resourcemanagement_ResourceType */
        foreach ($resourceTypes as $resourceType) {
            $types[$resourceType->type->id] = $resourceType->type;
        }

        return $types;
    }





    /**
     *
     * @param resourcemanagement_Type|int $type
     */
    public function addType($type)
    {
        $typeId = ($type instanceof resourcemanagement_Type) ? $type->id : $type;

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();

        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($this->id)->_AND_($resourceTypeSet->type->is($typeId))
        );

        if ($resourceTypes->count() <= 0) {
            $resourceType = $resourceTypeSet->newRecord();
            $resourceType->resource = $this->id;
            $resourceType->type = $typeId;

            $resourceType->save();
        }

        return $this;
    }




    /**
     *
     * @param resourcemanagement_Type|int $type
     */
    public function removeType($type)
    {
        $typeId = ($type instanceof resourcemanagement_Type) ? $type->id : $type;

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();

        $resourceTypes = $resourceTypeSet->delete(
            $resourceTypeSet->resource->is($this->id)->_AND_($resourceTypeSet->type->is($typeId))
        );

        return $this;
    }




    /**
     * Replace existing association between the resource and resource types with the
     * values specified in the array.
     *
     * @param array $types
     */
    public function setTypes(array $types)
    {
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();

        $resourceTypeSet->delete($resourceTypeSet->resource->is($this->id));

        foreach ($types as $typeId) {
            $this->addType($typeId);
        }

        return $this;
    }



    public function extend()
    {
        $set = $this->getParentSet();
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($this->id)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            $resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
            $set->addFields($resourceExtSet);

            $resourceExt = $resourceExtSet->get($this->id);
            if (!isset($resourceExt)) {
                $resourceExt = $resourceExtSet->newRecord();
            }

            $resourceExtSetName = $resourceExtSet->getName();
            $this->setValue($resourceExtSetName, $resourceExt);
        }
    }




    /**
     * Check if the current user can view this resource reservations.
     *
     * @return boolean
     */
    public function hasVisibleReservations()
    {
        $hasVisibleReservations = bab_isAccessValid('resourcemanagement_resourceviewer_groups', $this->id)
            || bab_isAccessValid('resourcemanagement_domainresourceviewer_groups', $this->domain);

        return $hasVisibleReservations;
    }




    /**
     *
     * @param array $periods
     * @return array
     */
    protected function intersect($periods)
    {
        $intersects = array();

        foreach ($periods as $period) {

            $intersections = array();
            foreach ($intersects as $i => $intersect) {
                if ($period['end'] >= $intersect['start'] && $period['start'] <= $intersect['end']) {
                    $intersections[] = $i;
                }
            }
            if (count($intersections) == 0) {
                $intersects[] = $period;
            } else {
                $min = $max = null;
                foreach ($intersections as $i) {
                    if (!isset($min) || $intersects[$i]['start'] < $min) {
                        $min = $intersects[$i]['start'];
                    }
                    if (!isset($max) || $intersects[$i]['end'] > $max) {
                        $max = $intersects[$i]['end'];
                    }
                    unset($intersects[$i]);
                }
                $intersects[] = array($min, $max);
            }
        }

        return $intersects;
    }





    /**
     * Deletes the resource and associated data.
     *
     * @return resourcemanagement_Resource
     */
    public function delete()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        // Delete associations with types.
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();

        $resourceTypeSet->delete($resourceTypeSet->resource->is($this->id));


        // Delete availability related information.
        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();

        $resourceAvailabilitySet->delete($resourceAvailabilitySet->resource->is($this->id));



        // Remove extended field values.
        $set = $this->getParentSet();
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($this->id)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            if (!$type->id) {
                continue;
            }
            $resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
//			$set->addFields($resourceExtSet);

            $resourceExt = $resourceExtSet->delete($resourceExtSet->id->is($this->id));
        }

        aclDelete('resourcemanagement_resourcemanager_groups', $this->id);
        aclDelete('resourcemanagement_resourceviewer_groups', $this->id);
        aclDelete('resourcemanagement_resourcebooker_groups', $this->id);
        aclDelete('resourcemanagement_resourceextendedbooker_groups', $this->id);
        aclDelete('resourcemanagement_resourcenotified_groups', $this->id);

        $resourceSet = new resourcemanagement_ResourceSet();
        $resourceSet->delete($resourceSet->id->is($this->id));

        return $this;
    }




    /**
     * Criterion matching resources for which the specified user (or the current user if not specified) can view reservations.
     *
     * @param int	$userId
     *
     * @return ORM_Criterion
     */
    public function getMaxAvailableDurationBetween($start, $end, $userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        // 		$bookableResourceIds = bab_getAccessibleObjects('resourcemanagement_resourcebooker_groups', $userId);

        // 		$hasVisibleReservations = $this->id->in($visibleResourceIds);

        // 		$visibleDomainIds = bab_getAccessibleObjects('resourcemanagement_domainresourceviewer_groups', $userId);

        // 		$hasVisibleReservations = $hasVisibleReservations->_OR_($this->domain->in($visibleDomainIds));

        $set = new resourcemanagement_ReservationSet();

        $conditions = $set->end->greaterThan($start)
            ->_AND_($set->start->lessThan($end));

        $conditions = $conditions->_AND_($set->resource->is($this->id));

        $conditions = $conditions->_AND_(
            $set->status->in(
                array(resourcemanagement_Reservation::STATUS_OK,resourcemanagement_Reservation::STATUS_OPTION,resourcemanagement_Reservation::STATUS_WAITING)
            )
        );

        $reservations = $set->select($conditions);

        $unavailablePeriods = array();

        foreach ($reservations as $reservation) {
            $unavailablePeriods[] = array(
                'start' => $reservation->start,
                'end' => $reservation->end
            );
        }


        $merged = $this->intersect($unavailablePeriods);

        bab_Sort::asort($merged, 'start');


        $availablePeriods = array();

        if (count($merged) > 0) {
            $nextStart = $start;
            foreach ($merged as $period) {
                if ($nextStart < $period['start']) {
                    $availablePeriods[] = array('start' => $nextStart, 'end' => $period['start']);
                    $nextStart = $period['end'];
                }
            }
            if ($end > $period['end']) {
                $availablePeriods[] = array('start' => $period['end'], 'end' => $end);
            }
        } else {
            $availablePeriods[] = array('start' => $start, 'end' => $end);
        }


        $max = 0;
        foreach ($availablePeriods as $period) {
            $s = bab_mktime($period['start']);
            $e = bab_mktime($period['end']);
            $seconds = $e - $s;
            if ($seconds > $max) {
                $max = $seconds;
            }
        }

        return $max;
    }


}


