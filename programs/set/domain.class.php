<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once dirname(__FILE__).'/../set/resource.class.php';
resourcemanagement_loadOrm();



/**
 * Resources can be associated to one domain.
 *
 * @property ORM_PkField					$id
 * @property ORM_StringField				$name
 * @property ORM_BoolField					$ownAccessRights
 * @property resourcemanagement_DomainSet	$parent
 *
 */
class resourcemanagement_DomainSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),

            ORM_BoolField('ownAccessRights')
        );

        $this->hasOne('parent', 'resourcemanagement_DomainSet');

        $this->hasMany('resources', 'resourcemanagement_ResourceSet', 'domain');
    }



    /**
     * @deprecated See resourcemanagement_DomainSet::isManageable()
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isManaged($userId = null)
    {
        return $this->isManageable($userId);
    }


    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return new ORM_TrueCriterion();
        }

        $domainIds = bab_getAccessibleObjects('resourcemanagement_domainresourcemanager_groups', $userId);

        $domainSet = new resourcemanagement_DomainSet();

        do {
            $inheritingDomains = $domainSet->select(
                $domainSet->ownAccessRights->is(false)
                    ->_AND_($domainSet->parent->in($domainIds))
                    ->_AND_($domainSet->id->notIn($domainIds))
            );

            foreach ($inheritingDomains as $inheritingDomain) {
                $domainIds[] = $inheritingDomain->id;
            }
        } while ($inheritingDomains->count() > 0);


        return $this->id->in($domainIds);
    }


    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isResaManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return new ORM_TrueCriterion();
        }

        $domainIds = bab_getAccessibleObjects('resourcemanagement_domainresourceresamanager_groups', $userId);

        $domainSet = new resourcemanagement_DomainSet();

        do {
            $inheritingDomains = $domainSet->select(
                $domainSet->ownAccessRights->is(false)
                    ->_AND_($domainSet->parent->in($domainIds))
                    ->_AND_($domainSet->id->notIn($domainIds))
            );

            foreach ($inheritingDomains as $inheritingDomain) {
                $domainIds[] = $inheritingDomain->id;
            }
        } while ($inheritingDomains->count() > 0);


        return $this->id->in($domainIds);
    }




    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isBookable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        $domainIds = bab_getAccessibleObjects('resourcemanagement_domainresourcebooker_groups', $userId);

        $domainSet = new resourcemanagement_DomainSet();

        do {
            $inheritingDomains = $domainSet->select(
                $domainSet->ownAccessRights->is(false)
                    ->_AND_($domainSet->parent->in($domainIds))
                    ->_AND_($domainSet->id->notIn($domainIds))
            );

            foreach ($inheritingDomains as $inheritingDomain) {
                $domainIds[] = $inheritingDomain->id;
            }
        } while ($inheritingDomains->count() > 0);


        return $this->id->in($domainIds);
    }




    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isViewable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        $domainIds = bab_getAccessibleObjects('resourcemanagement_domainresourceviewer_groups', $userId);

        $domainSet = new resourcemanagement_DomainSet();

        do {
            $inheritingDomains = $domainSet->select(
                $domainSet->ownAccessRights->is(false)
                    ->_AND_($domainSet->parent->in($domainIds))
                    ->_AND_($domainSet->id->notIn($domainIds))
            );

            foreach ($inheritingDomains as $inheritingDomain) {
                $domainIds[] = $inheritingDomain->id;
            }
        } while ($inheritingDomains->count() > 0);


        return $this->id->in($domainIds);
    }




    /**
     * Returns a criterion that matches domains for which the user is allowed to export
     * reservation informations.
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isExportable($userId = null)
    {
        $isManaged = $this->isManaged($userId);


        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        $domainIds = bab_getAccessibleObjects('resourcemanagement_domainresourceexport_groups', $userId);

        return $isManaged->_OR_($this->id->in($domainIds));
    }

}


/**
 * A resource can be associated to one domain.
 *
 * @property int						$id
 * @property string						$name
 * @property bool						$ownAccessRights
 * @property resourcemanagement_Domain	$parent
 */
class resourcemanagement_Domain extends ORM_MySqlRecord
{
    /**
     * @deprecated See resourcemanagement_DomainSet::isManageable()
     * @param string $userId
     * @return boolean
     */
    public function isManaged($userId = null)
    {
        return $this->isManageable($userId);
    }



    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return true;
        }

        if (!$this->ownAccessRights && $this->parent) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->parent;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isManageable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_domainresourcemanager_groups', $this->id, $userId);
    }



    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isResaManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return true;
        }

        if (!$this->ownAccessRights && $this->parent) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->parent;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isManageable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_domainresourceresamanager_groups', $this->id, $userId);
    }


    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isViewable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights && $this->parent) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->parent;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isViewable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_domainresourceviewer_groups', $this->id, $userId);
    }


    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isBookable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights && $this->parent) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->parent;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isBookable($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_domainresourcebooker_groups', $this->id, $userId);
    }





    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isBookableForOthers($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (!$this->ownAccessRights && $this->parent) {
            $domainSet = new resourcemanagement_DomainSet();
            $domainId = $this->parent;
            if ($domainId instanceof resourcemanagement_Domain) {
                $domainId = $domainId->id;
            }
            if ($parent = $domainSet->get($domainId)) {
                return $parent->isBookableForOthers($userId);
            }
        }

        return bab_isAccessValid('resourcemanagement_domainresourceextendedbooker_groups', $this->id, $userId);
    }



    /**
     * Return an array of user ids of people that should be notified when a resource reservation is made / deleted.
     *
     * @return array
     */
    public function getNotifiedUsers()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        if (!$this->ownAccessRights && $this->parent) {
            $parent = $this->parent;
            if (!($parent instanceof resourcemanagement_Domain)) {
                $domainSet = new resourcemanagement_DomainSet();
                $parent = $domainSet->get($parent);
            }
            if ($parent) {
                return $parent->getNotifiedUsers();
            }
        }

        $notifiedUsers = array();

        $users = aclGetAccessUsers('resourcemanagement_domainresourcenotified_groups', $this->id);
        foreach ($users as $userId => $user) {
            $notifiedUsers[$userId] = $userId;
        }

        return $notifiedUsers;
    }



    /**
     * Deletes the domain and associated data.
     *
     * @return resourcemanagement_Domain
     */
    public function delete()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        // Remove associations with resources.
        $resourceSet = new resourcemanagement_ResourceSet();

        $resources = $resourceSet->select($resourceSet->domain->is($this->id));
        foreach ($resources as $resource) {
            $resource->domain = 0;
            $resource->save();
        }

        // Remove associations with other domains.
        $domainSet = new resourcemanagement_DomainSet();

        $domains = $domainSet->select($domainSet->parent->is($this->id));
        foreach ($domains as $domain) {
            $domain->parent = $this->parent;
            $domain->save();
        }


        aclDelete('resourcemanagement_domainresourcemanager_groups', $this->id);
        aclDelete('resourcemanagement_domainresourceresamanager_groups', $this->id);
        aclDelete('resourcemanagement_domainresourceviewer_groups', $this->id);
        aclDelete('resourcemanagement_domainresourcebooker_groups', $this->id);
        aclDelete('resourcemanagement_domainresourceextendedbooker_groups', $this->id);
        aclDelete('resourcemanagement_domainresourcenotified_groups', $this->id);
        aclDelete('resourcemanagement_domainresourceexport_groups', $this->id);

        $domainSet->delete($domainSet->id->is($this->id));

        return $this;
    }





    /**
     * Returns the name of the domain and its ancestors in an array.
     *
     * @return string[]
     */
    public function getFullPath()
    {
        $domainNames = array();
        $domainSet = new resourcemanagement_DomainSet();

        $domain = $domainSet->get($this->id);
        $limit = 3;
        while ($domain && $domain->id) {
            array_unshift($domainNames, $domain->name);

            if (!$domain->parent || --$limit <= 0) {
                return $domainNames;
            }
            $domain = $domainSet->get($domain->parent);
        }

        return $domainNames;
    }
}
