<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once dirname(__FILE__).'/../set/resource.class.php';
resourcemanagement_loadOrm();



/**
 * Resources can be associated to one domain.
 *
 * @property ORM_PkField                    $id
 * @property ORM_StringField                $name
 * @property ORM_TextField                  $description
 * @property ORM_IntField                   $quantity
 * @property resourcemanagement_ResourceSet	$resource
 */
class resourcemanagement_EquipmentSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_TextField('description')
                ->setDescription('Description'),
            ORM_IntField('quantity')
                ->setDescription('Quantity')
        );

        $this->hasOne('resource', 'resourcemanagement_ResourceSet');
    }


    /**
     *
     * @param string $userId
     * @return ORM_Criterion
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        $recordSet = new resourcemanagement_EquipmentSet();

        if (resourcemanagement_isManager($userId)) {
            return $recordSet->all();
        }

        return $recordSet->none();
    }
}


/**
 *
 * @property int                            $id
 * @property string                         $name
 * @property string                         $description
 * @property int                            $quantity
 * @property resourcemanagement_Resource	$resource
 */
class resourcemanagement_Equipment extends ORM_MySqlRecord
{
    /**
     *
     * @param string $userId
     * @return boolean
     */
    public function isManageable($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }

        if (resourcemanagement_isManager($userId)) {
            return true;
        }

        return false;
    }


    public function getAvailableQuantity()
    {
        return $this->quantity;
    }
}
