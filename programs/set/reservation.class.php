<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once dirname(__FILE__).'/recurrence.class.php';
require_once dirname(__FILE__).'/reservationtype.class.php';
require_once dirname(__FILE__).'/resource.class.php';
require_once dirname(__FILE__).'/resourcetype.class.php';
require_once dirname(__FILE__).'/resourceavailability.class.php';
require_once dirname(__FILE__).'/typefield.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/wfincl.php';

resourcemanagement_loadOrm();





/**
 * Reservation of a resource.
 *
 * @property ORM_PkField							$id
 * @property ORM_DatetimeField						$start
 * @property ORM_DatetimeField						$end
 * @property ORM_BoolField							$allDay
 * @property ORM_StringField						$rrule
 * @property ORM_StringField						$exrule
 * @property ORM_StringField						$description
 * @property ORM_TextField							$longDescription
 * @property ORM_UserField							$reservedBy
 * @property ORM_UserField							$reservedFor
 * @property ORM_UserField							$approvedBy
 * @property ORM_TextField							$approverComment
 * @property ORM_IntField							$approbation
 * @property ORM_EnumField							$status
 * @property resourcemanagement_ReservationTypeSet	$type
 * @property resourcemanagement_ResourceSet			$resource
 * @property resourcemanagement_ReservationSet		$isRecurrenceOf
 * @property ORM_DatetimeField						$createdOn
 * @property ORM_DatetimeField						$modifiedOn
 * @property ORM_UserField							$modifiedBy
 *
 * @method	resourcemanagement_Reservation	    get()
 * @method	resourcemanagement_Reservation[]	select()
 */
class resourcemanagement_ReservationSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DatetimeField('start'),
            ORM_DatetimeField('end'),
            ORM_BoolField('allDay'),
            //ORM_StringField('rrule'),
            ORM_StringField('exrule'),
            ORM_StringField('description'),
            ORM_TextField('longDescription'),
            ORM_UserField('reservedBy'),
            ORM_UserField('reservedFor'),
            ORM_UserField('approvedBy'),
            ORM_TextField('approverComment'),
            ORM_IntField('approbation'),
            ORM_EnumField('status', resourcemanagement_Reservation::$statuses),
            ORM_DatetimeField('createdOn'),
            ORM_DatetimeField('modifiedOn'),
            ORM_UserField('modifiedBy')//,
            //ORM_StringField('isRecurrenceOf')
        );

        $this->hasOne('type', 'resourcemanagement_ReservationTypeSet');
        $this->hasOne('recurrence', 'resourcemanagement_RecurrenceSet');
        $this->hasOne('resource', 'resourcemanagement_ResourceSet');

        //$this->hasOne('isRecurrenceOf', 'resourcemanagement_ReservationSet');
    }



    public function hasOptionLimitExceeded()
    {
        $this->join('resource');

        $isOption = $this->status->is(resourcemanagement_Reservation::STATUS_OPTION);

        $now = date('Y-m-d H:i:s');

        $diffBeforeStart = new ORM_DateDiffOperation($this->start, $now);
        $exceedsConfirmationDelayMin = $this->resource->confirmationDelayMin->greaterThan(0)->_AND_(
            $diffBeforeStart->lessThan($this->resource->confirmationDelayMin)
        );

        $diffAfterResa = new ORM_DateDiffOperation($now, $this->createdOn);
        $exceedsConfirmationDelayMax = $this->resource->confirmationDelayMax->greaterThan(0)->_AND_(
            $diffAfterResa->greaterThanOrEqual($this->resource->confirmationDelayMax)
        );


        $hasOptionLimitExceeded = $isOption->_AND_(
            $exceedsConfirmationDelayMin->_OR_($exceedsConfirmationDelayMax)
        );

        return $hasOptionLimitExceeded;
    }


    /**
     * Find reservations that exceed options limits for their associated resource.
     * These reservations are marked rejected and an email is sent to the owners.
     */
    public function deleteOptionLimitExceeded()
    {
        $reservations = $this->select($this->hasOptionLimitExceeded());

        foreach ($reservations as $reservation) {
            resourcemanagement_notifyOptionDeletion($reservation);
            $reservation->status = resourcemanagement_Reservation::STATUS_REJECTED;
            $reservation->save();
        }
    }
}





/**
 * Reservation of a resource.
 *
 * @property int								$id
 * @property string								$start
 * @property string								$end
 * @property bool								$allDay
 * @property string								$rrule
 * @property string								$exrule
 * @property string								$description
 * @property string								$longDescription
 * @property int								$reservedBy
 * @property int								$reservedFor
 * @property int								$approvedBy
 * @property string								$approverComment
 * @property int								$approbation
 * @property string								$status
 * @property resourcemanagement_ReservationType	$type
 * @property resourcemanagement_Resource		$resource
 * @property resourcemanagement_Reservation		$isRecurrenceOf
 * @property string								$createdOn
 * @property string								$modifiedOn
 * @property int								$modifiedBy
 */
class resourcemanagement_Reservation extends ORM_MySqlRecord
{
    public $textColor = null;
    public $color = null;
    private $resources = null;

    const STATUS_OK = '0';

    /**
     * Pending approbation.
     */
    const STATUS_WAITING = '1';

    /**
     * Draft (not saved).
     */
    const STATUS_DRAFT = '2';

    /**
     * Not confirmed by creator.
     */
    const STATUS_OPTION = '3';

    /**
     * Approbation rejected.
     */
    const STATUS_REJECTED = '4';



    public static $statuses = array(
        resourcemanagement_Reservation::STATUS_OK => 'Ok',
        resourcemanagement_Reservation::STATUS_WAITING => 'Waiting',
        resourcemanagement_Reservation::STATUS_DRAFT => 'Draft',
        resourcemanagement_Reservation::STATUS_OPTION => 'Option',
        resourcemanagement_Reservation::STATUS_REJECTED => 'Rejected',
    );


    /*public function save()
    {
        $rrule = false;
        if ($this->id && $this->rrule == '') {
            $this->rrule = null;
            $rrule = true;
        }
        parent::save();

        if ($rrule) {
            $this->rrule = '';
        }

        return $this;
    }*/

    public function extend()
    {
        $resourceId = $this->resource;
        if ($resourceId instanceof ORM_Record) {
            $resourceId = $resourceId->id;
        }

        $set = $this->getParentSet();
        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($resourceId)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            $reservationExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_RESERVATION);
            $set->addFields($reservationExtSet);

            if ($this->id) {
                $reservationExt = $reservationExtSet->get($this->id);
            }
            if (!isset($reservationExt)) {
                $reservationExt = $reservationExtSet->newRecord();
            }

            $reservationExtSetName = $reservationExtSet->getName();
            $this->setValue($reservationExtSetName, $reservationExt);
        }
    }





    /**
     * Checks if the current reservation has conflicts with the resource schedule (opening hours).
     *
     * Returns all the reservation periods having problems.
     *
     * @return multitype:resourcemanagement_Reservation
     */
    /*public function checkResourceScheduleConflicts()
    {
        $conflicts = array();

        $resourceId = $this->resource;
        if ($resourceId instanceof ORM_Record) {
            $resourceId = $resourceId->id;
        }
        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();


        $resourceAvailabilities = $resourceAvailabilitySet->select($resourceAvailabilitySet->resource->is($resourceId));

        $periodsStart = $this->getPeriodsStart();

// 		$startTs = bab_mktime($this->start);
// 		$endTs = bab_mktime($this->end);

// 		$duration = $endTs - $startTs;

// 		foreach ($periodsStart as $periodStart) {

// 			$periodStartTs = bab_mktime($periodStart);
// 			$periodEndTs = $periodStartTs + $duration;
// 			$periodEnd = date('Y-m-d H:i:s', $periodEndTs);

// 			$conditions = $set->end->greaterThan($periodStart)
// 			->_AND_($set->start->lessThan($periodEnd));

// 			$conditions = $conditions->_AND_($set->resource->is($resourceId));

// 			if (!empty($this->id)) {
// 				$conditions = $conditions->_AND_($set->id->isNot($this->id));
// 			}
// 			$reservations = $set->select(
// 					$conditions
// 			);

// 			foreach ($reservations as $reservation) {
// 				$conflicts[$reservation->id] = $reservation;
// 			}
// 		}


        return $conflicts;
    }*/




    /**
     * Checks if the current reservation is in conflict with others.
     * Returns all the reservations in conflict.
     * Surbooking.
     *
     * @param $ignoreReservations
     *
     * @return resourcemanagement_Reservation[]
     */
    public function checkConflicts($ignoreReservations = array())
    {
        $conflicts = array();

        $set = new resourcemanagement_ReservationSet();
        $set->resource();

        $resourceId = $this->resource;
        if ($resourceId instanceof ORM_Record) {
            $resourceId = $resourceId->id;
        }

        $conditions = $set->end->greaterThan($this->start)
            ->_AND_($set->start->lessThan($this->end));

        $conditions = $conditions->_AND_($set->resource->id->is($resourceId));
        $conditions = $conditions->_AND_($set->resource->surbooking->is(false));//SHOULD NOT BE NEEDED, just in case

        $conditions = $conditions->_AND_(
                $set->status->in(array(resourcemanagement_Reservation::STATUS_OK,resourcemanagement_Reservation::STATUS_OPTION,resourcemanagement_Reservation::STATUS_WAITING))
        );

        if (!empty($ignoreReservations)) {
            $conditions = $conditions->_AND_($set->id->notIn($ignoreReservations));
        }
        $reservations = $set->select($conditions);

        foreach ($reservations as $reservation) {
            $conflicts[$reservation->id] = $reservation;
        }

        return $conflicts;
    }




    /**
     * Return an array of user ids.
     *
     * @return array
     */
    public function getNotifiedUsers()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        $notifiedUsers = array();

        $resource = $this->resource();

        if ($resource) {
            $notifiedUsers = $resource->getNotifiedUsers();
        }

        return $notifiedUsers;
    }




    /**
     * Returns the background color.
     *
     * @return string
     */
    public function getColor()
    {
        if ($this->status == '1') {
            $this->color = 'FFFFFF';
        }
        if (!isset($this->color)) {
            $reservationType = $this->type();
            if ($reservationType) {
                $this->color = $reservationType->color;
            } else {
                $this->color = 'EEEEEE';
            }
        }
        return $this->color;
    }



    /**
     * Returns the text color.
     * If not already set, the text color depends on the background color.
     *
     * @return string
     */
    public function getTextColor()
    {
        $W = bab_Widgets();

        if ($this->status == resourcemanagement_Reservation::STATUS_WAITING) {
            $this->textColor = 'CC7777';
        }
        if (!isset($this->textColor)) {
            $Color = $W->Color();
            $Color->setHexa($this->getColor());

            list(, , $lightness) = $Color->getHSL();

            if ($lightness > 0.7) {
                $this->textColor = '000000';
             } else {
                 $this->textColor = 'FFFFFF';
             }
        }

        return $this->textColor;
    }





    /**
     * An html string contening properties in a ul/li.
     *
     * @return string
     */
    public function getCalendarEventProperties()
    {
        $eventContent = '';

        $this->extend();

        $resource = $this->resource();

        $types = $resource->getTypes();

        $eventContent .= '<ul>';
        foreach ($types as $type) {
            $typeFieldSet = new resourcemanagement_TypeFieldSet();
            $typeFields = $typeFieldSet->select(
                $typeFieldSet->type->is($type->id)
                    ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
                    ->_AND_($typeFieldSet->visibleOnPlanning->is(true))
            );
            $typeFields->orderDesc($typeFieldSet->order);
            $typeFields->orderAsc($typeFieldSet->id);

            $typename = 'type' . $type->id;

            /* @var $typeField resourcemanagement_TypeField  */

            foreach ($typeFields as $typeField) {
                $fieldname = 'f' . $typeField->id;

                $fieldValue = $this->$typename->$fieldname;

                $fieldValue = resourcemanagement_TypeField::getFormattedValue($fieldValue, $typeField->fieldType);

                $eventContent .= '<li>' . $typeField->name . ': <b>' . $fieldValue . '</b></li>';
            }

        }
        $eventContent .= '</ul>';

        return $eventContent;
    }


    public function getCalendarEventDescription()
    {
        $eventContent = $this->description;
        if ($this->reservedFor) {
            $eventContent .= "\n(" . bab_getUserName($this->reservedFor) . ')';
        } else {
            $eventContent .= "\n(" . bab_getUserName($this->reservedBy) . ')';
        }


        return $eventContent;
    }



    protected function getHumanReadablePeriod()
    {
        list($startDay, $startTime) = explode(' ', $this->start);
        list($endDay, $endTime) = explode(' ', $this->end);

        $period = substr($startTime, 0, 5) . '-' . substr($endTime, 0, 5);
        if ($startDay != $endDay) {
            $period .= ' (' . bab_shortDate(bab_mktime($endDay), false) . ')';
        }

        return $period;
    }

    public function toJson()
    {
        $resource = $this->resource();
        if (!$resource) {
            $event = array(
                'id' => $this->id,
                'title' => $this->getCalendarEventDescription(),
                'allDay' => $this->allDay == true,
                'start' => str_replace(' ', 'T', $this->start),
                'end' => str_replace(' ', 'T', $this->end),
                'period' => $this->getHumanReadablePeriod(),
                'description' => $this->longDescription,
                'color' => '#' . $this->getColor(),
                'textColor' => '#' . $this->getTextColor(),
                'resource' => 0,
                'resourceName' => '',
                'properties' => '',
                'actions' => '<span class="actions ' . Func_Icons::ICON_LEFT_SYMBOLIC . '"></span>'
            );
            return resourcemanagement_json_encode($event);
        }

        $event = array(
            'id' => $this->id,
            'title' => $this->getCalendarEventDescription(),
            'allDay' => $this->allDay == true,
            'start' => str_replace(' ', 'T', $this->start),
            'end' => str_replace(' ', 'T', $this->end),
            'period' => $this->getHumanReadablePeriod(),
            'description' => $this->longDescription,
            'color' => '#' . $this->getColor(),
            'textColor' => '#' . $this->getTextColor(),
            'resource' => $this->resource()->id,
            'resourceName' => $this->resource()->name,
            'properties' => $this->getCalendarEventProperties(),
        );


        $actions = $this->getActions();


        if (!empty($actions)) {
            $W = bab_Widgets();
            $canvas = $W->HtmlCanvas();
            $event['actions'] = '<span class="actions ' . Func_Icons::ICON_LEFT_SYMBOLIC . '">';
            foreach ($actions as $id => $action) {
                /* @var $action Widget_Action */

                $url = $action->url();
                if ($url === '?') {
                    $link = $W->Label('');
                } else {
                    $link = $W->Link('', $url);
                    if ($id === 'display' || $id === 'edit' || $id === 'editApproval') {
                        $link->setOpenMode(Widget_Link::OPEN_DIALOG);
                    }
                }
                $link->setTitle($action->getTitle());
                $link->addClass('icon', $action->getIcon());

                if ($id === 'editApproval') {
                    $event['period'] .= '<span style="float: left" class="' . Func_Icons::ICON_LEFT_SYMBOLIC . '">' . $link->display($canvas) . '</span>';
                } else {
                    $event['actions'] .= $link->display($canvas);
                }
            }
            $event['actions'] .= '</span>';
            $event['actions'] .= '<script type="text/javascript">widget_baseInit(document.getElementsByTagName("body")[0]);</script>';

        }


        return resourcemanagement_json_encode($event);
    }


    public function getActions()
    {
        $actions = array();
        if (resourcemanagement_canViewReservation($this)) {
            $actions['display'] = resourcemanagement_Controller()->Reservation()->display($this->id, true);
            $actions['display']->setIcon(Func_Icons::ACTIONS_EDIT_FIND);
            $actions['display']->setTitle(resourcemanagement_translate('Show reservation details'));
        }
        if (resourcemanagement_canUpdateReservation($this)) {
            $actions['edit'] = resourcemanagement_Controller()->Reservation()->edit($this->id);
            $actions['edit']->setIcon(Func_Icons::ACTIONS_DOCUMENT_EDIT);
            $actions['edit']->setTitle(resourcemanagement_translate('Edit this reservation'));
            $actions['delete'] = resourcemanagement_Controller()->Reservation()->delete($this->id);
            $actions['delete']->setIcon(Func_Icons::ACTIONS_EDIT_DELETE);
            $actions['delete']->setTitle(resourcemanagement_translate('Delete this reservation'));
        }
        if ($this->status === resourcemanagement_Reservation::STATUS_WAITING) {
            $currentUserId = bab_getUserId();
            $appbrobations = bab_WFGetWaitingInstances($currentUserId);
            if (in_array($this->approbation, $appbrobations)) {
                $actions['editApproval'] = resourcemanagement_Controller()->Reservation()->display($this->id, true);
                $actions['editApproval']->setIcon(Func_Icons::ACTIONS_HELP);
                $actions['editApproval']->setTitle(resourcemanagement_translate('Edit approbation'));
            } else {
                $actions['editApproval'] = new Widget_Action();
                $actions['editApproval']->setIcon(Func_Icons::ACTIONS_HELP);
                $actions['editApproval']->setTitle(resourcemanagement_translate('Pending approbation'));
            }
        }

        return $actions;
    }


    public function getPeriodsStart()
    {
        require_once dirname(__FILE__).'/../RRule.php';

        $dates = array();

        $recurrence = $this->recurrence;
        if(!($recurrence instanceof resourcemanagement_Recurrence)) {
            $recurrenceSet = new resourcemanagement_RecurrenceSet();
            $recurrence = $recurrenceSet->get($recurrenceSet->id->is($recurrence));
        }

        if (!empty($recurrence->rrule)) {
            $startDate = BAB_DateTime::fromIsoDateTime($this->start);

            $begin = BAB_DateTime::fromIsoDateTime($this->start);
            $end = BAB_DateTime::fromIsoDateTime($this->start);
            $end->add(365, BAB_DATETIME_DAY);
            $endIso = $end->getIsoDateTime();

            $rrule = new RRule(new iCalDate($startDate->getICal(false)), $recurrence->rrule);


            while (($startNext = $rrule->GetNext()) && ($startNextIso = $startNext->Render()) < $endIso) {
                $dates[] = $startNextIso;
            }

        } else {
            $dates[] = $this->start;
        }

        return $dates;
    }



    /*public function _getPeriods()
    {
        require_once dirname(__FILE__).'/../RRule.php';

        $boundaries = array();

        $startDate = BAB_DateTime::fromIsoDateTime($this->start);
        $endDate = BAB_DateTime::fromIsoDateTime($this->end);

        $nbDay = BAB_DateTime::dateDiffIso($startDate->getIsoDate(), $endDate->getIsoDate());

        $begin = BAB_DateTime::fromIsoDateTime($this->start);
        $end = BAB_DateTime::fromIsoDateTime($this->start);
        $end->add(100, BAB_DATETIME_DAY);

        $rrule = new RRule(new iCalDate($startDate->getICal(true)), $this->recurrence->rrule);

        while ($startNext = $rrule->GetNext()) {
            $startNext = BAB_DateTime::fromIsoDateTime($startNext->Render());
            $endNext = $startNext->cloneDate();
            if ($endNext->getIsoDateTime() >= $begin->getIsoDateTime() && $startNext->getIsoDateTime() <= $end->getIsoDateTime()) {
                if ($nbDay > 0) {
                    $endNext->add($nbDay);
                }

                $endNext->setTime($endDate->getHour(), $endDate->getMinute(), $endDate->getSecond());

                if ($endNext->getIsoDateTime() > $end->getIsoDateTime()) {
                    $endNext = $end->cloneDate();
                }
                $boundaries[$startNext->getTimeStamp()][] = array('status' => '1', 'babDate' => $startNext);
                $boundaries[$endNext->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endNext);
            }
        }

        if ($endDate->getIsoDateTime() >= $begin->getIsoDateTime() && $startDate->getIsoDateTime() <= $end->getIsoDateTime()) {
            $boundaries[$startDate->getTimeStamp()][] = array('status' => '1', 'babDate' => $startDate);
            $boundaries[$endDate->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endDate);
        }

        bab_Sort::ksort($boundaries);
        $periodBoundaries = array();
        $openboundaries = 0;
        foreach ($boundaries as $boundary) {
            $tempOpenboundaries = 0;
            foreach ($boundary as $date) {
                $tempOpenboundaries += $date['status'];
            }

            if ($tempOpenboundaries == 0){
            } else if ($openboundaries == 0 && $tempOpenboundaries > 0) {
                $date['exclude'] = false;
                $periodBoundaries[$date['babDate']->getTimeStamp()] = $date;
            } else if (($openboundaries + $tempOpenboundaries) <= 0) {
                $date['exclude'] = false;
                $periodBoundaries[$date['babDate']->getTimeStamp()] = $date;
            }
            $openboundaries += $tempOpenboundaries;
        }

        return $periodBoundaries;
    }*/




    public function getDuration()
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

        $start = BAB_DateTime::fromIsoDateTime($this->start);
        $end = BAB_DateTime::fromIsoDateTime($this->end);

        $dayStart = $start->getIsoDate();
        $dayEnd = $end->getIsoDate();

        if ($dayStart == $dayEnd) {
            $startts = $start->getTimeStamp();
            $endts = $start->getTimeStamp();

            $duration = ($endts - $startts) / 3600;

            return $duration;
        }


    }




    /**
     * Creates approbation instance.
     * retourne TRUE si la demande est bien devenue en attente d'approbation
     * @return bool
     */
    public function createApprobationInstance()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';

        // If an instance already exists we delete it.
        if (isset($this->approbation) && $this->approbation > 0) {
            bab_WFDeleteInstance($this->approbation);
        }


        $resource = $this->resource();

        if(true) {
            // si le 3eme parametre est passe avec l'id user en cours, cela active l'auto-approbation
            $idfai = bab_WFMakeInstance($resource->approbation, 'resourcemanagement:'.$this->id, bab_getUserId());
        } else {
            $idfai = bab_WFMakeInstance($resource->approbation, 'resourcemanagement:'.$this->id);
        }

        if (is_int($idfai)) {
            $users = bab_WFGetWaitingApproversInstance($idfai, true);
            if (count($users) == 0) {
                // l'instance n'a pas fonctionne
                bab_WFDeleteInstance($idfai);
                return false;
            }
        } else { // $idfai == TRUE : auto-approbation end schema
            return true;
        }

        $this->approbation = $idfai;
        $this->status = self::STATUS_WAITING;
        $this->approvedBy = 0;
        $this->approverComment = '';
        $this->save();

        $this->notifyApprobators();

        return true;
    }


	public function fixApprobation()
	{
        require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
		if ($this->status == resourcemanagement_Reservation::STATUS_WAITING) {
            $userIds = bab_WFGetWaitingApproversInstance($this->approbation);
            if (count($userIds) == 0) {
		        bab_WFDeleteInstance($this->approbation);
		        $this->approbation = 0;
		        $this->status = self::STATUS_OK;
				$this->save();
			}
		}
	}




    /**
     * Sets the approver (current user) choice on this reservation.
     *
     * Move approbation to next appover or close instance and set the request accepted.
     *
     * @param	bool	$status				Accept (true) or reject (false) the approbation step.
     * @param	string	$comment
     *
     */
    public function saveApprobationStep($status, $comment = '')
    {
        require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';

        $userId = bab_getUserId();
        $result = bab_WFUpdateInstance($this->approbation, $userId, $status);

// 		$str_status = $status ? resourcemanagement_translate('accepted') : resourcemanagement_translate('rejected');
// 		$this->addMovement(
// 			sprintf(absences_translate('The %s has been %s by %s'), $this->getTitle(), $str_status, bab_getUserName(bab_getUserId())),
// 			$approver_comment
// 		);

        switch ($result) {

            case 0:
                bab_WFDeleteInstance($this->approbation);
                $this->approbation = 0;
                $this->status = self::STATUS_REJECTED;
//				$this->onReject();
                break;

            case 1:
                bab_WFDeleteInstance($this->approbation);
                $this->approbation = 0;
                $this->status = self::STATUS_OK;
//				$this->onConfirm();
                break;

            default:
                $nfusers = bab_WFGetWaitingApproversInstance($this->approbation, true);
                if (count($nfusers) > 0) {
//					$this->appr_notified = 0;
                } else {
                    // no approvers found
                    bab_WFDeleteInstance($this->approbation);
                    $this->approbation = 0;
                    $this->status = self::STATUS_OK;
//					$this->onConfirm();
                }
                break;
        }

        $this->approvedBy = $userId;
        $this->approverComment = $comment;
        $this->save();

        $this->notifyApprobators();
    }




    public function delete()
    {
        $resourceId = $this->resource;
        if ($resourceId instanceof ORM_Record) {
            $resourceId = $resourceId->id;
        }


        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($resourceId)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            if (!$type->id) {
                continue;
            }
            $reservationExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_RESERVATION);

            if ($this->id) {
                $reservationExtSet->delete($reservationExtSet->id->is($this->id));
            }
        }

        $reservationSet = new resourcemanagement_ReservationSet();
        $reservationSet->delete($reservationSet->id->is($this->id));

    }


    /**
     *  Check resource availability (aka opening hours) for the reservation.
     *
     *  @return bool	True if the resource is available on the reservation period.
     */
    public function checkResourceAvailabilityHours()
    {
        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($this->resource);

        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();
        $resourceAvailabilities = $resourceAvailabilitySet->select(
            $resourceAvailabilitySet->resource->is($resource->id)
        );

        // If no period of availability has been defined, we consider the resource always available.
        if ($resourceAvailabilities->count() == 0) {
            return true;
        }

        $startDateTime = bab_DateTime::fromIsoDateTime($this->start);
        $startDayOfWeek = ($startDateTime->getDayOfWeek() - 1) % 6;

        $resourceAvailabilities = $resourceAvailabilitySet->select(
            $resourceAvailabilitySet->resource->is($resource->id)
                ->_AND_($resourceAvailabilitySet->startweekday->is($startDayOfWeek))
        );

        foreach ($resourceAvailabilities as $resourceAvailability) {


            @list(,$start) = explode(' ', $this->start);
            @list(,$end) = explode(' ', $this->end);

//			echo $start . ' >= ' . $resourceAvailability->starttime . ':00 && ' . $end . ' <= ' . $resourceAvailability->endtime . ':00';

            if ($start >= $resourceAvailability->starttime . ':00' && ($end <= $resourceAvailability->endtime . ':00')) {
//				echo ' OK';
                // The reservation is included in an availability period.
                return true;
            }
//			echo ' NOTOK';
        }

        // There was no availability period including the reservation.
        return false;
    }


    /**
     * Contains the uniqid of the current session
     */
    public function setRessources($resources)
    {
        $this->resources = $resources;

        return $this;
    }

    /**
     * return the uniqid concerning the current session resources
     */
    public function getRessources()
    {
        return $this->resources;
    }

    public function notifyApprobators() {
        $mailer = bab_mail();
        $Spooler = @bab_functionality::get('Mailspooler');
        $spoolerHashes = array();

        if (!$mailer) {
            return;
        }

        $resource = $this->resource();
        $reservedBy = bab_getUserName($this->reservedBy, true);

        $periodDateStart = bab_shortDate(bab_mktime($this->start), false);
        $periodDateEnd = bab_shortDate(bab_mktime($this->end), false);

        if ($periodDateStart == $periodDateEnd) {
            $periodText = sprintf(
                resourcemanagement_translate('On %s, from %s to %s'),
                bab_shortDate(bab_mktime($this->start), false),
                bab_time(bab_mktime($this->start)),
                bab_time(bab_mktime($this->end))
                );
        } else {
            $periodText = sprintf(
                resourcemanagement_translate('From %s to %s'),
                bab_shortDate(bab_mktime($this->start), true),
                bab_shortDate(bab_mktime($this->end), true)
                );
        }

        $emailTitle = sprintf(
            resourcemanagement_translate('_NOTIFICATION_RESERVATION_WAITING_TITLE_'),
            $resource->name,
            $reservedBy,
            $periodText,
            $this->description
        );

        $template = new resourcemanagement_ReservationApprobationNotificationTemplate($this, $emailTitle, '');

        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $htmlMessage = $mailer->mailTemplate($addon->printTemplate($template, 'mailinfo.html', 'inforeservationapprobation'));
        $txtMessage = $addon->printTemplate($template, 'mailinfo.html', 'inforeservationapprobationtxt');
        $notifiedUsers = bab_WFGetWaitingApproversInstance($this->approbation);

        foreach ($notifiedUsers as $notifiedUser) {
            $email = bab_getUserEmail($notifiedUser);
            if (empty($email)) {
                continue;
            }

            $mailer->mailSubject($emailTitle);
            $mailer->mailBody($htmlMessage, 'html');
            $mailer->mailAltBody($txtMessage);
            $mailer->clearAllRecipients();
            $mailer->mailTo($email);

            /* @var $Spooler Func_Mailspooler */
            if ($Spooler) {
                $spoolerHash = $Spooler->save($mailer);
                $spoolerHashes[$spoolerHash] = $spoolerHash;
            }
        }

        foreach ($spoolerHashes as $spoolerHash) {
            $mail = $Spooler->getMail($spoolerHash);
            $mail->send();
        }
    }

}
