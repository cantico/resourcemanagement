<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
resourcemanagement_loadOrm();

/**
 * @property ORM_PkField				$id
 * @property ORM_StringField			$name
 * @property ORM_EnumField				$fieldType
 * @property ORM_BoolField				$visibleInDescription
 * @property ORM_BoolField				$visibleOnPlanning
 * @property ORM_BoolField				$exportable
 * @property ORM_BoolField				$exportFilter
 * @property ORM_BoolField				$availabilityFilter
 * @property ORM_EnumField				$nature
 * @property resourcemanagement_TypeSet	$type
 */
class resourcemanagement_TypeFieldSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription(resourcemanagement_translate('Field name')),
            ORM_EnumField('fieldType', resourcemanagement_TypeField::$fieldTypes)
                ->setDescription(resourcemanagement_translate('Type')),
            ORM_TextField('fieldTypeParameters')
                ->setDescription(resourcemanagement_translate('Field type parameters')),
            ORM_BoolField('visibleInDescription')
                ->setDescription(resourcemanagement_translate('Visible in description')),
            ORM_BoolField('visibleOnPlanning')
                ->setDescription(resourcemanagement_translate('Visible on planning')),
            ORM_BoolField('mandatory')
                ->setDescription(resourcemanagement_translate('Mandatory')),
            ORM_BoolField('exportable')
                ->setDescription(resourcemanagement_translate('Exportable')),
            ORM_BoolField('exportFilter')
                ->setDescription(resourcemanagement_translate('Export filter')),
            ORM_BoolField('availabilityFilter')
                ->setDescription(resourcemanagement_translate('Availability filter')),
            ORM_IntField('order')
                ->setDescription(resourcemanagement_translate('Field order')),



            ORM_EnumField('nature', resourcemanagement_TypeField::$fieldNatures)
        );

        $this->hasOne('type', 'resourcemanagement_TypeSet');
    }
}


$resourcemanagement_FielddTypes = array(
    'string'	=> resourcemanagement_translate('String (single line)'),
    'text'		=> resourcemanagement_translate('Text (multiline)'),
    'date'		=> resourcemanagement_translate('Date'),
    'datetime'	=> resourcemanagement_translate('Date/Time'),
    'enum'		=> resourcemanagement_translate('List of values'),
    'number'	=> resourcemanagement_translate('Number'),
    'bool'		=> resourcemanagement_translate('Boolean (true/false)')
);





/**
 * @property int						$id
 * @property string						$name
 * @property string						$type
 * @property bool						$visibleInDescription
 * @property bool						$visibleOnPlanning
 * @property bool						$exportable
 * @property bool                       $exportFilter
 * @property bool                       $availabilityFilter
 * @property int						$nature
 * @property resourcemanagement_Type	$type
 */
class resourcemanagement_TypeField extends ORM_MySqlRecord
{
    static $fieldTypes = array(
        'string'	=> 'String (single line)',
        'text'		=> 'Text (multiline)',
        'date'		=> 'Date',
        'datetime'	=> 'Date/Time',
        'enum'		=> 'List of values',
        'number'	=> 'Number',
        'bool'		=> 'Boolean (true/false)'
    );


    const NATURE_DESCRIPTION = 0;
    const NATURE_RESERVATION = 1;

    static $fieldNatures = array(
        self::NATURE_DESCRIPTION	=> 'Description',
        self::NATURE_RESERVATION	=> 'Reservation'
    );

    public function getOrmField()
    {
        $name = $this->getDynamicFieldName();
        $description = $this->name;

        switch ($this->fieldType) {
            case 'string':
                $field = new ORM_StringField($name, 255);
                break;

            case 'text':
                $field = new ORM_TextField($name);
                break;

            case 'date':
                $field = new ORM_DateField($name);
                break;

            case 'datetime':
                $field = new ORM_DateTimeField($name);
                break;

            case 'number':
                $field = new ORM_DecimalField($name, 2);
                break;

            case 'bool':
                $field = new ORM_BoolField($name);
                break;

             case 'enum':
                 $field = new ORM_StringField($name, 255);
                break;

             default:
                 return null;
        }

        $field->setDescription($description);

        return $field;
    }



    public function getFilterInputWidget()
    {
        $W = bab_Widgets();
        $name = $this->getDynamicFieldName();

        switch ($this->fieldType) {
            case 'string':
            case 'text':
                $widget = $W->LineEdit();
                break;

            case 'date':
                $widget = $W->DatePicker();
                break;

            case 'datetime':
                $widget = $W->DateTimePicker();
                break;

            case 'number':
//				$widget = $W->LineEdit();
                $widget = $W->RegExpLineEdit();
                $widget->setRegExp('^[-+]?[ 0-9]*[\.,]?[ 0-9]+$');
                break;

            case 'bool':
                $widget = $W->CheckBox();
                break;

            case 'enum':
                $widget = $W->Select();
                $widget->addOption('','');
                $values = explode('|', $this->fieldTypeParameters);
                $defaultValue = null;
                foreach ($values as $value) {
                    if (substr($value, 0, 1) === '&') {
                        $value = substr($value, 1);
                        $defaultValue = $value;
                    }
                    $widget->addOption($value, $value);
                }
                if (isset($defaultValue)) {
                    $widget->setValue($defaultValue);
                }
                break;

            default:
                return null;
        }

        $widget->setName($name);

        return $widget;
    }



    public function getInputWidget()
    {
        $W = bab_Widgets();
        $name = $this->getDynamicFieldName();

        switch ($this->fieldType) {
            case 'string':
                $widget = $W->LineEdit();
                break;

            case 'text':
                $widget = $W->TextEdit();
                break;

            case 'date':
                $widget = $W->DatePicker();
                break;

            case 'datetime':
                $widget = $W->DateTimePicker();
                break;

            case 'number':
//				$widget = $W->LineEdit();
                $widget = $W->RegExpLineEdit();
                $widget->setRegExp('^[-+]?[ 0-9]*[\.,]?[ 0-9]+$');
                break;

            case 'bool':
                $widget = $W->CheckBox();
                break;

            case 'enum':
                $widget = $W->Select();
                $values = explode('|', $this->fieldTypeParameters);
                $defaultValue = null;
                foreach ($values as $value) {
                    if (substr($value, 0, 1) === '&') {
                        $value = substr($value, 1);
                        $defaultValue = $value;
                    }
                    $widget->addOption($value, $value);
                }
                if (isset($defaultValue)) {
                    $widget->setValue($defaultValue);
                }
                break;

            default:
                return null;
        }

        $widget->setName($name);

        return $widget;
    }

    /**
     * Saves the record.
     *
     * @return boolean True on success, false otherwise
     */
    public function save()
    {
        if (!parent::save()) {
            return false;
        }

        require_once dirname(__FILE__).'/type.class.php';

        $this->type()->updateDynamicRecordSets();

        return true;
    }


    public function getDynamicFieldName()
    {
        return 'f' . $this->id;
    }



    /**
     *
     * @param mixed $fieldValue
     * @param string $fieldType
     * @return string
     */
    public static function getFormattedValue($fieldValue, $fieldType)
    {
        if ($fieldType === 'bool') {
            $fieldValue = resourcemanagement_translate($fieldValue ? 'Yes' : 'No');
        }
        if ($fieldType === 'number') {
            $fieldValue = sprintf('%g', $fieldValue);
        }
        if ($fieldType === 'date') {
            $fieldValue = bab_shortDate(bab_mktime($fieldValue), false);
        }
        if ($fieldType === 'datetime') {
            $fieldValue = bab_shortDate(bab_mktime($fieldValue), true);
        }
        return $fieldValue;
    }
}


