<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';
resourcemanagement_loadOrm();





/**
 * Resource availability.
 *
 * @property ORM_PkField						$id
 * @property ORM_StringField					$startweekday
 * @property ORM_StringField					$starthour
 * @property ORM_StringField					$endweekday
 * @property ORM_StringField					$endhour
 * @property resourcemanagement_ResourceSet		$resource
 */
class resourcemanagement_ResourceAvailabilitySet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('startweekday')
				->setDescription('Start week day'),
			ORM_StringField('starttime')
				->setDescription('Start time'),
			ORM_StringField('endweekday')
				->setDescription('End week day'),
			ORM_StringField('endtime')
				->setDescription('End time')
		);

		$this->hasOne('resource', 'resourcemanagement_ResourceSet');

	}

}




/**
 * Resource availability.
 *
 * @property int							$id
 * @property string							$startweekday
 * @property string							$starthour
 * @property string							$endweekday
 * @property string							$endhour
 * @property resourcemanagement_Resource	$resource
 */
class resourcemanagement_ResourceAvailability extends ORM_MySqlRecord
{
	
}


