<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
 ************************************************************************/
include 'base.php';



/**
 *
 * @return boolean
 */
function resourcemanagement_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    include_once $GLOBALS['babInstallPath'] . 'utilit/eventincl.php';

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('resourcemanagement');
        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $addon->unregisterFunctionality('Func_CalendarBackend_ResourceManagement');
    } else {
        bab_removeEventListener('bab_eventBeforeSiteMapCreated', 			'resourcemanagement_onSiteMapItems', 					'addons/resourcemanagement/init.php');
        bab_removeEventListener('bab_eventBeforeWaitingItemsDisplayed',		'resourcemanagement_onApprobationListDisplay',			'addons/resourcemanagement/approbations.php');
        bab_removeEventListener('bab_eventBeforePeriodsCreated', 			'resourcemanagement_onBeforePeriodsCreated',			'addons/resourcemanagement/events.php');
        bab_removeEventListener('bab_eventCollectCalendarsBeforeDisplay',	'resourcemanagement_onCollectCalendarsBeforeDisplay',	'addons/resourcemanagement/events.php');
        bab_removeEventListener('bab_eventBeforePageCreated',	            'resourcemanagement_onBeforePageCreated',	            'addons/resourcemanagement/init.php');

        $func = new bab_functionalities;
        $func->unregisterClass('Func_CalendarBackend_ResourceManagement');
    }

    return true;
}





/**
 *
 * @param string $version_base
 * @param string $version_ini
 * @return boolean
 */
function resourcemanagement_upgrade($version_base, $version_ini)
{
    include_once $GLOBALS['babInstallPath'] . 'utilit/eventincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';

    require_once dirname(__FILE__) . '/set/recurrence.class.php';
    require_once dirname(__FILE__) . '/set/domain.class.php';
    require_once dirname(__FILE__) . '/set/resource.class.php';
    require_once dirname(__FILE__) . '/set/equipment.class.php';
    require_once dirname(__FILE__) . '/set/type.class.php';
    require_once dirname(__FILE__) . '/set/resourcetype.class.php';
    require_once dirname(__FILE__) . '/set/typefield.class.php';
    require_once dirname(__FILE__) . '/set/resourceavailability.class.php';
    require_once dirname(__FILE__) . '/set/reservation.class.php';
    require_once dirname(__FILE__) . '/set/reservationtype.class.php';

    //FIX reccurencive events
    if ($version_base < '1.17') {
        if(bab_isTable('resourcemanagement_reservation') && bab_isTableField('resourcemanagement_reservation', 'isRecurrenceOf')) {
            $tmpSet = new resourcemanagement_ReservationSet();

            $synchronize = new bab_synchronizeSql();
            $synchronize->addOrmSet(new resourcemanagement_RecurrenceSet());
            $synchronize->updateDatabase();
            global $babDB;
            $sql = "UPDATE resourcemanagement_reservation
                SET isRecurrenceOf = id
                WHERE isRecurrenceOf = 0 OR isRecurrenceOf = ''";
            $babDB->db_query($sql);

            $sql = "SELECT isRecurrenceOf, MAX(rrule) as rrule FROM resourcemanagement_reservation GROUP BY isRecurrenceOf";
            $resas = $babDB->db_query($sql);

            $tmpSet = new resourcemanagement_ReservationSet();
            $tmpSet->addFields(
                ORM_StringField('rrule'),
                ORM_StringField('isRecurrenceOf')
            );

            $synchronize = new bab_synchronizeSql();
            $synchronize->addOrmSet($tmpSet);
            $synchronize->updateDatabase();

            $recurrenceSet = new resourcemanagement_RecurrenceSet();
            while ($resa = $babDB->db_fetch_assoc($resas)) {
                $record = $recurrenceSet->newRecord();
                $record->rrule = $resa['rrule'];
                $record->save();
                $reservations = $tmpSet->select($tmpSet->isRecurrenceOf->is($resa['isRecurrenceOf']));
                foreach ($reservations as $reservation) {
                    $reservation->recurrence = $record->id;
                    $reservation->save();
                }
            }
        }
    }

    //$GLOBALS['babDB']->db_queryWem('ALTER TABLE resourcemanagement_reservation DROP FOREIGN KEY resourcemanagement_reservation_isRecurrenceOf');
    //$GLOBALS['babDB']->db_queryWem('ALTER TABLE resourcemanagement_reservation DROP CONSTRAINT resourcemanagement_reservation_isRecurrenceOf');
    //$GLOBALS['babDB']->db_query('ALTER TABLE resourcemanagement_reservation DROP INDEX resourcemanagement_reservation_isRecurrenceOf;');

    $addon = bab_getAddonInfosInstance('resourcemanagement');
    $addonPhpPath = $addon->getPhpPath();

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet(new resourcemanagement_RecurrenceSet());
    $synchronize->addOrmSet(new resourcemanagement_DomainSet());
    $synchronize->addOrmSet(new resourcemanagement_ResourceSet());
    $synchronize->addOrmSet(new resourcemanagement_EquipmentSet());
    $synchronize->addOrmSet(new resourcemanagement_TypeSet());
    $synchronize->addOrmSet(new resourcemanagement_ResourceTypeSet());
    $synchronize->addOrmSet(new resourcemanagement_TypeFieldSet());
    $synchronize->addOrmSet(new resourcemanagement_ResourceAvailabilitySet());
    $synchronize->addOrmSet(new resourcemanagement_ReservationSet());
    $synchronize->addOrmSet(new resourcemanagement_ReservationTypeSet());
    $synchronize->updateDatabase();

    aclCreateTable('resourcemanagement_manager_groups');

    aclCreateTable('resourcemanagement_resourcemanager_groups');
    aclCreateTable('resourcemanagement_resourceresamanager_groups');
    aclCreateTable('resourcemanagement_resourceviewer_groups');
    aclCreateTable('resourcemanagement_resourcebooker_groups');
    aclCreateTable('resourcemanagement_resourceextendedbooker_groups');
    aclCreateTable('resourcemanagement_resourcenotified_groups');

    aclCreateTable('resourcemanagement_domainresourcemanager_groups');
    aclCreateTable('resourcemanagement_domainresourceresamanager_groups');
    aclCreateTable('resourcemanagement_domainresourceviewer_groups');
    aclCreateTable('resourcemanagement_domainresourcebooker_groups');
    aclCreateTable('resourcemanagement_domainresourceextendedbooker_groups');
    aclCreateTable('resourcemanagement_domainresourcenotified_groups');
    aclCreateTable('resourcemanagement_domainresourceexport_groups');


    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('resourcemanagement');
        $addon->addEventListener('bab_eventBeforeSiteMapCreated',			'resourcemanagement_onSiteMapItems',					'init.php');
        $addon->addEventListener('bab_eventBeforeWaitingItemsDisplayed',	'resourcemanagement_onApprobationListDisplay',			'approbations.php');
        $addon->addEventListener('bab_eventBeforePeriodsCreated',			'resourcemanagement_onBeforePeriodsCreated',			'events.php');
        $addon->addEventListener('bab_eventCollectCalendarsBeforeDisplay',	'resourcemanagement_onCollectCalendarsBeforeDisplay',	'events.php');
        $addon->addEventListener('bab_eventBeforePageCreated',				'resourcemanagement_onBeforePageCreated',				'init.php', 10);

        $addon->registerFunctionality('Func_CalendarBackend_ResourceManagement', 'calendarbackend.class.php');
    } else {
        bab_addEventListener(
            'bab_eventBeforeSiteMapCreated',
            'resourcemanagement_onSiteMapItems',
            'addons/resourcemanagement/init.php',
            'resourcemanagement'
        );
        bab_addEventListener(
            'bab_eventBeforeWaitingItemsDisplayed',
            'resourcemanagement_onApprobationListDisplay',
            'addons/resourcemanagement/approbations.php',
            'resourcemanagement'
        );
         bab_addEventListener(
            'bab_eventBeforePeriodsCreated',
            'resourcemanagement_onBeforePeriodsCreated',
            'addons/resourcemanagement/events.php',
            'resourcemanagement'
        );
         bab_addEventListener(
            'bab_eventCollectCalendarsBeforeDisplay',
            'resourcemanagement_onCollectCalendarsBeforeDisplay',
            'addons/resourcemanagement/events.php',
             'resourcemanagement'
        );
         bab_addEventListener(
            'bab_eventBeforePageCreated',
            'resourcemanagement_onBeforePageCreated',
            'addons/resourcemanagement/init.php',
            'resourcemanagement',
            10
        );

        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_CalendarBackend_ResourceManagement', $addonPhpPath . 'calendarbackend.class.php');
    }

    echo 'Cleanup database...';
    resourcemanagement_cleanup();
    echo ' Done <br />';



    echo 'Fix recurrence...';
    $recurrenceSet = new resourcemanagement_RecurrenceSet();
    $reservationSet = new resourcemanagement_ReservationSet();
    $resas = $reservationSet->select($reservationSet->recurrence->is(0));
    $links = array();
    global $babDB;
    $i = 0;
    foreach ($resas as $resa) {
        $i++;
        if(!isset($links[$resa->id])) {
            $recurrence = $recurrenceSet->newRecord();
            $recurrence->rrule = '';

            $sql = "SELECT
                        link2.reservation as id
                    FROM
                        resourcemanagement_linkedreservation as link1,
                        resourcemanagement_linkedreservation as link2
                    WHERE
                        link1.name = link2.name
                    AND link1.id != link2.id
                    AND link1.reservation = " . $babDB->quote($resa->id);
            $res = $babDB->db_query($sql);
            while($link = $babDB->db_fetch_assoc($res)) {
                $links[$link->id] = $recurrence;
            }
            $recurrence->save();
        } else {
            $recurrence = $links[$resa->id];
        }

        $resa->recurrence = $recurrence->id;
        $resa->save();
    }
    echo ' Done ('.$i.')';

    return true;
}





/**
 * Sitemap creation.
 *
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function resourcemanagement_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    if (!resourcemanagement_canViewResources() && !resourcemanagement_isManager()) {
        return;
    }

    bab_functionality::includefile('Icons');

    $link = $event->createItem('resourcemanagement');
    $link->setLabel(resourcemanagement_translate('Resource management'));
    $link->setLink('?tg=addon/resourcemanagement/main');
    $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $link->addIconClassname(Func_Icons::APPS_CALENDAR);

    $event->addFolder($link);

    $link = $event->createItem('resourcemanagement_calendar');
    $link->setLabel(resourcemanagement_translate('Resource calendar'));
    $link->setLink('?tg=addon/resourcemanagement/main&idx=calendar.display');
    $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement'));
    $link->addIconClassname(Func_Icons::APPS_CALENDAR);

    $event->addFunction($link);


    if (resourcemanagement_canManageResources() || resourcemanagement_isManager()) {
        $link = $event->createItem('resourcemanagement_manage');
        $link->setLabel(resourcemanagement_translate('Management'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=admin.manage');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement'));
        $link->addIconClassname(Func_Icons::APPS_CALENDAR);

        $event->addFunction($link);
    }


    if (resourcemanagement_isManager()) {
        $link = $event->createItem('resourcemanagement_domains');
        $link->setLabel(resourcemanagement_translate('Resource domains'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=domain.displayList');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement', 'resourcemanagement_manage'));
        $link->addIconClassname(Func_Icons::APPS_ORGCHARTS);

        $event->addFunction($link);

        $link = $event->createItem('resourcemanagement_reservation_types');
        $link->setLabel(resourcemanagement_translate('Reservation types'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=reservationtype.displayList');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement', 'resourcemanagement_manage'));
        $link->addIconClassname(Func_Icons::ACTIONS_VIEW_LIST_DETAILS);

        $event->addFunction($link);

        $link = $event->createItem('resourcemanagement_resource_types');
        $link->setLabel(resourcemanagement_translate('Resource types'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=type.displayList');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement', 'resourcemanagement_manage'));
        $link->addIconClassname(Func_Icons::ACTIONS_VIEW_LIST_TEXT);

        $event->addFunction($link);

    }

    if (resourcemanagement_canManageResources()) {
        $link = $event->createItem('resourcemanagement_resources');
        $link->setLabel(resourcemanagement_translate('Resources'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=resource.displayList');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement', 'resourcemanagement_manage'));
        $link->addIconClassname(Func_Icons::APPS_FILE_MANAGER);

        $event->addFunction($link);
    }
    if (resourcemanagement_canExportReservations()) {
        $link = $event->createItem('resourcemanagement_reservation_export');
        $link->setLabel(resourcemanagement_translate('Reservation export'));
        $link->setLink('?tg=addon/resourcemanagement/main&idx=reservation.displayList');
        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement', 'resourcemanagement_manage'));
        $link->addIconClassname(Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD);

        $event->addFunction($link);
    }



    $link = $event->createItem('resourcemanagement_search');
    $link->setLabel(resourcemanagement_translate('Search availability'));
    $link->setLink('?tg=addon/resourcemanagement/main&idx=search.displayList');
    $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'resourcemanagement'));
    $link->addIconClassname(Func_Icons::ACTIONS_EDIT_FIND);

    $event->addFunction($link);


    $link = $event->createItem('resourcemanagement_admin');
    $link->setLabel(resourcemanagement_translate('Resource management'));
    $link->setLink('?tg=addon/resourcemanagement/main&idx=admin.edit');
    $link->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
    $link->addIconClassname(Func_Icons::APPS_CALENDAR);

    $event->addFunction($link);
}




function resourcemanagement_isTable($table)
{
    global $babDB;

    $arr = $babDB->db_fetch_array($babDB->db_query("SHOW TABLES LIKE '".$table."'"));
    return ($arr[0] == $table);
}

/**
 * Sanity checks and cleanup of database.
 */
function resourcemanagement_cleanup()
{
    global $babDB;

    // Cleanup ACL tables for domains.
    $domainIds = array();
    $domainSet = new resourcemanagement_DomainSet();
    $domains = $domainSet->select();
    foreach ($domains as $domain) {
        $domainIds[$domain->id] = $domain->id;
    }

    $domainAclTables = array(
        'resourcemanagement_domainresourcemanager_groups',
        'resourcemanagement_domainresourceviewer_groups',
        'resourcemanagement_domainresourcebooker_groups',
        'resourcemanagement_domainresourceextendedbooker_groups',
        'resourcemanagement_domainresourcenotified_groups',
        'resourcemanagement_domainresourceexport_groups',
    );

    // For each ACL table we look for entries referencing domains that
    // do not exist.
    foreach ($domainAclTables as $aclTable) {
        $idObjects = array();

        $sql = 'SELECT DISTINCT id_object FROM ' . $aclTable;
        $res = $babDB->db_query($sql);
        while ($arr = $babDB->db_fetch_assoc($res)) {
            $idObjects[$arr['id_object']] = $arr['id_object'];
        }

        foreach ($idObjects as $idObject) {
            if (!isset($domainIds[$idObject])) {
                aclDelete($aclTable, $idObject);
                //echo "aclDelete($aclTable, $idObject)";
            }
        }
    }


    // Cleanup ACL tables for resources.
    $resourceIds = array();
    $resourceSet = new resourcemanagement_resourceSet();
    $resources = $resourceSet->select();
    foreach ($resources as $resource) {
        $resourceIds[$resource->id] = $resource->id;
    }

    $resourceAclTables = array(
        'resourcemanagement_resourcemanager_groups',
        'resourcemanagement_resourceviewer_groups',
        'resourcemanagement_resourcebooker_groups',
        'resourcemanagement_resourceextendedbooker_groups',
        'resourcemanagement_resourcenotified_groups',
    );


    // For each ACL table we look for entries referencing resources that
    // do not exist.
    foreach ($resourceAclTables as $aclTable) {
        $idObjects = array();

        $sql = 'SELECT DISTINCT id_object FROM ' . $aclTable;
        $res = $babDB->db_query($sql);
        while ($arr = $babDB->db_fetch_assoc($res)) {
            $idObjects[$arr['id_object']] = $arr['id_object'];
        }

        foreach ($idObjects as $idObject) {
            if (!isset($resourceIds[$idObject])) {
                aclDelete($aclTable, $idObject);
                //echo "aclDelete($aclTable, $idObject)";
            }
        }
    }

    // Delete associations between resources and non-existent types.
    $typeSet = new resourcemanagement_typeSet();
    $typeIds = array();
    $types = $typeSet->select();
    foreach ($types as $type) {
        $typeIds[$type->id] = $type->id;
    }
    $resourceTypeSet = new resourcemanagement_resourceTypeSet();
    $resourceTypeSet->delete($resourceTypeSet->type->notIn($typeIds));

    // Rename old dynamic tables.
    foreach ($typeIds as $typeId) {

        if (resourcemanagement_isTable('resourcemanagement__typeDescription_' . $typeId)) {
            $babDB->db_query("RENAME TABLE resourcemanagement__typeDescription_" . $typeId ." TO resourcemanagement__typedescription_" . $typeId);
        }

        if (resourcemanagement_isTable('resourcemanagement__typeReservation_' . $typeId)) {
            $babDB->db_query("RENAME TABLE resourcemanagement__typeReservation_" . $typeId ." TO resourcemanagement__typereservation_" . $typeId);
        }
    }
}





/**
 *
 * @param bab_eventBeforePageCreated $event
 */
function resourcemanagement_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    $tg = bab_rp('tg', null);

    if (substr($tg, 0, strlen('addon/resourcemanagement/')) === 'addon/resourcemanagement/') {
        require_once dirname(__FILE__) . '/functions.php';
        $skinName = resourcemanagement_getSkinName();
        $skinCss = resourcemanagement_getSkinCss();

        bab_skin::applyOnCurrentPage($skinName, $skinCss);
    }
}
