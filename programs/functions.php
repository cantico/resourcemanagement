<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

require_once dirname(__FILE__).'/set/domain.class.php';
require_once dirname(__FILE__).'/set/resource.class.php';
require_once dirname(__FILE__).'/set/reservation.class.php';


class resourcemanagement_Exception extends Exception
{
}

/**
 *
 *
 */
class resourcemanagement_SaveReservationException extends Exception
{
    protected $messages = array();
    protected $conflicts = array();
    //protected $warnings = array();

    protected $error = false;

    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        if ($message === null) {
            $message = resourcemanagement_translate('Reservation save exception.');
        }
        return parent::__construct($message, $code, $previous);
    }

    public function addMessage($message)
    {
        $this->error = true;
        $this->messages[] = $message;
        return $this;
    }

    public function getMessages()
    {
        return $this->messages;
    }


    public function addConflict($conflict)
    {
        $this->error = true;
        $this->conflicts[] = $conflict;
        return $this;
    }

    public function getConflicts()
    {
        return $this->conflicts;
    }


    /*public function addWarning($warning)
    {
        $this->error = true;
        $this->warnings[] = $warning;
        return $this;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }*/

    public function isError()
    {
        return $this->error;
    }


    public function throwIfError()
    {
        if ($this->isError()) {
            throw $this;
        }
    }

    public function mergeError(resourcemanagement_SaveReservationException $e)
    {
        if ($e->isError()) {
            $this->error = true;
        }
        $this->messages = array_merge($this->getMessages(), $e->getMessages());
        $this->conflicts = array_merge($this->getConflicts(), $e->getConflicts());
        //$this->warnings = array_merge($this->getWarnings(), $e->getWarnings());

        return $this;
    }
}

/**
 * Translate
 * @param string $str
 * @return string
 */
function resourcemanagement_translate($str, $str_plurals = null, $number = null)
{

    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('resourcemanagement');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}





/**
 * Initialize mysql ORM backend.
 */
function resourcemanagement_loadOrm()
{
    static $loadOrmDone = false;

     if (!$loadOrmDone) {

        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->initMySql();

        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        ORM_MySqlRecordSet::setBackend($mysqlbackend);

        $loadOrmDone = true;
    }
}





/**
 * @return resourcemanagement_Controller
 */
function resourcemanagement_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('resourcemanagement_Controller');
}





/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function resourcemanagement_redirect($url, $message = null)
{
    global $babBody;

    if (null === $url) {
        $script = '<script type="text/javascript">';
        if (isset($message)) {
            $script .= 'alert("' . bab_toHtml($message, BAB_HTML_JS) . '");';
        }
        $script .= 'history.back();';
        $script .= '</script>';

        die($script);
    }

    if ($url instanceof Widget_Action) {
        $url = $url->url();
    }
    if (!empty($babBody->msgerror)) {
        $url .=  '&msgerror=' . urlencode($babBody->msgerror);
    }
    if (isset($message)) {
        $url .=  '&msg=' . urlencode($message);
    }

    header('Location: ' . $url);
    die;
}





/**
 * @param	mixed	$value
 * @return	string
 */
function resourcemanagement_json_encode($a)
{
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a)) {
        if (is_float($a)) {
            // Always use "." for floats.
            return floatval(str_replace(",", ".", strval($a)));
        }

        if (is_string($a)) {
            static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
            return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
        } else {
            return $a;
        }
    }
    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
    if ($a instanceof BAB_DateTime) {
        /* @var $a BAB_DateTime */
        return 'new Date(Date.UTC(' . $a->getYear() . ',' . ($a->getMonth() - 1) . ',' . $a->getDayOfMonth() . ',' . $a->getHour() . ',' . $a->getMinute() . ',' . $a->getSecond() . ',0))';
    }
    if (is_object($a) && method_exists($a, 'toJson')) {
        return $a->toJson();
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
        if (key($a) !== $i) {
            $isList = false;
            break;
        }
    }
    $result = array();
    if ($isList) {
        foreach ($a as $v) {
            $result[] = resourcemanagement_json_encode($v);
        }
        return '[' . join(',', $result) . ']';
    } else {
        foreach ($a as $k => $v) {
            $result[] = resourcemanagement_json_encode($k).':'.resourcemanagement_json_encode($v);
        }
        return '{' . join(',', $result) . '}';
    }
}





/**
 * @param int|resourcemanagement_Resource $resource
 * @return resourcemanagement_Resource
 */
function resourcemanagement_resource($resource)
{
    if (!($resource instanceof resourcemanagement_Resource)) {
        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($resource);
    }

    return $resource;
}


/**
 * @param int|resourcemanagement_Equipment $equipment
 * @return resourcemanagement_Equipment
 */
function resourcemanagement_equipment($equipment)
{
    if (!($equipment instanceof resourcemanagement_Equipment)) {
        $equipmentSet = new resourcemanagement_EquipmentSet();
        $equipment = $equipmentSet->get($equipment);
    }

    return $equipment;
}




/**
 * @param int|resourcemanagement_Domain $domain
 * @return resourcemanagement_Domain
 */
function resourcemanagement_domain($domain)
{
    if (!($domain instanceof resourcemanagement_Domain)) {
        $domainSet = new resourcemanagement_DomainSet();
        $domain = $domainSet->get($domain);
    }

    return $domain;
}





/**
 * @return bool
 */
function resourcemanagement_isManager($userId = null)
{
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }

    return bab_isAccessValid('resourcemanagement_manager_groups', 1, $userId);
}





/**
 * Checks if the user has access to the management of at least one resource.
 *
 * @return bool
 */
function resourcemanagement_canManageResources($userId = null)
{
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }


    return bab_isAccessValid('resourcemanagement_manager_groups', 1, $userId)
        || (count(bab_getAccessibleObjects('resourcemanagement_resourcemanager_groups', $userId)) > 0)
        || (count(bab_getAccessibleObjects('resourcemanagement_domainresourcemanager_groups', $userId)) > 0);
}


function resourcemanagement_canManageServices($userId = null)
{
    return resourcemanagement_canManageResources($userId);
}


function resourcemanagement_canManageEquipment($userId = null)
{
    return resourcemanagement_canManageResources($userId);
}

/**
 * Checks if the user has any access to at least one resource.
 *
 * @return bool
 */
function resourcemanagement_canViewResources($userId = null)
{
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }

    return bab_isAccessValid('resourcemanagement_manager_groups', 1, $userId)
    || (count(bab_getAccessibleObjects('resourcemanagement_resourceviewer_groups', $userId)) > 0)
    || (count(bab_getAccessibleObjects('resourcemanagement_resourcebooker_groups', $userId)) > 0)
    || (count(bab_getAccessibleObjects('resourcemanagement_resourcemanager_groups', $userId)) > 0)
    || (count(bab_getAccessibleObjects('resourcemanagement_domainresourceviewer_groups', $userId)) > 0)
    || (count(bab_getAccessibleObjects('resourcemanagement_domainresourcebooker_groups', $userId)) > 0)
    || (count(bab_getAccessibleObjects('resourcemanagement_domainresourcemanager_groups', $userId)) > 0);
}



/**
 * Checks if the user has access to the export of at least one domain.
 *
 * @return bool
 */
function resourcemanagement_canExportReservations($userId = null)
{
    if (resourcemanagement_canManageResources($userId)) {
        return true;
    }

    if (!isset($userId)) {
        $userId = bab_getUserId();
    }

    return (count(bab_getAccessibleObjects('resourcemanagement_domainresourceexport_groups', $userId)) > 0);
}





/**
 * @param resourcemanagement_Domain $domain
 * @return bool
 */
function resourcemanagement_canCreateResource($domain)
{
    $domain = resourcemanagement_domain($domain);

    return $domain->isManaged();
}





/**
 * @param resourcemanagement_Resource $resource
 * @return boolean
 */
function resourcemanagement_canUpdateResource($resource)
{
    $resource = resourcemanagement_resource($resource);

    return $resource->isManaged();
}





/**
 * @param resourcemanagement_Resource $resource
 * @return boolean
 */
function resourcemanagement_canDeleteResource($resource)
{
     return resourcemanagement_canUpdateResource($resource);
}





/**
 * @param resourcemanagement_Resource $resource
 * @return boolean
 */
function resourcemanagement_canEditResourceAccess($resource)
{
    // Resource addon managers will always be allowed to modify access rights on resources.
    return resourcemanagement_canUpdateResource($resource) || resourcemanagement_isManager();
}




/**
 * @param resourcemanagement_Equipment $equipment
 * @return boolean
 */
function resourcemanagement_canUpdateEquipment($equipment)
{
    $equipment = resourcemanagement_equipment($equipment);

    return $equipment->isManageable();
}


/**
 * @param resourcemanagement_Equipment $equipment
 * @return boolean
 */
function resourcemanagement_canDeleteEquipment($equipment)
{
    return resourcemanagement_canUpdateEquipment($equipment);
}



/**
 * @return bool
 */
function resourcemanagement_canCreateType()
{
    return bab_isAccessValid('resourcemanagement_manager_groups', 1);
}





/**
 * @return bool
 */
function resourcemanagement_canUpdateType($type)
{
    return resourcemanagement_canCreateType();
}





/**
 * @return bool
 */
function resourcemanagement_canDeleteType($type)
{
    return resourcemanagement_canUpdateType($type);
}





/**
 * @return bool
 */
function resourcemanagement_canCreateDomain()
{
    return bab_isAccessValid('resourcemanagement_manager_groups', 1);
}





/**
 * @return bool
 */
function resourcemanagement_canUpdateDomain($domain)
{
    return bab_isAccessValid('resourcemanagement_manager_groups', 1);
// 	return true;
//  	$canUpdate = bab_isAccessValid('resourcemanagement_resourcemanager_groups', $record->id)
//  		|| bab_isAccessValid('resourcemanagement_domainresourcemanager_groups', $record->domain);
}


/**
 * @return bool
 */
function resourcemanagement_canCreateReservationType()
{
    return bab_isAccessValid('resourcemanagement_manager_groups', 1);
}


/**
 * @return bool
 */
function resourcemanagement_canUpdateReservationType($reservationType)
{
    return resourcemanagement_canCreateReservationType();
}


/**
 * @return boolean
 */
function resourcemanagement_canDeleteReservationType($reservationType)
{
    return resourcemanagement_canUpdateReservationType($reservationType);
}





/**
 * @param resourcemanagement_Resource $resource
 * @return boolean
 */
function resourcemanagement_canCreateReservation($resource)
{
    if (is_array($resource)) {
        foreach ($resource as $res) {
            $res = resourcemanagement_resource($res);
            if(!$res->isBookable()){
                return false;
            }
        }
        return true;
    } else {
        $resource = resourcemanagement_resource($resource);

        return $resource->isBookable();
    }

// 	$domainId = $resource->domain;
// 	if ($resource->domain instanceof resourcemanagement_Domain) {
// 		$domainId = $resource->domain->id;
// 	}
// 	return bab_isAccessValid('resourcemanagement_resourcebooker_groups', $resource->id)
// 		|| bab_isAccessValid('resourcemanagement_domainresourcebooker_groups', $domainId);
}


/**
 * @return bool
 */
function resourcemanagement_canUpdateReservation($reservation)
{
    $currentUserId = bab_getUserId();

    if ($reservation->reservedBy == $currentUserId || $reservation->reservedFor == $currentUserId) {
        return true;
    }

    $resource = resourcemanagement_resource($reservation->resource);
    if($resource->isManaged() || $resource->isResaManageable()){
        return true;
    }

    return false;
}


/**
 * @return bool
 */
function resourcemanagement_canCreateReservationForOthers($resource)
{
    $resource = resourcemanagement_resource($resource);

    return $resource->isBookableForOthers();
// 	$domainId = $resource->domain;
// 	if ($resource->domain instanceof resourcemanagement_Domain) {
// 		$domainId = $resource->domain->id;
// 	}
// 	return bab_isAccessValid('resourcemanagement_resourceextendedbooker_groups', $resource->id)
// 		|| bab_isAccessValid('resourcemanagement_domainresourceextendedbooker_groups', $domainId);
}


/**
 * @return bool
 */
function resourcemanagement_canViewReservation($reservation)
{
    return true;
}


/**
 * Checks if the current user has a waiting approbation instance on this reservation.
 * @return bool
 */
function resourcemanagement_canUpdateReservationApprobation($reservation)
{
    $currentUserId = bab_getUserId();
    $appbrobations = bab_WFGetWaitingInstances($currentUserId);
    if (in_array($reservation->approbation, $appbrobations)) {
        return true;
    }
    return false;
}





function resourcemanagement_getCheckedResources($uid)
{
    if(!isset($_SESSION['resourcemanagement']['selectedressource'])){
        $_SESSION['resourcemanagement']['selectedressource'] = array();
    }
    if(!isset($_SESSION['resourcemanagement']['selectedressource'][$uid])){
        $_SESSION['resourcemanagement']['selectedressource'][$uid] = array();
    }
    $return = array();
    foreach($_SESSION['resourcemanagement']['selectedressource'][$uid] as $k => $v) {
        if ($v) {
            $return[$k] = $k;
        }
    }
    return $return;
}


function resourcemanagement_getCalendarSelectedResources()
{
    if (!isset($_SESSION['resourcemanagement']['resources'])) {
        $_SESSION['resourcemanagement']['resources'] = array();
    }
    return $_SESSION['resourcemanagement']['resources'];
}


function resourcemanagement_toggleSelectedResource($resource)
{
    resourcemanagement_getCalendarSelectedResources();
    if (!isset($_SESSION['resourcemanagement']['resources'][$resource])) {
        $_SESSION['resourcemanagement']['resources'][$resource] = $resource;
    } else {
        unset($_SESSION['resourcemanagement']['resources'][$resource]);
    }
}


function resourcemanagement_setSelectedResource($resource, $selected = true)
{
    resourcemanagement_getCalendarSelectedResources();
    if ($selected) {
        $_SESSION['resourcemanagement']['resources'][$resource] = $resource;
    } else {
        unset($_SESSION['resourcemanagement']['resources'][$resource]);
    }
}

function resourcemanagement_isResourceSelected($resource)
{
    return isset($_SESSION['resourcemanagement']['resources'][$resource]);
}



function resourcemanagement_getCalendarCurrentDate()
{
    if (!isset($_SESSION['resourcemanagement']['dateToShow'])) {
        $_SESSION['resourcemanagement']['dateToShow'] = date('Y-m-d');
    }
    return $_SESSION['resourcemanagement']['dateToShow'];
}






function resourcemanagement_getRRuleString($startDateTime, $endDateTime, $data)
{
    $rrule = array();
    $startDate = bab_mktime($startDateTime);

    $endDate = bab_mktime($endDateTime);


    $duration = $endDate - $startDate;

    switch ($data['repeat_frequency']) {

        case 'weekly':

            if (empty($data['repeat_n_w'])) {
                $data['repeat_n_w'] = 1;
            }

            if ($duration > 24 * 3600 * 7 * $data['repeat_n_w']) {
                throw new ErrorException(bab_translate('The duration of the event must be shorter than how frequently it occurs'));
                return false;
            }

            $rrule[]= 'INTERVAL=' . (int)$data['repeat_n_w'];

            if (!isset($data['repeat_wd'])) {
                // no week day specified, reapeat event every week
                $rrule[]= 'FREQ=WEEKLY';
            } else {
                $rrule[]= 'FREQ=WEEKLY';
                $weekDay = array();
                foreach($data['repeat_wd'] as $v){
                    if ($v != "0") {
                        $weekDay[] = $v;
                    }
                }

                // BYDAY : add list of weekday    = "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
                $rrule[] = 'BYDAY='.implode(',', $weekDay);
            }

            break;


        case 'monthly':
            if (empty($data['repeat_n_m'])) {
                $data['repeat_n_m'] = 1;
            }

            if ($duration > 24*3600*28*$data['repeat_n_m']) {
                throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
                return false;
            }

            $rrule[]= 'INTERVAL='.$data['repeat_n_m'];
            $rrule[]= 'FREQ=MONTHLY';
            break;

        case 'yearly':
            if (empty($data['repeat_n_y'])) {
                $data['repeat_n_y'] = 1;
            }

            if ($duration > 24 * 3600 * 365 * $data['repeat_n_y']) {
                throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
                return false;
            }
            $rrule[]= 'INTERVAL='.$data['repeat_n_y'];
            $rrule[]= 'FREQ=YEARLY';
            break;

        case 'daily':
            if (empty($data['repeat_n_d'])) {
                $data['repeat_n_d'] = 1;
            }
            if ($duration > 24 * 3600 * $data['repeat_n_d']) {
                throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
                return false;
            }
            $rrule[]= 'INTERVAL='.$data['repeat_n_d'];
            $rrule[]= 'FREQ=DAILY';
            break;
    }

    if (isset($data['repeat_end_date']) && $data['repeat_end_date']) {
        require_once $GLOBALS['babInstallPath'].'utilit/evtincl.php';
        $repeatDate = explode('-',$data['repeat_end_date']);
        $data['until'] = array(
            'year'	=> (int) $repeatDate[2],
            'month'	=> (int) $repeatDate[1],
            'day'	=> (int) $repeatDate[0]
        );
        $until = bab_event_posted::getDateTime($data['until']);
        $until->add(1);

        if ($until->getTimeStamp() < $endDate) {
            throw new ErrorException(bab_translate('Repeat date must be older than end date'));
            return false;
        }


        $rrule[] = 'UNTIL='.$until->getICal(true);
    }
    return implode(';',$rrule);

}



function resourcemanagement_getRRuleForm($string)
{
    $stringArray = explode(';',$string);
    $data = array();
    foreach($stringArray as $item)
    {
        $tempArray = explode('=', $item);
        if(isset($tempArray[0]) && isset($tempArray[1])){
            $data[$tempArray[0]] = $tempArray[1];
        }
    }

    $return = array();

    if(isset($data['FREQ'])){
        $return['repeat'] = 1;
        switch( $data['FREQ'] )
        {
            case 'WEEKLY':
                $return['repeat_n_w'] = $data['INTERVAL'];
                $return['repeat_frequency'] = 'weekly';

                if(isset($data['BYDAY']))
                {
                    $daysWeek = explode(',', $data['BYDAY']);
                    foreach($daysWeek as $v){
                        if($v){
                            $return['repeat_wd'][$v] = $v;
                        }
                    }
                }

                break;

            case 'MONTHLY':
                $return['repeat_n_m'] = $data['INTERVAL'];
                $return['repeat_frequency'] = 'monthly';
                break;

            case 'YEARLY': /* yearly */
                $return['repeat_n_y'] = $data['INTERVAL'];
                $return['repeat_frequency'] = 'yearly';
                break;

            case 'DAILY': /* daily */
                $return['repeat_n_d'] = $data['INTERVAL'];
                $return['repeat_frequency'] = 'daily';
                break;
        }

        if (isset($data['UNTIL']))
        {
            require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

            $tempBabDate = BAB_DateTime::fromICal($data['UNTIL']);
            $tempBabDate->add(-1);

            $return['repeat_end_date'] = $tempBabDate->getIsoDate();

            $return['repeat'] = 1;
        }
    }

    return $return;
}


function resourcemanagement_weekdayTranslate($str)
{
    switch ($str) {
        case 'MO':
            return resourcemanagement_translate('Monday');
            break;
        case 'TU':
            return resourcemanagement_translate('Tuesday');
            break;
        case 'WE':
            return resourcemanagement_translate('Wednesday');
            break;
        case 'TH':
            return resourcemanagement_translate('Thursday');
            break;
        case 'FR':
            return resourcemanagement_translate('Friday');
            break;
        case 'SA':
            return resourcemanagement_translate('Saturday');
            break;
        case 'SU':
            return resourcemanagement_translate('Sunday');
            break;
    }
}



/**
 * @return string
 */
function resourcemanagement_rruleDescription($rrule)
{
    require_once dirname(__FILE__) . '/RRule.php';

    $returnDescription = '';;
    $arryRule = resourcemanagement_getRRuleForm($rrule);
    switch($arryRule['repeat_frequency']){
        case 'weekly':

            if(isset($arryRule['repeat_wd'])){
                foreach($arryRule['repeat_wd'] as $k => $v)
                {
                    $arryRule['repeat_wd'][$k] = resourcemanagement_weekdayTranslate($v);
                }
                $returnDescription = resourcemanagement_translate('On the') . ' ' . implode(', ', $arryRule['repeat_wd']) . ' ';
            }

            if($arryRule['repeat_n_w'] != 1){
                $returnDescription.= sprintf(resourcemanagement_translate('Every %s weeks'),$arryRule['repeat_n_w']);
            }else{
                $returnDescription.= resourcemanagement_translate('Every week');
            }

            break;


        case 'monthly':
            if($arryRule['repeat_n_m'] != 1){
                $returnDescription.= sprintf(resourcemanagement_translate('Every %s months'),$arryRule['repeat_n_m']);
            }else{
                $returnDescription.= resourcemanagement_translate('Every month');
            }
            break;

        case 'yearly': /* yearly */
            if($arryRule['repeat_n_y'] != 1){
                $returnDescription.= sprintf(resourcemanagement_translate('Every %s years'),$arryRule['repeat_n_y']);
            }else{
                $returnDescription.= resourcemanagement_translate('Every year');
            }
            break;

        case 'daily': /* daily */
            if($arryRule['repeat_n_d'] != 1){
                $returnDescription.= sprintf(resourcemanagement_translate('Every %s days'),$arryRule['repeat_n_d']);
            }else{
                $returnDescription.= resourcemanagement_translate('Every day');
            }
            break;
    }

    if (isset($arryRule['repeat_end_date']) && $arryRule['repeat_end_date'])
    {
        $endRepeatDate = BAB_DateTime::fromIsoDateTime($arryRule['repeat_end_date']);
        $returnDescription.= ' ' . sprintf(resourcemanagement_translate('until %s'), $endRepeatDate->longFormat(false));

    }

    return $returnDescription;
}








class resourcemanagement_ReservationNotificationTemplate
{
    public $message;
    public $from;
    public $site;
    public $bview;
    public $by;
    public $reason = '';
    public $reasontxt = '';

    private $res;

    public function __construct(resourcemanagement_Reservation $reservation, $subject, $comment = '')
    {
        $this->message = $subject;

        if (!empty($comment)) {
            $this->reasontxt = resourcemanagement_translate('Reason / Additional information');
            $this->reason = bab_toHtml($comment, BAB_HTML_BR | BAB_HTML_ENTITIES);
        }

        if ($reservation->status == resourcemanagement_Reservation::STATUS_OK && $reservation->approvedBy) {
            $this->by = resourcemanagement_translate('By');
            $this->username = bab_getUserName($reservation->approvedBy);
            $this->bview = true;
        } else {
            $this->bview = false;
        }

        $this->res = array();
        if ($resource = $reservation->resource()) {

            $domain = $resource->domain();
            $domainPath = $domain->getFullPath();
            $domainName = implode(' > ', $domainPath);
            $this->res[resourcemanagement_translate('Resource')] = $domainName . ' > ' . $resource->name();
        }

        $periodDateStart = bab_shortDate(bab_mktime($reservation->start), false);
        $periodDateEnd = bab_shortDate(bab_mktime($reservation->end), false);

        if ($periodDateStart == $periodDateEnd) {
            $periodText = sprintf(
                resourcemanagement_translate('On %s, from %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), false),
                bab_time(bab_mktime($reservation->start)),
                bab_time(bab_mktime($reservation->end))
            );
        } else {
            $periodText = sprintf(
                resourcemanagement_translate('From %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), true),
                bab_shortDate(bab_mktime($reservation->end), true)
            );
        }
        $this->res[resourcemanagement_translate('Date')] = $periodText;


//        $this->res[resourcemanagement_translate('To')] = bab_longDate(bab_mktime($reservation->end), true);

        if ($reservation->description) {
            $this->res[resourcemanagement_translate('Description')] = $reservation->description;
        }
        if ($reservation->longDescription) {
            $this->res[resourcemanagement_translate('Long description')] = $reservation->longDescription;
        }

    }


    public function getnext()
    {
        if ($arr = each($this->res)) {
            $this->label = bab_toHtml($arr[0]);
            $this->value = bab_toHtml($arr[1]);
            return true;
        }

        return false;
    }
}

class resourcemanagement_ReservationApprobationNotificationTemplate extends resourcemanagement_ReservationNotificationTemplate
{
    public $approburl;
    public $t_approbation_page;

    public function __construct(resourcemanagement_Reservation $reservation, $subject, $comment = '') {
        parent::__construct($reservation, $subject, $comment);
        $this->t_approbation_page = resourcemanagement_translate('Go to approbation page');

        $goTo = resourcemanagement_Controller()->Reservation()->editApprobation($reservation->id)->url();
        $this->approburl = $GLOBALS['babUrl'].bab_getSelf().$goTo;
    }
}




/**
 * Notify author of a reservation.
 *
 * @param resourcemanagement_Reservation 	$reservation
 * @param string							$subject
 */
function resourcemanagement_notifyReservationStatusChange(resourcemanagement_Reservation $reservation, $subject, $comment = '', $noSender = false)
{
    global $BAB_SESS_USER, $BAB_SESS_EMAIL;

    $mail = bab_mail();
    if ($mail == false) {
        return;
    }
    if ($reservation->reservedFor && ($reservation->reservedFor != $reservation->reservedBy)) {
        $mail->mailTo(bab_getUserEmail($reservation->reservedFor), bab_getUserName($reservation->reservedFor));
        $mail->mailCC(bab_getUserEmail($reservation->reservedBy), bab_getUserName($reservation->reservedBy));
    } else {
        $mail->mailTo(bab_getUserEmail($reservation->reservedBy), bab_getUserName($reservation->reservedBy));
    }

    if (!$noSender) {
        $mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);
    }
    $mail->mailSubject($subject);

    $template = new resourcemanagement_ReservationNotificationTemplate($reservation, $subject, $comment);

    $addon = bab_getAddonInfosInstance('resourcemanagement');
    $message = $mail->mailTemplate($addon->printTemplate($template, 'mailinfo.html', 'inforeservation'));
    $mail->mailBody($message, 'html');

    $message = $addon->printTemplate($template, 'mailinfo.html', 'inforeservationtxt');
    $mail->mailAltBody($message);

    $mail->send();
}




/**
 * Notify author of a reservation about approval.
 *
 * @param resourcemanagement_Reservation 	$reservation
 * @param string							$comment
 */
function resourcemanagement_notifyValidation(resourcemanagement_Reservation $reservation, $comment = '')
{
    resourcemanagement_notifyReservationStatusChange(
        $reservation,
        sprintf(
            resourcemanagement_translate('Your reservation %s has been accepted'),
            $reservation->description
        ),
        $comment
    );
}


/**
 * Notify author of a reservation about refusal.
 *
 * @param resourcemanagement_Reservation 	$reservation
 * @param string							$comment
 */
function resourcemanagement_notifyRejection(resourcemanagement_Reservation $reservation, $comment = '')
{
    resourcemanagement_notifyReservationStatusChange(
        $reservation,
        sprintf(
            resourcemanagement_translate('Your reservation %s has been refused'),
            $reservation->description
        ),
        $comment
    );
}


/**
 * Notify author of a reservation about automatic option deletion.
 *
 * @param resourcemanagement_Reservation 	$reservation
 */
function resourcemanagement_notifyOptionDeletion(resourcemanagement_Reservation $reservation)
{
    resourcemanagement_notifyReservationStatusChange(
        $reservation,
        sprintf(
            resourcemanagement_translate('Your reservation in option %s has been deleted because the delay for confirmation has been exceeded'),
            $reservation->description
        )
    );
}





function resourcemanagement_getSkinName()
{
    return 'theme_crm_like';
}

function resourcemanagement_getSkinCss()
{
    return 'style.css';
}


/**
 * @param int $reservation
 */
function resourcemanagement_getAssociatedResources($reservation)
{
    if ($reservation) {

        $set = new resourcemanagement_ReservationSet();
        $reservation = $set->get($reservation);

        if ($reservation) {
            $reservations = $set->select(
                $set->recurrence->is($reservation->recurrence)
                ->_AND_($set->start->is($reservation->start))
                ->_AND_($set->end->is($reservation->end))
            );
            $resources = array();
            foreach ($reservations as $resa) {
                $resources[$resa->resource] = $resa->resource;
            }

            return $resources;
        }
    }

    return false;
}


/**
 * @param int $reservation
 */
function resourcemanagement_getAssociatedReservations($reservation)
{
    if ($reservation) {

        $set = new resourcemanagement_ReservationSet();
        $reservation = $set->get($reservation);

        if ($reservation) {
            $reservations = $set->select(
                $set->recurrence->is($reservation->recurrence)
                ->_AND_($set->start->is($reservation->start))
                ->_AND_($set->end->is($reservation->end))
            );
            $resources = array();
            foreach ($reservations as $resa) {
                $resources[$resa->resource] = $resa->id;
            }

            return $resources;
        }
    }

    return false;
}


/**
 * @param int $reservation
 */
function resourcemanagement_getAssociatedReservationsObject($reservation)
{
    if ($reservation) {

        $set = new resourcemanagement_ReservationSet();
        $reservation = $set->get($reservation);

        if ($reservation) {
            $reservations = $set->select(
                $set->recurrence->is($reservation->recurrence)
                ->_AND_($set->start->is($reservation->start))
                ->_AND_($set->end->is($reservation->end))
            );
            $resources = array();
            foreach ($reservations as $resa) {
                $resources[$resa->resource] = $resa;
            }

            return $resources;
        }
    }

    return false;
}

/**
 * @param $labelText
 * @param $item
 * @param $fieldName
 * @param $description
 * @param $suffix
 *
 * @return Widget_FlowLayout
 */
function resourcemanagement_LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
{
    $W = bab_Widgets();
    if (isset($description)) {
        $item->setTitle($description);
    }

    $label = $W->Label($labelText);
    if ($item instanceof Widget_InputWidget) {
        $label->setAssociatedWidget($item);
    }

    return $W->FlowItems(
        $label
            ->setSizePolicy('resourcemanagement-field-label-container')
            ->colon(),
        $item
            ->setSizePolicy('resourcemanagement-field-widget-container')
    )->setVerticalAlign('top');
}
