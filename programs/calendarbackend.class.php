<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__).'/controller.class.php';
require_once dirname(__FILE__).'/ui/reservation.ui.php';
require_once dirname(__FILE__).'/set/reservation.class.php';
require_once dirname(__FILE__).'/set/resource.class.php';


bab_functionality::includefile('CalendarBackend');




class Func_CalendarBackend_ResourceManagement extends Func_CalendarBackend
{
	
	
	/**
	 * Returns the period corresponding to the specified identifier
	 * this is necessary for all events with a link
	 * 
	 * @param	bab_PeriodCollection	$periodCollection		where to search for event, the collection can be populated with the selected event
	 * @param 	string 					$identifier				The UID property of event
	 * @param	string					[$dtstart]				The DTSTART value of the event (this can be usefull if the event is a recurring event, DTSTART will indicate the correct instance)
	 * 
	 * @return bab_CalendarPeriod
	 */
	public function getPeriod(bab_PeriodCollection $periodCollection, $identifier, $dtstart = null)
	{
		$reservationSet = new resourcemanagement_ReservationSet();
		
		$reservation = $reservationSet->get($identifier);
		
		$start = BAB_DateTime::fromIsoDateTime($reservation->start);
		$end = BAB_DateTime::fromIsoDateTime($reservation->end);
			
		$period = new bab_CalendarPeriod();
			
		$period->setDates($start, $end);
			
		$period->setProperty('UID',	$reservation->id);
		$period->setProperty('SUMMARY',	$reservation->description);
		$period->setProperty('DESCRIPTION',	$reservation->longDescription);
		$period->setProperty('LOCATION', '');
		$period->setProperty('X-CTO-COLOR', $reservation->getColor());
		$periodData = $period->getData();
		
		$periodData['viewurl'] = resourcemanagement_Controller()->Reservation()->display($reservation->id)->url();
		$periodData['viewinsamewindow'] = true;
		$period->setData($periodData);
		
		return $period;
	}
	
	public function storageBackend()
	{
		// The backend CANNOT be used as a storage backend for the existing calendars.
		return false;
	} 

}


	
