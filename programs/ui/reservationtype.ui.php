<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';


require_once dirname(__FILE__) . '/../set/reservationtype.class.php';


bab_Widgets()->includePhpClass('Widget_TableModelView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');




class resourcemanagement_ReservationTypeEditor extends Widget_Form
{

	/**
	 * 
	 * @param string $id
	 * @param Widget_Layout $layout
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();

		if (null === $layout) {
			$layout = $W->VBoxLayout()
				->setVerticalSpacing(1, 'em');
		}

		parent::__construct($id, $layout);

		$this->setName('reservationType');

		$this->setHiddenValue('tg', bab_rp('tg'));

		$this->addFields();

		$this->addItem(
			$W->FlowItems(
				$W->SubmitButton()
					->setAction(resourcemanagement_Controller()->ReservationType()->save())
					->setSuccessAction(resourcemanagement_Controller()->ReservationType()->displayList())
					->setFailedAction(resourcemanagement_Controller()->ReservationType()->edit())
					->setLabel(resourcemanagement_translate('Save')),
				$W->Link(
					resourcemanagement_translate('Cancel'),
					resourcemanagement_Controller()->ReservationType()->displayList()
				)->addClass('widget-actionbutton')
			)->setHorizontalSpacing(1, 'em')
			->setVerticalAlign('middle')
		);
	}


	protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
	{
		$W = bab_Widgets();
		return $W->FlowItems(
			$W->Label($labelText)
				->setSizePolicy('widget-20em')
				->colon(),
			$item
		);
	}


	protected function addFields()
	{
		$W = bab_Widgets();
		

		$nameItem = $W->LineEdit();
		$nameItem->setSizePolicy('widget-30em');
		$nameItem->addClass('widget-100pc');
		$nameItem->setName('name');

		$this->addItem(
			$this->LabelledWidget(
				resourcemanagement_translate('Name'),
				$nameItem
			)
		);
		
		$descriptionItem = $W->TextEdit();
		$descriptionItem->setSizePolicy('widget-30em');
		$descriptionItem->addClass('widget-100pc');
		$descriptionItem->setName('description');

		$this->addItem(
			$this->LabelledWidget(
				resourcemanagement_translate('Description'),
				$descriptionItem
			)
		);
		
		$colorItem = $W->ColorPicker();
		$colorItem->setName('color');

		$this->addItem(
			$this->LabelledWidget(
				resourcemanagement_translate('Color'),
				$colorItem
			)
		);

		
	}
	
	
	
	public function setReservationType(resourcemanagement_ReservationType $reservationType)
	{	
		$this->setValues($reservationType->getValues(), array('reservationType'));

		$this->setHiddenValue('reservationType[id]', $reservationType->id);

	}


}





class resourcemanagement_ReservationTypeTableView extends widget_TableModelView
{

	public function addDefaultColumns(resourcemanagement_ReservationTypeSet $set)
	{
		$this->addClass(Func_Icons::ICON_LEFT_16);

		$this->addColumn(
			widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->description, resourcemanagement_translate('Description'))
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->color, resourcemanagement_translate('Color'))
				->addClass('widget-col-5em')
				->setSearchable(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn('_actions_', '')->addClass('widget-col-10em', 'widget-align-center')
		);
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(resourcemanagement_ReservationType $record, $fieldPath)
	{
		$W = bab_Widgets();

		switch ($fieldPath) {

			case 'name':
				return $W->Label(
					$record->name
				);

			case 'color':
				return $W->Frame()->setCanvasOptions(
					Widget_Item::Options()
						->backgroundColor('#' . $record->color)->width(80, 'px')->height(20, 'px')
				);
				
			case '_actions_':
				
				$actionsBox = $W->FlowLayout();
				$actionsBox->setSpacing(4, 'px');
				$actionsBox->addClass(Func_Icons::ICON_LEFT_16);
				
				$actionsBox->addItem(
					$W->Link(
						resourcemanagement_translate('Edit'),
						resourcemanagement_Controller()->ReservationType()->edit($record->id)
					)->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT . ' icon widget-actionbutton')
				);
				
				if (resourcemanagement_canDeleteReservationType($record)) {
					$actionsBox->addItem(
						$W->Link(
							'',
							resourcemanagement_Controller()->ReservationType()->checkDelete($record->id)
						)->setTitle(resourcemanagement_translate('Delete'))
						->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon', 'widget-actionbutton')
					);
				}
				
				
				return $actionsBox;
		}

		return parent::computeCellContent($record, $fieldPath);
	}



}








