<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';


bab_Widgets()->includePhpClass('Widget_Form');

class resourcemanagement_AdminEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        parent::__construct($id, $layout);

        $this->setName('options');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->addItem(
            $W->SubmitButton()
                ->setAction(resourcemanagement_Controller()->Admin()->save())
                ->setSuccessAction(resourcemanagement_Controller()->Admin()->edit())
                ->setFailedAction(resourcemanagement_Controller()->Admin()->edit())
                ->setLabel(resourcemanagement_translate('Save'))
        );

        $this->loadValues();
    }


    protected function loadValues()
    {
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';
        $this->setValue(array('options', 'manager'), aclGetRightsString('resourcemanagement_manager_groups', 1));
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($W->Acl()->setName('manager')->setTitle(resourcemanagement_translate('Resource application managers')));
    }



}

class resourcemanagement_AdminTimeSlotEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        parent::__construct($id, $layout);

        $this->setName('timeslot');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->addItem(
            $W->SubmitButton()
                ->setAction(resourcemanagement_Controller()->Admin()->saveTimeSlot())
                ->setSuccessAction(resourcemanagement_Controller()->Admin()->editTimeSlot())
                ->setFailedAction(resourcemanagement_Controller()->Admin()->editTimeSlot())
                ->setLabel(resourcemanagement_translate('Save'))
        );

        $this->loadValues();
    }


    protected function loadValues()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');

        $this->setValue(array('timeslot', 'start'), $registry->getValue('start', ''));
        $this->setValue(array('timeslot', 'end'), $registry->getValue('end', ''));
    }


    protected function start()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            resourcemanagement_translate('Starting hour'),
            $W->TimePicker(),
            'start'
        );
    }


    protected function end()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            resourcemanagement_translate('Ending hour'),
            $W->TimePicker(),
            'end'
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->FlowItems(
                $this->start(),
                $this->end()
            )->setHorizontalSpacing(1, 'em')
        );

        $this->addItem($W->Label(resourcemanagement_translate('If you want full day just empty those fields.')));
    }



}

class resourcemanagement_AdminOptionEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        parent::__construct($id, $layout);

        $this->setName('globalOptions');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->addItem(
            $W->SubmitButton()
            ->setAction(resourcemanagement_Controller()->Admin()->saveOptions())
            ->setSuccessAction(resourcemanagement_Controller()->Admin()->editOptions())
            ->setFailedAction(resourcemanagement_Controller()->Admin()->editOptions())
            ->setLabel(resourcemanagement_translate('Save'))
            );

        $this->loadValues();
    }


    protected function loadValues()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');
        $this->setValue(array('globalOptions', 'linkedEventVisibility'), $registry->getValue('linkedEventVisibility', true));
    }


    protected function linkedEventVisibility()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            resourcemanagement_translate('Display linked events'),
            $W->CheckBox(),
            'linkedEventVisibility'
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->FlowItems(
                $this->linkedEventVisibility()
            )->setHorizontalSpacing(1, 'em')
        );
    }
}