<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/reservation.class.php';
require_once dirname(__FILE__) . '/../set/reservationtype.class.php';
require_once dirname(__FILE__) . '/../set/resource.class.php';
require_once dirname(__FILE__) . '/../ui/resource.ui.php';
require_once dirname(__FILE__) . '/../ui/rule.ui.php';


bab_Widgets()->includePhpClass('Widget_Layout');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');



class resourcemanagement_ReservationEditor extends Widget_Form
{

    protected $typesBox = null;

    protected $servicesSection = null;

    protected $resourcesSection = null;

    protected $participantsSection = null;

    public $buttons = null;


    /**
     * @var Widget_DatePicker
     */
    protected $startDateItem = null;

    /**
     * @var Widget_DatePicker
     */
    protected $endDateItem = null;


    /**
     * @var Widget_Select
     */
    protected $reservationTypeItem = null;



    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct($reservation)
    {
        $W = bab_Widgets();

        $this->reservation = $reservation;

        $layout = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'em');

        parent::__construct('rm-reservation'.uniqid(), $layout);

        $this->setName('reservation');
        $this->setPersistent(true, Widget_Widget::STORAGE_SESSION);

        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();


        $this->buttons = $W->FlowItems()
            ->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('middle');

        $this->addItem($this->buttons);

        $this->setReservation($this->reservation);
    }


    public function addButtons()
    {
        $args = func_get_args();

        foreach ($args as $arg) {
            $this->buttons->addItem($arg);
        }

        return $this;
    }



    protected function labelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix);
    }

    protected function horizontalLabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        if (isset($description)) {
            $item->setTitle($description);
        }

        $label = $W->Label($labelText);
        if ($item instanceof Widget_InputWidget) {
            $label->setAssociatedWidget($item);
        }

        return $W->FlowItems(
            $label,
            $item
        )->setHorizontalSpacing(1, 'em')
        ->setVerticalAlign('top');
    }



    protected function LabelledMainWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->VBoxItems(
            $W->Label($labelText)
                ->colon(),
            $item
        );
    }



    protected function addFields()
    {
        $W = bab_Widgets();

        $titleItem = $W->LineEdit();
        $titleItem->addClass('widget-50em');
        $titleItem->setName('description');

        $titleItem->setMandatory(true, resourcemanagement_translate('You must provide a description.'));

        $reservationTypeItem = $W->Select();
        $reservationTypeItem->setName('type');
        $reservationTypeSet = new resourcemanagement_ReservationTypeSet();
        $reservationTypes = $reservationTypeSet->select();
        $reservationTypeItem->addOption('', '');
        foreach ($reservationTypes as $reservationType) {
            $reservationTypeItem->addOption($reservationType->id, $reservationType->name);
        }

        $this->reservationTypeItem = $reservationTypeItem;

        $this->addItem(
            $W->FlowItems(
                $this->LabelledWidget(
                    resourcemanagement_translate('Title'),
                    $titleItem
                ),
                $this->LabelledWidget(
                    resourcemanagement_translate('Reservation type'),
                    $reservationTypeItem
                )
            )->setVerticalAlign('top')
            ->setHorizontalSpacing(2, 'em')
        );

        $descriptionItem = $W->TextEdit();
        $descriptionItem->addClass('widget-100pc');
        $descriptionItem->setName('longDescription');

        $this->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Description'),
                $descriptionItem
            )
        );



        $this->addItem(
            $this->labelledWidget(
                resourcemanagement_translate('Location'),
                $W->LineEdit()->addClass('widget-100pc')
            )
        );



        $this->startDateItem = $startDateItem = $W->DatePicker();
        $startDateItem->setName('startDate');
        $startDateItem->setMandatory(true, resourcemanagement_translate('You must select a start date.'));

        $endTimeItem = $W->TimeEdit();
        $endTimeItem->setName('endTime');
        $endTimeItem->setMandatory(true, resourcemanagement_translate('You must select an end time.'));

        $this->endDateItem = $endDateItem = $W->DatePicker();
        $endDateItem->setName('endDate');

        $startTimeItem = $W->TimeEdit();
        $startTimeItem->setName('startTime');
        $startTimeItem->setMandatory(true, resourcemanagement_translate('You must select a start time.'));

        $endDateItem->setMinDate($startDateItem);

        $ruleItem = new resourcemanagement_RuleEditor();


        $repeatItem = $W->CheckBox();
        $repeatItem->setName('repeat');
        $repeatItem->setCheckedValue('1')->setAssociatedDisplayable($ruleItem, array('1'));


        $this->addItem(
            $W->FlowItems(
                $this->labelledWidget(
                    resourcemanagement_translate('Period'),
                    $W->FlowItems(
                        $startDateItem,
                        $startTimeItem,
                        $W->Label(resourcemanagement_translate('to')),
                        $endTimeItem,
                        $endDateItem
                    )->setHorizontalSpacing(1, 'ex')
                )->setSizePolicy('widget-60pc'),
                $this->labelledWidget(
                    resourcemanagement_translate('Repeat...'),
                    $ruleItem
                )->setSizePolicy('widget-40pc')
            )->setVerticalAlign('top')
            ->setHorizontalSpacing(2, 'em')
        );



        $this->participantsSection = $W->Section(
            resourcemanagement_translate('Participants'),
            $W->VBoxItems(
//                 $W->Link(
//                     resourcemanagement_translate('Add')
//                 )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD),
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Label(resourcemanagement_translate('Collaborators'))
                            ->addClass('widget-strong')
                            ->setSizePolicy('widget-30em'),
                        $W->Label(resourcemanagement_translate('Invited'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('For information'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('Excused'))
                            ->setSizePolicy('widget-10em widget-align-center')
                    ),
                    $W->FlowItems(
                        $W->LineEdit()
                            ->addClass('widget-90pc')
                            ->setValue('Sandrine DUPONT')
                            ->setSizePolicy('widget-30em'),
                        $W->Checkbox()
                            ->setValue(true)
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center')
                    )->setSizePolicy('widget-list-element'),
                    $W->FlowItems(
                        $W->LineEdit()
                            ->addClass('widget-90pc')
                            ->setValue('Jean MARTIN')
                            ->setSizePolicy('widget-30em'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setValue(true)
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Link('')->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                            ->setTitle(resourcemanagement_translate('Add collaborator'))
                    )->setSizePolicy('widget-list-element')
                ),
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Label(resourcemanagement_translate('Group'))
                            ->addClass('widget-strong')
                            ->setSizePolicy('widget-30em'),
                        $W->Label(resourcemanagement_translate('Invited'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('For information'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('Excused'))
                            ->setSizePolicy('widget-10em widget-align-center')
                    ),
                    $W->FlowItems(
                        $W->LineEdit()
                            ->addClass('widget-90pc')
                            ->setSizePolicy('widget-30em'),
                        $W->Checkbox()
                            ->addClass('widget-align-center')
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->addClass('widget-align-center')
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Link('')->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                            ->setTitle(resourcemanagement_translate('Add group'))
                    )->setSizePolicy('widget-list-element')
                ),
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Label(resourcemanagement_translate('External'))
                            ->addClass('widget-strong')
                            ->setSizePolicy('widget-30em'),
                        $W->Label(resourcemanagement_translate('Invited'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('For information'))
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Label(resourcemanagement_translate('Excused'))
                            ->setSizePolicy('widget-10em widget-align-center')
                    ),
                    $W->FlowItems(
                        $W->LineEdit()
                            ->addClass('widget-90pc')
                            ->setSizePolicy('widget-30em'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Checkbox()
                            ->setSizePolicy('widget-10em widget-align-center'),
                        $W->Link('')->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                            ->setTitle(resourcemanagement_translate('Add external person'))
                    )->setSizePolicy('widget-list-element')
                )
            ),

            3
        );

        $this->addItem($this->participantsSection);



        $this->resourcesSection = $W->VBoxItems();

        $this->addItem($this->resourcesSection);



        $this->typesFrame = $W->VBoxLayout();

        $this->addItem($this->typesFrame);



        $this->servicesSection = $W->Section(
            resourcemanagement_translate('Services'),
            $W->VBoxItems(
                $W->Link(
                    resourcemanagement_translate('Add')
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD),
                $W->FlowItems(
                    $this->horizontalLabelledWidget(
                        'Type',
                        $W->Select()
                            ->addOption('', '')
                            ->addOption('1', 'Cafe')
                            ->addOption('2', 'Petit dejeuner')
                            ->addOption('3', 'Dejeuner')
                            ->addOption('4', 'Cocktail')
                    ),
                    $this->horizontalLabelledWidget(
                       'Debut',
                        $W->TimeEdit()
                    ),
                    $this->horizontalLabelledWidget(
                       'Fin',
                        $W->TimeEdit()
                    ),
                    $this->horizontalLabelledWidget(
                        'Nombre de couverts',
                        $W->NumberEdit()
                    )
                )->setHorizontalSpacing(1, 'em')
                ->setSizePolicy('widget-list-element'),
                $W->FlowItems(
                    $this->horizontalLabelledWidget(
                        'Type',
                        $W->Select()
                            ->addOption('', '')
                            ->addOption('1', 'Cafe')
                            ->addOption('2', 'Petit dejeuner')
                            ->addOption('3', 'Dejeuner')
                            ->addOption('4', 'Cocktail')
                    ),
                    $this->horizontalLabelledWidget(
                       'Debut',
                        $W->TimeEdit()
                    ),
                    $this->horizontalLabelledWidget(
                       'Fin',
                        $W->TimeEdit()
                    ),
                    $this->horizontalLabelledWidget(
                        'Nombre de couverts',
                        $W->NumberEdit()
                    )
                )->setHorizontalSpacing(1, 'em')
                ->setSizePolicy('widget-list-element')
            ),
            3
        );

        $this->addItem($this->servicesSection);
    }


//     /**
//      * The section containing fields coming from types associated the resource reserved.
//      *
//      * @param resourcemanagement_Type $type
//      * @return Widget_Section
//      */
//     public function typeSection(resourcemanagement_Type $type)
//     {
//         $W = bab_Widgets();

//         $typeSection = $W->Section(
//             $type->name,
//             $W->VBoxItems()
//                 ->setVerticalSpacing(1, 'em'),
//             3
//         );
//         $typeSection->addClass('compact');
//         $typeSection->setName('type' . $type->id);

//         $typeFieldSet = new resourcemanagement_TypeFieldSet();
//         $typeFields = $typeFieldSet->select(
//             $typeFieldSet->type->is($type->id)
//                 ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
//         );
//         $typeFields->orderDesc($typeFieldSet->order);
//         $typeFields->orderAsc($typeFieldSet->id);

//         /* @var $typeField resourcemanagement_TypeField  */
//         foreach ($typeFields as $typeField) {
//             $input = $typeField->getInputWidget();
//             if ($typeField->mandatory) {
//                 $input->setMandatory(true, sprintf(resourcemanagement_translate('The field "%s" is mantory.'), $typeField->name));
//             }
//             $item = $this->LabelledWidget(
//                 $typeField->name,
//                 $input
//             );
//             $typeSection->addItem($item);
//         }

//         return $typeSection;
//     }



//     /**
//      *
//      * @param multitype:resourcemanagement_Type $types
//      * @return Widget_VBoxLayout
//      */
//     public function sectionsFrame($types)
//     {
//         $W = bab_Widgets();
//         $box = $W->Frame(null, $W->VBoxItems());

//         foreach ($types as $type) {
//             $typeSection = $this->typeSection($type);

//             $box->addItem($typeSection);
//         }

//         return $box;
//     }


    public function copyReservation(resourcemanagement_Reservation $reservation, $rruleForm)
    {
        $values = array(
            'start' => '0000-00-00 00:00:00',
            'end' => '0000-00-00 00:00:00'
        );

        $values+= array('rule' => $rruleForm);

        if ($rruleForm && isset($rruleForm['repeat'])) {
            $values['rule']['repeat_end_date'] = '0000-00-00';
            $values['repeat'] = '1';
        }

        @list($values['startDate'], $values['startTime']) = explode(' ', $values['start']);
        @list($values['endDate'], $values['endTime']) = explode(' ', $values['end']);

        $this->setHiddenValue('reservation[id]', '0');


        $this->setValues($values, array('reservation'));

    }


    public function setResource($uid = null, $resources = null)
    {
        if (!$uid) {
            $uid = uniqid();

            if(!isset($_SESSION['resourcemanagement']['selectedressource'])){
                $_SESSION['resourcemanagement']['selectedressource'] = array();
            }
            if(!isset($_SESSION['resourcemanagement']['selectedressource'][$uid])){
                $_SESSION['resourcemanagement']['selectedressource'][$uid] = array();
            }
            foreach ($resources as $res) {
                $_SESSION['resourcemanagement']['selectedressource'][$uid][$res] = true;
            }
        }

        $this->uid = $uid;
        $this->setHiddenValue('reservation[uid]', $uid);

        return resourcemanagement_Controller()->Reservation(false)->ResourceForm($uid);
    }


    public function conditionalBooking($uid)
    {
        return resourcemanagement_Controller()->Reservation(false)->conditionalBookingForm($uid);
    }


    public function reservedFor($uid)
    {
        return resourcemanagement_Controller()->Reservation(false)->reservedForForm($uid);
    }


    public function typesSection($uid, $reservation)
    {
        return resourcemanagement_Controller()->Reservation(false)->typeForm($uid, $reservation);
    }


    public function setReservation(resourcemanagement_Reservation $reservation)
    {
        $W = bab_Widgets();

        $reservation->extend();

        $this->setHiddenValue('reservation[id]', $reservation->id);

        if ($reservation->id) {
            $resources = resourcemanagement_getAssociatedResources($reservation->id);
            if ($resources) {
                $resourceInfo = $this->setResource(null, $resources);
            } else {
                $resourceInfo = $this->setResource(null, array($reservation->resource));
                //$this->setHiddenValue('reservation[resource]['.$reservation->id.']', $reservation->resource);
            }
        } else {
            $resourceInfo = $this->setResource($reservation->getRessources());
        }

        if ($reservation->recurrence->rrule) {
            $this->rruleItem->addAttribute('style','display:none;');
            $this->rruleLayout->addItem(
                $this->labelledWidget(
                    resourcemanagement_translate('Repeat...'),
                    $W->VBoxItems(
                        $W->Label(resourcemanagement_rruleDescription($reservation->recurrence->rrule))
                    )
                )
            );
            //$this->rruleLayout->addItem($W->Hidden(null, 'rrule', $reservation->rrule));
            /*$rruleForm = resourcemanagement_getRRuleForm($reservation->rrule);
            $values['rule'] = $rruleForm;
            if (isset($rruleForm['repeat']) && $rruleForm['repeat']) {
                $values['repeat'] = 1;
            }*///@TODO Futur fixe
        }

        /* @var $resource resourcemanagement_Resource */
        $resourcesArray = resourcemanagement_getCheckedResources($this->uid);
        $set = new resourcemanagement_ResourceSet();
        $resources = $set->select($set->id->in($resourcesArray));

//         //Conditional booking
//         if ($reservation->status == resourcemanagement_Reservation::STATUS_DRAFT) {
//             $this->addItem(
//                 $this->conditionalBooking($this->uid),
//                 3
//             );
//         }
//         //Conditional booking


        //Reserve for
        $this->addItem(
            $this->reservedFor($this->uid),
            0
        );
        //Reserve for

        foreach ($resources as $resource) {
        }

        if (count($resources) == 1) {
            if ($resource->reservationDelayMin > 0) {
                $minDate = BAB_DateTime::now();
                $minDate->add($resource->reservationDelayMin, BAB_DATETIME_DAY);

                $this->startDateItem->setMinDate($minDate);
            }

            if ($resource->reservationDelayMax > 0) {
                $maxDate = BAB_DateTime::now();
                $maxDate->add($resource->reservationDelayMax, BAB_DATETIME_DAY);

                $this->startDateItem->setMaxDate($maxDate);
            }
        }

        $this->resourcesSection->addItem(
            $resourceInfo
        );


        $this->typesFrame->addItem(
            $this->typesSection($this->uid, $reservation->id)
        );

        $values = $reservation->getValues();

        if(count($resources) == 1) {
            if ((!isset($values['type']) || empty($values['type'])) && $resource->defaultReservationType) {
                $values['type'] = $resource->defaultReservationType;
            }
        }

        @list($values['startDate'], $values['startTime']) = explode(' ', $values['start']);
        @list($values['endDate'], $values['endTime']) = explode(' ', $values['end']);

        $this->setValues($values, array('reservation'));
    }

}




class resourcemanagement_ReservationFrame extends Widget_Frame
{


    public $buttons = null;


    /**
     * @var resourcemanagement_Reservation $reservation The reservation displayed in the frame.
     */
    protected $reservation = null;

    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);

        $W = bab_Widgets();
        $this->buttons = $W->FlowItems();
        $this->buttons->setHorizontalSpacing(2, 'em')->addClass(Func_Icons::ICON_LEFT_16);
    }


    /**
     *
     * @param resourcemanagement_Reservation $reservation	The resource to display.
     * @return resourcemanagement_ReservationFrame
     */
    public function setReservation(resourcemanagement_Reservation $reservation)
    {
        $this->reservation = $reservation;
        return $this;
    }

    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->FlowItems(
            $W->Label($labelText)
                ->setSizePolicy('widget-20em')
                ->addClass('widget-strong'),
                $item
        )->setVerticalAlign('top');
    }


    public function initialize()
    {
        $W = bab_Widgets();

        /* @var resourcemanagement_Reservation $reservation */
        $reservation = $this->reservation;

        $resource = $reservation->resource();

        $mainBox = $W->HBoxItems();
        $mainBox->setHorizontalSpacing(1, 'em');
        $mainBox->setVerticalAlign('top');

        $descriptionBox = $W->VBoxItems()->setVerticalSpacing(1, 'em');
        $descriptionBox->setSizePolicy('widget-col-50pc');


        $periodDateStart = bab_shortDate(bab_mktime($reservation->start), false);
        $periodDateEnd = bab_shortDate(bab_mktime($reservation->end), false);

        if ($periodDateStart == $periodDateEnd) {
            $periodText = sprintf(resourcemanagement_translate('On %s, from %s to %s'), bab_shortDate(bab_mktime($reservation->start), false), bab_time(bab_mktime($reservation->start)), bab_time(bab_mktime($reservation->end)));
        } else {
            $periodText = sprintf(resourcemanagement_translate('From %s to %s'), bab_shortDate(bab_mktime($reservation->start), true), bab_shortDate(bab_mktime($reservation->end), true));
        }

        $descriptionBox->addItem(
            $W->Label($periodText)
        );

        if (!empty($reservation->recurrence->rrule)) {
            $descriptionBox->addItem(
                $W->FlowItems(
                    $W->Label(resourcemanagement_translate('Repeat'))->colon(),
                    $W->Label(resourcemanagement_rruleDescription($reservation->recurrence->rrule))
                )->setHorizontalSpacing(1, 'em')
            );
        }

        if ($reservation->reservedFor) {
            $descriptionBox->addItem(
                $W->FlowItems(
                    $W->Label(resourcemanagement_translate('Reservation made for')),
                    $W->Label(bab_getUserName($reservation->reservedFor))->addClass('widget-strong'),
                    $W->Label(resourcemanagement_translate('by')),
                    $W->Label(bab_getUserName($reservation->reservedBy))->addClass('widget-strong')
                )->setHorizontalSpacing(1, 'ex')
            );
        } else {
            $descriptionBox->addItem(
                $W->FlowItems(
                    $W->Label(resourcemanagement_translate('Reservation made by')),
                    $W->Label(bab_getUserName($reservation->reservedBy))->addClass('widget-strong')
                )->setHorizontalSpacing(1, 'ex')
            );
        }

        $descriptionBox->addItem(
            $W->Html(
                bab_toHtml($reservation->longDescription)
            )
        );

        $statusBox = null;
        if (resourcemanagement_canUpdateResource($resource)) {
            $statusBox = $W->VBoxItems(
                $W->FlowItems(
                    $W->Label(resourcemanagement_translate('Reservation created on')),
                    $W->Label(bab_shortDate(bab_mktime($reservation->createdOn))),
                    $W->Label(resourcemanagement_translate('by')),
                    $W->Label(bab_getUserName($reservation->reservedBy))
                )->setHorizontalSpacing(1, 'ex')
            )->setVerticalSpacing(0.5, 'em')
            ->addClass('widget-list-element', 'widget-small');

            if ($reservation->modifiedBy != 0 && ($reservation->createdOn != $reservation->modifiedOn || $reservation->reservedBy != $reservation->modifiedBy)) {
                $statusBox->addItem(
                    $W->FlowItems(
                        $W->Label(resourcemanagement_translate('Reservation modified on')),
                        $W->Label(bab_shortDate(bab_mktime($reservation->modifiedOn))),
                        $W->Label(resourcemanagement_translate('by')),
                        $W->Label(bab_getUserName($reservation->modifiedBy))
                    )->setHorizontalSpacing(1, 'ex')
                );
            }

            if ($reservation->status == resourcemanagement_Reservation::STATUS_WAITING) {
                $userIds = bab_WFGetWaitingApproversInstance($reservation->approbation);
	            if (count($userIds) == 0) {
	            	$reservation->fixApprobation();
	            } else {
	                $users = array();
	                foreach ($userIds as $userId) {
	                    $users[] = bab_getUserName($userId);
	                }
	                $statusBox->addItem(
	                    $W->Label(sprintf(bab_translate('Waiting for approval by : %s'), implode(', ',$users)))
	                );
				}
            }
        }

        $mainBox->addItem($descriptionBox);

        $types = $resource->getTypes();
        $mainBox->addItem($this->sectionsFrame($types));

        $resourceInfo = resourcemanagement_resourceShortPreview($resource);

//		$descriptionBox->addItem($resourceInfo);

        $this->setLayout(
            $W->VBoxItems(
                $W->Title($reservation->description),
                $resourceInfo,
                $mainBox,
                $this->linkedReservation(),
                $statusBox,
                $this->buttons
            )->setVerticalSpacing(1, 'em')
        );

        return $this;
    }


    public function linkedReservation()
    {
        $W = bab_Widgets();

        /* @var resourcemanagement_Reservation $reservation */
        $reservation = $this->reservation;

        $resources = resourcemanagement_getAssociatedReservationsObject($reservation->id);
        if (!$resources || count($resources) <= 1) {
            return null;
        }

        $section = $W->VBoxItems(
            $W->Title(
                resourcemanagement_translate(
                    'This reservation is also link to this resource',
                    'This reservation is also link to thoses resources',
                    count($resources)-1
                ),
                3
            ),
            $layout = $W->HBoxItems()->setHorizontalSpacing(2, 'em')
        )->setVerticalSpacing(1, 'em');

        foreach ($resources as $resource) {
            if($resource->id != $reservation->id) {
                $layout->addItem(resourcemanagement_resourceShortPreview($resource->resource(), $resource->id));
            }
        }

        return $section;
    }



    /**
     * The section containing fields coming from types associated the resource reserved.
     *
     * @param resourcemanagement_Type $type
     * @return Widget_Section
     */
    public function typeSection(resourcemanagement_Type $type)
    {
        $W = bab_Widgets();

        $typeSection = $W->VBoxItems();

        $typeFieldSet = new resourcemanagement_TypeFieldSet();
        $typeFields = $typeFieldSet->select(
            $typeFieldSet->type->is($type->id)
                ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
                ->_AND_($typeFieldSet->visibleInDescription->is(true))
        );
        $typeFields->orderDesc($typeFieldSet->order);
        $typeFields->orderAsc($typeFieldSet->id);

        $typename = 'type' . $type->id;

        /* @var $typeField resourcemanagement_TypeField  */
        foreach ($typeFields as $typeField) {
            $fieldname = 'f' . $typeField->id;

			if ($type = $this->reservation->$typename) {
	            $fieldValue = $type->$fieldname;
	            $fieldValue = resourcemanagement_TypeField::getFormattedValue($fieldValue, $typeField->fieldType);

	            $item = $this->LabelledWidget(
	                $typeField->name,
	                $W->Label($fieldValue)
	            );
	            $item->setSizePolicy('widget-list-element');
	            $typeSection->addItem($item);
			}
        }

        return $typeSection;
    }




    /**
     *
     * @param multitype:resourcemanagement_Type $types
     * @return Widget_VBoxLayout
     */
    public function sectionsFrame($types)
    {
        $W = bab_Widgets();
        $box = $W->Frame(null, $W->VBoxItems());

        foreach ($types as $type) {
            $typeSection = $this->typeSection($type);

            $box->addItem($typeSection);
        }

        return $box;
    }




    public function addButtons()
    {
        $args = func_get_args();

        foreach ($args as $arg) {
            $this->buttons->addItem($arg);
        }

        return $this;
    }

}





class resourcemanagement_ReservationTableView extends widget_TableModelView
{
    protected $resourceSet = null;

    protected $type = null;

    protected $typeFields = null;

    protected $fieldType = array();

    public function __construct($id = null)
    {
        parent::__construct($id);

    }



    /**
     * Sets the data source.
     *
     * @param ORM_Iterator $iterator
     * @return $this
     */
    public function setDataSource(ORM_Iterator $iterator)
    {
        parent::setDataSource($iterator);

        $this->types = array();
        $resources = array();

        $i = clone $iterator;
        foreach ($i as $record) {
            $resources[$record->resource->id] = $record->resource->id;
        }

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select($resourceTypeSet->resource->in($resources));
        $resourceTypes->orderAsc($resourceTypeSet->type);
        foreach ($resourceTypes as $resourceType) {
            $this->types[$resourceType->type->id] = $resourceType->type->name;
        }

        $typeFieldSet = new resourcemanagement_TypeFieldSet();
        $this->typeFields = $typeFieldSet->select(
            $typeFieldSet->type->in(array_keys($this->types))
                ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
                ->_AND_($typeFieldSet->exportable->is(true))
        );
        $this->typeFields->orderDesc($typeFieldSet->order);
        $this->typeFields->orderAsc($typeFieldSet->id);

        return $this;
    }

    public function setType($type)
    {
        $typeSet = new resourcemanagement_TypeSet();
        $this->type = $typeSet->get($type);

        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see resourcemanagement_ResourceTableView::addDefaultColumns($set)
     * @param resourcemanagement_ReservationSet $set
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {

        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addColumn(
            widget_TableModelViewColumn($set->id, resourcemanagement_translate('UID'))
                ->setVisible(false)
                ->addClass('widget-column-thin')
        );
        $this->addColumn(
            widget_TableModelViewColumn('image', '')
                ->setExportable(false)
                ->addClass('widget-column-thin')
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->resource->name, resourcemanagement_translate('Resource'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->resource->domain, resourcemanagement_translate('Domain'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->description, resourcemanagement_translate('Description'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->start, resourcemanagement_translate('Start'))
                ->setExportable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn('startDate', resourcemanagement_translate('Start date'))
        );
        $this->addColumn(
            widget_TableModelViewColumn('startTime', resourcemanagement_translate('Start time'))
        );
// 		$this->addColumn(
// 			widget_TableModelViewColumn($set->end, resourcemanagement_translate('End'))
// 		);
        $this->addColumn(
            widget_TableModelViewColumn('endDate', resourcemanagement_translate('End date'))
        );
        $this->addColumn(
            widget_TableModelViewColumn('endTime', resourcemanagement_translate('End time'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->reservedBy, resourcemanagement_translate('Reserved by'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->reservedFor, resourcemanagement_translate('Reserved for'))
        );

        foreach ($this->typeFields as $typeField) {
            $fieldPath = 'type' . $typeField->type . '.f' . $typeField->id;
            $this->fieldType[$fieldPath] = $typeField->fieldType;

            $this->addColumn(
                widget_TableModelViewColumn($fieldPath, $this->types[$typeField->type] . ' / ' . $typeField->name)
                    ->setVisible(false)
            );
        }
    }



    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        switch ($fieldPath) {

            case 'startDate':
                return date('d/m/Y', bab_mktime($record->start));

            case 'startTime':
                return date('H:i', bab_mktime($record->start));

            case 'endDate':
                return date('d/m/Y', bab_mktime($record->end));

            case 'endTime':
                return date('H:i', bab_mktime($record->end));

            case 'resource/domain':
                $domain = $record->resource->domain();
                $domainPath = $domain->getFullPath();
                return implode(' > ', $domainPath);
        }

        $record->extend();


        if (substr($fieldPath, 0, 4) === 'type') {
            $record->extend();
            list($type, $f) = explode('.', $fieldPath);

            if (isset($record->$type)) {
                $fieldValue = resourcemanagement_TypeField::getFormattedValue($record->$type->$f, $this->fieldType[$fieldPath]);
            } else {
                $fieldValue = '';
            }

            return $fieldValue;
        }

        return parent::computeCellTextContent($record, $fieldPath);

    }

    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case '_actions_':
                $actionsBox = $W->FlowLayout();
                $actionsBox->addClass(Func_Icons::ICON_LEFT_16);
                return $actionsBox;

            case 'resource/domain':
                $domain = $record->resource->domain();
                $domainPath = $domain->getFullPath();
                return $W->Label(implode(' > ', $domainPath));
        }

        if (substr($fieldPath, 0, 4) === 'type') {
            $record->extend();
            list($type, $f) = explode('.', $fieldPath);

            if (isset($record->$type)) {
                $fieldValue = resourcemanagement_TypeField::getFormattedValue($record->$type->$f, $this->fieldType[$fieldPath]);
            } else {
                $fieldValue = '';
            }

            return $W->Label($fieldValue);
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}

