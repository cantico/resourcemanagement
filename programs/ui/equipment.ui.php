<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/domain.class.php';
require_once dirname(__FILE__) . '/../set/resource.class.php';
require_once dirname(__FILE__) . '/../set/equipment.class.php';
require_once dirname(__FILE__) . '/../set/type.class.php';
require_once dirname(__FILE__) . '/../set/typefield.class.php';
require_once dirname(__FILE__) . '/../set/resourcetype.class.php';
require_once dirname(__FILE__) . '/../set/resourceavailability.class.php';
require_once dirname(__FILE__) . '/../ui/domain.ui.php';


bab_Widgets()->includePhpClass('Widget_TableModelView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');




class resourcemanagement_EquipmentEditor extends Widget_Form
{

    public $imagesFormItem = null;

    public $typeSpecificBox = null;

    public $editorUID;

    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct()
    {
        $id = 'rm-equipmentEditor';
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'em');

        parent::__construct($id, $layout);

        $this->editorUID = uniqid();

        $this->setName('equipment');


        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $successAction = Widget_Action::fromUrl($_SERVER['HTTP_REFERER']);
        } else {
            $successAction = resourcemanagement_Controller()->Equipment()->displayList();
        }

        $this->addItem(
            $W->FlowItems(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Equipment()->save())
                    ->setAjaxAction()
                    ->setSuccessAction($successAction)
                    ->setFailedAction(resourcemanagement_Controller()->Equipment()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Save')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Equipment()->displayList()
                )->addClass('widget-actionbutton', 'widget-close-dialog')
            )->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('middle')
        );



    }


    /**
     * @param string						$labelText
     * @param Widget_Displayable_Interface	$item
     * @param string						$description
     *
     * @return Widget_FlowLayout
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $description = null)
    {
        $W = bab_Widgets();

        if (isset($description)) {
            $item->setTitle($description);
        }

        $label = $W->Label($labelText);
        $label->setAssociatedWidget($item);

        return $W->FlowItems(
            $label->colon()
                ->setSizePolicy('resourcemanagement-field-label-container'),
            $item->setSizePolicy('resourcemanagement-field-widget-container')
        )->setVerticalAlign('top');
    }




    protected function addFields()
    {
        $W = bab_Widgets();

        $generalSection = $W->Section(
            resourcemanagement_translate('General information'),
            $W->VBoxItems()
                ->setVerticalSpacing(1, 'em'),
            3
        );

        $this->addItem($generalSection);


        $resourceSet = new resourcemanagement_ResourceSet();
        $resourceSet->domain();
        $resources = $resourceSet->select();
        $resources->orderAsc($resourceSet->domain->name);
        $resources->orderAsc($resourceSet->name);
        $resourceItem = $W->Select();
        $resourceItem->setName('resource');
        $resourceItem->addOption('', '');
        foreach ($resources as $resource) {
            $resourceItem->addOption($resource->id, $resource->name, $resource->domain->name);
        }

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Resource'),
                $resourceItem
            )
        );

        $nameItem = $W->LineEdit();
        $nameItem->setSizePolicy('widget-30em');
        $nameItem->addClass('widget-100pc');
        $nameItem->setName('name');
        $nameItem->setMandatory(true, resourcemanagement_translate('You specify an name.'));

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Name'),
                $nameItem
            )
        );

        $quantityItem = $W->LineEdit();
        $quantityItem->setSize(4);
        $quantityItem->setSizePolicy('widget-30em');
        $quantityItem->setName('quantity');


        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Quantity'),
                $quantityItem
            )
        );

        $descriptionItem = $W->TextEdit();
        $descriptionItem->setName('description');
        $descriptionItem->setSizePolicy('widget-50em');
        $descriptionItem->addClass('widget-100pc');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Description'),
                $descriptionItem
            )
        );
    }



    public function setEquipment(resourcemanagement_Equipment $equipment = null)
    {
        if (isset($equipment)) {
            $this->setHiddenValue('equipment[id]', $equipment->id);
            $this->setId('rm-equipmentEditor-'.$equipment->id);
        }

        if (isset($equipment)) {
            $this->setValues($equipment->getValues(), array('equipment'));
        }
    }
}





class resourcemanagement_EquipmentTableView extends widget_TableModelView
{

    /**
     * (non-PHPdoc)
     * @see widget_TableModelView::addDefaultColumns($set)
     * @param resourcemanagement_EquipmentSet    ORM_RecordSet $set
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addColumn(
            widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->resource()->name, resourcemanagement_translate('Resource'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->quantity, resourcemanagement_translate('Quantity'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->description, resourcemanagement_translate('Description'))
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-align-center')
        );
    }



    /**
     * @param resourcemanagement_Equipment	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'name':
                $link = $W->Link(
                    $record->name,
                    resourcemanagement_Controller()->Equipment()->display($record->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG);

                return $link;

            case 'resource':
                $link = $W->Label($record->resource()->name);
                return $link;

            case 'description':
                return $W->Label(bab_abbr($record->description, BAB_ABBR_FULL_WORDS, 100));

            case '_actions_':
                $actionsBox = $W->FlowLayout();
                if (resourcemanagement_canUpdateEquipment($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '',
                            resourcemanagement_Controller()->Equipment()->edit($record->id)
                        )->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT, 'icon')
                        ->setTitle(resourcemanagement_translate('Edit'))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                    );
                }

                if (resourcemanagement_canDeleteEquipment($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '', //resourcemanagement_translate('Delete'),
                            resourcemanagement_Controller()->Equipment()->checkDelete($record->id)
                        )->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon')//, 'widget-actionbutton')
//						->setConfirmationMessage(resourcemanagement_translate('Are you sure you want to delete this resource?'))
                    );
                }
                return $actionsBox;

        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class resourcemanagement_EquipmentFrame extends Widget_Frame
{

    public $buttons = null;

    /**
     * @var resourcemanagement_Equipment $resource The resource displayed in the frame.
     */
    protected $equipment = null;

    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);
    }


    /**
     *
     * @param resourcemanagement_Equipment	$equipment	The equipment to display.
     * @return resourcemanagement_EquipmentFrame
     */
    public function setEquipment(resourcemanagement_Equipment $equipment)
    {
        $this->equipment = $equipment;
        return $this;
    }



    public function addButtons()
    {
        $args = func_get_args();

        foreach ($args as $arg) {
            $this->buttons->addItem($arg);
        }

        return $this;
    }


    public function initialize()
    {
        $W = bab_Widgets();

        $equipment = $this->equipment;

        $descriptionSection = $W->Section(
            resourcemanagement_translate('Description'),
            $W->Html(bab_toHtml($equipment->description, BAB_HTML_ALL))
        );

        $secondBox = $W->VBoxItems(
            $this->images(),
            $this->scheduleSection()
                ->addClass('compact')
        );
        $secondBox->setVerticalSpacing(1, 'em');
        $secondBox->setSizePolicy('widget-33pc widget-bordered-left');

        $layout = $W->VBoxItems(
            $W->Title($equipment->name)
        );

        if ($equipment->unavailable) {
            $layout->addItem(
                $W->Label(resourcemanagement_translate('The equipment is temporarily unavailable.'))
                    ->addClass('icon', 'widget-bordered', 'widget-strong')
            );
        }

        $layout->addItem(
            $W->FlowItems(
                $mainBox,//->addClass('widget-bordered'),
                $secondBox//->addClass('widget-bordered')
            )->setVerticalAlign('top')
        );

        $this->setLayout($W->VBoxItems($layout));



        foreach ($types as $type) {
            $characteristicsSection->addItem(
                $this->typeSections($type)
            );
        }

        $this->buttons = $W->FlowItems();
        $this->buttons->setHorizontalSpacing(2, 'em')
            ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC, 'widget-toolbar');

        $mainBox->addItem($this->buttons);

        return $this;
    }



    protected function images()
    {
        $W = bab_Widgets();
        $imagesBox = $W->FlowLayout();
        $imagesBox->setVerticalAlign('top');


        $imagePathes = array();
        $imageFolder = $this->equipment->mainImagePath();
        foreach ($imageFolder as $imagePath) {
            $imagePathes[] = $imagePath;
        }

        $imageFolder = $this->equipment->imagePath();
        foreach ($imageFolder as $imagePath) {
            $imagePathes[] = $imagePath;
        }

        $sizePolicy = 'widget-100pc';
        $thumbnailSize = 400;
        foreach ($imagePathes as $imagePath) {
            if ($imagePath->isDir()) {
                continue;
            }
            $image = $W->ImageZoomerThumbnail($imagePath);
            $image->setThumbnailSize($thumbnailSize, $thumbnailSize);
            $image->setZoomSize(800, 600);
            $image->addClass('widget-100pc')
                ->setSizePolicy($sizePolicy);

            $sizePolicy = 'widget-50pc';
            $thumbnailSize = 200;

            $imagesBox->addItem($image);
        }

        return $imagesBox;
    }
}
