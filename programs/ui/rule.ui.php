<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/../functions.php';


bab_Widgets()->includePhpClass('Widget_Frame');





class resourcemanagement_RuleEditor extends Widget_Frame
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }

        parent::__construct($id, $layout);

        $this->setName('rule');

        $this->layout = $layout;

        $this->addFields();
    }



    protected function addFields()
    {
        $repeat = $this->repeat();

        $this->layout->addItem($repeat);
    }



    protected function daily()
    {
        $W = bab_Widgets();

        return $W->VBoxItems(
            $W->HBoxItems(
                $W->Label(resourcemanagement_translate('Repeat every')),
                $W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_d'),
                $W->Label(resourcemanagement_translate('days'))
            )->setHorizontalSpacing(.25, 'em')
        );
    }



    protected function weekly()
    {
        $W = bab_Widgets();

        return $W->VBoxItems(
            $W->HBoxItems(
                $W->Label(resourcemanagement_translate('Repeat every')),
                $W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_w'),
                $W->Label(resourcemanagement_translate('weeks'))
            )->setHorizontalSpacing(.25, 'em'),
            $W->HBoxItems(
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Sun')),
                    $W->CheckBox()->setName(array('repeat_wd','SU'))->setCheckedValue('SU')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Mon')),
                    $W->CheckBox()->setName(array('repeat_wd','MO'))->setCheckedValue('MO')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Tue')),
                    $W->CheckBox()->setName(array('repeat_wd','TU'))->setCheckedValue('TU')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Wen')),
                    $W->CheckBox()->setName(array('repeat_wd','WE'))->setCheckedValue('WE')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Thu')),
                    $W->CheckBox()->setName(array('repeat_wd','TH'))->setCheckedValue('TH')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Fri')),
                    $W->CheckBox()->setName(array('repeat_wd','FR'))->setCheckedValue('FR')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em'),
                $W->HBoxItems(
                    $tmpLbl = $W->Label(resourcemanagement_translate('Sat')),
                    $W->CheckBox()->setName(array('repeat_wd','SA'))->setCheckedValue('SA')->setAssociatedLabel($tmpLbl)
                )->setHorizontalSpacing(.25, 'em')
            )->setHorizontalSpacing(.5, 'em')
        );
    }



    protected function monthly()
    {
        $W = bab_Widgets();

        return $W->VBoxItems(
            $W->HBoxItems(
                $W->Label(resourcemanagement_translate('Repeat every')),
                $W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_m'),
                $W->Label(resourcemanagement_translate('months'))
            )->setHorizontalSpacing(.25, 'em')
        );
    }



    protected function yearly()
    {
        $W = bab_Widgets();

        return $W->VBoxItems(
            $W->HBoxItems(
                $W->Label(resourcemanagement_translate('Repeat every')),
                $W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_y'),
                $W->Label(resourcemanagement_translate('years'))
            )->setHorizontalSpacing(.25, 'em')

        );
    }


    protected function repeatEndDate()
    {
        $W = bab_Widgets();
        return $W->DatePicker()
            ->setName('repeat_end_date')
            ->setMandatory(true, resourcemanagement_translate('The end date of the recurence is mandatory.'));
    }


    protected function repeat()
    {
        $W = bab_Widgets();

        $daily = $this->daily();
        $weekly = $this->weekly();
        $monthly = $this->monthly();
        $yearly = $this->yearly();

        $repeatItem = $W->labelledWidget(
            resourcemanagement_translate('Repeat period ends on'),
            $this->repeatEndDate()
        );

        $frequencySelector = $W->Select();
        $frequencySelector->addOption('once', resourcemanagement_translate('Once'));
        $frequencySelector->addOption('daily', resourcemanagement_translate('Daily'));
        $frequencySelector->addOption('weekly', resourcemanagement_translate('Weekly'));
        $frequencySelector->addOption('monthly', resourcemanagement_translate('Monthly'));
        $frequencySelector->addOption('yearly', resourcemanagement_translate('Yearly'));

        $frequencySelector->setAssociatedDisplayable($daily, array('daily'));
        $frequencySelector->setAssociatedDisplayable($weekly, array('weekly'));
        $frequencySelector->setAssociatedDisplayable($monthly, array('monthly'));
        $frequencySelector->setAssociatedDisplayable($yearly, array('yearly'));
        $frequencySelector->setAssociatedDisplayable($repeatItem, array('daily', 'weekly', 'monthly', 'yearly'));

        return $W->VBoxItems(
            $frequencySelector
                ->setName('repeat_frequency')
                ->setValue('once'),
            $W->Items(
                $daily,
                $weekly,
                $monthly,
                $yearly
            ),
            $repeatItem
        )->setId('repeat-form')
        ->setVerticalSpacing(1, 'em');
    }
}
