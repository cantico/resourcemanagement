<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/reservation.class.php';
require_once dirname(__FILE__) . '/../set/resource.class.php';
require_once dirname(__FILE__) . '/../ui/resource.ui.php';


bab_Widgets()->includePhpClass('Widget_FullCalendar');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');

function resourcemanagement_calendarToolbarAll()
{
    $W = bab_Widgets();

    $toolbar = $W->FlowLayout();
    $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
    $toolbar->setHorizontalSpacing(1, 'ex');


    switch ($_SESSION['resourcemanagement']['calendarType']) {
        case resourcemanagement_CtrlCalendar::TYPE_MONTH:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_WEEK:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_WORKWEEK:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_DAY:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_MONTH)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_WEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY:

            $switchViewButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
            resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_DAY)
            )->addClass('bab_toolbarItem');
            break;
    }

    switch ($_SESSION['resourcemanagement']['calendarType']) {
        case resourcemanagement_CtrlCalendar::TYPE_MONTH:
        case resourcemanagement_CtrlCalendar::TYPE_WEEK:
        case resourcemanagement_CtrlCalendar::TYPE_WORKWEEK:
        case resourcemanagement_CtrlCalendar::TYPE_DAY:
            $monthViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Month'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_MONTH)
            )->addClass('bab_toolbarItem');

            $weekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_WEEK)
            )->addClass('bab_toolbarItem');

            $workweekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Work week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK)
            )->addClass('bab_toolbarItem');

            $dayViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Day'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_DAY)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY:
        default:

            $monthViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Month'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH)
            )->addClass('bab_toolbarItem');

            $weekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK)
            )->addClass('bab_toolbarItem');

            $workweekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Work week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK)
            )->addClass('bab_toolbarItem');

            $dayViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Day'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY),
                resourcemanagement_Controller()->Calendar()->displayAll(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY)
            )->addClass('bab_toolbarItem');
            break;
    }


//     if (isset($_SESSION['resourcemanagement']['calendarLayout']) && $_SESSION['resourcemanagement']['calendarLayout'] === 'horizontal') {
//         $layoutButton = $W->Link(
//             $W->Icon(resourcemanagement_translate('Vertical layout'), Func_Icons::ACTIONS_ZOOM_FIT_HEIGHT),
//             resourcemanagement_Controller()->Calendar()->toggleCalendarMode()
//         );
//     } else {
//         $layoutButton = $W->Link(
//             $W->Icon(resourcemanagement_translate('Horizontal layout'), Func_Icons::ACTIONS_ZOOM_FIT_WIDTH),
//             resourcemanagement_Controller()->Calendar()->toggleCalendarMode()
//         );
//     }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/resourcemanagement');
    $start = $registry->getValue('start', '');
    $end = $registry->getValue('end', '');
    $fullDayLayout = null;
    if(($start && $start != '00:00') ||($end && $end != '24:00')){
        if(!isset($_SESSION['resmanagement-fullday']) || $_SESSION['resmanagement-fullday'] == 0){
            $fullDayLayout = $W->Link(
                $W->Icon(resourcemanagement_translate('View full day'), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
                resourcemanagement_Controller()->Calendar()->toggleFullDayView()
                );
        } else {
            $fullDayLayout = $W->Link(
                $W->Icon(resourcemanagement_translate('View restrected'), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
                resourcemanagement_Controller()->Calendar()->toggleFullDayView()
                );
        }
    }

    $printButton = $W->Link(
        $W->Icon(resourcemanagement_translate('Print the calendar'), Func_Icons::ACTIONS_DOCUMENT_PRINT),
        resourcemanagement_Controller()->Calendar()->displayPrint()
        )->addAttribute('target', '_blank');


        //		->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleFullwidth(), 'calendar');
//         $layoutButton->addClass('bab_toolbarItem'); //, 'resourcemanagement-calendar-fullwidth-toogle');
        //	$fullwidthViewButton->setTitle(resourcemanagement_translate('Use full page width to display calendar, click again to return to smaller view.'));


        // 	$workWeekViewIcon = $W->Icon(resourcemanagement_translate('Work week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK);
        // 	$workWeekViewButton = $W->Link($workWeekViewIcon, resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK));


        $toolbar->addItem($W->FlowItems($monthViewButton, $weekViewButton, $workweekViewButton, $dayViewButton, $fullDayLayout));

        // 	$toolbar->addItem($monthViewButton);
        // 	$toolbar->addItem($weekViewButton);
        // 	$toolbar->addItem($workweekViewButton);
        // 	$toolbar->addItem($dayViewButton);

        $toolbar->addItem($W->Label(''));

//         $toolbar->addItem($switchViewButton);

//         $toolbar->addItem($W->Label(''));

//        $toolbar->addItem($layoutButton);

        $toolbar->addItem($W->Label(''));

        $toolbar->addItem($printButton);

        // 	$toolbar->addItem(
        // 		$W->Html(
        // 			'<script type="text/javascript">
        // 				jQuery(".resourcemanagement-calendar-fullwidth-toogle").click(function() {
            // 					jQuery("#resourcemanagement-calendar").toggleClass("fullwidth");
            // 					return false;
            // 				})
        // 			</script>'
            // 		)
        // 	);


        return $toolbar;
}


function resourcemanagement_calendarToolbar()
{
    $W = bab_Widgets();

    $toolbar = $W->FlowLayout();
    $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
    $toolbar->setHorizontalSpacing(1, 'ex');


    switch ($_SESSION['resourcemanagement']['calendarType']) {
        case resourcemanagement_CtrlCalendar::TYPE_MONTH:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_WEEK:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_WORKWEEK:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_DAY:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_MONTH)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY:

            $switchViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Standard view'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_DAY)
            )->addClass('bab_toolbarItem');
            break;
    }

    switch ($_SESSION['resourcemanagement']['calendarType']) {
        case resourcemanagement_CtrlCalendar::TYPE_MONTH:
        case resourcemanagement_CtrlCalendar::TYPE_WEEK:
        case resourcemanagement_CtrlCalendar::TYPE_WORKWEEK:
        case resourcemanagement_CtrlCalendar::TYPE_DAY:
            $monthViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Month'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_MONTH)
            )->addClass('bab_toolbarItem');

            $weekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WEEK)
            )->addClass('bab_toolbarItem');

            $workweekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Work week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK)
            )->addClass('bab_toolbarItem');

            $dayViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Day'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_DAY)
            )->addClass('bab_toolbarItem');
            break;

        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK:
        case resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY:
        default:

            $monthViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Month'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH)
            )->addClass('bab_toolbarItem');

            $weekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK)
            )->addClass('bab_toolbarItem');

            $workweekViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Work week'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WORKWEEK)
            )->addClass('bab_toolbarItem');

            $dayViewButton = $W->Link(
                $W->Icon(resourcemanagement_translate('Day'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY),
                resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY)
            )->addClass('bab_toolbarItem');
            break;
    }


    if (isset($_SESSION['resourcemanagement']['calendarLayout']) && $_SESSION['resourcemanagement']['calendarLayout'] === 'horizontal') {
        $layoutButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Vertical layout'), Func_Icons::ACTIONS_ZOOM_FIT_HEIGHT),
            resourcemanagement_Controller()->Calendar()->toggleCalendarMode()
        );
    } else {
        $layoutButton = $W->Link(
            $W->Icon(resourcemanagement_translate('Horizontal layout'), Func_Icons::ACTIONS_ZOOM_FIT_WIDTH),
            resourcemanagement_Controller()->Calendar()->toggleCalendarMode()
        );

    }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/resourcemanagement');
    $start = $registry->getValue('start', '');
    $end = $registry->getValue('end', '');
    $fullDayLayout = null;
    if(($start && $start != '00:00') ||($end && $end != '24:00')){
        if(!isset($_SESSION['resmanagement-fullday']) || $_SESSION['resmanagement-fullday'] == 0){
            $fullDayLayout = $W->Link(
                $W->Icon(resourcemanagement_translate('View full day'), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
                resourcemanagement_Controller()->Calendar()->toggleFullDayView()
            );
        } else {
            $fullDayLayout = $W->Link(
                $W->Icon(resourcemanagement_translate('View restrected'), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
                resourcemanagement_Controller()->Calendar()->toggleFullDayView()
            );
        }
    }

    $printButton = $W->Link(
        $W->Icon(resourcemanagement_translate('Print the calendar'), Func_Icons::ACTIONS_DOCUMENT_PRINT),
        resourcemanagement_Controller()->Calendar()->displayPrint()
    )->addAttribute('target', '_blank');


//		->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleFullwidth(), 'calendar');
    $layoutButton->addClass('bab_toolbarItem'); //, 'resourcemanagement-calendar-fullwidth-toogle');
//	$fullwidthViewButton->setTitle(resourcemanagement_translate('Use full page width to display calendar, click again to return to smaller view.'));


// 	$workWeekViewIcon = $W->Icon(resourcemanagement_translate('Work week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK);
// 	$workWeekViewButton = $W->Link($workWeekViewIcon, resourcemanagement_Controller()->Calendar()->display(resourcemanagement_CtrlCalendar::TYPE_WORKWEEK));


    $toolbar->addItem($W->FlowItems($monthViewButton, $weekViewButton, $workweekViewButton, $dayViewButton, $fullDayLayout));

// 	$toolbar->addItem($monthViewButton);
// 	$toolbar->addItem($weekViewButton);
// 	$toolbar->addItem($workweekViewButton);
// 	$toolbar->addItem($dayViewButton);

    $toolbar->addItem($W->Label(''));

    $toolbar->addItem($switchViewButton);

    $toolbar->addItem($W->Label(''));

    $toolbar->addItem($layoutButton);

    $toolbar->addItem($W->Label(''));

    $toolbar->addItem($printButton);

// 	$toolbar->addItem(
// 		$W->Html(
// 			'<script type="text/javascript">
// 				jQuery(".resourcemanagement-calendar-fullwidth-toogle").click(function() {
// 					jQuery("#resourcemanagement-calendar").toggleClass("fullwidth");
// 					return false;
// 				})
// 			</script>'
// 		)
// 	);


    return $toolbar;
}




/**
 *
 * @param string $type			One of Widget_FullCalendar::VIEW_xxx.
 * @param string $showDate		Iso date.
 *
 * @return Widget_FullCalendar
 */
function resourcemanagement_calendarView($type = 'week', $showDate = null)
{
    $W = bab_Widgets();


    $calendar = $W->FullCalendar('calendar');
    $calendar->setFirstDayOfWeek(1)
        ->setSizePolicy(Widget_SizePolicy::MAXIMUM);


    if(!isset($_SESSION['resmanagement-fullday']) || $_SESSION['resmanagement-fullday'] == 0){
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');
        $start = $registry->getValue('start', '');
        $end = $registry->getValue('end', '');

        if($start){
            $calendar->setStartTime($start);
        }
        if($end){
            $calendar->setEndTime($end);
        }
    }

    $calendar->onViewDisplayed(resourcemanagement_Controller()->Calendar()->setDate());

    if (isset($showDate)) {
        $showDate = BAB_DateTime::fromIsoDateTime($showDate . ' 00:00:00');
        $calendar->setDate($showDate);
    }


    switch ($type) {
        case 'resourceDay'://resourcemanagement_CtrlCalendar::TYPE_RESOURCE_DAY:
            $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_DAY);
            break;
        case 'resourceNextWeeks'://resourcemanagement_CtrlCalendar::TYPE_RESOURCE_NEXTWEEKS:
            $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_NEXTWEEKS);
            break;
        case 'resourceMonth'://resourcemanagement_CtrlCalendar::TYPE_RESOURCE_MONTH:
            $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_MONTH);
            break;
        case 'resourceWeek'://resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:
            $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_WEEK);
            break;
        case 'resourceWorkweek'://resourcemanagement_CtrlCalendar::TYPE_RESOURCE_WEEK:
            $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_WORKWEEK);
            break;
        case 'day'://resourcemanagement_CtrlCalendar::TYPE_DAY:
            $calendar->setView(Widget_FullCalendar::VIEW_DAY);
            break;
        case 'week'://resourcemanagement_CtrlCalendar::TYPE_WEEK:
            $calendar->setView(Widget_FullCalendar::VIEW_WEEK);
            break;
        case 'workweek'://resourcemanagement_CtrlCalendar::TYPE_WORKWEEK:
            $calendar->setView(Widget_FullCalendar::VIEW_WORKWEEK);
            break;
        case 'month'://resourcemanagement_CtrlCalendar::TYPE_MONTH:
        default:
            $calendar->setView(Widget_FullCalendar::VIEW_MONTH);
            break;
    }


    $calendar->setMetadata('showAddEventButton', true);

//	$calendar->setMetadata('headerRight', 'resourceDay resourceWeek resourceMonth');


    $calendar->fetchPeriods(resourcemanagement_Controller()->Calendar()->events());

    $calendar->onDoubleClick(resourcemanagement_Controller()->Reservation()->add());

    $calendar->onPeriodMoved(resourcemanagement_Controller()->Reservation()->move());

    $calendar->onPeriodDoubleClick(resourcemanagement_Controller()->Reservation()->display());

//	$calendar->onPeriodClick(resourcemanagement_Controller()->Reservation()->display());


    return $calendar;
}


function workspace_date($date, $justDate = 0)
{
    if ($date === '' || $date === null) {
        return '';
    }
    if (strlen($date) === 6 && is_numeric($date)) {
        preg_match_all('/\d{2}/', $date, $dateArray);
        return '20' . $dateArray[0][2] . '-' . $dateArray[0][1] .'-' . $dateArray[0][0];
    }
    $dateTime = explode(' ', $date);
    $dateTab = explode('-', $dateTime[0]);
    if (isset($dateTime[1]) && $justDate != 1 ){
        $time = explode(':', $dateTime[1]);
        if ($justDate == 2) {
            return $time[0] . 'h' . $time[1];
        }
        return $dateTab[2] .'-' . $dateTab[1] . '-' . $dateTab[0] . ' ' . $time[0] . 'h' . $time[1];
    } else {
        return $dateTab[2] . '-' . $dateTab[1] . '-' . $dateTab[0];
    }
}


