<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/domain.class.php';
require_once dirname(__FILE__) . '/../set/resource.class.php';
require_once dirname(__FILE__) . '/../set/type.class.php';
require_once dirname(__FILE__) . '/../set/typefield.class.php';
require_once dirname(__FILE__) . '/../set/resourcetype.class.php';


bab_Widgets()->includePhpClass('Widget_TableModelView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');




class resourcemanagement_TypeEditor extends Widget_Form
{

    public $descriptionFieldTableView;
    public $reservationFieldTableView;

    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout) {
            $layout = $W->VBoxLayout()
                ->setVerticalSpacing(1, 'em');
        }

        parent::__construct($id, $layout);

        $this->setName('type');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->addItem(
            $W->FlowItems(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Type()->save())
                    ->setSuccessAction(resourcemanagement_Controller()->Type()->displayList())
                    ->setFailedAction(resourcemanagement_Controller()->Type()->edit())
                    ->setLabel(resourcemanagement_translate('Save')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Type()->displayList()
                )->addClass('widget-actionbutton')
            )->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('middle')
        );

    }


    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->FlowItems(
            $W->Label($labelText)
                ->setSizePolicy('widget-20em')
                ->colon(),
            $item
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        $this->descriptionFieldTableView = $W->DelayedItem(
            resourcemanagement_Controller()->Type()->descriptionFieldTableView()
        );
        $this->reservationFieldTableView = $W->DelayedItem(
            resourcemanagement_Controller()->Type()->reservationFieldTableView()
        );

        $descriptionSection = $W->Section(
            resourcemanagement_translate('Description fields'),
            $W->VBoxItems(
                $this->descriptionFieldTableView
            ),
            3
        );

        $reservationSection = $W->Section(
            resourcemanagement_translate('Reservation fields'),
            $W->VBoxItems(
                $this->reservationFieldTableView
            ),
            3
        );


        $nameItem = $W->LineEdit();
        $nameItem->setSizePolicy('widget-30em');
        $nameItem->addClass('widget-100pc');
        $nameItem->setName('name');

        $this->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Resource type name'),
                $nameItem
            )
        );

        $this->addItem($descriptionSection);
        $this->addItem($reservationSection);

    }



    public function setType(resourcemanagement_Type $type)
    {
        $this->setValues($type->getValues(), array('type'));

        $this->setHiddenValue('type[id]', $type->id);

        $this->descriptionFieldTableView->setDelayedAction(
            resourcemanagement_Controller()->Type()->descriptionFieldTableView($type->id)
        );

        $this->reservationFieldTableView->setDelayedAction(
            resourcemanagement_Controller()->Type()->reservationFieldTableView($type->id)
        );
    }


}





class resourcemanagement_TypeTableView extends widget_TableModelView
{

    public function addDefaultColumns(resourcemanagement_TypeSet $set)
    {
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addColumn(
            widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
                ->setSortable(true)
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-col-10em widget-align-center')
        );
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(resourcemanagement_Type $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'name':
                return $W->Link(
                    $record->name,
                    resourcemanagement_Controller()->Resource()->displayList(array('filter' => array('type' => $record->id)))
                );

            case '_actions_':
                $actionsBox = $W->FlowLayout();
                $actionsBox->setSpacing(4, 'px');

                if (resourcemanagement_canUpdateType($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            resourcemanagement_translate('Edit'),
                            resourcemanagement_Controller()->Type()->edit($record->id)
                        )->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT, 'icon', 'widget-actionbutton')
                    );

                }
                if (resourcemanagement_canDeleteType($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '',
                            resourcemanagement_Controller()->Type()->delete($record->id)
                        )
                        ->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon', 'widget-actionbutton')
                        ->setConfirmationMessage(resourcemanagement_translate('Are you sure you want to delete this resource type?'))

                    );
                }

                return $actionsBox;
        }

        return parent::computeCellContent($record, $fieldPath);
    }



}









class resourcemanagement_TypeFieldTableView extends widget_TableModelView
{


    public function addDefaultColumns(resourcemanagement_TypeFieldSet $set)
    {

        $this->addColumn(
            widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
                ->addClass('widget-col-30em')
        );

        $this->addColumn(
            widget_TableModelViewColumn($set->fieldType, resourcemanagement_translate('Field type'))
                ->addClass('widget-col-10em')
        );

        $this->addColumn(
            widget_TableModelViewColumn($set->visibleInDescription, resourcemanagement_translate('Visible in description'))
                ->addClass('widget-align-center', 'widget-col-10em')
        );

        $this->addColumn(
            widget_TableModelViewColumn($set->visibleOnPlanning, resourcemanagement_translate('Visible on planning'))
                ->addClass('widget-align-center', 'widget-col-10em')
        );

        $this->addColumn(
            widget_TableModelViewColumn($set->mandatory, resourcemanagement_translate('Mandatory'))
            ->addClass('widget-align-center', 'widget-col-10em')
        );
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @param int			$row
     * @param int			$col
     * @return bool
     */
    protected function handleCell(resourcemanagement_TypeField $record, $fieldPath, $row, $col)
    {
        $W = bab_Widgets();


        switch ($fieldPath) {

            case 'name':

                $this->addItem(
                    $W->LineEdit()
                        ->setName(array('fields', $record->id, 'name'))
                        ->setValue($record->name)
                        ->addClass('widget-fullwidth'),
                    $row, $col
                );
                return true;

            case 'fieldType':

                $select = $W->Select();
                foreach (resourcemanagement_TypeField::$fieldTypes as $fieldType => $fieldTypeName) {
                    $select->addOption($fieldType, resourcemanagement_translate($fieldTypeName));
                }
                $select->setName(array('fields', $record->id, 'fieldType'));
                $parametersItem = $W->LineEdit();
                $parametersItem->setName(array('fields', $record->id, 'fieldTypeParameters'));		$parametersItem->setTitle(resourcemanagement_translate('A list of values separated by "|". Optionally add "&" before the default value. For example: Value 1|&Value 2|Value 3'));

                $this->addItem(
                    $W->VBoxItems(
                        $select,
                        $parametersItem
                    ),
                    $row, $col
                );
                $select->setValue($record->fieldType);
                $parametersItem->setValue($record->fieldTypeParameters);


                $select->setAssociatedDisplayable($parametersItem, array('enum'));
                return true;

            case 'visibleInDescription':
                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'visibleInDescription'))
                        ->setValue($record->visibleInDescription),
                    $row, $col
                );
                return true;


            case 'visibleOnPlanning':
                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'visibleOnPlanning'))
                        ->setValue($record->visibleOnPlanning),
                    $row, $col
                );
                return true;


            case 'mandatory':

                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'mandatory'))
                        ->setValue($record->mandatory),
                    $row, $col
                );
                return true;

            case 'exportable':
                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'exportable'))
                        ->setValue($record->exportable),
                    $row, $col
                );
                return true;

            case 'exportFilter':
                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'exportFilter'))
                        ->setValue($record->exportFilter),
                    $row, $col
                );
                return true;

            case 'availabilityFilter':
                $this->addItem(
                    $W->CheckBox()
                        ->setName(array('fields', $record->id, 'availabilityFilter'))
                        ->setValue($record->availabilityFilter),
                    $row, $col
                );
                return true;

            case '_actions_':
                $deleteButton = $W->Link(
                    resourcemanagement_translate('Delete'),
                    resourcemanagement_Controller()->Type()->deleteField($record->id)
                );

                $deleteButton->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon', 'widget-actionbutton');
                $deleteButton->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                $deleteButton->setConfirmationMessage(resourcemanagement_translate('Do you really want to delete this field?'));

                $deleteButton->setAjaxAction(resourcemanagement_Controller()->Type()->deleteField($record->id), $deleteButton);
                $this->addItem(
                    $deleteButton,
                    $row, $col
                );
                return true;
        }

        return parent::handleCell($record, $fieldPath, $row, $col);
    }

}






class resourcemanagement_OrderFieldEditor extends Widget_Form
{

    public $saveButton;

    private $ressource = null;
    private $nature = null;

    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct($ressource, $nature)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()
        ->setVerticalSpacing(1, 'em');

        parent::__construct(null, $layout);

        $this->ressource = $ressource;
        $this->nature = $nature;

        $this->setName('fields');

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addFields();

        $this->saveButton = $W->SubmitButton();

        $this->addItem(
            $this->saveButton
                ->setLabel(resourcemanagement_translate('Save order'))
        );

    }

    /**
     * @param Widget_Action $action
     * @param mixed   $reloadItem
     * @return Widget_SubmitButton
     */
    public function setAjaxAction(Widget_Action $action, $reloadItem = null)
    {
        $this->saveButton->setAjaxAction($action, $reloadItem);
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        $this->addItem($W->Label(resourcemanagement_translate('Drag and drop fields to reorder them')));

        $set = new resourcemanagement_TypeFieldSet();

        $fields = $set->select(
            $set->nature->is($this->nature)
            ->_AND_($set->type->is($this->ressource))
        );
        $fields->orderDesc($set->order);
        $fields->orderAsc($set->id);

        $sortLayout = $W->VBoxItems()->addClass('resourcemanagement-sort-field')->sortable();
        foreach ($fields as $field) {
            $sortLayout->addItem(
                $W->FlowItems(
                    $W->Hidden()->setValue($field->id)->setName($field->id),
                    $W->Icon($field->name, Func_Icons::PLACES_FOLDER)
                )
            );
        }

        $this->addItem($sortLayout);
    }


}



class resourcemanagement_TypeFieldEditor extends Widget_Form
{

    public $saveButton;

    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout) {
            $layout = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'em');
        }

        parent::__construct($id, $layout);

        $this->setName('field');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->saveButton = $W->SubmitButton();

        $this->addItem(
            $this->saveButton
//				->setAction(resourcemanagement_Controller()->Type()->saveField())

//				->setFailedAction(resourcemanagement_Controller()->Type()->edit())
//				->setFailedAction(resourcemanagement_Controller()->Type()->edit())
                ->setLabel(resourcemanagement_translate('Save'))
        );

    }

    /**
     * @param Widget_Action $action
     * @param mixed   $reloadItem
     * @return Widget_SubmitButton
     */
    public function setAjaxAction(Widget_Action $action, $reloadItem = null)
    {
        $this->saveButton->setAjaxAction($action, $reloadItem);
    }


    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->FlowItems(
                $W->Label($labelText)
                ->setSizePolicy('widget-20em')
                ->colon(),
                $item
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();


        $nameItem = $W->LineEdit();
        $nameItem->setSizePolicy('widget-30em');
        $nameItem->addClass('widget-100pc');
        $nameItem->setName('name');

        $select = $W->Select();
        $select->setName('fieldType');
        foreach (resourcemanagement_TypeField::$fieldTypes as $fieldType => $fieldTypeName) {
            $select->addOption($fieldType, resourcemanagement_translate($fieldTypeName));
        }




        $parametersItem = $W->LineEdit();
        $parametersItem->setSizePolicy('widget-30em');
        $parametersItem->addClass('widget-100pc');
        $parametersItem->setName('fieldTypeParameters');
        $parametersItem->setTitle(resourcemanagement_translate('A list of values separated by "|". Optionally add "&" before the default value. For example: Value 1|&Value 2|Value 3'));


        $this->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Name'),
                $nameItem
            )
        );

        $this->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Type'),
                $W->VboxItems(
                    $select
                )
            )
        );

        $this->addItem(
            $enumParameters = $this->LabelledWidget(
                resourcemanagement_translate('List values'),
                $parametersItem
            )
        );

        $select->setAssociatedDisplayable($enumParameters, array('enum'));


    }



    public function setTypeField(resourcemanagement_TypeField $typeField)
    {
        $this->setValues($typeField->getValues(), array('field'));

        $this->setHiddenValue('field[id]', $typeField->id);

        $this->saveButton->setSuccessAction(resourcemanagement_Controller()->Type()->edit($typeField->type));

    }


}
