<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/domain.class.php';
require_once dirname(__FILE__) . '/../set/resource.class.php';
require_once dirname(__FILE__) . '/../set/type.class.php';
require_once dirname(__FILE__) . '/../set/typefield.class.php';
require_once dirname(__FILE__) . '/../set/resourcetype.class.php';
require_once dirname(__FILE__) . '/../set/resourceavailability.class.php';
require_once dirname(__FILE__) . '/../ui/domain.ui.php';


bab_Widgets()->includePhpClass('Widget_TableModelView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');




class resourcemanagement_ResourceEditor extends Widget_Form
{

    public $imagesFormItem = null;

    public $typeSpecificBox = null;

    public $editorUID;

    /**
     *
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct()
    {
        $id = 'rm-resourceEditor';
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'em');

        parent::__construct($id, $layout);

        $this->editorUID = uniqid();

        $this->setName('resource');


        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $successAction = Widget_Action::fromUrl($_SERVER['HTTP_REFERER']);
        } else {
            $successAction = resourcemanagement_Controller()->Resource()->displayList();
        }

        $this->addItem(
            $W->FlowItems(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Resource()->save())
                    ->setAjaxAction()
                    ->setSuccessAction($successAction)
                    ->setFailedAction(resourcemanagement_Controller()->Resource()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Save')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Resource()->displayList()
                )->addClass('widget-actionbutton', 'widget-close-dialog')
            )->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('middle')
        );



    }


    /**
     * @param string						$labelText
     * @param Widget_Displayable_Interface	$item
     * @param string						$description
     *
     * @return Widget_FlowLayout
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $description = null)
    {
        $W = bab_Widgets();

        if (isset($description)) {
            $item->setTitle($description);
        }

        $label = $W->Label($labelText);
        $label->setAssociatedWidget($item);

        return $W->FlowItems(
            $label->colon()
                ->setSizePolicy('resourcemanagement-field-label-container'),
            $item->setSizePolicy('resourcemanagement-field-widget-container')
        )->setVerticalAlign('top');
    }




    protected function addFields()
    {
        $W = bab_Widgets();

        $generalSection = $W->Section(
            resourcemanagement_translate('General information'),
            $W->VBoxItems()
                ->setVerticalSpacing(1, 'em'),
            3
        );

        $this->addItem($generalSection);

        $this->typeSpecificBox = $W->VBoxItems();
        $this->addItem($this->typeSpecificBox);

        $typesItem = $W->Multiselect();
        $typesItem->setSelectedList(4);
        $typesItem->setName('types');
        $typesItem->setSizePolicy('widget-30em');
        $typesItem->addClass('widget-100pc');

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select();
        foreach ($types as $type) {
            $typesItem->addOption($type->id, $type->name);
        }


        $typesItem->setAjaxAction(resourcemanagement_Controller()->Resource()->setResourceEditorTypes($this->editorUID), 'types-section', 'change');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Types'),
                $typesItem
            )
        );

        $nameItem = $W->LineEdit();
        $nameItem->setSizePolicy('widget-30em');
        $nameItem->addClass('widget-100pc');
        $nameItem->setName('name');
        $nameItem->setMandatory(true, resourcemanagement_translate('You specify an name.'));

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Name'),
                $nameItem
            )
        );

        $descriptionItem = $W->TextEdit();
        $descriptionItem->setName('description');
        $descriptionItem->setSizePolicy('widget-50em');
        $descriptionItem->addClass('widget-100pc');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Description'),
                $descriptionItem
            )
        );

        $surbookingItem = $W->CheckBox();
        $surbookingItem->setName('surbooking');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Allow surbooking'),
                $surbookingItem,
                resourcemanagement_translate('If surbooking is disabled all curently surbooked event will stay.')
            )
        );

// 		$domainItem = $W->Select();
// 		$domainItem->setName('domain');
// 		$domainSet = new resourcemanagement_DomainSet();
// 		$domains = $domainSet->select($domainSet->isManaged());
// 		$domainItem->addOption('', '');
// 		foreach ($domains as $domain) {
// 			$domainItem->addOption($domain->id, $domain->name);
// 		}

        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isManaged());

        $domainItem = resourcemanagement_DomainSelect($domains);
         $domainItem->setName('domain');
        $domainItem->setMandatory(true, resourcemanagement_translate('You must select a domain.'));

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Domain'),
                $domainItem
            )
        );

        $reservationTypeItem = $W->Select();
        $reservationTypeItem->setName('defaultReservationType');
        $reservationTypeSet = new resourcemanagement_ReservationTypeSet();
        $reservationTypes = $reservationTypeSet->select();
        $reservationTypeItem->addOption('', '');
        foreach ($reservationTypes as $reservationType) {
            $reservationTypeItem->addOption($reservationType->id, $reservationType->name);
        }

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Default reservation type'),
                $reservationTypeItem
            )
        );

        $allowConditionalBookingItem = $W->CheckBox();
        $allowConditionalBookingItem->setName('allowConditionalBooking');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Allow conditional booking'),
                $allowConditionalBookingItem
            )
        );



        $confirmationDelayBox = $W->VBoxItems();
        $confirmationDelayBox->setVerticalSpacing(1, 'em');


        $confirmationDelayMinItem = $W->LineEdit();
        $confirmationDelayMinItem->setSize(3);
        $confirmationDelayMinItem->setName('confirmationDelayMin');

        $confirmationDelayBox->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Min number'),
                $confirmationDelayMinItem,
                resourcemanagement_translate('Confirmation must take place at least this number of days before the reservation start.')
            )
        );


        $confirmationDelayMaxItem = $W->LineEdit();
        $confirmationDelayMaxItem->setSize(3);
        $confirmationDelayMaxItem->setName('confirmationDelayMax');

        $confirmationDelayBox->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Max number'),
                $confirmationDelayMaxItem,
                    resourcemanagement_translate('Confirmation must take place at most this number of days after the reservation is done.')
            )
        );

        $generalSection->addItem($confirmationDelayBox);

        $allowConditionalBookingItem->setAssociatedDisplayable($confirmationDelayBox, array(1));


        require_once $GLOBALS['babInstallPath'] . 'utilit/wfincl.php';
        $approbationItem = $W->Select();
        $approbationItem->setName('approbation');
        $approbations = bab_WFGetApprobationsList();
        $approbationItem->addOption('', '');
        foreach ($approbations as $approbation) {
            $approbationItem->addOption($approbation['id'], $approbation['name']);
        }


        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Approbation workflow'),
                $approbationItem
            )
        );



        $reservationDelayBox = $W->VBoxItems();
        $reservationDelayBox->setVerticalSpacing(1, 'em');

        $reservationDelayMinItem = $W->LineEdit();
        $reservationDelayMinItem->setSize(3);
        $reservationDelayMinItem->setName('reservationDelayMin');

        $reservationDelayBox->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Minimum duration before reservation start'),
                $reservationDelayMinItem,
                resourcemanagement_translate('Duration before reservation start after which reservation/modification is not allowed.')
            )
        );

        $reservationDelayMaxItem = $W->LineEdit();
        $reservationDelayMaxItem->setSize(3);
        $reservationDelayMaxItem->setName('reservationDelayMax');

        $reservationDelayBox->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Maximum duration before reservation start'),
                $reservationDelayMaxItem,
                resourcemanagement_translate('Duration before reservation start before which reservation/modification is not allowed.')
            )
        );


        $generalSection->addItem($reservationDelayBox);



        $this->mainImageFormItem = $W->ImagePicker()
            ->setDimensions(128, 128)
            ->oneFileMode(true);

        $this->mainImageFormItem->setName('mainphoto');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Main image'),
                $this->mainImageFormItem
            )
        );

        $this->imagesFormItem = $W->ImagePicker()
            ->setDimensions(128, 128);

        $this->imagesFormItem->setName('photo');

        $generalSection->addItem(
            $this->LabelledWidget(
                resourcemanagement_translate('Image gallery'),
                $this->imagesFormItem
            )
        );

    }





    public function typeSection(resourcemanagement_Type $type)
    {
        $W = bab_Widgets();

        $typeSection = $W->Section(
            $type->name,
            $W->VBoxItems()
                ->setVerticalSpacing(1, 'em'),
            3
        );

//		$typeSection->addClass('compact');

        $typeSection->setName('type' . $type->id);

        $typeFieldSet = new resourcemanagement_TypeFieldSet();
        $typeFields = $typeFieldSet->select(
            $typeFieldSet->type->is($type->id)
                ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION))
        );
        $typeFields->orderDesc($typeFieldSet->order);
        $typeFields->orderAsc($typeFieldSet->id);

        /* @var $typeField resourcemanagement_TypeField  */
        foreach ($typeFields as $typeField) {
            $input = $typeField->getInputWidget();
            if ($typeField->mandatory) {
                $input->setMandatory(true, sprintf(resourcemanagement_translate('The field "%s" is mantory.'), $typeField->name));
            }
            $item = $this->LabelledWidget(
                $typeField->name,
                $input
            );
            $typeSection->addItem($item);
        }

        return $typeSection;
    }





    /**
     *
     * @param multitype:resourcemanagement_Type $types
     * @return Widget_VBoxLayout
     */
    public function dynamicSectionsFrame($types)
    {
        $W = bab_Widgets();
        $box = $W->Frame('types-section', $W->VBoxItems()->setVerticalSpacing(1, 'em'));

        //$box->setName('resource');

        foreach ($types as $type) {
            $typeSection = $this->typeSection($type);

            $box->addItem($typeSection);
        }

        $box->setReloadAction(resourcemanagement_Controller()->Resource()->dynamicEditorFrame($this->editorUID));

        return $box;
    }





    public function setResource(resourcemanagement_Resource $resource = null)
    {


        if (isset($resource)) {
            $resource->extend();
            $types = $resource->getTypes();

            $this->setHiddenValue('resource[id]', $resource->id);
            $this->setId('rm-resourceEditor-'.$resource->id);

            $mainImagePath = $resource->mainImagePath();
            if (!is_dir($mainImagePath->toString())) {
                $mainImagePath->createDir();
            }
//			$this->mainImageFormItem->setFolder($mainImagePath);
            $this->mainImageFormItem->importPath($mainImagePath, 'UTF-8');

            $imagePath = $resource->imagePath();
            if (!is_dir($imagePath->toString())) {
                $imagePath->createDir();
            }
//			$this->imagesFormItem->setFolder($imagePath);
            $this->imagesFormItem->importPath($imagePath, 'UTF-8');
        } else {
            $types = array();
            $this->imagesFormItem->cleanup();
            $this->mainImageFormItem->cleanup();
        }
        //$this->dynamicSectionsFrame->setPlaceHolderItem($this->dynamicSectionsFrame($types));
        $this->typeSpecificBox->addItem($this->dynamicSectionsFrame($types));

        if (isset($resource)) {
            $this->setValues($resource->getValues(), array('resource'));
        }
        $typeIds = array_keys($types);
        $this->setValues($typeIds,  array('resource', 'types'));

        $_SESSION['resourcemanagement']['editor'][$this->editorUID]['types'] = $typeIds;
    }


}





class resourcemanagement_ResourceTableView extends widget_TableModelView
{

    /**
     * (non-PHPdoc)
     * @see widget_TableModelView::addDefaultColumns($set)
     * @param resourcemanagement_ResourceSet    ORM_RecordSet $set
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addColumn(
            widget_TableModelViewColumn('image', '')->addClass('widget-column-thin')
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->domain, resourcemanagement_translate('Domain'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->description, resourcemanagement_translate('Description'))
        );
        $this->addColumn(
            widget_TableModelViewColumn('types', resourcemanagement_translate('Types'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->allowConditionalBooking, resourcemanagement_translate('Conditional booking'))
                ->addClass('widget-col-8em widget-align-center')
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-col-15em widget-align-center')
        );
    }



    /**
     * @param resourcemanagement_Resource	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'image':
                $imagePath = $record->getImage();
                if ($imagePath) {
                    $image = $W->ImageThumbnail($imagePath);
                    $image->setThumbnailSize(32, 32);
                    $image->setResizeMode(Func_Thumbnailer::CROP_CENTER);
                    $image->createThumbnail();
                    return $W->Link(
                        $image,
                        resourcemanagement_Controller()->Resource()->display($record->id)
                    )->setOpenMode(Widget_Link::OPEN_DIALOG);
                }
                return $W->Label('');


            case 'types':
                $types = $record->getTypes();
                $separator = '';
                $typestext = '';
                foreach ($types as $type) {
                    $typestext .= $separator . $type->name;
                    $separator = ', ';
                }
                return $W->Label($typestext);


            case 'domain':
                $domain = $record->domain();
                $domainPath = $domain->getFullPath();
                return $W->Label(implode(' > ', $domainPath));


            case 'name':
                $link = $W->Link(
                    $record->name,
                    resourcemanagement_Controller()->Resource()->display($record->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG);

                return $link;

            case 'description':
                return $W->Label(bab_abbr($record->description, BAB_ABBR_FULL_WORDS, 100));

            case 'allowConditionalBooking':
                return $W->Label($record->allowConditionalBooking ? resourcemanagement_translate('Yes') : resourcemanagement_translate('No'));

            case '_actions_':
                $actionsBox = $W->FlowLayout();
//                $actionsBox->setSpacing(4, 'px');
                if (resourcemanagement_canUpdateResource($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '',
                            resourcemanagement_Controller()->Resource()->edit($record->id)
                        )->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT, 'icon')//, 'widget-actionbutton')
                        ->setTitle(resourcemanagement_translate('Edit'))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                    );
                }
                $actionsBox->addItem(
                    $W->Link(
                        '',
                        resourcemanagement_Controller()->Resource()->downloadUnavailableICS($record->id)
                    )->setTitle(resourcemanagement_translate('Download unavailability ICS'))
                    ->addClass(Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD, 'icon')//, 'widget-actionbutton')
                );
                if (resourcemanagement_canEditResourceAccess($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '', // resourcemanagement_translate('Access rights'),
                            resourcemanagement_Controller()->Resource()->editAccess($record->id)
                        )->setTitle(resourcemanagement_translate('Access rights'))
                        ->addClass(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS, 'icon')//, 'widget-actionbutton')
                    );
                }

                if (resourcemanagement_canDeleteResource($record)) {
                    $actionsBox->addItem(
                        $W->Link(
                            '', //resourcemanagement_translate('Delete'),
                            resourcemanagement_Controller()->Resource()->checkDelete($record->id)
                        )->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon')//, 'widget-actionbutton')
//						->setConfirmationMessage(resourcemanagement_translate('Are you sure you want to delete this resource?'))
                    );
                }
                return $actionsBox;

        }

        return parent::computeCellContent($record, $fieldPath);
    }

}





class resourcemanagement_ResourceFrame extends Widget_Frame
{

    public $buttons = null;

    /**
     * @var resourcemanagement_Resource $resource The resource displayed in the frame.
     */
    protected $resource = null;

    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);
    }


    /**
     *
     * @param resourcemanagement_Resource	$resource	The resource to display.
     * @return resourcemanagement_ResourceFrame
     */
    public function setResource(resourcemanagement_Resource $resource)
    {
        $this->resource = $resource;
        return $this;
    }



    public function addButtons()
    {
        $args = func_get_args();

        foreach ($args as $arg) {
            $this->buttons->addItem($arg);
        }

        return $this;
    }


    public function initialize()
    {
        $W = bab_Widgets();

        $resource = $this->resource;

        $descriptionSection = $W->Section(
            resourcemanagement_translate('Description'),
            $W->Html(bab_toHtml($resource->description, BAB_HTML_ALL))
        );


        $constraintsSection = $this->constraintsSection();

        $typesSection = $W->FlowItems(
            $W->Label(resourcemanagement_translate('Types'))
                ->setSizePolicy('widget-33pc')
                ->addClass('widget-strong'),
            $typeList = $W->FlowLayout()->setHorizontalSpacing(1, 'em')
        )->setSizePolicy('widget-list-element')
        ->setVerticalAlign('top');

        $types = $resource->getTypes();

        foreach ($types as $type) {
            $typeList->addItem(
                $W->label($type->name)
            );
        }


        $characteristicsSection = $W->Section(
            resourcemanagement_translate('Characteristics'),
            $W->VBoxItems()//->setVerticalSpacing(1, 'em')
        );

        $characteristicsSection->addItem($typesSection);

        $mainBox = $W->VBoxItems(
            $descriptionSection,
            $constraintsSection,
            $characteristicsSection
        );
        $mainBox->setVerticalSpacing(1, 'em');
        $mainBox->setSizePolicy('widget-67pc');



        $secondBox = $W->VBoxItems(
            $this->images(),
            $this->scheduleSection()
                ->addClass('compact')
        );
        $secondBox->setVerticalSpacing(1, 'em');
        $secondBox->setSizePolicy('widget-33pc widget-bordered-left');

        $layout = $W->VBoxItems(
            $W->Title($resource->name)
        );

        if ($resource->unavailable) {
            $layout->addItem(
                $W->Label(resourcemanagement_translate('The resource is temporarily unavailable.'))
                    ->addClass('icon', 'widget-bordered', 'widget-strong')
            );
        }

        $layout->addItem(
            $W->FlowItems(
                $mainBox,//->addClass('widget-bordered'),
                $secondBox//->addClass('widget-bordered')
            )->setVerticalAlign('top')
//			->addClass('widget-bordered')
        );

        $this->setLayout($W->VBoxItems($layout));



        foreach ($types as $type) {
            $characteristicsSection->addItem(
                $this->typeSections($type)
            );
        }

        $this->buttons = $W->FlowItems();
        $this->buttons->setHorizontalSpacing(2, 'em')
            ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC, 'widget-toolbar');

        $mainBox->addItem($this->buttons);

        return $this;
    }



    /**
     * Returns a widget section containing information about constraints on
     * reservations for the resource.
     *
     * @return Widget_Section
     */
    protected function constraintsSection()
    {
        $W = bab_Widgets();

        $resource = $this->resource;
        $constraintsSection = $W->Section(
            resourcemanagement_translate('Constraints'),
            $W->VBoxItems()
        );

        $text = null;
        if ($resource->reservationDelayMin && $resource->reservationDelayMax) {
            $text = sprintf(
                resourcemanagement_translate('Reservation at least %d days before and at most %d days before'),
                $resource->reservationDelayMin,
                $resource->reservationDelayMax
            );
        } elseif ($resource->reservationDelayMin) {
            $text = sprintf(resourcemanagement_translate('Reservation at least %d days before'), $resource->reservationDelayMin);
        } elseif ($resource->reservationDelayMax) {
            $text = sprintf(resourcemanagement_translate('Reservation at most %d days before'), $resource->reservationDelayMax);
        }

        if ($text) {
            $constraintsSection->addItem(
                $W->FlowItems(
                    $W->Label($text)
                        ->setSizePolicy('widget-100pc')
                )
                ->setSizePolicy('widget-list-element')
                ->setVerticalAlign('top')
            );
        }

        return $constraintsSection;
    }




    protected function typeSections(resourcemanagement_Type $type)
    {
        $W = bab_Widgets();
        $typeSection = $W->VBoxItems();

        $typeFieldName = $type->getDynamicFieldName();
        $typeRecord = $this->resource->$typeFieldName;
        if ($typeRecord) {

            $typeFieldSet = new resourcemanagement_TypeFieldSet();
            $typeFields = $typeFieldSet->select(
                $typeFieldSet->type->is($type->id)
                    ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION))
                    ->_AND_($typeFieldSet->visibleInDescription->is(true))
            );
            $typeFields->orderDesc($typeFieldSet->order);
            $typeFields->orderAsc($typeFieldSet->id);

            $values = $typeRecord->getValues();

            foreach ($typeFields as $typeField) {

                $fieldValue = $values[$typeField->getDynamicFieldName()];

                $fieldValue = resourcemanagement_TypeField::getFormattedValue($fieldValue, $typeField->fieldType);

                $fieldItem = $W->FlowItems(
                    $W->Label($typeField->name())
                        ->setSizePolicy('widget-33pc')
                        ->addClass('widget-strong'),
                    $W->Label($fieldValue)
                )->setSizePolicy('widget-list-element')
                ->setVerticalAlign('top');

                $typeSection->addItem($fieldItem);
            }

        }

        return $typeSection;
    }





    protected function images()
    {
        $W = bab_Widgets();
        $imagesBox = $W->FlowLayout();
        $imagesBox->setVerticalAlign('top');


        $imagePathes = array();
        $imageFolder = $this->resource->mainImagePath();
        foreach ($imageFolder as $imagePath) {
            $imagePathes[] = $imagePath;
        }

        $imageFolder = $this->resource->imagePath();
        foreach ($imageFolder as $imagePath) {
            $imagePathes[] = $imagePath;
        }

        $sizePolicy = 'widget-100pc';
        $thumbnailSize = 400;
        foreach ($imagePathes as $imagePath) {
            if ($imagePath->isDir()) {
                continue;
            }
            $image = $W->ImageZoomerThumbnail($imagePath);
            $image->setThumbnailSize($thumbnailSize, $thumbnailSize);
            $image->setZoomSize(800, 600);
            $image->addClass('widget-100pc')
                ->setSizePolicy($sizePolicy);

            $sizePolicy = 'widget-50pc';
            $thumbnailSize = 200;
//			$image->setResizeMode(Func_Thumbnailer::CROP_CENTER);

            $imagesBox->addItem($image);
        }

        return $imagesBox;
    }




    protected function scheduleSection()
    {
        $W = bab_Widgets();

        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();

        $availabilities = $resourceAvailabilitySet->select($resourceAvailabilitySet->resource->is($this->resource->id));
        $availabilities
            ->orderAsc($resourceAvailabilitySet->startweekday)
            ->orderAsc($resourceAvailabilitySet->starttime);

        if ($availabilities->count() > 0) {
            $tableview = $W->TableView();

            $tableview->addSection('header', null, 'widget-table-header');
            $tableview->setCurrentSection('header');

            $row = 0;
            $tableview->addItem($W->label(resourcemanagement_translate('Day')), $row, 0);
            $tableview->addItem($W->label(resourcemanagement_translate('Hours')), $row, 1);

            $tableview->addSection('body', null, 'widget-table-body');
            $tableview->setCurrentSection('body');
            $row = 0;

            $weekOpeningHours = array();
            foreach ($availabilities as $availability) {
                $weekOpeningHours[$availability->startweekday][] =  sprintf(resourcemanagement_translate('%s to %s'), $availability->starttime, $availability->endtime);
            }

            foreach ($weekOpeningHours as $startweekday => $openingHours) {
                $tableview->addItem($W->Label(resourcemanagement_getWeekDay($startweekday)), $row, 0);
                $tableview->addItem($W->Label(implode(' - ', 	$openingHours)), $row, 1);
                $row++;
            }
        } else {
            $tableview = $W->Label(resourcemanagement_translate('No schedule defined.'));

        }

        $scheduleSection = $W->Section(
            resourcemanagement_translate('Schedule'),
            $tableview
        );

        return $scheduleSection;
    }


}



function resourcemanagement_getWeekDay($weekday)
{
    $days = array(
        '0' => resourcemanagement_translate('Monday'),
        '1' => resourcemanagement_translate('Tuesday'),
        '2' => resourcemanagement_translate('Wednesday'),
        '3' => resourcemanagement_translate('Thursday'),
        '4' => resourcemanagement_translate('Friday'),
        '5' => resourcemanagement_translate('Saturday'),
        '6' => resourcemanagement_translate('Sunday')
    );

    return $days[$weekday];
}


function resourcemanagement_WeekDaySelect()
{
    $W = bab_Widgets();
    $weekdaySelect = $W->Select();
    $weekdaySelect
        ->addOption('0', resourcemanagement_translate('Monday'))
        ->addOption('1', resourcemanagement_translate('Tuesday'))
        ->addOption('2', resourcemanagement_translate('Wednesday'))
        ->addOption('3', resourcemanagement_translate('Thursday'))
        ->addOption('4', resourcemanagement_translate('Friday'))
        ->addOption('5', resourcemanagement_translate('Saturday'))
        ->addOption('6', resourcemanagement_translate('Sunday'));
    return $weekdaySelect;
}




class resourcemanagement_AvailabilityTableView extends Widget_TableModelView
{
    protected $resourceAvailabilitySet = null;


    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'startweekday':


                return resourcemanagement_WeekDaySelect()
                    ->setName('startweekday')
                    ->setValue($record->startweekday)
                    ->setDisplayMode();


            case 'endweekday':

                return resourcemanagement_WeekDaySelect()
                    ->setName('endweekday')
                    ->setValue($record->endweekday)
                    ->setDisplayMode();


            case 'starttime':

                return $W->TimeEdit()//->setSize(5)
                    ->setName('starttime')
                    ->setValue($record->starttime)
                    ->setDisplayMode();


            case 'endtime':

                return $W->TimeEdit()//->setSize(5)
                    ->setName('endtime')
                    ->setValue($record->endtime)
                    ->setDisplayMode();


            case '_actions_':

                $actionsBox = $W->FlowLayout();
                $actionsBox->addClass(Func_Icons::ICON_LEFT_16);

                $actionsBox->addItem(
                    $W->Link(
                        $W->Icon(resourcemanagement_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE)
                    )
                    ->addClass('bab_toolbarItem')
                    ->setAjaxAction(resourcemanagement_Controller()->Resource()->deleteAvailability($record->id), $actionsBox)
                    ->setConfirmationMessage(resourcemanagement_translate('Are you sure you want to delete this period?'))
                );

                return $actionsBox;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class resourcemanagement_ResourceSearchTableView extends resourcemanagement_ResourceTableView
{
    protected $resourceSet = null;


    public function __construct($id = null)
    {
        parent::__construct($id);

    }


    public function setPeriod($start, $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * (non-PHPdoc)
     * @see resourcemanagement_ResourceTableView::addDefaultColumns($set)
     * @param resourcemanagement_ResourceSet $set
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->addColumn(
            widget_TableModelViewColumn('image', '')->addClass('widget-column-thin')
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->name, resourcemanagement_translate('Name'))
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->domain, resourcemanagement_translate('Domain'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn($set->description, resourcemanagement_translate('Description'))->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn('types', resourcemanagement_translate('Types'))
        );
        $this->addColumn(
            widget_TableModelViewColumn('max', resourcemanagement_translate('Max available period'))
                ->addClass('widget-col-10em widget-align-center')
        );
        $this->addColumn(
                widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-col-15em widget-align-center')
        );
    }

    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'name':
                return $W->VBoxItems(
                    $W->Link($record->name, resourcemanagement_Controller()->Resource()->display($record->id))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Html(bab_abbr($record->description, BAB_ABBR_FULL_WORDS, 100))
                );

            case 'max':
                $seconds = $record->getMaxAvailableDurationBetween($this->start, $this->end);

                if ($seconds % 60 != 0) {
                    $seconds++;
                }

                $d = floor($seconds / 86400);
                $s = $seconds - ($d * 86400);
                $h = floor($s / 3600);
                $m = floor(($s % 3600) / 60);

                $text = '';
                if ($d > 0) {
                    $text = $d . ' ' . resourcemanagement_translate('j') . ' ';
                }

                if ($h > 0 || $m > 0) {
                    $text .= $h . ' h ';
                }

                if ($m > 0) {
                    $text .= $m . ' min';
                }
                return $W->Label($text);


            case '_actions_':

                $actionsBox = $W->FlowLayout();
                $actionsBox->addClass(Func_Icons::ICON_LEFT_16);

                $actionsBox->addItem(
                    $W->Link(
                        $W->Icon(resourcemanagement_translate('Show planning'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
                        resourcemanagement_Controller()->Calendar()->display(null, substr($this->start, 0, 10), $record->id)
                    )
                    ->addClass('bab_toolbarItem')
                );

                return $actionsBox;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}



/**
 *
 * @return Widget_SimpleTreeView
 */
function resourcemanagement_ResourceTreeView(Widget_Item $reloadItem = null, $widgetId = null)
{
    $W = bab_Widgets();

    $resourcesByDomain = array();
    $parentByDomain = array();
    $totalResourcesByDomain = array();

    $domainSet = new resourcemanagement_DomainSet();

    $domains = $domainSet->select();
    foreach ($domains as $domain) {
        $totalResourcesByDomain[$domain->id] = array();
        $parentByDomain[$domain->id] = $domain->parent;
    }

    $resourceSet = new resourcemanagement_ResourceSet();

    $resources = $resourceSet->select(
        $resourceSet->isViewable()/*hasVisibleReservations()*/
        ->_OR_($resourceSet->isBookable())
    );

//	var_dump($resources->getSelectQuery());

    foreach ($resources as $resource) {
        if (!$resource->domain) {
            // Should not happen
            continue;
        }
        $resourcesByDomain[$resource->domain][] = $resource->id;

        for ($domainId = $resource->domain; $domainId; $domainId = (isset($parentByDomain[$domainId]) ? $parentByDomain[$domainId] : null)) {
            $totalResourcesByDomain[$domainId][] = $resource->id;
        }
    }





    $treeview = $W->SimpleTreeView($widgetId);
    $treeview->setPersistent(true);
//	$treeview->addAttributes(Widget_SimpleTreeView::MEMORIZE_OPEN_NODES);

    $domains = $domainSet->select();

    foreach ($domains as $domain) {


        $element = $treeview->createElement('d' . $domain->id, '', $domain->name, '', '');

        $checkbox = $W->Checkbox()->setName('d' . $domain->id);
        $checkbox->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleResourceVisibility('1'), $reloadItem);

        if (count($totalResourcesByDomain[$domain->id]) <= 0) {
            continue;
        }

        $resources = $totalResourcesByDomain[$domain->id];
        $resources = implode(',', $resources);
        $checkbox->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleResourceVisibility($resources), $reloadItem);

        $element->setItem(
            $W->FlowItems(
                $W->Icon($domain->name, Func_Icons::PLACES_FOLDER)
                    ->setTitle($domain->name)
                    ->addClass('widget-15em'),
                $W->FlowItems(
                    $checkbox
                )
                ->addClass('right_elements')
                ->setHorizontalSpacing(1, 'em')
            )
            ->addClass(Func_Icons::ICON_LEFT_16)
        );

        $parentId = ($domain->parent == 0) ? null : 'd' . $domain->parent;
        $treeview->appendElement($element, $parentId);
    }



    $resourceSet = new resourcemanagement_ResourceSet();

    $resources = $resourceSet->select(
        $resourceSet->isViewable()
            ->_OR_($resourceSet->isBookable())
    );

    foreach ($resources as $resource) {
        $element = $treeview->createElement('r' . $resource->id, '', $resource->name, '', '');

        $checkbox = $W->Checkbox();
        $checkbox->setName('r' . $resource->id);
        $checkbox->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleResourceVisibility($resource->id), $reloadItem);


        $elementItem = $W->FlowItems(
            $icon = $W->Link(
                $W->Icon($resource->name, Func_Icons::OBJECTS_TAG),
                resourcemanagement_Controller()->Resource()->display($resource->id)
            )->addClass('widget-15em')
            ->setTitle($resource->name)
            ->setOpenMode(Widget_Link::OPEN_DIALOG),
            $W->FlowItems(
                $cb = $checkbox->setValue(resourcemanagement_isResourceSelected($resource->id))
            )
            ->addClass('right_elements')
            ->setHorizontalSpacing(1, 'em')
        );


        if ($resource->unavailable) {
            $icon->addClass('unavailable');
            $icon->setTitle($resource->name . "\n" . resourcemanagement_translate('The resource is temporarily unavailable.'));
            $cb->setTitle(resourcemanagement_translate('The resource is temporarily unavailable.'));
        }

        $element->setItem(
            $elementItem->addClass(Func_Icons::ICON_LEFT_16)
        );

        $resourcesByDomain[$resource->domain][] = $resource->id;
        $parentId = ($resource->domain == 0) ? null : 'd' . $resource->domain;
        $treeview->appendElement($element, $parentId);
    }


    return $treeview;
}

function resourcemanagement_linkedEventOption(Widget_Item $reloadItem = null)
{
    $W = bab_Widgets();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/resourcemanagement');

    $checkBox = $W->CheckBox()->setName('linkedEventCheckBox');
    $checkBox->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleLinkedEventValue(), $reloadItem);

    $checkBoxValue = (isset($_SESSION['resourcemanagement']['linkedEventVisibility']) ? $_SESSION['resourcemanagement']['linkedEventVisibility'] : $registry->getValue('linkedEventVisibility', true));
    $checkBox->setValue($checkBoxValue);

    return $checkBox;
}


/**
 * An array containing calendar visible by the current user
 * indexed by calendar type and calendar identifier.
 *
 * @param string $mode	"view" or "add"
 * @return <multitype:multitype: , bab_EventCalendar>
 */
function resourcemanagement_getAvailableCalendars($mode = 'view')
{
    $availableCalendars = array('personal' => array());

    $icalendars = bab_getICalendars();

    $calendars = $icalendars->getCalendars();

    foreach ($calendars as $key => $calendar) {

        /* @var $calendar bab_EventCalendar */
        if ($mode === 'view' && $calendar->canView()
            || $mode === 'add' && $calendar->canAddEvent()) {
                $type = $calendar->getReferenceType();
                if (!isset($availableCalendars[$type])) {
                    $availableCalendars[$type] = array();
                }

                $availableCalendars[$type][$key] = $calendar;
            }
    }


    return $availableCalendars;
}

/**
 *
 * @return Widget_SimpleTreeView
 */
function resourcemanagement_CalendarTreeView(Widget_Item $reloadItem = null, $widgetId = null)
{
    $W = bab_Widgets();


    $calendarsByType = resourcemanagement_getAvailableCalendars();

//    var_dump($calendars);

    $treeview = $W->SimpleTreeView($widgetId);
    $treeview->setPersistent(true);
    $treeview->addAttributes(Widget_SimpleTreeView::MEMORIZE_OPEN_NODES);


    foreach ($calendarsByType as $type => $calendars) {

        if ($type != 'personal') {
            continue;
        }
        $element = $treeview->createElement($type, '', $type, '', '');

        $element->setItem(
            $W->FlowItems(
                $W->Icon($type, Func_Icons::PLACES_FOLDER)
                ->setTitle($type)
                ->addClass('widget-15em')
            )
            ->addClass(Func_Icons::ICON_LEFT_16)
        );

        $parentId =  null;
        $treeview->appendElement($element, $parentId);

        foreach ($calendars as $calendarId => $calendar) {

            /* @var $calendar bab_EventCalendar */
            $element = $treeview->createElement('c' . $calendarId, '', $calendar->getName(), '', '');

            $checkbox = $W->Checkbox();
            $checkbox->setName('c' . $calendarId);
            $checkbox->setAjaxAction(resourcemanagement_Controller()->Calendar()->toggleResourceVisibility('c' . $calendarId), $reloadItem);


            $elementItem = $W->FlowItems(
                $icon = $W->Link(
                    $W->Icon($calendar->getName(), Func_Icons::OBJECTS_TAG),
                    resourcemanagement_Controller()->Resource()->display($calendarId)
                )->addClass('widget-15em')
                ->setTitle($calendar->getName())
                ->setOpenMode(Widget_Link::OPEN_DIALOG),
                $W->FlowItems(
                    $cb = $checkbox->setValue(resourcemanagement_isResourceSelected($calendarId))
                )
                ->addClass('right_elements')
                ->setHorizontalSpacing(1, 'em')
            );



            $element->setItem(
                $elementItem->addClass(Func_Icons::ICON_LEFT_16)
            );

            $treeview->appendElement($element, $type);

        }
    }


    return $treeview;
}









function resourcemanagement_resourceShortPreview(resourcemanagement_Resource $resource = null, $reservation = null)
{
    $W = bab_Widgets();

    if ($resource === null) {
        return null;
    }

    if (!$reservation) {
        $action = resourcemanagement_Controller()->Resource()->display($resource->id);
    } else {
        $action = resourcemanagement_Controller()->Reservation()->display($reservation);
    }

    $resourceInfo = $W->VBoxItems(
        $W->Link(
            $resource->name,
            $action
        )->setOpenMode(Widget_Link::OPEN_DIALOG)
        ->addClass('widget-strong'),
        $W->Label(bab_abbr($resource->description, BAB_ABBR_FULL_WORDS, 50))
    );

    $imagePath = $resource->getImage();
    if ($imagePath) {
        $image = $W->ImageThumbnail($imagePath);
        $image->setThumbnailSize(32, 32);
        $image->setResizeMode(Func_Thumbnailer::CROP_CENTER);
        $image->createThumbnail();
        $resourceInfo = $W->FlowItems(
            $image,
            $resourceInfo
        )->setHorizontalSpacing(1, 'em')
        ->setVerticalAlign('top');
    }

    return $resourceInfo;
}