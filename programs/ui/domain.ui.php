<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/domain.class.php';


bab_Widgets()->includePhpClass('Widget_Layout');
bab_Widgets()->includePhpClass('Widget_SimpleTreeView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');




class resourcemanagement_DomainEditor extends Widget_Form
{

	/**
	 *
	 * @param string $id
	 * @param Widget_Layout $layout
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();

		if (null === $layout) {
			$layout = $W->VBoxLayout()
				->setVerticalSpacing(1, 'em');
		}

		parent::__construct($id, $layout);

		$this->setName('domain');

		$this->setHiddenValue('tg', bab_rp('tg'));

		$this->addFields();

		$this->addItem(
			$W->FlowItems(
				$W->SubmitButton()
					->setAction(resourcemanagement_Controller()->Domain()->save())
			         ->setAjaxAction()
					->setSuccessAction(resourcemanagement_Controller()->Domain()->displayList())
					->setFailedAction(resourcemanagement_Controller()->Domain()->edit())
					->setLabel(resourcemanagement_translate('Save')),
				$W->Link(
					resourcemanagement_translate('Cancel'),
					resourcemanagement_Controller()->Domain()->displayList()
				)->addClass('widget-actionbutton', 'widget-close-dialog')
			)->setHorizontalSpacing(1, 'em')
			->setVerticalAlign('middle')
		);

	}


	protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
	{
		$W = bab_Widgets();
		return $W->FlowItems(
			$W->Label($labelText)
				->setSizePolicy('widget-20em')
				->colon(),
			$item
		);
	}


	protected function addFields()
	{
		$W = bab_Widgets();

		$nameItem = $W->LineEdit();
		$nameItem->setSizePolicy('widget-30em');
		$nameItem->addClass('widget-100pc');
		$nameItem->setName('name');

		$this->addItem(
			$this->LabelledWidget(
				resourcemanagement_translate('Name'),
				$nameItem
			)
		);

	}



	public function setDomain(resourcemanagement_Domain $domain)
	{
		$this->setValues($domain->getValues(), array('domain'));

		$this->setHiddenValue('domain[id]', $domain->id);
	}


}





/**
 * Returns a Widget Select containing the domain presented hierarchically.
 *
 * @return Widget_Select
 */
function resourcemanagement_DomainSelect($domains = null, $allowEmpty = true)
{
	$W = bab_Widgets();
	$select = $W->Select();

	if (!isset($domains)) {
		$domainSet = new resourcemanagement_DomainSet();
		$domains = $domainSet->select();
	}

	include_once $GLOBALS['babInstallPath'].'utilit/treebase.php';

	$tree = new bab_OrphanRootNode();
	foreach ($domains as $domain) {
		$node = $tree->createNode($domain, $domain->id);
		$parentId = ($domain->parent == 0) ? null : $domain->parent;
		$tree->appendChild($node, $parentId);
	}

	if ($allowEmpty) {
		$select->addOption('', '');
	}
	$it = $tree->createNodeIterator($tree);
	$level = 0;
	while ($node = $it->nextNode()) {
		$data = $node->getData();
		if (!$data) {
			continue;
		}

		$padding = str_repeat(bab_nbsp(), 4 * $level);
		$select->addOption($data->id, $padding . $data->name);
		$level = $it->level() - 1;
	}

	return $select;
}




/**
 *
 * @return Widget_SimpleTreeView
 */
function resourcemanagement_DomainTreeView()
{
	$W = bab_Widgets();

	$domainSet = new resourcemanagement_DomainSet();

	$treeview = $W->SimpleTreeView();
	$treeview->setPersistent(true);
	$treeview->addAttributes(Widget_SimpleTreeView::MEMORIZE_OPEN_NODES);

	$domains = $domainSet->select();

	foreach ($domains as $domain) {
		$element = $treeview->createElement($domain->id, '', $domain->name, '', '');

		$parentId = ($domain->parent == 0) ? null : $domain->parent;
		$treeview->appendElement($element, $parentId);
	}

	return $treeview;
}

