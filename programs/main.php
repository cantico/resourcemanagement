<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';






$W = bab_Widgets();

bab_skin::applyOnCurrentPage('theme_crm_like', '');

// /* @var $Less Func_Less */
// if ($Less = @bab_functionality::get('less'))
// {

// 	$variables = theme_crm_like_getLessVariables();
// 	$path = realpath('.') . '/skins/theme_crm_like/styles/';

// 	try {
// 		$stylesheet = $Less->getCssUrl($path.'style.less', $variables);
// 		$babBody->addStyleSheet($stylesheet);
// 	} catch (Exception $e) {
// 		bab_debug($e->getMessage());
// 	}
// }

/* @var $Icons Func_Icons */
$Icons = bab_functionality::get('Icons');

$Icons->includeCss();

$addon = bab_getAddonInfosInstance('resourcemanagement');
$babBody->addStyleSheet($addon->getStylePath().'resourcemanagement.css');

$babBody->addStyleSheet('toolbar.css');

$babDB->db_query('SET FOREIGN_KEY_CHECKS = 0');

$msg = bab_rp('msg', null);
if (isset($msg)) {
    $lines = explode("\n", $msg);
    foreach ($lines as $msgline) {
        $babBody->addMessage($msgline);
    }
}
bab_Sitemap::setPosition('resourcemanagement');

try {
    $ctrl = resourcemanagement_Controller();

    $action = Func_Widgets::Action()->fromRequest();
    if ($action->getMethod() == '') {
        // If no method (idx) is set in the url, we use the default action.
        $action = $ctrl->Calendar()->display();
    }

    $ctrl->execute($action);
} catch (resourcemanagement_Exception $e) {
    $babBody->addError($e->getMessage());
}

