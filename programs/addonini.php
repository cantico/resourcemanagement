;<?php /*

[general]
name							="resourcemanagement"
version							="3.0.80"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Resource management"
description.fr					="Module de gestion de réservations de ressources"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.6.4"
php_version						="5.4.0"
addon_access_control			="0"
author							="Cantico"
icon							="icon.png"
configuration_page 				="main&idx=admin.edit"


[addons]

widgets							="1.1.0"

;*/