<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/resource.ui.php';
require_once dirname(__FILE__).'/../ui/calendar.ui.php';
require_once dirname(__FILE__).'/../set/recurrence.class.php';
require_once dirname(__FILE__).'/../set/resource.class.php';
require_once dirname(__FILE__).'/../set/reservation.class.php';





/**
 * This controller manages actions that can be performed on calendars.
 */
class resourcemanagement_CtrlCalendar extends resourcemanagement_Controller
{
    const TYPE_MONTH = 'month';
    const TYPE_WEEK = 'week';
    const TYPE_WORKWEEK = 'workweek';
    const TYPE_DAY = 'day';

    const TYPE_RESOURCE_MONTH = 'resourceMonth';
    const TYPE_RESOURCE_WEEK = 'resourceWeek';
    const TYPE_RESOURCE_WORKWEEK = 'resourceWorkweek';
    const TYPE_RESOURCE_NEXTWEEKS = 'resourceNextWeeks';
    const TYPE_RESOURCE_DAY = 'resourceDay';

    const TYPE_TIMELINE = 'timeline';



    /**
     * Returns a json encoded array of events
     *
     * @param string	$inputDateFormat	The format of start and end parameters.
     * @param string	$start				Start date/time
     * @param string	$end				End date/time
     *
     * @see resourcemanagement_Reservation::toJson()
     *
     * @return Widget_Action
     */
    public function events($inputDateFormat = 'timestamp', $start = null, $end = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $displayedStatuses = array(
            resourcemanagement_Reservation::STATUS_OK,
            resourcemanagement_Reservation::STATUS_OPTION,
            resourcemanagement_Reservation::STATUS_WAITING
        );

        if ($inputDateFormat === 'timestamp') {
            $startDate = BAB_DateTime::fromTimeStamp($start);
            $endDate = BAB_DateTime::fromTimeStamp($end);
        } else {
            $startDate = BAB_DateTime::fromIsoDateTime($start);
            $endDate = BAB_DateTime::fromIsoDateTime($end);
        }


        $reservationSet = new resourcemanagement_ReservationSet();


        $reservations = $reservationSet->select(
            $reservationSet->start->lessThanOrEqual($endDate->getIsoDateTime())
            ->_AND_($reservationSet->end->greaterThanOrEqual($startDate->getIsoDateTime()))
            ->_AND_($reservationSet->resource->in(resourcemanagement_getCalendarSelectedResources()))
            ->_AND_($reservationSet->status->in($displayedStatuses))
        );

        $jsonEvents = array();
        $recurrenceId = array();

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');

        $linkedEventVisibility = (isset($_SESSION['resourcemanagement']['linkedEventVisibility']) ? $_SESSION['resourcemanagement']['linkedEventVisibility'] : $registry->getValue('linkedEventVisibility', true));

        if($linkedEventVisibility) {
            foreach ($reservations as $reservation) {
                $recurrenceId[$reservation->recurrence] = $reservation->recurrence;
                //$jsonEvents[] = $reservation;
            }

            $reservations = $reservationSet->select(
                $reservationSet->start->lessThanOrEqual($endDate->getIsoDateTime())
                ->_AND_($reservationSet->end->greaterThanOrEqual($startDate->getIsoDateTime()))
                ->_AND_($reservationSet->recurrence->in($recurrenceId))
                ->_AND_($reservationSet->status->in($displayedStatuses))
            );
        }

        foreach ($reservations as $reservation) {
            if (resourcemanagement_canViewReservation($reservation)) {
    			$reservation->fixApprobation();
                $jsonEvents[] = $reservation;
            }
        }

        require_once $GLOBALS['babInstallPath'].'utilit/cal.userperiods.class.php';

        $userPeriods = new bab_UserPeriods($startDate, $endDate);

        $factory = bab_getInstance('bab_PeriodCriteriaFactory');
        /* @var $factory bab_PeriodCriteriaFactory */

        $criteria = $factory->Collection(
            array(
                'bab_NonWorkingDaysCollection'
            )
        );

        $userPeriods->createPeriods($criteria);
        $userPeriods->orderBoundaries();

        foreach ($userPeriods as $event) {
            $startDatetime = $event->getProperty('DTSTART');
            $start = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T00:00:00';
            $end = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T23:59:59';

            $jsonEvents[] = array(
                'title' => $event->getProperty('SUMMARY'),
                'start' => $start,
                'end' => $end,
                'editable' => false
            );
        }

        echo resourcemanagement_json_encode($jsonEvents);

        die;
    }




    /**
     * Sets the date to be displayed next time a calendar view is displayed.
     *
     * @param string $date		ISO formatted date.
     */
    public function setDate($date = null)
    {
        if (isset($date) && is_string($date)) {
            $_SESSION['resourcemanagement']['dateToShow'] = $date;
        }
        die;
    }




    /**
     * Toggle full width view of calendar/
     */
    public function toggleCalendarMode()
    {
        $calendarLayout = isset($_SESSION['resourcemanagement']['calendarLayout']) ? $_SESSION['resourcemanagement']['calendarLayout'] : 'horizontal';
        if ($calendarLayout === 'horizontal') {
            $_SESSION['resourcemanagement']['calendarLayout'] = 'vertical';
        } else {
            $_SESSION['resourcemanagement']['calendarLayout'] = 'horizontal';
        }

        resourcemanagement_redirect($this->proxy()->display());
    }




    /**
     * Toggle full width view of calendar/
     */
    public function toggleFullDayView()
    {
        if(!isset($_SESSION['resmanagement-fullday']) || $_SESSION['resmanagement-fullday'] == 0){
            $_SESSION['resmanagement-fullday'] = 1;
        } else {
            $_SESSION['resmanagement-fullday'] = 0;
        }

        resourcemanagement_redirect($this->proxy()->display());
    }




    /**
     * Return a view containing a date picker to select the date, the resource treeview to select resources
     * and the resource calendar.
     *
     * @param string $mode		Onr of 'horizontal' or 'vertical'.
     *
     * @return Widget_Frame
     */
    public function splitView($mode = 'vertical')
    {
        $W = bab_Widgets();

        $calendarview = $this->resourceCalendar();

        $resourceTreeView = resourcemanagement_ResourceTreeView($calendarview, 'resource_treeview_calendar');

        $datePicker = $W->DatePicker('resourcemanagement-calendar-date-selector');
        $datePicker->setInline(true);
        $datePicker->showWeek(true);

        $datePicker->setFormat('yy-m-d');

        $linkedEventCheckBox = resourcemanagement_LinkedEventOption($calendarview);
        $optionSection = $W->Section(
            resourcemanagement_translate('Options'),
            $W->LabelledWidget(
                resourcemanagement_translate('Display linked events'),
                $linkedEventCheckBox
            )
        )->setFoldable(true, false)
        ->addClass('compact');

        $resourceSection = $W->Section(
            resourcemanagement_translate('Resources'),
            $resourceTreeView
        )->setFoldable(true, false)
        ->addClass('compact');


        if ($mode === 'horizontal') {

            // Date picker and resource treeview on the left,
            // calendar on the right.
            $datePicker->setNumberOfMonths(1);
            $splitview = $W->HBoxItems(
                $W->VBoxItems(
                    $datePicker,
//                     $optionSection,
                     $resourceSection
                )->setSizePolicy('left-col widget-col-20pc'),
                $W->VBoxItems(
                    $calendarview
                )->setSizePolicy('resourcemanagement-large-calendar widget-col-80pc')
            )->addClass('expand');

        } else {

            // Date picker and resource treeview at the top,
            // calendar at the bottom.
            $datePicker->setNumberOfMonths(3);
            $splitview = $W->VBoxItems(
                $W->HBoxItems(
                    $resourceSection
                        ->setSizePolicy('widget-col-33pc'),
                    $datePicker
                )
                ->setVerticalAlign('top'),
                $W->VBoxItems(
                        $calendarview->setMetadata('cancelHeight', 'true')
                )->setSizePolicy('resourcemanagement-large-calendar')
            )->addClass('expand');

        }


        $explorer = $W->Frame('calendar_explorer')
            ->setLayout($W->VBoxLayout())
            ->addItem($splitview);

        return $explorer;
    }


    /**
     * Return a view containing a date picker to select the date, the resource treeview to select resources
     * and the resource calendar.
     *
     * @param string $mode		Onr of 'horizontal' or 'vertical'.
     *
     * @return Widget_Frame
     */
    public function splitViewAll($mode = 'vertical')
    {
        $W = bab_Widgets();

        $calendarview = $this->resourceCalendarAll();

        $datePicker = $W->DatePicker('resourcemanagement-calendar-date-selector');
        $datePicker->setInline(true);
        $datePicker->showWeek(true);

        $datePicker->setFormat('yy-m-d');


        if ($mode === 'horizontal') {

            // Date picker and resource treeview on the left,
            // calendar on the right.
            $datePicker->setNumberOfMonths(1);
            $splitview = $W->HBoxItems(
                $W->VBoxItems(
                    $datePicker
                )->setSizePolicy('left-col widget-col-20pc'),
                $W->VBoxItems(
                    $calendarview
                )->setSizePolicy('resourcemanagement-large-calendar widget-col-80pc')
            )->addClass('expand');
        } else {

            // Date picker and resource treeview at the top,
            // calendar at the bottom.
            $datePicker->setNumberOfMonths(3);
            $splitview = $W->VBoxItems(
                $W->HBoxItems(
                    $datePicker
                )
                ->setVerticalAlign('top'),
                $W->VBoxItems(
                    $calendarview->setMetadata('cancelHeight', 'true')
                )->setSizePolicy('resourcemanagement-large-calendar')
            )->addClass('expand');
        }


        $explorer = $W->Frame('calendar_explorer')
            ->setLayout($W->VBoxLayout())
            ->addItem($splitview);

        return $explorer;
    }




    /**
     *
     * @return Widget_Frame|Widget_FullCalendar
     */
    public function resourceCalendar()
    {
        $W = bab_Widgets();

        $dateToShow = $_SESSION['resourcemanagement']['dateToShow'];

        $calendarType = $_SESSION['resourcemanagement']['calendarType'];


        $selectedResourceIds = resourcemanagement_getCalendarSelectedResources();

//         if (count($selectedResourceIds) < 1) {

//             $frame = $W->Frame();
//             $frame->addClass('resourcemanagement-empty-placeholder', 'widget-bordered', 'BabLoginMenuBackground', 'widget-align-center', Func_Icons::ICON_LEFT_24);

//             $frame->addItem(
//                 $W->VBoxItems(
//                     $W->Label(resourcemanagement_translate('No resource is selected'))
//                 )
//             );
//             return $frame;
//         }

        $calendarview = resourcemanagement_calendarView($calendarType, $dateToShow);

        $calendarview->setAssociatedDatePicker('resourcemanagement-calendar-date-selector');

        $calendarview->addClass(Func_Icons::ICON_LEFT_48);

        $resourceSet = new resourcemanagement_ResourceSet();

        $resources = $resourceSet->select(
            $resourceSet->id->in($selectedResourceIds)
            ->_AND_(
                $resourceSet->isViewable()->_OR_($resourceSet->isBookable())
            )
        );
        $ressourcesData = array();

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();

        $typeFieldSet = new resourcemanagement_TypeFieldSet();


        $imageWidth = 25;
        $imageHeight = 25;

        $htmlCanvas = $W->HtmlCanvas();
        $imageHtml = '';
        foreach ($resources as $resource) {

            $imagePath = $resource->getImage();
            if ($imagePath) {
                $image = $W->ImageThumbnail($imagePath);
                $image->setThumbnailSize($imageWidth, $imageHeight);
                $image->setResizeMode(Func_Thumbnailer::CROP_CENTER);
                $image->createThumbnail();
                $imageHtml = $image->display($htmlCanvas);

            } else {
                $imageHtml = '<span class="widget-image" style="display: inline-block; width: ' . $imageWidth . 'px; height: ' . $imageHeight . 'px"></span> ';
            }

            if ($resource->unavailable) {
                $unavailableClass = ' class="unavailable"';
                $unavailableTitle = ' title="' . bab_toHtml(resourcemanagement_translate('The resource is temporarily unavailable.')) . '"';
            } else {
                $unavailableClass = '';
                $unavailableTitle = '';
            }
            $resourceTypes = $resourceTypeSet->select(
                $resourceTypeSet->resource->is($resource->id)
            );
            $resource->extend();

            $values = $resource->getValues();

            $complement = '';
            foreach ($resourceTypes as $resourceType) {
                $typeFields = $typeFieldSet->select(
                    $typeFieldSet->type->is($resourceType->type->id)
                        ->_AND_($typeFieldSet->visibleOnPlanning->is(true))
                        ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION))
                );
                $typeFields->orderDesc($typeFieldSet->order);
                $typeFields->orderAsc($typeFieldSet->id);
                foreach ($typeFields as $typeField) {

                    $dynamicFieldName = $typeField->getDynamicFieldName();
                    $fieldValue = $values[$resourceType->type->getDynamicFieldName()][$dynamicFieldName];
                    $fieldValue = resourcemanagement_TypeField::getFormattedValue($fieldValue, $typeField->fieldType);

                    $complement .= $typeField->name . ' : <b>' . $fieldValue . '</b><br />';
                }
            }

            $W = bab_Widgets();
            $canvas = $W->HtmlCanvas();
            $action = resourcemanagement_Controller()->Resource()->display($resource->id);
            $link = $W->Link($W->Html($imageHtml . '<span class="resource-name">' . $resource->name . '</span>'), $action->url());
            $link->setOpenMode(Widget_Link::OPEN_DIALOG);
            $linkHtml = $link->display($canvas) . '<script type="text/javascript">widget_baseInit(document.getElementsByTagName("body")[0]);</script>';

            $ressourcesData[] = array(
                'name' => '<div' . $unavailableClass . $unavailableTitle . '>' . $linkHtml . '<br /><i>' . $complement . '</i></div>',
                'id' => $resource->id
            );

        }
        $calendarview->setResources($ressourcesData);

        $calendarview->setCanvasOptions(Widget_Item::Options()->height('95', '%')->width('100', '%'));

        return $calendarview;
    }


    /**
     *
     * @return Widget_Frame|Widget_FullCalendar
     */
    public function resourceCalendarAll()
    {
        $W = bab_Widgets();

        $dateToShow = $_SESSION['resourcemanagement']['dateToShow'];

        $calendarType = $_SESSION['resourcemanagement']['calendarType'];


        $selectedResourceIds = resourcemanagement_getCalendarSelectedResources();


        $calendarview = resourcemanagement_calendarView($calendarType, $dateToShow);

        $calendarview->setAssociatedDatePicker('resourcemanagement-calendar-date-selector');

        $calendarview->addClass(Func_Icons::ICON_LEFT_48);

        $resourceSet = new resourcemanagement_ResourceSet();

        $resources = $resourceSet->select(
            $resourceSet->id->in($selectedResourceIds)
            ->_AND_(
                $resourceSet->isViewable()->_OR_($resourceSet->isBookable())
                )
            );
        $ressourcesData = array();

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();

        $typeFieldSet = new resourcemanagement_TypeFieldSet();


        $imageWidth = 25;
        $imageHeight = 25;

        $htmlCanvas = $W->HtmlCanvas();
        $imageHtml = '';
        foreach ($resources as $resource) {

            $imagePath = $resource->getImage();
            if ($imagePath) {
                $image = $W->ImageThumbnail($imagePath);
                $image->setThumbnailSize($imageWidth, $imageHeight);
                $image->setResizeMode(Func_Thumbnailer::CROP_CENTER);
                $image->createThumbnail();
                $imageHtml = $image->display($htmlCanvas);

            } else {
                $imageHtml = '<span class="widget-image" style="display: inline-block; width: ' . $imageWidth . 'px; height: ' . $imageHeight . 'px"></span> ';
            }

            if ($resource->unavailable) {
                $unavailableClass = ' class="unavailable"';
                $unavailableTitle = ' title="' . bab_toHtml(resourcemanagement_translate('The resource is temporarily unavailable.')) . '"';
            } else {
                $unavailableClass = '';
                $unavailableTitle = '';
            }
            $resourceTypes = $resourceTypeSet->select(
                $resourceTypeSet->resource->is($resource->id)
                );
            $resource->extend();

            $values = $resource->getValues();

            $complement = '';
            foreach ($resourceTypes as $resourceType) {
                $typeFields = $typeFieldSet->select(
                    $typeFieldSet->type->is($resourceType->type->id)
                    ->_AND_($typeFieldSet->visibleOnPlanning->is(true))
                    ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION))
                );
                $typeFields->orderDesc($typeFieldSet->order);
                $typeFields->orderAsc($typeFieldSet->id);
                foreach ($typeFields as $typeField) {

                    $dynamicFieldName = $typeField->getDynamicFieldName();
                    $fieldValue = $values[$resourceType->type->getDynamicFieldName()][$dynamicFieldName];
                    $fieldValue = resourcemanagement_TypeField::getFormattedValue($fieldValue, $typeField->fieldType);

                    $complement .= $typeField->name . ' : <b>' . $fieldValue . '</b><br />';
                }
            }

            $W = bab_Widgets();
            $canvas = $W->HtmlCanvas();
            $action = resourcemanagement_Controller()->Resource()->display($resource->id);
            $link = $W->Link($W->Html($imageHtml . '<span class="resource-name">' . $resource->name . '</span>'), $action->url());
            $link->setOpenMode(Widget_Link::OPEN_DIALOG);
            $linkHtml = $link->display($canvas) . '<script type="text/javascript">widget_baseInit(document.getElementsByTagName("body")[0]);</script>';

            $ressourcesData[] = array(
                'name' => '<div' . $unavailableClass . $unavailableTitle . '>' . $linkHtml . '<br /><i>' . $complement . '</i></div>',
                'id' => $resource->id
            );

        }
        $calendarview->setResources($ressourcesData);

        $calendarview->setCanvasOptions(Widget_Item::Options()->height('95', '%')->width('100', '%'));

        return $calendarview;
    }


    /**
     * Displays the calendar.
     *
     * @param string	$type			The type of view.
     * @param string	$dateToShow		An ISO-formatted date that should be visible on this calendar.
     */
    public function display($type = null, $dateToShow = null, $resource = null, $mode = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

        $W = bab_Widgets();

        if (!(isset($dateToShow) && is_string($dateToShow))) {
            if (!isset($_SESSION['resourcemanagement']['dateToShow'])) {
                $dateToShow = BAB_DateTime::now();
                $dateToShow = $dateToShow->getIsoDate();
            } else {
                $dateToShow = $_SESSION['resourcemanagement']['dateToShow'];
            }
        }
        $_SESSION['resourcemanagement']['dateToShow'] = $dateToShow;

        if (!(isset($type) && is_string($type))) {
            if (!isset($_SESSION['resourcemanagement']['calendarType'])) {
                $type = resourcemanagement_CtrlCalendar::TYPE_MONTH;
            } else {
                $type = $_SESSION['resourcemanagement']['calendarType'];
            }
        }
        $_SESSION['resourcemanagement']['calendarType'] = $type;

        if (!(isset($mode) && is_string($mode))) {
            if (!isset($_SESSION['resourcemanagement']['calendarLayout'])) {
                $mode = 'horizontal';
            } else {
                $mode = $_SESSION['resourcemanagement']['calendarLayout'];
            }
        }
        $_SESSION['resourcemanagement']['calendarLayout'] = $mode;

        if (isset($resource)) {
            $_SESSION['resourcemanagement']['resources'] = array($resource => $resource);
        }

        $page = $W->BabPage('resourcemanagement-calendar');

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('search', resourcemanagement_translate('Search availability'), resourcemanagement_Controller()->Search()->displayList()->url());
        $page->setCurrentItemMenu('calendar');

        $layout = $W->VBoxLayout();
        $page->addItem($layout);

        $toolbar = resourcemanagement_calendarToolbar();
        $toolbar->addClass('widget-100pc');
        $layout->addItem($toolbar);

        $splitview = $this->splitView($mode);
        $splitview->setReloadAction($this->proxy()->splitView($mode));

        $splitview->setSizePolicy('widget-overflow-x-scroll ' . $type);

        $layout->addItem($splitview);

        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $page->addJavascriptFile($addon->getTemplatePath() . 'calendar.js');

        return $page;
    }




    /**
     * Displays the calendar.
     *
     * @param string	$type			The type of view.
     * @param string	$dateToShow		An ISO-formatted date that should be visible on this calendar.
     */
    public function displayAll($type = null, $dateToShow = null, $resource = null, $mode = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

        $W = bab_Widgets();

        if (!(isset($dateToShow) && is_string($dateToShow))) {
            if (!isset($_SESSION['resourcemanagement']['dateToShow'])) {
                $dateToShow = BAB_DateTime::now();
                $dateToShow = $dateToShow->getIsoDate();
            } else {
                $dateToShow = $_SESSION['resourcemanagement']['dateToShow'];
            }
        }
        $_SESSION['resourcemanagement']['dateToShow'] = $dateToShow;

        if (!(isset($type) && is_string($type))) {
            if (!isset($_SESSION['resourcemanagement']['calendarType'])) {
                $type = resourcemanagement_CtrlCalendar::TYPE_MONTH;
            } else {
                $type = $_SESSION['resourcemanagement']['calendarType'];
            }
        }
        $_SESSION['resourcemanagement']['calendarType'] = $type;

        if (!(isset($mode) && is_string($mode))) {
            if (!isset($_SESSION['resourcemanagement']['calendarLayout'])) {
                $mode = 'horizontal';
            } else {
                $mode = $_SESSION['resourcemanagement']['calendarLayout'];
            }
        }
        $_SESSION['resourcemanagement']['calendarLayout'] = $mode;

        if (isset($resource)) {
            $_SESSION['resourcemanagement']['resources'] = array($resource => $resource);
        }

        $page = $W->BabPage('resourcemanagement-calendar');

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('search', resourcemanagement_translate('Search availability'), resourcemanagement_Controller()->Search()->displayList()->url());
        $page->setCurrentItemMenu('calendarall');

        $layout = $W->VBoxLayout();
        $page->addItem($layout);

        $toolbar = resourcemanagement_calendarToolbarAll();
        $toolbar->addClass('widget-100pc');
        $layout->addItem($toolbar);

        $splitview = $this->splitViewAll($mode);
        $splitview->setReloadAction($this->proxy()->splitViewAll($mode));

        $splitview->setSizePolicy('widget-overflow-x-scroll ' . $type);

        $layout->addItem($splitview);

        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $page->addJavascriptFile($addon->getTemplatePath() . 'calendar.js');

        return $page;
    }





    /**
     * Displays the calendar.
     *
     * @param string	$type			The type of view.
     * @param string	$dateToShow		An ISO-formatted date that should be visible on this calendar.
     */
    public function displayPrint($type = null, $dateToShow = null, $resource = null, $mode = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

        $W = bab_Widgets();

        if (!(isset($dateToShow) && is_string($dateToShow))) {
            if (!isset($_SESSION['resourcemanagement']['dateToShow'])) {
                $dateToShow = BAB_DateTime::now();
                $dateToShow = $dateToShow->getIsoDate();
            } else {
                $dateToShow = $_SESSION['resourcemanagement']['dateToShow'];
            }
        }
        $_SESSION['resourcemanagement']['dateToShow'] = $dateToShow;

        if (!(isset($type) && is_string($type))) {
            if (!isset($_SESSION['resourcemanagement']['calendarType'])) {
                $type = resourcemanagement_CtrlCalendar::TYPE_MONTH;
            } else {
                $type = $_SESSION['resourcemanagement']['calendarType'];
            }
        }
        $_SESSION['resourcemanagement']['calendarType'] = $type;

        if (!(isset($mode) && is_string($mode))) {
            if (!isset($_SESSION['resourcemanagement']['calendarLayout'])) {
                $mode = 'horizontal';
            } else {
                $mode = $_SESSION['resourcemanagement']['calendarLayout'];
            }
        }
        $_SESSION['resourcemanagement']['calendarLayout'] = $mode;

        if (isset($resource)) {
            $_SESSION['resourcemanagement']['resources'] = array($resource => $resource);
        }

        $page = $W->BabPage('resourcemanagement-calendar');


        $page->addItem(
            $W->VBoxItems(
                $W->Link(
                    $W->HBoxItems(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_PRINT),
                        $W->Title(resourcemanagement_translate('Print'), 3)
                    )->setVerticalAlign('middle'),
                    '#'
                )->addAttribute("onclick", "window.print(); return false;")
            )->addClass('rm-printlink '.Func_Icons::ICON_LEFT_24)
        );

        $page->setEmbedded(false);

        $calendarview = $this->resourceCalendar();
        $calendarview->setReloadAction($this->proxy()->resourceCalendar());

        $page->addItem($calendarview);
        $J = bab_functionality::get('jquery');
        if ($J) {
            $addon = bab_getAddonInfosInstance('resourcemanagement');
            $page->addJavascriptFile($addon->getTemplatePath() . 'print.js');
        }
        return $page;
    }






    /**
     * Displays the reservations search results by period.
     *
     * @return Widget_Action
     */
    public function search($workspace = null, $keywords = null, $from = null, $to = null)
    {
        require_once dirname(__FILE__) . '/search.ui.php';

        $W = bab_Widgets();

        $page = $W->BabPage();
        $page->setTitle(workspace_translate('Search results'))
            ->addItemMenu('calendars', workspace_translate('Calendars'), $this->proxy()->display()->url())
            ->addItemMenu('search', workspace_translate('Search events'), $this->proxy()->search()->url())
            ->setCurrentItemMenu('search');

        $resultBox = workspace_calendarsSearchResultList($keywords, $from, $to);


        $resultFrame = $W->Frame();
        $resultFrame->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $searchForm = $W->Form()->setLayout($W->FlowLayout()->setSpacing(2, 'em'))
            ->setReadOnly()
            ->addItem(
                $W->FlowItems(
                    $keywordsLbl = $W->Label(workspace_translate('Keywords')),
                    $W->LineEdit()->setName('keywords')->setValue($keywords)->setAssociatedLabel($keywordsLbl)
                )->setHorizontalSpacing(0.5, 'em')
            )
            ->addItem(
                $W->FlowItems(
                    $FromLbl = $W->Label(workspace_translate('From')),
                    $W->DatePicker()->setName('from')->setValue(bab_rp('from'))->setAssociatedLabel($FromLbl),
                    $ToLbl = $W->Label(workspace_translate('to')),
                    $W->DatePicker()->setName('to')->setValue(bab_rp('to'))->setAssociatedLabel($ToLbl)
                )->setHorizontalSpacing(0.5, 'em')
            )
            ->addItem($W->SubmitButton()->setLabel(workspace_translate('New search')))
            ->setSelfPageHiddenFields();

        $resultFrame->addItem($searchForm);
        $resultFrame->addItem($resultBox);


        $page->addItem($resultFrame);

        return $page;
    }





    /**
     * Toggles the visibility of the specified resource.
     *
     * @param string $resources		The resources comma-separated id.
     *
     * @return Widget_Action
     */
    public function toggleResourceVisibility($resources)
    {
        $resources = explode(',', $resources);

        $doSelectAll = false;
        foreach ($resources as $resource) {
            if (!resourcemanagement_isResourceSelected($resource)) {
                // If there is at least one unselected resource, we select all.
                $doSelectAll = true;
            }
        }

        foreach ($resources as $resource) {
            resourcemanagement_setSelectedResource($resource, $doSelectAll);
        }
        die;
    }

	public function toggleLinkedEventValue()
    {
        //Session is not set, we use the default value set in admin options
        if(!isset($_SESSION['resourcemanagement']['linkedEventVisibility'])) {
            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/resourcemanagement');
            $_SESSION['resourcemanagement']['linkedEventVisibility'] = !$registry->getValue('linkedEventVisibility', true);
            die;
        }
        //Session is set, we toggle its value
        $_SESSION['resourcemanagement']['linkedEventVisibility'] = !$_SESSION['resourcemanagement']['linkedEventVisibility'];
        die;
    }





    /**
     *
     * @param string $resources		Comma-separated resource ids.
     * @param string $start			ISO formatted datetime
     * @param string $end			ISO formatted datetime
     */
    public function ical($resources = null, $start = null, $end = null)
    {
        if (!isset($resources)) {
            die;
        }

        $resources = explode(',', $resources);

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';


        if (!isset($start)) {
            $start = bab_DateTime::now();
            $start->add(-1, BAB_DATETIME_MONTH);
            $start = $start->getIsoDateTime();
            $end = bab_DateTime::now();
            $end->add(6, BAB_DATETIME_MONTH);
            $end = $end->getIsoDateTime();
        }

        $caldav = bab_functionality::get('CalendarBackend/Caldav');

        $caldav->includeCalendarPeriod();

        $reservationSet = new resourcemanagement_ReservationSet();

        $displayedStatuses = array(
            resourcemanagement_Reservation::STATUS_OK,
            resourcemanagement_Reservation::STATUS_OPTION,
            resourcemanagement_Reservation::STATUS_WAITING
        );

        $reservations = $reservationSet->select(
            $reservationSet->start->lessThanOrEqual($end)
                ->_AND_($reservationSet->end->greaterThanOrEqual($start))
                ->_AND_($reservationSet->resource->in($resources))
                ->_AND_($reservationSet->status->in($displayedStatuses))
        );

        $icalEvents = array();
        foreach ($reservations as $reservation) {
            $start = BAB_DateTime::fromIsoDateTime($reservation->start);
            $end = BAB_DateTime::fromIsoDateTime($reservation->end);

            $calendarPeriod = new bab_CalendarPeriod();
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('SUMMARY', $reservation->description);
            $calendarPeriod->setProperty('DESCRIPTION', $reservation->longDescription);

            $icalEvents[] = caldav_CalendarPeriod::toIcal($calendarPeriod);
        }

        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: inline; filename=calendar.ics');

        echo 'BEGIN:VCALENDAR' . "\r\n"
            . 'VERSION:2.0' . "\r\n"
            . $caldav->getProdId() . "\r\n"
            . $caldav->getTimeZone()
            . implode("\r\n", $icalEvents) . "\r\n"
            . 'END:VCALENDAR';

        die;
    }

}
