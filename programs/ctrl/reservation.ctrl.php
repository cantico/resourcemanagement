<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/reservation.ui.php';
require_once dirname(__FILE__).'/../set/recurrence.class.php';
require_once dirname(__FILE__).'/../set/reservation.class.php';
require_once dirname(__FILE__).'/../set/resource.class.php';
require_once dirname(__FILE__).'/../set/equipment.class.php';


/**
 *
 */
class resourcemanagement_CtrlReservation extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ReservationSet	$reservationSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $reservationSet)
    {
        // Initial conditions are base on read access rights.
        $conditions = $reservationSet->resource->domain->isExportable();

        $conditions = $conditions->_AND_($reservationSet->status->isNot(resourcemanagement_Reservation::STATUS_REJECTED));

        if (isset($filter['resource/name']) && !empty($filter['resource/name'])) {
            $conditions = $conditions->_AND_($reservationSet->resource->name->contains($filter['resource/name']));
        }

        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($reservationSet->description->contains($filter['description']));
        }

        if (isset($filter['domain']) && !empty($filter['domain'])) {
            $conditions = $conditions->_AND_($reservationSet->resource->domain->is($filter['domain']));
        }

        if (isset($filter['type']) && !empty($filter['type'])) {
            $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
            $conditions = $conditions->_AND_($reservationSet->resource->id->in($resourceTypeSet->type->is($filter['type']), 'resource'));

            if (isset($filter['typefilter'.$filter['type']])) {
                $typeSet = new resourcemanagement_TypeSet();
                $type = $typeSet->get(
                    $typeSet->id->is($filter['type'])
                );
                $reservationExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_RESERVATION);
                $typecondition = new ORM_TrueCriterion();

                foreach ($filter['typefilter'.$filter['type']] as $k => $v) {
                    if ($v) {
                        if (is_numeric($v)) {
                            $typecondition = $typecondition->_AND_($reservationExtSet->$k->is($v));
                        } else {
                            $typecondition = $typecondition->_AND_($reservationExtSet->$k->contains($v));
                        }
                    }
                }

                $resservations = array();
                $reservationExts = $reservationExtSet->select($typecondition);
                foreach ($reservationExts as $reservationExt) {
                    $resservations[] = $reservationExt->id;
                }
                $conditions = $conditions->_AND_($reservationSet->id->in($resservations));
            }
        }

        if (isset($filter['reservedBy']) && !empty($filter['reservedBy'])) {
            $conditions = $conditions->_AND_($reservationSet->reservedBy->is($filter['reservedBy']));
        }

        if (isset($filter['reservedFor']) && !empty($filter['reservedFor'])) {
            $conditions = $conditions->_AND_($reservationSet->reservedFor->is($filter['reservedFor']));
        }

        if (!empty($filter['start']['from'])) {
            $conditions = $conditions->_AND_($reservationSet->start->greaterThanOrEqual($reservationSet->start->input($filter['start']['from'])));
        }
        if (!empty($filter['start']['to'])) {
            $conditions = $conditions->_AND_($reservationSet->start->lessThanOrEqual($reservationSet->start->input($filter['start']['to'])));
        }

        if (!empty($filter['end']['from'])) {
            $conditions = $conditions->_AND_($reservationSet->end->greaterThanOrEqual($reservationSet->end->input($filter['end']['from'])));
        }
        if (!empty($filter['end']['to'])) {
            $conditions = $conditions->_AND_($reservationSet->end->lessThanOrEqual($reservationSet->end->input($filter['end']['to'])));
        }

        return $conditions;
    }






    /**
     *
     */
    protected function tableView($filter = null)
    {
        $reservationSet = new resourcemanagement_ReservationSet();
        $reservationSet->resource();
        $reservationSet->resource->domain();

        $tableView = new resourcemanagement_ReservationTableView();

        if (isset($filter['type']) && !empty($filter['type'])) {
            $tableView->setType($filter['type']);
        }

        $conditions = $this->getFilterCriteria($filter, $reservationSet);
        $selection = $reservationSet->select($conditions);

        $tableView->setDataSource($selection);

        $tableView->addDefaultColumns($reservationSet);

        return $tableView;
    }





    public function displayList($reservations = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        if (!resourcemanagement_canExportReservations()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');


        $filter = isset($reservations['filter']) ? $reservations['filter'] : array();


        $page->addItem($W->Title(resourcemanagement_translate('Reservations'), 1));


        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $W->Link(
                $W->Icon(resourcemanagement_translate('Export CSV'), Func_Icons::ACTIONS_ARROW_DOWN ),
                $this->proxy()->exportList($reservations)
            )->addClass('bab_toolbarItem')
        );

        $page->addItem($toolbar);



        $tableView = $this->tableView($filter);
        $filterPanel = $tableView->filterPanel('reservations', $filter);

        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isExportable());
        $domainSelect = resourcemanagement_DomainSelect($domains, true);
        $domainSelect->setName('domain');
        $filterForm = $filterPanel->getFilter();
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Domain')),
                $domainSelect
            ),
            0
        );

        /* Type field layour */
        $typeSection = $W->FlowItems()->setVerticalAlign('bottom');

        $typeFieldSet = new resourcemanagement_TypeFieldSet();
        $typeFields = $typeFieldSet->select(
            $typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION)
            ->_AND_($typeFieldSet->exportFilter->is(true))
        );
        $typeFields->orderAsc($typeFieldSet->type);
        $typeFields->orderDesc($typeFieldSet->order);
        $typeFields->orderAsc($typeFieldSet->id);

        $displayable = array();
        /* @var $typeField resourcemanagement_TypeField  */
        foreach ($typeFields as $typeField) {
            if(!isset($displayable[$typeField->type])){
                $displayable[$typeField->type] = $W->FlowItems()->setVerticalAlign('bottom')->setHorizontalSpacing(1, 'em');
                $typeSection->addItem($displayable[$typeField->type]);
            }
            $item = $W->LabelledWidget(
                $typeField->name,
                $typeField->getFilterInputWidget()
            );
            $displayable[$typeField->type]->addItem($W->NamedContainer('typefilter'.$typeField->type)->addItem($item));
        }

        $typeSelect = $W->Select();
        $typeSelect->setName('type');
        $typeSelect->addOption('', '');

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select();
        foreach ($types as $type) {
            $typeSelect->addOption($type->id, $type->name);
            if (isset($displayable[$type->id])) {
                $typeSelect->setAssociatedDisplayable($displayable[$type->id], array($type->id));
            }
        }

        //$filter = $filterPanel->getFilter();
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Type'))
                    ->colon(),
                $typeSelect
            ),
            1
        );
        $filterForm->addItem(
            $typeSection,
            2
        );

        $page->addItem($filterPanel);

        return $page;
    }



    /**
     * @param	array	$reservations		List parameters
     *
     * @return Widget_Action
     */
    public function exportList($reservations = null, $inline = true, $fileName = 'reservations.csv')
    {

        $filter = isset($reservations['filter']) ? $reservations['filter'] : array();


        $tableView = $this->tableView($filter);


        $csvExport = $tableView->exportCsv(';');

        //DOWNLOAD
        bab_setTimeLimit(3600);

        if (mb_strtolower(bab_browserAgent()) == 'msie') {
            header('Cache-Control: public');
        }

        if ($inline) {
            header('Content-Disposition: inline; filename="'.$fileName.'"'."\n");
        } else {
            header('Content-Disposition: attachment; filename="'.$fileName.'"'."\n");
        }

        $mime = 'text/csv';

        if (function_exists('mb_strlen')) {
            $size = mb_strlen($csvExport, '8bit');
        } else {
            $size = strlen($csvExport);
        }


        header('Content-Type: '.$mime."\n");
        header('Content-Length: '.$size."\n");
        header('Content-transfert-encoding: binary'."\n");

        echo $csvExport;

        die;
    }




    /**
     * When making a reservation while several resources are selected in the calendar,
     * this page proposes to choose which resource to book to the user then procedes
     * to Reservation()->add().
     *
     * @param string $date     The selected start date 'YYYY-MM-DD'
     * @param string $time     The selected start time 'hh:mm'
     * @param string $dateEnd  The selected end date 'YYYY-MM-DD'
     * @param string $timeEnd  The selected end time 'hh:mm'
     */
    public function selectResource($date = null, $time = null, $dateEnd = null, $timeEnd = null, $uid = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->setTitle(resourcemanagement_translate('Select resources to book'));

        $resourceSet = new resourcemanagement_ResourceSet();

        $selectedResources = resourcemanagement_getCalendarSelectedResources();

        if ($uid === null) {
            $uid = uniqid();
        }

        $resourcesBox = $W->Form()->setLayout($W->VBoxItems()->setVerticalSpacing(1, 'em'));

        $resources = $resourceSet->select(
            $resourceSet->id->in($selectedResources)
            ->_AND_($resourceSet->isBookable())
        );

        foreach ($resources as $resource) {
            $resourceInfo = $W->VBoxItems(
                $label = $W->Label($resource->name)
                    ->addClass('widget-strong'),
                $W->Label(bab_abbr($resource->description, BAB_ABBR_FULL_WORDS, 100))
            );
            $resourcesBox->addItem(
                $W->FlowItems(
                    $item = resourcemanagement_Controller()->Reservation(false)->getCheckbox($resource->id, $uid),
                    $resourceInfo
                )->setVerticalAlign('middle')
                ->setHorizontalSpacing(2, 'em')
                ->setSizePolicy('widget-list-element')
            );
        }

        $resources = $resourceSet->select(
            $resourceSet->id->notIn($selectedResources)
            ->_AND_($resourceSet->isBookable())
        );

        $otherSection = $W->Section(
            resourcemanagement_translate('Others resources'),
            $W->VBoxItems()
        )->setFoldable(true, !bab_isAjaxRequest());

        $other = false;
        foreach ($resources as $resource) {
            $other = true;
            $resourceInfo = $W->VBoxItems(
                $label = $W->Label($resource->name)
                    ->addClass('widget-strong'),
                $W->Label(bab_abbr($resource->description, BAB_ABBR_FULL_WORDS, 100))
            );
            $otherSection->addItem(
                $W->FlowItems(
                    $item = resourcemanagement_Controller()->Reservation(false)->getCheckbox($resource->id, $uid),
                    $resourceInfo
                )->setVerticalAlign('middle')
                ->setHorizontalSpacing(2, 'em')
                ->setSizePolicy('widget-list-element')
            );
        }

        if ($other) {
            $resourcesBox->addItem($otherSection);
        }

        if (bab_isAjaxRequest()){
            $resourcesBox->addItem(
                $W->Label(
                    resourcemanagement_translate('Close')
                )->addClass('widget-actionbutton widget-close-dialog')
            );
        } else {
            $resourcesBox->addItem(
                $W->Link(
                    resourcemanagement_translate('Select those'),
                    resourcemanagement_Controller()->Reservation()->add($date, $time, $dateEnd, $timeEnd, null, $uid)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_GO_NEXT)
                ->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC/* . ' widget-list-element'*/)
            );
        }

        $page->addItem($resourcesBox);

        return $page;
    }


    public function getCheckbox($id, $uid)
    {
        if(!isset($_SESSION['resourcemanagement']['selectedressource'])){
            $_SESSION['resourcemanagement']['selectedressource'] = array();
        }
        if(!isset($_SESSION['resourcemanagement']['selectedressource'][$uid])){
            $_SESSION['resourcemanagement']['selectedressource'][$uid] = array();
        }
        $checked = false;
        if(isset($_SESSION['resourcemanagement']['selectedressource'][$uid][$id]) && $_SESSION['resourcemanagement']['selectedressource'][$uid][$id]){
            $checked = true;
        }

        $W = bab_Widgets();
        return $W->CheckBox()
            ->setAjaxAction(resourcemanagement_Controller()->Reservation()->setReservation($id, $uid, !$checked))
            ->setReloadAction(resourcemanagement_Controller()->Reservation()->getCheckbox($id, $uid))
            ->setValue($checked)
            ->setName('resource['.$id.']');
    }


    public function setReservation($id, $uid, $checked)
    {
        if(!isset($_SESSION['resourcemanagement']['selectedressource'])){
            $_SESSION['resourcemanagement']['selectedressource'] = array();
        }
        if(!isset($_SESSION['resourcemanagement']['selectedressource'][$uid])){
            $_SESSION['resourcemanagement']['selectedressource'][$uid] = array();
        }
        $_SESSION['resourcemanagement']['selectedressource'][$uid][$id] = $checked;

        die;
    }


    /**
     * @param resourcemanagement_Equipment $equipment
     * @return Widget_Displayable_Interface
     */
    private function equipmentLine(resourcemanagement_Equipment $equipment)
    {
        $W = bab_Widgets();

        $availableQuantity = $equipment->getAvailableQuantity();

        if ($availableQuantity > 1) {
            $input = $W->LineEdit()
                ->setName(array('equipments', $equipment->id, 'quantity'))
                ->setSize(3)
                ->setSizePolicy('widget-5em');
            $box = $W->FlowItems(
                $W->Label($equipment->name)
                    ->setTitle($equipment->description)
                    ->setSizePolicy('widget-15em'),
                $input,
                $W->Label(' / ' .$availableQuantity)
                    ->setSizePolicy('widget-5em')
            )->setSizePolicy('widget-list-element');
        } else {
            $input = $W->CheckBox()
                ->setCheckedValue(1)
                ->setUncheckedValue(0)
                ->setName(array('equipments', $equipment->id, 'quantity'));
            $box = $W->FlowItems(
                $W->Label($equipment->name)
                    ->setTitle($equipment->description)
                ->setSizePolicy('widget-15em'),
                $input
            )->setSizePolicy('widget-list-element');
        }

        return $box;
    }



    public function ResourceForm($uid)
    {
        $resourcesArray = resourcemanagement_getCheckedResources($uid);
        $set = new resourcemanagement_ResourceSet();
        $resources = $set->select($set->id->in($resourcesArray));

        $W = bab_Widgets();

        //resourcemanagement_resourceShortPreview();

        $layout = $W->VBoxItems()
            ->setVerticalAlign('middle')
            ->setHorizontalSpacing(1.5, 'em')
            ->setVerticalSpacing(1, 'em')
            ->setId('rm-ResourceForm')
            ;

        $resourceBox = $W->Section(
            resourcemanagement_translate('Resources'),
            $layout
        );

        $noResource = true;
        foreach ($resources as $resource) {
            $noResource = false;
            $layout->addItem(
                $W->HBoxItems(
                    resourcemanagement_resourceShortPreview($resource),
                    $W->Link('', '')
                        ->setTitle(resourcemanagement_translate('Remove this resource'))
                        ->setConfirmationMessage(resourcemanagement_translate('This will remove this resource, proceed?'))
                        ->setAjaxAction(
                            resourcemanagement_Controller()->Reservation()->setReservation($resource->id, $uid, false),
                            array('rm-ResourceForm', 'rm-conditionalBookingForm', 'rm-reservedForForm', 'rm-typeForm')
                        )
                        ->addClass('icon ' . Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setSizePolicy(Func_Icons::ICON_LEFT_16)
                )->setVerticalAlign('middle')->setHorizontalSpacing(3, 'px')
            );

        }

//         if ($noResource) {
//             $layout->addItem($W->Hidden(null, 'mandatory', '')->setMandatory(true, resourcemanagement_translate('At least one ressource has to be selected.')));
//         }

        $layout->addItem(
            $W->Link(
                resourcemanagement_translate('Add'),
                resourcemanagement_Controller()->Reservation()->selectResource(null, null, null, null, $uid)
            )->setOpenMode(
                Widget_Link::OPEN_DIALOG_AND_RELOAD,
                array('rm-ResourceForm', 'rm-conditionalBookingForm', 'rm-reservedForForm', 'rm-typeForm')
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            //array('rm-ResourceForm', 'rm-conditionalBookingForm')
        );


        $equipmentBox = $W->Section(
            resourcemanagement_translate('Equipment'),
            $W->VBoxItems()
        );

        $equipmentSet = new resourcemanagement_EquipmentSet();
        $equipments = $equipmentSet->select($equipmentSet->resource->is(0));
        if ($equipments->count() > 0) {
            $resourceSection = $W->Section(
                'Mobile',
                $W->VBoxItems(),
                6
            )->setFoldable(true, true);

            $equipmentBox->addItem($resourceSection);
            foreach ($equipments as $equipment) {
                $resourceSection->addItem($this->equipmentLine($equipment));
            }
        }


        foreach ($resources as $resource) {
            $equipments = $equipmentSet->select($equipmentSet->resource->is($resource->id));
            if ($equipments->count() > 0) {
                $resourceSection = $W->Section(
                    $resource->name,
                    $W->VBoxItems(),
                    6
                )->setFoldable(true, true);
                $equipmentBox->addItem($resourceSection);
                foreach ($equipments as $equipment) {
                    $resourceSection->addItem($this->equipmentLine($equipment));
                }
            }
        }

        $box = $W->FlowItems(
            $resourceBox->setSizePolicy('widget-50pc'),
            $equipmentBox->setSizePolicy('widget-50pc')
        )->setVerticalAlign('top');

        $box->setReloadAction(resourcemanagement_Controller()->Reservation()->ResourceForm($uid));

        return $box;
    }



    public function conditionalBookingForm($uid)
    {
        $resourcesArray = resourcemanagement_getCheckedResources($uid);
        $set = new resourcemanagement_ResourceSet();
        $resources = $set->select($set->id->in($resourcesArray));

        $W = bab_Widgets();

        $conditionlBooking = true;
        foreach($resources as $resource){
            if (!$resource->allowConditionalBooking) {
                $conditionlBooking = false;
                break;
            }
        }

        if ($conditionlBooking) {
            $conditionalBookingItem = $W->CheckBox();
            if (bab_isAjaxRequest()) {
                $conditionalBookingItem->setName(array('reservation','conditionalBooking'));
            } else {
                $conditionalBookingItem->setName('conditionalBooking');
            }
            $layout = resourcemanagement_LabelledWidget(
                resourcemanagement_translate('Conditional booking'),
                $conditionalBookingItem
            )->setId('rm-conditionalBookingForm')->setReloadAction(resourcemanagement_Controller()->Reservation()->conditionalBookingForm($uid));
        } else {
            $layout = $W->FlowItems()->setId('rm-conditionalBookingForm')->setReloadAction(resourcemanagement_Controller()->Reservation()->conditionalBookingForm($uid));
        }

        return $layout;
    }

    public function reservedForForm($uid)
    {
        $resourcesArray = resourcemanagement_getCheckedResources($uid);
        $set = new resourcemanagement_ResourceSet();
        $resources = $set->select($set->id->in($resourcesArray));

        $W = bab_Widgets();

        $reservedFor = true;
        foreach($resources as $resource){
            if (!resourcemanagement_canCreateReservationForOthers($resource)) {
                $reservedFor = false;
                break;
            }
        }

        if (!isset($_SESSION['reservedForStatus'])) {
            $_SESSION['reservedForStatus'] = array();
        }
        if (!isset($_SESSION['reservedForStatus'][$uid])) {
            $_SESSION['reservedForStatus'][$uid] = -1;
        }

        if ($reservedFor) {
            $userItem = $W->UserPicker();
            $userItem->setSizePolicy('widget-30em');
            if (bab_isAjaxRequest()) {
                if ($_SESSION['reservedForStatus'][$uid] === true) {
                    header("HTTP/1.1 304 Not Modified");
                    die;
                }
                $userItem->setName(array('reservation','reservedFor'));
            } else {
                $userItem->setName('reservedFor');
            }
            $_SESSION['reservedForStatus'][$uid] = true;
            $layout = resourcemanagement_LabelledWidget(
                resourcemanagement_translate('Reserve for'),
                $userItem
            )->setId('rm-reservedForForm')->setReloadAction(resourcemanagement_Controller()->Reservation()->reservedForForm($uid));
        } else {
            if (bab_isAjaxRequest()) {
                if ($_SESSION['reservedForStatus'][$uid] === false) {
                    header("HTTP/1.1 304 Not Modified");
                    die;
                }
            }
            $_SESSION['reservedForStatus'][$uid] = false;
            $layout = $W->FlowLayout('rm-reservedForForm')->setReloadAction(resourcemanagement_Controller()->Reservation()->reservedForForm($uid));
        }

        return $layout;
    }

    public function typeForm($uid, $reservation = null)
    {
        $resourcesArray = resourcemanagement_getCheckedResources($uid);
        $set = new resourcemanagement_ResourceSet();
        $resources = $set->select($set->id->in($resourcesArray));

        $reservations = false;
        if ($reservation) {
            $reservations = resourcemanagement_getAssociatedReservations($reservation);
        }

        $W = bab_Widgets();

        $layout = $W->VBoxItems()->setId('rm-typeForm')->setReloadAction(resourcemanagement_Controller()->Reservation()->typeForm($uid));

        foreach ($resources as $resource) {
            $types = $resource->getTypes();
            foreach ($types as $type) {
                $typeSection = $W->Section(
                    $type->name . ' - ' . $resource->name,
                    $W->VBoxItems()
                        ->setVerticalSpacing(1, 'em'),
                    3
                );
                $typeSection->addClass('compact');
                if (bab_isAjaxRequest()) {
                    $typeSection->setName(array('reservation','types',$resource->id,'type' . $type->id));
                } else {
                    $typeSection->setName(array('types',$resource->id,'type' . $type->id));
                }

                $typeFieldSet = new resourcemanagement_TypeFieldSet();
                $typeFields = $typeFieldSet->select(
                    $typeFieldSet->type->is($type->id)
                        ->_AND_($typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
                );
                $typeFields->orderDesc($typeFieldSet->order);
                $typeFields->orderAsc($typeFieldSet->id);

                $reservationExt = false;
                if ($reservations && isset($reservations[$resource->id])) {
                    $reservationExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_RESERVATION);
                    $reservationExt = $reservationExtSet->get($reservations[$resource->id]);
                }

                if ($typeFields->count() > 0) {
                    /* @var $typeField resourcemanagement_TypeField  */
                    foreach ($typeFields as $typeField) {
                        $input = $typeField->getInputWidget();
                        if ($reservationExt) {
                            $fieldName = $typeField->getDynamicFieldName();
                            $input->setValue($reservationExt->$fieldName);
                        }
                        if ($typeField->mandatory) {
                            $input->setMandatory(true, sprintf(resourcemanagement_translate('The field "%s" is mantory.'), $typeField->name));
                        }
                        $item = $W->LabelledWidget(
                            $typeField->name,
                            $input
                        );
                        $typeSection->addItem($item);
                    }
                    $layout->addItem(
                        $typeSection
                    );
                }
            }
        }

        return $layout;
    }



    public function participantsForm($id, $itemId = null)
    {
        $W = bab_Widgets();
        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }


        $box->setReloadAction($this->proxy()->participantsForm($id, $box->getId()));
        return $box;
    }

    /**
     * Displays the reservation form to book the specified resource.
     *
     * @param string $date
     * @param string $time
     * @param string $dateEnd
     * @param string $timeEnd
     * @param string $resource
     */
    public function add($date = null, $time = null, $dateEnd = null, $timeEnd = null, $resource = null, $uid = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');

        $resource = bab_rp('resource', null);
        $checkedResources = array();
        //         if (!isset($resource)) {
//             if (isset($uid)) {
//                 $checkedResources = resourcemanagement_getCheckedResources($uid);
//                 if (count($checkedResources) == 0) {
//                     resourcemanagement_redirect(resourcemanagement_Controller()->Reservation()->selectResource($date, $time, $dateEnd, $timeEnd));
//                 }
//             } else {
//                 $selectedResources = resourcemanagement_getCalendarSelectedResources();
//                 if (count($selectedResources) == 1) {
//                     // Get first (and unique) element of the array.
//                     $selectedResourceId = reset($selectedResources);
//                     resourcemanagement_redirect(resourcemanagement_Controller()->Reservation()->add($date, $time, $dateEnd, $timeEnd, $selectedResourceId));
//                 } else {
//                     resourcemanagement_redirect(resourcemanagement_Controller()->Reservation()->selectResource($date, $time, $dateEnd, $timeEnd));
//                 }
//             }
//         } else {
//             $checkedResources = array($resource);
//             $uid = uniqid();
//             $_SESSION['resourcemanagement']['selectedressource'][$uid][$resource] = true;
//         }

        $resourceSet = new resourcemanagement_ResourceSet();
        $resources = $resourceSet->select($resourceSet->id->in($checkedResources));

        if (!resourcemanagement_canCreateReservation($checkedResources)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        foreach ($resources as $resource) {
            if ($resource->unavailable) {
                //$page->addError(resourcemanagement_translate('The resource is temporarily unavailable.'));

                $page->addItem(
                    $W->VBoxItems(
                        $W->Title(
                            sprintf(resourcemanagement_translate('The resource "%s" is temporarily unavailable.'), $resource->name),
                            1
                        ),
                        $W->Label(
                            resourcemanagement_translate('The resource has been marked as temporarily unavailable by its managers.')
                        ),
                        $W->Link(
                            resourcemanagement_translate('Go back to calendar'),
                            resourcemanagement_Controller()->Calendar()->display()
                        )->addClass('widget-actionbutton')
                    )->addClass('BabLoginMenuBackground', 'widget-bordered')
                    ->setVerticalSpacing(2, 'em')
                );

                return $page;
            }
        }


        if ($time == '00:00' && $timeEnd == '00:00' && count($resources) == 1) {

            // If there is no specified times we try to find the resource opening hours for the
            // reservation week day.

            foreach ($resources as $resource) {

            }//Select the only element

            $startDay = BAB_DateTime::fromIsoDateTime($date . ' 00:00:00');
            /* @var $startWeekDay int 	0 => monday, 2 => tuesday, ..., 6 => sunday */
            $startWeekDay = ($startDay->getDayOfWeek() - 1) % 6;


            $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();
            $resourceAvailability = $resourceAvailabilitySet->get(
                $resourceAvailabilitySet->resource->is($resource->id)
                    ->_AND_($resourceAvailabilitySet->startweekday->is($startWeekDay))
            );

            if ($resourceAvailability) {
                $time = substr($resourceAvailability->starttime, 0, 5);
                $endDate =  BAB_DateTime::fromIsoDateTime($date . ' ' . $time . ':00');
                $endDate->add(1, BAB_DATETIME_HOUR);
                list($dateEnd, $timeEnd) = explode(' ', $endDate->getIsoDateTime());
                $timeEnd = substr($timeEnd, 0, 5);

                // We use the opening hour for start and opening hour +1 for the end of the reservation period.

                $startDateTime = $date . ' ' . $time;
                $endDateTime = $dateEnd . ' ' . $timeEnd;

            } else {

                // If there is no opening hour specified for the week day,
                // we dont show a default hour on the reservation form.
                $startDateTime = $date;
                $endDateTime = $dateEnd;
            }
        } else {
            $startDateTime = $date . ' ' . $time;
            $endDateTime = $dateEnd . ' ' . $timeEnd;
        }


        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        $record = $set->newRecord();
        $record->start = $startDateTime;
        $record->end = $endDateTime;
        $record->setRessources($uid);
        $record->status = resourcemanagement_Reservation::STATUS_DRAFT;

        $editor = new resourcemanagement_ReservationEditor($record);

        $editor->addButtons(
            $W->SubmitButton()
                ->setAction(resourcemanagement_Controller()->Reservation()->save())
                ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                ->validate()
                ->setLabel(resourcemanagement_translate('Save')),
            $W->Link(
                resourcemanagement_translate('Cancel'),
                resourcemanagement_Controller()->Calendar()->display()
            )->addClass('widget-actionbutton', 'widget-close-dialog')
        );

        $page->setTitle(resourcemanagement_translate('New reservation'));

        $page->addItem($editor);

        return $page;
    }





    public function fullFrame($reservation = null)
    {
        $W = bab_Widgets();

        $set = new resourcemanagement_ReservationSet();
        $record = $set->get($reservation);
        $record->extend();

        $frame = new resourcemanagement_ReservationFrame();
        $frame->setReservation($record);

        $frame->initialize();

        if (resourcemanagement_canUpdateReservation($record)) {

            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Edit this reservation'),
                    resourcemanagement_Controller()->Reservation()->edit($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
            );

            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Delete this reservation'),
                    resourcemanagement_Controller()->Reservation()->delete($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            );

            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Copy this reservation'),
                    resourcemanagement_Controller()->Reservation()->copy($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_COPY)
            );


            if ($record->status == resourcemanagement_Reservation::STATUS_OPTION) {

                $frame->addButtons(
                    $W->Link(
                        resourcemanagement_translate('Confirm this reservation'),
                        resourcemanagement_Controller()->Reservation()->confirm($record->id)
                    )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_OK)
                );

            }

        }

        if (resourcemanagement_canUpdateReservationApprobation($record)) {

            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Approve'),
                    resourcemanagement_Controller()->Reservation()->approved($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_OK)
            );

            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Reject'),
                    resourcemanagement_Controller()->Reservation()->reject($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            );

        }

        // 		$editor->addButtons(
        // 			$W->Link(
        // 				resourcemanagement_translate('Cancel'),
        // 				resourcemanagement_Controller()->Calendar()->display()
        // 			)->addClass('widget-actionbutton')
        // 		);

        return $frame;
    }



    /**
     * Displays the page with the reservation information.
     *
     * @param string $reservation		The reservation id or null to create a new reservation.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function display($event = null, $popup = null)
    {
        $W = bab_Widgets();

        $reservationFrame = $this->fullFrame($event);

        if ($this->isAjaxRequest()) {
            return $reservationFrame;
        }

        $page = $W->BabPage();
        $page->setCurrentItemMenu('');

        $page->addItem($reservationFrame);
        if ($popup) {
            $page->setEmbedded(false);
        } else {
            $page->addItemMenu('calendarall', resourcemanagement_translate('CSN Calendar'), resourcemanagement_Controller()->Calendar()->displayAll()->url());
            $page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
            if (resourcemanagement_isManager()) {
                $page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
            }
        }

        return $page;
    }




    /**
     *
     * @param resourcemanagement_Reservation[] $conflicts
     */
    protected function conflictsBox($conflicts)
    {
        $W = bab_Widgets();
        $conflictsBox = $W->VBoxItems(
            $W->Title(resourcemanagement_translate('This reservation is in conflict with'), 5)
        );
        $conflictsBox->setVerticalSpacing(1, 'em');
        $conflictsBox->addClass('widget-bordered');

        foreach ($conflicts as $conflict) {

            $periodDateStart = bab_shortDate(bab_mktime($conflict->start), false);
            $periodDateEnd = bab_shortDate(bab_mktime($conflict->end), false);

            if ($periodDateStart == $periodDateEnd) {
                $periodText = sprintf(resourcemanagement_translate('On %s, from %s to %s'), bab_shortDate(bab_mktime($conflict->start), false), bab_time(bab_mktime($conflict->start)), bab_time(bab_mktime($conflict->end)));
            } else {
                $periodText = sprintf(resourcemanagement_translate('From %s to %s'), bab_shortDate(bab_mktime($conflict->start), true), bab_shortDate(bab_mktime($conflict->end), true));
            }

            $resourceSet = new resourcemanagement_ResourceSet();
            $resource = $resourceSet->get($conflict->resource->id);

            $conflictsBox->addItem(
                $W->FlowItems(
                    $W->Link(
                        $conflict->description.' ('.$resource->name.')',
                        resourcemanagement_Controller()->Reservation()->display($conflict->id)
                    ),
                    $W->Label($periodText)
                )->setHorizontalSpacing(1, 'ex')
            );
        }

        return $conflictsBox;
    }




    public function reedit($reservation, $conflicts = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');


        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        if(isset($reservation['id']) && $reservation['id']) {
            $record = $set->get($reservation['id']);
        } else {
            $record = $set->newRecord();
        }
        $record->setValues($reservation);
        $record->setRessources($reservation['uid']);

        $checkedResources = resourcemanagement_getCheckedResources($reservation['uid']);
        foreach($checkedResources as $checkedResource) {
            $record->resource = $checkedResource;
            if (!resourcemanagement_canUpdateReservation($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }
            $record->resource = '';
        }
        $page->addItem($W->Title(resourcemanagement_translate('Edit reservation')));


        if (isset($conflicts) && !empty($conflicts)) {
            $conflictsBox = $this->conflictsBox($conflicts);
            $page->addItem($conflictsBox);
        }

        $editor = new resourcemanagement_ReservationEditor($record);

        $editor->addClass('BabLoginMenuBackground');
        $editor->addClass('widget-bordered');

        $editor->setValues($reservation, array('reservation'));
        if ($record->recurrence->rrule) {
            $editor->addButtons(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateThisReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Only this instance')),
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateFollowingReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('All following')),
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateAllReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('All reservations in the series')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Calendar()->display()
                )->addClass('widget-actionbutton')
            );


        } else {
            $editor->addButtons(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->save())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Save')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Calendar()->display()
                )->addClass('widget-actionbutton')
            );

        }


        $page->addItem($editor);

        return $page;
    }




    /**
     * Displays the page with the reservation editor.
     *
     * @param string $reservation		The reservation id or null to create a new reservation.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($event = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');

        if (!isset($event)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        $record = $set->get($event);

        if (!resourcemanagement_canUpdateReservation($record)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $page->setTitle(resourcemanagement_translate('Edit reservation'));

        $editor = new resourcemanagement_ReservationEditor($record);

        if ($record->recurrence->rrule) {

            $editor->addButtons(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateThisReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Only this instance')),
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateFollowingReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('All following')),
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->updateAllReservation())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('All reservations in the series')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Calendar()->display()
                )->addClass('widget-actionbutton')
            );


        } else {

            $editor->addButtons(
                $W->SubmitButton()
                    ->setAction(resourcemanagement_Controller()->Reservation()->save())
                    ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                    ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                    ->validate()
                    ->setLabel(resourcemanagement_translate('Save')),
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Calendar()->display()
                )->addClass('widget-actionbutton')
            );

        }


        $page->addItem($editor);

        return $page;
    }


    protected function createReccurrence()
    {
        $recurrenceSet = new resourcemanagement_RecurrenceSet();
        $recurrence = $recurrenceSet->newRecord();
        $recurrence->rrule = '';
        //$recurrence->rrule = $reservation['rrule'];
        //$recurrence->save();

        return $recurrence;
    }


    /**
     * @param $saveType array(
     * 	1 => only the instance
     * 	2 => only following
     * 	3 => all
     * )
     */
    protected function saveMultipleInstance($reservation, $saveType = 1)
    {
        $exception = new resourcemanagement_SaveReservationException();
        $set = new resourcemanagement_ReservationSet();
        $checkedResources = resourcemanagement_getCheckedResources($reservation['uid']);

        //IGINITIAL VALIDATION
        $reservation = $this->validate($reservation);

        //IGNORE EVENT THAT WILL BE UPDATE FOR CONFLICT
        $ignoreReservations = array();
        $resas = array();
        $start = array();
        if (isset($reservation['id']) && $reservation['id'] && $record = $set->get($reservation['id'])) {
            $reservation['recurrence'] = $record->recurrence;
            $criteria = $set->recurrence->is($record->recurrence);
            if($saveType == 1) {
                $criteria = $set->start->is($record->start)->_AND_($set->end->is($record->end))->_AND_($criteria);
            } elseif ($saveType == 2) {
                $criteria = $set->start->greaterThanOrEqual($record->start)->_AND_($criteria);
            }

            $periods = $set->select($criteria);
            foreach ($periods as $resa) {
                $ignoreReservations[$resa->id] = $resa->id;
                $resas[$resa->id] = $resa;
                if(!isset($start[$resa->start.'+'.$resa->end])) {
                    $start[$resa->start.'+'.$resa->end] = array();
                }
                $start[$resa->start.'+'.$resa->end][$resa->resource] = true;
            }
        } else {
            //CHECK CREATION
            foreach($checkedResources as $resource) {
                $reservation['resource'] = $resource;
                $exception->mergeError($this->checkBefore_save($reservation, array()));
            }
        }

        //CHECK
        $tmpresa = $reservation;
        $warnings = array();
        foreach($resas as $resa) {
            $tmpresa['id'] = $resa->id;
            $tmpresa['resource'] = $resa->resource;
            if (isset($checkedResources[$resa->resource])) {//DELETED
                if($saveType != 1) {
                    //YOU CAN'T CHANGE DATE OF OTHER RECURING EVENTS ONLY HOURS
                    list($date, $time) = explode(' ', $resa->start);
                    $tmpresa['startDate'] = $date;
                    list($date, $time) = explode(' ', $resa->end);
                    $tmpresa['endDate'] = $date;
                }

                $exception->mergeError($this->checkBefore_save($tmpresa, $ignoreReservations));
            }
        }

        //CHECK ADDED
        $tmpresa = $reservation;
        foreach($start as $dates => $resources) {
            $dates = explode('+', $dates);
            foreach ($checkedResources as $resource) {
                if(!isset($resources[$resource])) {
                    $tmpresa['id'] = null;
                    $tmpresa['resource'] = $resource;
                    if($saveType != 1) {
                        list($date, $time) = explode(' ', $dates[0]);
                        $tmpresa['startDate'] = $date;
                        list($date, $time) = explode(' ', $dates[1]);
                        $tmpresa['endDate'] = $date;
                    }

                    $exception->mergeError($this->checkBefore_save($tmpresa, $ignoreReservations));
                }
            }
        }

        if (!isset($reservation['recurrence']) || !$reservation['recurrence']) {
            $recurrence = $this->createReccurrence();
        } else {
            $recurrence = $record->recurrence();
        }

        //CHECK ADDED RRULE
        if(!$recurrence->rrule && $reservation['rrule']) {
            $recurrence->rrule = $reservation['rrule'];

            $reservation['start'] = ORM_DateField('startDate')->input($reservation['startDate']) . ' ' . ORM_TimeField('startTime')->input($reservation['startTime'] . ':00');
            $reservation['end'] = ORM_DateField('endDate')->input($reservation['endDate']) . ' ' . ORM_TimeField('endTime')->input($reservation['endTime'] . ':00');

            $startTs = bab_mktime($reservation['start']);
            $endTs = bab_mktime($reservation['end']);

            $duration = $endTs - $startTs;
            $tmpresa = $reservation;
            foreach ($checkedResources as $resource) {
                $dates = $this->getPeriodsStart($reservation);
                array_shift($dates);
                foreach ($dates as $periodStart) {
                    $tmpresa['id'] = null;
                    $tmpresa['resource'] = $resource;

                    $periodStartTs = bab_mktime($periodStart);
                    $periodEndTs = $periodStartTs + $duration;
                    $periodEnd = date('Y-m-d H:i:s', $periodEndTs);

                    list($date, $time) = explode(' ', $periodStart);
                    $tmpresa['startDate'] = $date;
                    list($date, $time) = explode(' ', $periodEnd);
                    $tmpresa['endDate'] = $date;
                    $exception->mergeError($this->checkBefore_save($tmpresa, $ignoreReservations));
                }
            }
        } else {
            unset($reservation['rrule']);
        }

        $exception->throwIfError();
        $recurrence->save();
        $reservation['recurrence'] = $recurrence->id;

        //SAVE or Delete
        $tmpresa = $reservation;
        foreach($resas as $resa) {
            $tmpresa['id'] = $resa->id;
            if (!isset($checkedResources[$resa->resource])) {//DELETED
                $resa->delete();
            } else {
                $tmpresa['resource'] = $resa->resource;
                if($saveType != 1) {
                    //YOU CAN'T CHANGE DATE OF OTHER RECURING EVENTS ONLY HOURS
                    list($date, $time) = explode(' ', $resa->start);
                    $tmpresa['startDate'] = $date;
                    list($date, $time) = explode(' ', $resa->end);
                    $tmpresa['endDate'] = $date;
                }

                $this->_reservationSave($tmpresa);
            }
        }

        //ADDED
        $tmpresa = $reservation;
        foreach($start as $dates => $resources) {
            $dates = explode('+', $dates);
            foreach ($checkedResources as $resource) {
                if(!isset($resources[$resource])) {
                    $tmpresa['id'] = null;
                    $tmpresa['resource'] = $resource;
                    if($saveType != 1) {
                        list($date, $time) = explode(' ', $dates[0]);
                        $tmpresa['startDate'] = $date;
                        list($date, $time) = explode(' ', $dates[1]);
                        $tmpresa['endDate'] = $date;
                    }

                    $this->_reservationSave($tmpresa);
                }
            }
        }

        //CREATION
        if (!isset($reservation['id']) || !$reservation['id']/* || !$set->get($reservation['id'])*/) {
        	$tmpresa = $reservation;
            foreach($checkedResources as $resource) {
                $tmpresa['id'] = null;
                $tmpresa['resource'] = $resource;
                $this->_reservationSave($tmpresa);
            }
        }

        //ADDED RRULE
        if(isset($reservation['rrule'])) {
            $reservation['start'] = ORM_DateField('startDate')->input($reservation['startDate']) . ' ' . ORM_TimeField('startTime')->input($reservation['startTime'] . ':00');
            $reservation['end'] = ORM_DateField('endDate')->input($reservation['endDate']) . ' ' . ORM_TimeField('endTime')->input($reservation['endTime'] . ':00');

            $startTs = bab_mktime($reservation['start']);
            $endTs = bab_mktime($reservation['end']);

            $duration = $endTs - $startTs;
            $tmpresa = $reservation;
            foreach ($checkedResources as $resource) {
                $dates = $this->getPeriodsStart($reservation);
                array_shift($dates);
                foreach ($dates as $periodStart) {
                    $tmpresa['id'] = null;
                    $tmpresa['resource'] = $resource;

                    $periodStartTs = bab_mktime($periodStart);
                    $periodEndTs = $periodStartTs + $duration;
                    $periodEnd = date('Y-m-d H:i:s', $periodEndTs);

                    list($date, $time) = explode(' ', $periodStart);
                    $tmpresa['startDate'] = $date;
                    list($date, $time) = explode(' ', $periodEnd);
                    $tmpresa['endDate'] = $date;

                    $this->_reservationSave($tmpresa);
                }
            }
        }

        return true;
    }


    private function getPeriodsStart($reservation)
    {
        require_once dirname(__FILE__).'/../RRule.php';

        $dates = array();

        if (!empty($reservation['rrule'])) {
            $startDate = BAB_DateTime::fromIsoDateTime($reservation['start']);

            $begin = BAB_DateTime::fromIsoDateTime($reservation['start']);
            $end = BAB_DateTime::fromIsoDateTime($reservation['start']);
            $end->add(365, BAB_DATETIME_DAY);
            $endIso = $end->getIsoDateTime();

            $rrule = new RRule(new iCalDate($startDate->getICal(false)), $reservation['rrule']);


            while (($startNext = $rrule->GetNext()) && ($startNextIso = $startNext->Render()) < $endIso) {
                $dates[] = $startNextIso;
            }

        } else {
            $dates[] = $this->start;
        }

        return $dates;
    }


    /**
     *
     * @param $saveType array(
     * 	1 => only the instance
     * 	2 => only following
     * 	3 => all
     * )
     * @param array $reservation		The reservation data as posted by the reservation editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function updateReservation($reservation, $saveType = 1)
    {
        try{
            $this->saveMultipleInstance($reservation, $saveType);
        } catch (resourcemanagement_SaveReservationException $e) {
            $messages = $e->getMessages();
            if(!empty($messages)) {
                $page = $this->reedit($reservation, $e->getConflicts());

                $messages = $e->getMessages();
                if ($messages && !empty($messages)) {
                    foreach ($messages as $msg) {
                        $page->addError($msg);
                    }
                }
                return $page;
            }
        }

        resourcemanagement_redirect(resourcemanagement_Controller()->Calendar()->display());
    }

    public function updateThisReservation($reservation = null)
    {
        return $this->updateReservation($reservation, 1);
    }

    public function updateFollowingReservation($reservation = null)
    {
        return $this->updateReservation($reservation, 2);
    }

    public function updateAllReservation($reservation = null)
    {
        return $this->updateReservation($reservation, 3);
    }




    /**
     * Checks validity of reservation instance $record.
     * Returns an array of errors containing the following keys :
     * 'msg' => array of strings of error messages.
     * 'conflicts' => array of conflicts
     * 'warning' => string
     *
     * @param resourcemanagement_Reservation	$record				The reservation instance record to check.
     * @param array 							$reservation		Reservation data from posted form.
     * @param array 							$noConflicts		Reservations that should be update in this update and should be ignore for conflicts.
     */
    protected function checkInstanceUpdate($record, $reservation, $noConflicts = array())
    {
        $exception = new resourcemanagement_SaveReservationException();

        $record = clone $record;
        $record->setFormInputValues($reservation);

        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($record->resource);

        // Check minimum period before reservation.
        if ($resource->reservationDelayMin > 0) {
            $minDate = BAB_DateTime::now();
            $minDate->add($resource->reservationDelayMin, BAB_DATETIME_DAY);

            $minDateIso = $minDate->getIsoDateTime();

            if ($record->start < $minDateIso) {
                $exception->addMessage(
                    sprintf(
                        resourcemanagement_translate('You cannot make or edit a reservation for the resource "%s" less than %s days before the beginning of the reservation.'),
                        $resource->name,
                        $resource->reservationDelayMin
                    )
                );
            }
        }

        // Check maximum period before reservation.
        if ($resource->reservationDelayMax > 0) {
            $maxDate = BAB_DateTime::now();
            $maxDate->add($resource->reservationDelayMax, BAB_DATETIME_DAY);

            $maxDateIso = $maxDate->getIsoDateTime();

            if ($record->start > $maxDateIso) {
                $exception->addMessage(
                    sprintf(
                        resourcemanagement_translate('You cannot make or edit a reservation for the resource "%s" more than %s days before the beginning of the reservation.'),
                        $resource->name,
                        $resource->reservationDelayMax
                    )
                );
            }
        }

        // Check resource availability (aka opening hours).
        $resourceAvailable = $record->checkResourceAvailabilityHours();
        if (!$resourceAvailable) {
            $exception->addMessage(
                sprintf(
                    resourcemanagement_translate('The reservation is outside the opening hours of the resource "%s".'),
                    $resource->name
                )
            );
        }

        //Check conflicts
        if (!$resource->surbooking) {
            $conflicts = $record->checkConflicts($noConflicts);
            if (!empty($conflicts)) {
                $exception->addMessage(
                    sprintf(
                        resourcemanagement_translate('The resource "%s" is already reserved during this period.'),
                        $resource->name
                    )
                );
                foreach ($conflicts as $conflict) {
                    $exception->addConflict($conflict);
                }
            }
        }


        return $exception;
    }


    /**
     * Does no verification, should always be calle after _save method
     */
    protected function _reservationSave($reservation)
    {
        $set = new resourcemanagement_ReservationSet();
        if (!empty($reservation['id'])) {
            $record = $set->get($reservation['id']);
        } else {
            $record = $set->newRecord();
        }

        $reservation['start'] = ORM_DateField('startDate')->input($reservation['startDate']) . ' ' . ORM_TimeField('startTime')->input($reservation['startTime'] . ':00');
        $reservation['end'] = ORM_DateField('endDate')->input($reservation['endDate']) . ' ' . ORM_TimeField('endTime')->input($reservation['endTime'] . ':00');

        $record->setFormInputValues($reservation);

        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($record->resource);

        $startTs = bab_mktime($record->start);
        $endTs = bab_mktime($record->end);

        $duration = $endTs - $startTs;

        if (isset($reservation['conditionalBooking']) && $reservation['conditionalBooking'] == 1) {
            $record->status = resourcemanagement_Reservation::STATUS_OPTION;
        } else {//OPTION IS A PRIORITY, WORK FLOW WILL BE CREATED IN CONFIRM METHOD
	        if ($resource->approbation != 0) {
	            // An approbation workflow is defined for this resource.
	            $record->createApprobationInstance();
	        }
        }

        $record->save();

        if ($record->status != resourcemanagement_Reservation::STATUS_WAITING && $record->status != resourcemanagement_Reservation::STATUS_OPTION) {
            $this->notify($record);
        }

        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($record->resource)
        );
        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type;
            $reservationExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_RESERVATION);
            if (!isset($reservation['types'][$reservation['resource']][$reservationExtSet->getName()])) {
                continue;
            };
            $reservationExt = $reservationExtSet->get($record->id);
            if (!isset($reservationExt)) {
                $reservationExt = $reservationExtSet->newRecord();
            }
            $reservationExt->id = $record->id;
            $reservationExt->setFormInputValues($reservation['types'][$reservation['resource']][$reservationExtSet->getName()]);
            $reservationExt->save();
        }
    }



    /**
     *
     * @param array $reservation		The reservation data as posted by the reservation editor.
     *
     * @throws Exception
     * @return
     */
    public function _save($reservation = null)
    {
        $this->_reservationSave($reservation);
    }

    /**
     * Return correctly inialized and validate $reservation
     *
     *
     * @return array()
     */
    protected function validate($reservation)
    {
        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        if (!isset($reservation['uid']) || !$reservation['uid']) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing ressource selected.'));
        }

        $exception = new resourcemanagement_SaveReservationException();

        $modifiedOn = date('Y-m-d H:i:s');
        if (empty($reservation['id'])) {
            $reservation['createdOn'] = $modifiedOn;
        }
        $reservation['modifiedOn'] = $modifiedOn;
        $reservation['modifiedBy'] = bab_getUserId();

        if (trim($reservation['endDate']) === '') {
            $reservation['endDate'] = $reservation['startDate'];
        }
        $reservation['start'] = ORM_DateField('startDate')->input($reservation['startDate']) . ' ' . ORM_TimeField('startTime')->input($reservation['startTime'] . ':00');
        $reservation['end'] = ORM_DateField('endDate')->input($reservation['endDate']) . ' ' . ORM_TimeField('endTime')->input($reservation['endTime'] . ':00');

        if ($reservation['repeat'] == 1) {
            try {
                $rrule = resourcemanagement_getRRuleString($reservation['start'], $reservation['end'], $reservation['rule']);
            } catch (ErrorException $e) {
                $exception->addMessage($e->getMessage());
                /*$page = $this->reedit($reservation);
                $page->addError($e->getMessage());
                return $page;*/
            }
        } else {
            $rrule = '';
        }

        $reservation['rrule'] = $rrule;
        if (!isset($reservation['reservedBy']) || !$reservation['reservedBy']) {
            $reservation['reservedBy'] = bab_getUserId();
        }

        if ($reservation['end'] < $reservation['start']) {
            $exception->addMessage(resourcemanagement_translate('The period end should come after the period start.'));
            /*$page = $this->reedit($reservation);
            $page->addError(resourcemanagement_translate('The period end should come after the period start.'));
            return $page;*/
        }
        if ($reservation['end'] == $reservation['start']) {
            $exception->addMessage(resourcemanagement_translate('The period should not have a null duration.'));
            /*$page = $this->reedit($reservation);
            $page->addError(resourcemanagement_translate('The period should not have a null duration.'));
            return $page;*/
        }

        $exception->throwIfError();

        return $reservation;
    }



    /**
     *
     * @param array $reservation		The reservation data as posted by the reservation editor.
     *
     * @throws Exception
     * @return
     */
    protected function checkBefore_save($reservation = null, $noConflicts = array())
    {
        $set = new resourcemanagement_ReservationSet();

        if (!resourcemanagement_canCreateReservation($reservation['resource'])) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        //$modifiedOn = date('Y-m-d H:i:s');

        /* @var $links resourcemanagement_Reservation[] */
        $links = array();
        /* @var $record resourcemanagement_Reservation */
        if (!empty($reservation['id'])) {
            $record = $set->get($reservation['id']);
            //$links[$record->resource] = $record;
        } else {
            $record = $set->newRecord();
        }

        $reservation['start'] = ORM_DateField('startDate')->input($reservation['startDate']) . ' ' . ORM_TimeField('startTime')->input($reservation['startTime'] . ':00');
        $reservation['end'] = ORM_DateField('endDate')->input($reservation['endDate']) . ' ' . ORM_TimeField('endTime')->input($reservation['endTime'] . ':00');

        return $this->checkInstanceUpdate($record, $reservation, $noConflicts);
    }





    /**
     *
     * @param array $reservation		The reservation data as posted by the reservation editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($reservation = null/*, $message = null*/)//$message parameter seams to not do anypthing
    {
        return $this->updateReservation($reservation);
    }





    /**
     *
     * @param int $reservation		The reservation id.
     *
     * @throws Exception
     * @return boolean
     */
    public function confirm($reservation = null)
    {

        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }


        $set = new resourcemanagement_ReservationSet();
        //		$set->resource();

        /* @var $record resourcemanagement_Reservation */
        $record = $set->get($reservation);

        if (!resourcemanagement_canUpdateReservation($record)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }



        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($record->resource);

        if ($resource->approbation != 0) {
            // An approbation workflow is defined for this resource.
            $record->createApprobationInstance();
        }

        $record->status = resourcemanagement_Reservation::STATUS_OK;

        $record->save();


        if ($record->status == resourcemanagement_Reservation::STATUS_WAITING) {
            $message = sprintf(resourcemanagement_translate('The resource %s has been reserved, pending approbation.'), $resource->name);
        } elseif ($record->status == resourcemanagement_Reservation::STATUS_OPTION) {
            $message = sprintf(resourcemanagement_translate('The resource %s has been conditionaly reserved, pending confirmation.'), $resource->name);
        } else {
            $message = sprintf(resourcemanagement_translate('The resource %s has been reserved.'), $resource->name);
            $this->notify($record);
        }


        resourcemanagement_redirect(resourcemanagement_Controller()->Calendar()->display(), $message);
    }




    /**
     * Notifies members of resourcemanagement_domainresourcenotified_groups and resourcemanagement_resourcenotified_groups
     * about a new resource reservation.
     *
     * @param resourcemanagement_Reservation $reservation
     */
    public function notify($reservation)
    {
        $mailer = bab_mail();
        $Spooler = @bab_functionality::get('Mailspooler');
        $spoolerHashes = array();

        if (!$mailer) {
            return;
        }

        $resource = $reservation->resource();
        $reservedBy = bab_getUserName($reservation->reservedBy, true);

        $periodDateStart = bab_shortDate(bab_mktime($reservation->start), false);
        $periodDateEnd = bab_shortDate(bab_mktime($reservation->end), false);

        if ($periodDateStart == $periodDateEnd) {
            $periodText = sprintf(
                resourcemanagement_translate('On %s, from %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), false),
                bab_time(bab_mktime($reservation->start)),
                bab_time(bab_mktime($reservation->end))
            );
        } else {
            $periodText = sprintf(
                resourcemanagement_translate('From %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), true),
                bab_shortDate(bab_mktime($reservation->end), true)
            );
        }

        $emailTitle = sprintf(
            resourcemanagement_translate('_NOTIFICATION_RESERVATION_TITLE_'),
            $resource->name,
            $reservedBy,
            $periodText,
            $reservation->description
        );

        $template = new resourcemanagement_ReservationNotificationTemplate($reservation, $emailTitle, '');

        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $htmlMessage = $mailer->mailTemplate($addon->printTemplate($template, 'mailinfo.html', 'inforeservation'));
        $txtMessage = $addon->printTemplate($template, 'mailinfo.html', 'inforeservationtxt');
        $notifiedUsers = $reservation->getNotifiedUsers();

        foreach ($notifiedUsers as $notifiedUser) {
            $email = bab_getUserEmail($notifiedUser);
            if (empty($email)) {
                continue;
            }

            $mailer->mailSubject($emailTitle);
            $mailer->mailBody($htmlMessage, 'html');
            $mailer->mailAltBody($txtMessage);
            $mailer->clearAllRecipients();
            $mailer->mailTo($email);

            /* @var $Spooler Func_Mailspooler */
            if ($Spooler) {
                $spoolerHash = $Spooler->save($mailer);
                $spoolerHashes[$spoolerHash] = $spoolerHash;
            }
        }

        foreach ($spoolerHashes as $spoolerHash) {
            $mail = $Spooler->getMail($spoolerHash);
            $mail->send();
        }
    }


    /**
     * Displays the page with the reservation editor.
     *
     * @param string $reservation		The reservation id or null to create a new reservation.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function copy($event = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');

        if (!isset($event)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        $record = $set->get($event);

        if (!resourcemanagement_canUpdateReservation($record)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $page->addItem($W->Title(resourcemanagement_translate('Edit reservation')));



        $editor = new resourcemanagement_ReservationEditor($record);
        $editor->addClass('BabLoginMenuBackground');
        $editor->addClass('widget-bordered');
        $rruleForm = resourcemanagement_getRRuleForm($record->recurrence->rrule);
        $editor->copyReservation($record, $rruleForm);

        $editor->addButtons(
            $W->SubmitButton()
                ->setAction(resourcemanagement_Controller()->Reservation()->save())
                ->setSuccessAction(resourcemanagement_Controller()->Calendar()->display())
                ->setFailedAction(resourcemanagement_Controller()->Reservation()->edit())
                ->validate()
                ->setLabel(resourcemanagement_translate('Save')),
            $W->Link(
                resourcemanagement_translate('Cancel'),
                resourcemanagement_Controller()->Calendar()->display()
            )->addClass('widget-actionbutton')
        );


        $page->addItem($editor);

        return $page;
    }

    public function delete($reservation = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('reservations');


        $layout = $W->VBoxLayout();
        $layout->setVerticalSpacing(2, 'em');

        $page->addItem($layout);

        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        $record = $set->get($reservation);
        if (!isset($record)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        if (!resourcemanagement_canUpdateReservation($record)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied.'));
        }

        $resources = resourcemanagement_getAssociatedReservationsObject($record->id);

        if ($record->recurrence->rrule) {
            $layout->addItem(
                $W->Title(
                    resourcemanagement_translate(
                        'Delete recurring reservation with this resource',
                        'Delete recurring reservation with those resources',
                        count($resources)
                    ),
                    2
                )
            );

            if ($resources) {
                $resourceLayout = $W->HBoxItems()->setHorizontalSpacing(2, 'em');
                foreach ($resources as $resource) {
                    $resourceLayout->addItem(resourcemanagement_resourceShortPreview($resource->resource(), $resource->id));
                }
                $layout->addItem(
                    $resourceLayout
                );
            }

            $layout->addItem(

                $W->VBoxItems(

                    $W->Label(resourcemanagement_translate('Would you like to delete only this event, all events in the series, or this and all future events in the series?'))
                        ->addClass('widget-strong'),

                    $W->FlowItems(
                        $W->Link(
                            resourcemanagement_translate('Only this instance'),
                            resourcemanagement_Controller()->Reservation()->deleteInstance($record->id)
                        )->addClass('widget-actionbutton', 'widget-20em', 'widget-align-center'),
                        $W->Label(resourcemanagement_translate('All other events in the series will remain.'))
                    )->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('middle'),

                    $W->FlowItems(
                        $W->Link(
                            resourcemanagement_translate('All following'),
                            resourcemanagement_Controller()->Reservation()->deleteInstanceFollowing($record->id)
                        )->addClass('widget-actionbutton', 'widget-20em', 'widget-align-center'),
                        $W->Label(resourcemanagement_translate('This and all the following events will be deleted.'))
                    )->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('middle'),

                    $W->FlowItems(
                        $W->Link(
                            resourcemanagement_translate('All reservations in the series'),
                            resourcemanagement_Controller()->Reservation()->deleteAll($record->id)
                        )->addClass('widget-actionbutton', 'widget-20em', 'widget-align-center'),
                        $W->Label(resourcemanagement_translate('All events in the series will be deleted.'))
                    )->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('middle')

                )->addClass('widget-bordered', 'BabLoginMenuBackground')
                ->setVerticalSpacing(1, 'em')
            );

            $layout->addItem(
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Reservation()->display($record->id)
                )->addClass('widget-actionbutton')
            );

        } else {

            $layout->addItem(
                $W->Title(
                    resourcemanagement_translate(
                        'Do you really want to delete the event associated to this resource?',
                        'Do you really want to delete the event associated to those resources?',
                        count($resources)
                    ),
                    2
                )
            );

            if ($resources) {
                $resourceLayout = $W->HBoxItems()->setHorizontalSpacing(2, 'em');
                foreach ($resources as $resource) {
                    $resourceLayout->addItem(resourcemanagement_resourceShortPreview($resource->resource(), $resource->id));
                }
                $layout->addItem(
                    $resourceLayout
                );
            }

            $layout->addItem(
                $W->FlowItems(
                    $W->Link(
                        resourcemanagement_translate('Confirm deletion'),
                        resourcemanagement_Controller()->Reservation()->deleteInstance($record->id)
                    )->addClass('widget-actionbutton'),

                    $W->Link(
                        resourcemanagement_translate('Cancel'),
                        resourcemanagement_Controller()->Reservation()->display($record->id)
                    )->addClass('widget-actionbutton')
                )->setHorizontalSpacing(2, 'em')
                ->addClass('widget-bordered', 'BabLoginMenuBackground')
            );
        }

        return $page;
    }





    public function deleteInstance($reservation = null)
    {
        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        $set = new resourcemanagement_ReservationSet();

        $ress = resourcemanagement_getAssociatedReservationsObject($reservation);
        foreach ($ress as $res) {
            $record = $set->get($res->id);
            if (!isset($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.').'(2)');
            }

            if (!resourcemanagement_canUpdateReservation($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied.'));
            }
        }

        foreach ($ress as $res) {
            $set->delete($set->id->is($res->id));

            $recurrences = $set->select($set->recurrence->is($res->recurrence));
            if($recurrences->count() == 0) {
                $recurrenceSet = new resourcemanagement_RecurrenceSet();
                $recurrenceSet->delete($recurrenceSet->id->is($res->recurrence));
            }

            $this->notifyDeletion($res);
        }

        resourcemanagement_redirect(resourcemanagement_Controller()->Calendar()->display());
    }





    public function deleteInstanceFollowing($reservation = null)
    {
        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        $set = new resourcemanagement_ReservationSet();


        $ress = resourcemanagement_getAssociatedReservationsObject($reservation);
        foreach ($ress as $res) {
            $record = $set->get($res->id);
            if (!isset($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
            }

            if (!resourcemanagement_canUpdateReservation($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied.'));
            }
        }

        foreach ($ress as $res) {
            $record = $set->get($res->id);
            $start = $record->start;

            $set->delete(
                $set->start->greaterThanOrEqual($start)
                ->_AND_($set->recurrence->is($record->recurrence))
            );

            $recurrences = $set->select($set->recurrence->is($record->recurrence));
            if($recurrences->count() == 0) {
                $recurrenceSet = new resourcemanagement_RecurrenceSet();
                $recurrenceSet->delete($recurrenceSet->id->is($record->recurrence));
            }

            $this->notifyDeletion($record);
        }

        resourcemanagement_redirect(resourcemanagement_Controller()->Calendar()->display());
    }





    public function deleteAll($reservation = null)
    {
        if (!isset($reservation)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
        }

        $set = new resourcemanagement_ReservationSet();

        $ress = resourcemanagement_getAssociatedReservationsObject($reservation);
        foreach ($ress as $res) {
            $record = $set->get($res->id);
            if (!isset($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation information.'));
            }

            if (!resourcemanagement_canUpdateReservation($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied.'));
            }
        }

        foreach ($ress as $res) {
            $record = $set->get($res->id);
            if ($record) {//shouldnot
                $set->delete($set->recurrence->is($record->recurrence));
                $recurrenceSet = new resourcemanagement_RecurrenceSet();
                $recurrenceSet->delete($recurrenceSet->id->is($record->recurrence));
                $this->notifyDeletion($record);
            }
        }

        resourcemanagement_redirect(resourcemanagement_Controller()->Calendar()->display());
    }





    /**
     * Notifies members of resourcemanagement_domainresourcenotified_groups and resourcemanagement_resourcenotified_groups
     * about a new resource reservation.
     *
     * @param resourcemanagement_Reservation $reservation
     */
    public function notifyDeletion($reservation)
    {
        $mailer = bab_mail();
        $Spooler = @bab_functionality::get('Mailspooler');
        $spoolerHashes = array();

        if (!$mailer) {
            return;
        }

        $resource = $reservation->resource();
        $reservedBy = bab_getUserName($reservation->reservedBy, true);

        $periodDateStart = bab_shortDate(bab_mktime($reservation->start), false);
        $periodDateEnd = bab_shortDate(bab_mktime($reservation->end), false);

        if ($periodDateStart == $periodDateEnd) {
            $periodText = sprintf(
                resourcemanagement_translate('On %s, from %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), false),
                bab_time(bab_mktime($reservation->start)),
                bab_time(bab_mktime($reservation->end))
            );
        } else {
            $periodText = sprintf(
                resourcemanagement_translate('From %s to %s'),
                bab_shortDate(bab_mktime($reservation->start), true),
                bab_shortDate(bab_mktime($reservation->end), true)
            );
        }

        $emailTitle = sprintf(
            resourcemanagement_translate('_NOTIFICATION_RESERVATION_DELETION_TITLE_'),
            $resource->name,
            $reservedBy,
            $periodText,
            $reservation->description
        );

        $template = new resourcemanagement_ReservationNotificationTemplate($reservation, $emailTitle, '');

        $addon = bab_getAddonInfosInstance('resourcemanagement');
        $htmlMessage = $mailer->mailTemplate($addon->printTemplate($template, 'mailinfo.html', 'inforeservation'));
        $txtMessage = $addon->printTemplate($template, 'mailinfo.html', 'inforeservationtxt');

        $notifiedUsers = $reservation->getNotifiedUsers();

        foreach ($notifiedUsers as $notifiedUser) {
            $email = bab_getUserEmail($notifiedUser);
            if (empty($email)) {
                continue;
            }

            $mailer->mailSubject($emailTitle);
			$mailer->mailBody($htmlMessage, 'html');
            $mailer->mailAltBody($txtMessage);
            $mailer->clearAllRecipients();
            $mailer->mailTo($email);

            /* @var $Spooler Func_Mailspooler */
            if ($Spooler) {
                $spoolerHash = $Spooler->save($mailer);
                $spoolerHashes[$spoolerHash] = $spoolerHash;
            }
        }

        foreach ($spoolerHashes as $spoolerHash) {
            $mail = $Spooler->getMail($spoolerHash);
            $mail->send();
        }
    }





    /**
     * Moves the event by the specified offset in days and/or minutes relatively to its current start.
     *
     * @param int	$event		     The calendar + event id (calendarId:eventId, eg.: public/2:b8512ece-9cb9-4cee-97ea-59988cea389e)
     * @param int	$offsetDays
     * @param int	$offsetMinutes
     * @param int	$offsetDuration  The offset in minute of the duration.
     * @return Widget_Action
     */
    public function move($event = null, $offsetDays = null, $offsetMinutes = null, $offsetDuration = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $reservationSet = new resourcemanagement_ReservationSet();
        $reservationSet->recurrence();
        /* @var $reservation resourcemanagement_Reservation */
        $reservation = $reservationSet->get($event);

        if (!resourcemanagement_canUpdateReservation($reservation)) {
            echo bab_json_encode(
                array('error' => resourcemanagement_translate('Sorry, you are not allowed to move this reservation.'))
            );
            die;
        }

        $start = bab_DateTime::fromIsoDateTime($reservation->start);
        $end = bab_DateTime::fromIsoDateTime($reservation->end);

        if (isset($offsetDays)) {
            $start->add($offsetDays, BAB_DATETIME_DAY);
            $end->add($offsetDays, BAB_DATETIME_DAY);
        }
        if (isset($offsetMinutes)) {
            $start->add($offsetMinutes, BAB_DATETIME_MINUTE);
            $end->add($offsetMinutes, BAB_DATETIME_MINUTE);
        }
        if (isset($offsetDuration)) {
            $end->add($offsetDuration, BAB_DATETIME_MINUTE);
        }

        $reservation->start = $start->getIsoDateTime();
        $reservation->end = $end->getIsoDateTime();

        $conflicts = $reservation->checkConflicts();

        if (empty($conflicts)) {
            $reservation->save();

            $resourceAvailable = $reservation->checkResourceAvailabilityHours();
            if (!$resourceAvailable) {
                echo bab_json_encode(
                    array('warning' => resourcemanagement_translate('The reservation is outside the opening hours of the resource.'))
                );
            }
        } else {
            $errorMessage = resourcemanagement_translate('This reservation is in conflict with other reservations:');

            foreach ($conflicts as $conflict) {

                $periodDateStart = bab_shortDate(bab_mktime($conflict->start), false);
                $periodDateEnd = bab_shortDate(bab_mktime($conflict->end), false);

                if ($periodDateStart == $periodDateEnd) {
                    $periodText = sprintf(resourcemanagement_translate('On %s, from %s to %s'), bab_shortDate(bab_mktime($conflict->start), false), bab_time(bab_mktime($conflict->start)), bab_time(bab_mktime($conflict->end)));
                } else {
                    $periodText = sprintf(resourcemanagement_translate('From %s to %s'), bab_shortDate(bab_mktime($conflict->start), true), bab_shortDate(bab_mktime($conflict->end), true));
                }

                $errorMessage .= "\n" .	$conflict->description . ' ' . $periodText;
            }


            echo bab_json_encode(
                array('error' => $errorMessage)
            );
        }

        die;
    }




    /**
     * Displays a page for the reservation approver.
     *
     * @param int $reservation		The reservation id.
     * @return Widget_BabPage
     */
    public function editApprobation($reservation)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->setTitle(resourcemanagement_translate('Approbation'));

        $set = new resourcemanagement_ReservationSet();
        $set->recurrence();
        $record = $set->get($reservation);
//        $record->extend();

        $frame = new resourcemanagement_ReservationFrame();
        $frame->setReservation($record);

        $frame->initialize();

        if (resourcemanagement_canUpdateReservation($record)) {
            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Edit this reservation'),
                    resourcemanagement_Controller()->Reservation()->edit($record->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
            );
        }

        $frame->addClass('widget-bordered');

        $page->addItem($frame);

        $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $form->addClass('widget-bordered');

        $page->addItem($form);


        $form->setHiddenValue('reservation', $record->id);
        $form->setHiddenValue('tg', bab_rp('tg'));

        $form->colon();

        $nbInstances = 0;
        if ($record->recurrence->rrule != '') {
            $instances = $set->select(
                $set->status->is(resourcemanagement_Reservation::STATUS_WAITING)
                ->_AND_($set->recurrence->id->is($record->recurrence->id))
            );
            $nbInstances = $instances->count();
        }



        if (resourcemanagement_canUpdateReservationApprobation($record)) {
	        if ($nbInstances > 1) {
	            $form->addItem(
	                $W->LabelledWidget(
	                    resourcemanagement_translate('This reservation is repeated on several dates, do you want to update'),
	                    $W->select()
	                        ->addOption('0', resourcemanagement_translate('Only this date'))
	                        ->addOption('1', resourcemanagement_translate('All dates') . ' (' . $nbInstances . ')'),
	                    'updateAll'
	                )
	            );
	        }

	        $form->addItem(
	            $W->LabelledWidget(
	                resourcemanagement_translate('Reason'),
	                $W->TextEdit()
	                    ->addClass('widget-100pc'),
	                'comment'
	            )
	        );
	        $form->addItem(
	            $W->FlowItems(
	                $W->SubmitButton()
	                ->setLabel(resourcemanagement_translate('Accept'))
	                ->setAction(resourcemanagement_Controller()->Reservation()->approved())
	                ->addClass('widget-15em', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_OK),
	                $W->SubmitButton()
	                ->setLabel(resourcemanagement_translate('Reject'))
	                ->setAction(resourcemanagement_Controller()->Reservation()->reject())
	                ->addClass('widget-15em', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
	            )->setHorizontalSpacing(2, 'em')
	            ->addClass(Func_Icons::ICON_LEFT_16)
	        );
		}

        $page->setEmbedded(false);

        return $page;
    }




    /**
     * Marks the reservation as accepted and returns to the approbations page.
     *
     * @param int		$reservation
     * @param bool      $updateAll
     * @param string	$comment
     */
    public function approved($reservation = null, $updateAll = false, $comment = '')
    {
        $set = new resourcemanagement_ReservationSet();
        $record = $set->get($reservation);

    	if (resourcemanagement_canUpdateReservationApprobation($record)) {
	        if ($updateAll) {
	            $instances = $set->select(
	                $set->status->is(resourcemanagement_Reservation::STATUS_WAITING)
	                ->_AND_($set->recurrence->is($record->recurrence))
	            );
	        } else {
	            $instances = array($record);
	        }

	        $validatedInstances = array();

	        foreach ($instances as $instance) {
	            $approbation = $instance->approbation;

	            $reservations = $set->select(
	                $set->approbation->is($approbation)
	                ->_AND_($set->status->is(resourcemanagement_Reservation::STATUS_WAITING))
	            );

	            foreach ($reservations as $reservation) {
	                $reservation->saveApprobationStep(true, $comment);
	                if ($reservation->status == resourcemanagement_Reservation::STATUS_OK) {
	                    $validatedInstances[$reservation->id] = $reservation;
	                }
	            }
	        }

	        if (count($validatedInstances) > 0) {
	            resourcemanagement_notifyValidation($record, $comment);
				$this->notify($record);
	        }
        }

        resourcemanagement_redirect(resourcemanagement_Controller()->Reservation()->acknowledgeChange());
    }



    /**
     * Marks the reservation as rejected and returns to the approbations page.
     *
     * @param int		$reservation
     * @param bool      $updateAll
     * @param string	$comment
     */
    public function reject($reservation = null, $updateAll = false, $comment = '')
    {
        $set = new resourcemanagement_ReservationSet();
        $record = $set->get($reservation);

    	if (resourcemanagement_canUpdateReservationApprobation($record)) {
	        if ($updateAll) {
	            $instances = $set->select(
	                $set->status->is(resourcemanagement_Reservation::STATUS_WAITING)
	                ->_AND_($set->recurrence->is($record->recurrence))
	            );
	        } else {
	            $instances = array($record);
	        }

	        $rejectedInstances = array();

	        foreach ($instances as $instance) {
	            $approbation = $instance->approbation;

	            $reservations = $set->select(
	                $set->approbation->is($approbation)
	                ->_AND_($set->status->is(resourcemanagement_Reservation::STATUS_WAITING))
	            );

	            foreach ($reservations as $reservation) {
	                $reservation->saveApprobationStep(false, $comment);
	                if ($reservation->status == resourcemanagement_Reservation::STATUS_REJECTED) {
	                    $rejectedInstances[$reservation->id] = $reservation;
	                }
	            }
	        }

	        if (count($rejectedInstances) > 0) {
	            resourcemanagement_notifyRejection($record, $comment);
	        }
        }
        resourcemanagement_redirect(resourcemanagement_Controller()->Reservation()->acknowledgeChange());
    }




    public function acknowledgeChange()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->setTitle(resourcemanagement_translate('Approbation'));

        $page->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('The approbation has been saved.')),
                $W->Link(resourcemanagement_translate('Close'))
                    ->addAttribute('onclick', 'window.opener.location.reload();window.close()')
                    ->addClass('widget-actionbutton')
            )->setVerticalSpacing(1, 'em')
            ->addClass('widget-bordered', 'widget-align-center')
        );

        $page->setEmbedded(false);

        return $page;
    }


    /**
     * Find reservations that exceed options limits for their associated resource.
     * These reservation are marked rejected and an email is sent to the owners.
     */
    public function deleteOptionLimitExceeded()
    {
        $set = new resourcemanagement_ReservationSet();
        $reservations = $set->select($set->hasOptionLimitExceeded());

        foreach ($reservations as $reservation) {
            resourcemanagement_notifyOptionDeletion($reservation);
            $reservation->status = resourcemanagement_Reservation::STATUS_REJECTED;
            $reservation->save();
        }

        die;
    }
}