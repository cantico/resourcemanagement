<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/resource.ui.php';
require_once dirname(__FILE__).'/../ui/domain.ui.php';
require_once dirname(__FILE__).'/../set/recurrence.class.php';
require_once dirname(__FILE__).'/../set/resource.class.php';
require_once dirname(__FILE__).'/../set/resourceavailability.class.php';
require_once dirname(__FILE__).'/../set/reservation.class.php';


/**
 *
 */
class resourcemanagement_CtrlResource extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ResourceSet	$resourceSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $resourceSet)
    {

        // Initial conditions are based on "manage" access rights.
        $conditions = $resourceSet->isManageable();

        if (isset($filter['domain']) && !empty($filter['domain'])) {
            $conditions = $conditions->_AND_($resourceSet->isInDomain($filter['domain']));
        }

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($resourceSet->name->contains($filter['name']));
        }

        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($resourceSet->description->contains($filter['description']));
        }

        if (isset($filter['allowConditionalBooking'])) {
            if ($filter['allowConditionalBooking'] === '0') {
                $conditions = $conditions->_AND_($resourceSet->allowConditionalBooking->is(false));
            } elseif ($filter['allowConditionalBooking'] === '1') {
                $conditions = $conditions->_AND_($resourceSet->allowConditionalBooking->is(true));
            }
        }

        if (isset($filter['type']) && !empty($filter['type'])) {
            $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
            $conditions = $conditions->_AND_($resourceSet->id->in($resourceTypeSet->type->is($filter['type']), 'resource'));
        }

        return $conditions;
    }




    /**
     *
     * @param array	$filter
     * @return resourcemanagement_ResourceTableView
     */
    protected function tableView($filter = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();
        $resourceSet->domain();

        $tableView = new resourcemanagement_ResourceTableView();

        $tableView->addDefaultColumns($resourceSet);
        $conditions = $this->getFilterCriteria($filter, $resourceSet);
        $selection = $resourceSet->select($conditions);

        $tableView->setDataSource($selection);

        return $tableView;
    }


    public function downloadUnavailableICS($resource)
    {
        $resourceSet = new resourcemanagement_ResourceSet();
        $resource = $resourceSet->get($resourceSet->id->is($resource));
        $resource_name = str_replace(' ', '_', $resource->name);

        $timezone = date_default_timezone_get();
        if (strtolower($timezone) === 'system/localtime') {
            $timezone = 'Europe/Paris';
        }

        header('Content-Disposition: attachment; filename="'.$resource_name.'_' . date('Y-m-d_H-i-s') . '.ics"'."\n");
        header('Content-Type: text/calendar');
        //header('Content-Length: '. $size ."\n");
        header('Content-transfert-encoding: binary'."\n");
        header('Pragma: no-cache');
        header('Expires: 0');

        echo 'BEGIN:VCALENDAR'."\r\n";
        echo 'VERSION:2.0'."\r\n";
        echo 'PRODID:-//Cantico//NONSGML Ovidentia CalDAV Client//EN'."\r\n";
        echo 'BEGIN:VTIMEZONE'."\r\n";
        echo 'TZID:Europe/Paris'."\r\n";
        echo 'X-LIC-LOCATION:Europe/Paris'."\r\n";
        echo 'BEGIN:DAYLIGHT'."\r\n";
        echo 'TZOFFSETFROM:+0100'."\r\n";
        echo 'TZOFFSETTO:+0200'."\r\n";
        echo 'TZNAME:CEST'."\r\n";
        echo 'DTSTART:19700329T020000'."\r\n";
        echo 'RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU'."\r\n";
        echo 'END:DAYLIGHT'."\r\n";
        echo 'BEGIN:STANDARD'."\r\n";
        echo 'TZOFFSETFROM:+0200'."\r\n";
        echo 'TZOFFSETTO:+0100'."\r\n";
        echo 'TZNAME:CET'."\r\n";
        echo 'DTSTART:19701025T030000'."\r\n";
        echo 'RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU'."\r\n";
        echo 'END:STANDARD'."\r\n";
        echo 'END:VTIMEZONE'."\r\n";

        $reservationSet = new resourcemanagement_ReservationSet();
        $reservations = $reservationSet->select(
            $reservationSet->resource->is($resource->id)
            ->_AND_($reservationSet->status->is(resourcemanagement_Reservation::STATUS_OK))
        );

        foreach($reservations as $reservation) {
            $start = BAB_DateTime::fromIsoDateTime($reservation->start);
            $end = BAB_DateTime::fromIsoDateTime($reservation->end);

            echo 'BEGIN:VEVENT'."\r\n";
            echo 'DTSTART;TZID='.$timezone.':'.$start->getICal()."\r\n";
            echo 'DTEND;TZID='.$timezone.':'.$end->getICal()."\r\n";
            echo 'SUMMARY:'.resourcemanagement_translate('Unavailable')."\r\n";
            echo 'DESCRIPTION:'.resourcemanagement_translate('Unavailable')."\r\n";
            //echo 'SUMMARY:'.$reservation->description."\r\n";
            //echo 'DESCRIPTION:'.$reservation->longDescription."\r\n";
            echo 'END:VEVENT'."\r\n";
        }

        echo 'END:VCALENDAR'."\r\n";

        die();
    }


    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $W->Link(
                resourcemanagement_translate('Create a new resource'),
                $this->proxy()->edit()
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );

        $box->addItem($toolbar);


        $tableView = $this->tableView($filter);
        if (isset($itemId)) {
            $tableView->setId($itemId);
        }
        $tableView->setAjaxAction();

        $filter = $tableView->getFilterValues();

        $filterPanel = $tableView->filterPanel('resources', $filter);

        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isManaged());
        $domainSelect = resourcemanagement_DomainSelect($domains, true);
        $domainSelect->setName('domain');
        $filterForm = $filterPanel->getFilter();
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Domain')),
                $domainSelect
                ),
            0
        );

        $typeSelect = $W->Select();
        $typeSelect->setName('type');
        $typeSelect->addOption('', '');

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select();
        foreach ($types as $type) {
            $typeSelect->addOption($type->id, $type->name);
        }

        $filter = $filterPanel->getFilter();
        $filter->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Type'))
                ->colon(),
                $typeSelect
                ),
            1
        );
        $box->addItem($filterPanel);

        $box->setReloadAction($this->proxy()->filteredView(null, null, $tableView->getId()));

        return $box;
    }


    /**
     * Displays a filterable list of resources.
     *
     * @param array $resources		Filter data.
     */
    public function displayList($resources = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('resources', resourcemanagement_translate('Resources'), resourcemanagement_Controller()->Resource()->displayList()->url());
        $page->setCurrentItemMenu('resources');


        $filter = isset($resources['filter']) ? $resources['filter'] : array();


        $page->setTitle(resourcemanagement_translate('Resources management'));


        $filteredView = $this->filteredView($filter);

        $page->addItem($filteredView);

        return $page;
    }




    /**
     *
     * @param resourcemanagement_ResourceFrame $resource
     */
    public function fullFrame($resource = null)
    {
        $W = bab_Widgets();

        $set = new resourcemanagement_ResourceSet();

        $resource = $set->getExtended($resource);

        if (!isset($resource)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $frame = new resourcemanagement_ResourceFrame();
        $frame->setResource($resource);

        $frame->initialize();

        if (resourcemanagement_canUpdateResource($resource)) {
            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Edit'),
                    resourcemanagement_Controller()->Resource()->edit($resource->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
            );
        }

        if ($resource->isViewable()) {
            $frame->addButtons(
                $W->Link(
                    resourcemanagement_translate('Export reservations'),
                    resourcemanagement_Controller()->Calendar()->ical($resource->id)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_DOWN)
            );
        }

        return $frame;
    }







    /**
     *
     * @param string $resource
     * @return Widget_BabPage
     */
    public function display($resource = null)
    {
        $resourceFrame = $this->fullFrame($resource);

        if ($this->isAjaxRequest()) {
            return $resourceFrame;
        }

        $W = bab_Widgets();

        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('resources', resourcemanagement_translate('Resources'), resourcemanagement_Controller()->Resource()->displayList()->url());
        $page->setCurrentItemMenu('resources');

        $page->addItem($resourceFrame);

        return $page;
    }







    /**
     * Displays the page with the resource editor.
     *
     * @param string $resource		The resource id or null to create a new resource.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($resource = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('resources', resourcemanagement_translate('Resources'), resourcemanagement_Controller()->Resource()->displayList()->url());

        $editor = new resourcemanagement_ResourceEditor();
        $editor->addClass('widget-50em');
        $editor->setPersistent(true, Widget_Widget::STORAGE_SESSION);

        if (isset($resource)) {

            $set = new resourcemanagement_ResourceSet();
            $record = $set->get($resource);

            if (!resourcemanagement_canUpdateResource($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->addItemMenu('resource', resourcemanagement_translate('Resource'), resourcemanagement_Controller()->Resource()->edit($record->id)->url());
            $page->addItemMenu('availability', resourcemanagement_translate('Schedule'), resourcemanagement_Controller()->Resource()->editAvailability($record->id)->url());
            $page->setCurrentItemMenu('resource');

            $page->setTitle(resourcemanagement_translate('Edit resource') . ' ' . $record->name);

            $editor->setResource($record);

        } else {

            $editor->setResource(null);
            $page->setTitle(resourcemanagement_translate('New resource'));

        }

        $page->addItem($editor);

        return $page;
    }





    /**
     *
     * @param resourcemanagement_Resource $resource
     */
    protected function saveImages(resourcemanagement_Resource $resource)
    {
        $W = bab_Widgets();

        $files = $W->ImagePicker()->getTemporaryFiles('mainphoto');

        if (isset($files)) {
            $imagePath = $resource->mainImagePath();
            try {
                $imagePath->deleteDir();
            } catch (bab_FolderAccessRightsException $e) { }
            $imagePath->createDir();

            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $resource->importMainImage($filePickerItem);
            }
        }

        $files = $W->ImagePicker()->getTemporaryFiles('photo');

        if (isset($files)) {
            $imagePath = $resource->imagePath();
            try {
                $imagePath->deleteDir();
            } catch (bab_FolderAccessRightsException $e) { }
            $imagePath->createDir();
            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $resource->importImage($filePickerItem);
            }
        }


    }





    /**
     *
     * @param array $resource		The resource data as posted by the resource editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($resource = null)
    {

        if (!isset($resource)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing resource information.'));
        }

        $set = new resourcemanagement_ResourceSet();

        /* @var $record resourcemanagement_Resource */
        if (!empty($resource['id'])) {

            $record = $set->get($resource['id']);

            if (!resourcemanagement_canUpdateResource($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

        } else {

            if (!resourcemanagement_canCreateResource($resource['domain'])) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $record = $set->newRecord();
        }


        $record->setFormInputValues($resource);

        $record->save();


        if (isset($resource['types']) && !empty($resource['types'])) {
            $record->setTypes($resource['types']);
        }

        $this->saveImages($record);


        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
        $resourceTypeSet->type();
        $resourceTypes = $resourceTypeSet->select(
            $resourceTypeSet->resource->is($record->id)
        );

        foreach ($resourceTypes as $resourceType) {
            $type = $resourceType->type();
            $resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
            if (!isset($resource[$resourceExtSet->getName()])) {
                continue;
            }
            $resourceExt = $resourceExtSet->get($record->id);
            if (!isset($resourceExt)) {
                $resourceExt = $resourceExtSet->newRecord();
                $resourceExt->id = $record->id;
            }
            $resourceExt->setFormInputValues($resource[$resourceExtSet->getName()]);
            $resourceExt->save();
        }


        return true;
    }



    public function setResourceEditorTypes($editor, $resource = null)
    {
        $_SESSION['resourcemanagement']['editor'][$editor]['types'] = explode(',', $resource['types'][0]);
        die;
    }




    public function dynamicEditorFrame($editor)
    {
        $typeIds = $_SESSION['resourcemanagement']['editor'][$editor]['types'];

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select($typeSet->id->in($typeIds));

        $editor = new resourcemanagement_ResourceEditor();

        $frame = $editor->dynamicSectionsFrame($types);
        $frame->setName('resource');

        return $frame;
    }




    /**
     * Displays a page to edit the weekly availability of a resource.
     *
     * @param int $resource
     * @return Widget_BabPage
     */
    public function editAvailability($resource = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();
        /* @var $resource resourcemanagement_Resource */
        $resource = $resourceSet->get($resource);

        if (!isset($resource)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('resources', resourcemanagement_translate('Resources'), resourcemanagement_Controller()->Resource()->displayList()->url());
        $page->addItemMenu('resource', resourcemanagement_translate('Resource'), resourcemanagement_Controller()->Resource()->edit($resource->id)->url());
        $page->addItemMenu('availability', resourcemanagement_translate('Schedule'), resourcemanagement_Controller()->Resource()->editAvailability($resource->id)->url());
        $page->setCurrentItemMenu('availability');


        $page->addStyleSheet('toolbar.css');

        $page->addItem($W->Title(resourcemanagement_translate('Resource availability'), 1));
        $page->addItem(resourcemanagement_resourceShortPreview($resource));

        $availabilityBox = $W->DelayedItem($this->proxy()->availability($resource->id))
            ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_ON_PAGE_LOAD);

        $page->addItem(
            $W->VBoxItems(
                $availabilityBox
            )
            ->setVerticalSpacing(1, 'em')
        );

        return $page;
    }





    public function availability($resource = null)
    {
        $W = bab_Widgets();

        $resourceSet = new resourcemanagement_ResourceSet();
        /* @var $resource resourcemanagement_Resource */
        $resource = $resourceSet->get($resource);

        if (!isset($resource)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $vbox = $W->VBoxItems();
        $vbox->setVerticalSpacing(1, 'em');


        $instantEditor = $W->VboxItems(
            $W->Link(
                $W->Icon(resourcemanagement_translate('Add a period'), Func_Icons::ACTIONS_LIST_ADD),
                $this->proxy()->availability()
            )
            ->addClass('bab_toolbarItem icon widget-instant-button widget-actionbutton')
        )->addClass('widget-instant-container');

        $instantEditor->addItem(
            $this->availabilityPeriodEditor($resource->id, $instantEditor)
                ->setTitle(resourcemanagement_translate('Add a period'))
                ->addClass('widget-instant-form')
        );

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $instantEditor
        );

        $toolbar->addItem(
            $W->FlowItems(
                $W->CheckBox()
                    ->setValue($resource->unavailable)
                    ->setAjaxAction(
                        resourcemanagement_Controller()->Resource()->makeTemporarilyUnavailable($resource->id)
                    ),
                $W->Label(resourcemanagement_translate('The resource is temporarily unavailable.'))
            )->setVerticalSpacing(1, 'ex')
            ->setVerticalAlign('middle')
        );


        $vbox->addItem($toolbar);


        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();

        $availableColumns = array(
            widget_TableModelViewColumn($resourceAvailabilitySet->startweekday, resourcemanagement_translate('Start weekday'))
                ->addClass('widget-col-5em'),

            widget_TableModelViewColumn($resourceAvailabilitySet->starttime, resourcemanagement_translate('Start time'))
                ->addClass('widget-col-5em'),

            widget_TableModelViewColumn($resourceAvailabilitySet->endtime, resourcemanagement_translate('End time'))
                ->addClass('widget-col-5em'),

            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-col-5em')
        );

        $conditions = $resourceAvailabilitySet->resource->is($resource->id);

        $resourceAvailabilities = $resourceAvailabilitySet->select($conditions);
        $resourceAvailabilities->orderAsc($resourceAvailabilitySet->startweekday);

        $tableView = new resourcemanagement_AvailabilityTableView();

        $tableView->setDataSource($resourceAvailabilities);

        $tableView->setAvailableColumns($availableColumns);

        $vbox->addItem($tableView);


        return $vbox;
    }





    public function availabilityPeriodEditor($resource, $reloadItem)
    {
        $W = bab_Widgets();

        $startDaySelect = resourcemanagement_WeekDaySelect();

        $editor = $W->Form();

        $editor->setHiddenValue('tg', bab_rp('tg'));

        $editor->setName('availability');

        $editor->setLayout($W->VBoxLayout());

        $editor->addItem(
            $W->FlowItems(
                $W->LabelledWidget(resourcemanagement_translate('Day'), $startDaySelect, 'startweekday')
                    ->setSizePolicy('widget-10em'),
                $W->LabelledWidget(resourcemanagement_translate('Start hour'), $W->TimeEdit(), 'starttime')
                    ->setSizePolicy('widget-10em'),
                $W->LabelledWidget(resourcemanagement_translate('End hour'), $W->TimeEdit(), 'endtime')
                    ->setSizePolicy('widget-10em')
            )
        );
        $editor->addItem($W->Hidden()->setName('resource')->setValue($resource));

        $submitButton = $W->SubmitButton();

        $editor->addItem(
            $submitButton
        );
        $submitButton->setAjaxAction($this->proxy()->saveAvailability(), $reloadItem);

        return $editor;
    }





    /**
     * Saves/updates a resource availability information.
     *
     * @param array		$availability      An indexed array containing the data to store in the
     *                                     resource availability set.
     *
     * @return Widget_Action
     */
    public function saveAvailability($availability = null)
    {
        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();

        if (isset($availability['id'])) {
            $newAvailability = false;
            $record = $resourceAvailabilitySet->get($availability['id']);
            if (!isset($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Trying to access unknown id.'));
            }
        } else {
            $newAvailability = true;
            $record = $resourceAvailabilitySet->newRecord();
        }

        if (empty($availability['endweekday'])) {
            $availability['endweekday'] = $availability['startweekday'];
        }

        $record->setFormInputValues($availability);

        $record->save();

        die;
    }


    public function deleteAvailability($availability = null)
    {
        $resourceAvailabilitySet = new resourcemanagement_ResourceAvailabilitySet();

        $availability = $resourceAvailabilitySet->get($availability);
        $resourceAvailabilitySet->delete($resourceAvailabilitySet->id->is($availability->id));

        return true;
    }






    public function saveInheritsAccessRights($resource = null, $ownAccessRights = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();

        $resource = $resourceSet->get($resource);
        if (!isset($resource)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        $resource->ownAccessRights = $ownAccessRights;

        $resource->save();

        die;
    }






    /**
     * @param string $resource		The resource id.
     */
    public function editAccess($resource = null)
    {
        $W = bab_Widgets();

        $resourceSet = new resourcemanagement_ResourceSet();

        /* @var $resource resourcemanagement_Resource */
        $resource = $resourceSet->get($resource);
        if (!isset($resource)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        $page = $W->BabPage();

        $page->addItem($W->Title(resourcemanagement_translate('Edit resource access rights'), 1));
        $page->addItem($W->Title($resource->name, 2));

        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        $enable_group	= 0;
        $disable_group	= 1;

        $macl = new macl(bab_rp('tg'), 'resource.saveAccess', $resource->id, 'acl');

        $macl->addTable(
            'resourcemanagement_resourceviewer_groups',
            resourcemanagement_translate('Who can view this resource calendar')
        );
        $macl->addTable(
            'resourcemanagement_resourcebooker_groups',
            resourcemanagement_translate('Who can book this resource')
        );
        $macl->addTable(
            'resourcemanagement_resourceextendedbooker_groups',
            resourcemanagement_translate('Who can book this resource for other people')
        );
        $macl->addTable(
            'resourcemanagement_resourcemanager_groups',
            resourcemanagement_translate('Who can manage this resource')
        );
        $macl->addTable(
            'resourcemanagement_resourceresamanager_groups',
            resourcemanagement_translate('Who can manage reservation of this resource')
        );
        $macl->addTable(
            'resourcemanagement_resourcenotified_groups',
            resourcemanagement_translate('Who is notified when this resource is booked')
        );

        $macl->filter(
            $enable_group,
            0 /* this parameter is no longer used */,
            $disable_group,
            $enable_group,
            $disable_group
        );

        $maclFrame = $W->Html($macl->getHtml());


        $inheritCheckBox = $W->Select()
            ->addOption(0, resourcemanagement_translate('Inherits from parent domain'))
            ->addOption(1, resourcemanagement_translate('Own access rights'));

        $page->addItem(
            $W->FlowItems(
                $W->Label(resourcemanagement_translate('Access rights'))
                    ->colon(),
                $inheritCheckBox
                    ->setName('ownAccessRights')
            )->setSpacing(1, 'em')
        );

        $inheritCheckBox->setValue($resource->ownAccessRights);
        $inheritCheckBox->setAjaxAction(
            resourcemanagement_Controller()->Resource()->saveInheritsAccessRights($resource->id),
            array(),
            'change'
        );

        $inheritCheckBox->setAssociatedDisplayable($maclFrame, array(1));

        $page->addItem($maclFrame);

        return $page;
    }




    public function saveAccess()
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
        maclGroups();

        resourcemanagement_redirect($this->proxy()->displayList());
    }



    /**
     *
     * @param int $resource		The resource id.
     * @param int $unavailable 	Bool : 1 or 0
     */
    public function makeTemporarilyUnavailable($resource, $unavailable = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();

        $resource = $resourceSet->get($resource);
        if (!isset($resource)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        if (!isset($unavailable)) {
            $resource->unavailable = !$resource->unavailable;
        }

        $resource->save();

        die;
    }






    /**
     *
     * @param int $resource		The resource id.
     */
    public function checkDelete($resource = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();

        /* @var $resource resourcemanagement_Resource */
        $resource = $resourceSet->get($resource);
        if (!isset($resource)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteResource($resource)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $W = bab_Widgets();

        $page = $W->BabPage();



        $dialog = $W->Frame();
        $dialog->addClass('BabLoginMenuBackground');
        $dialog->addClass('widget-bordered');

        $dialog->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $dialog->addItem($W->Title(resourcemanagement_translate('Delete resource?'), 1));
        //$dialog->addItem($W->Title($resource->name, 2));
        $dialog->addItem(resourcemanagement_resourceShortPreview($resource));


        $page->addItem($dialog);

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select(
                $reservationSet->resource->is($resource->id)
        );

        $nbReservations = $reservations->count();

        if ($nbReservations > 0) {

            $dialog->addItem(

                $W->Section(
                    sprintf(resourcemanagement_translate('There are %d reservations of this resource:'), $nbReservations),
                    $reservationsBox = $W->ListLayout()
                        ->setVerticalSpacing(1, 'em')
                )

            );

            $max = 10;
            foreach ($reservations as $reservation) {
                /* @var $reservation resourcemanagement_Reservation */

                if ($max-- <= 0) {
                    $reservationsBox->addItem($W->Label('...'));
                    break;
                }
                $reservationFrame = $W->VBoxItems(
                    $W->Label($reservation->description)->addClass('widget-strong'),
                    $W->Label($reservationSet->start->output($reservation->start))
                );

                $reservationsBox->addItem($reservationFrame);
            }

            $dialog->addItem(
                $W->VboxItems(
                    $W->Label(resourcemanagement_translate('All those reservations will be deleted if you confirm deletion!'))
                        ->addClass('icon', Func_Icons::STATUS_DIALOG_WARNING)
                )->addClass(Func_Icons::ICON_LEFT_32)
            );

            $confirmDeletionLink = $W->Link(
                resourcemanagement_translate('Confirm deletion anyway'),
                resourcemanagement_Controller()->Resource()->delete($resource->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        } else {

            $confirmDeletionLink = $W->Link(
                resourcemanagement_translate('Confirm deletion'),
                resourcemanagement_Controller()->Resource()->delete($resource->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        }

        $dialog->addItem(
            $W->FlowItems(
                $confirmDeletionLink,
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Resource()->displayList()
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
            )->setHorizontalSpacing(2, 'em')->addClass(Func_Icons::ICON_LEFT_16)
        );

        return $page;
    }





    /**
     *
     * @param int $resource		The resource id.
     */
    public function delete($resource = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();

        /* @var $resource resourcemanagement_Resource */
        $resource = $resourceSet->get($resource);
        if (!isset($resource)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteResource($resource)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select($reservationSet->resource->is($resource->id));

        foreach ($reservations as $reservation) {
            $reservation->delete();
        }

        $resource->delete();

        resourcemanagement_redirect(resourcemanagement_Controller()->Resource()->displayList(), resourcemanagement_translate('The resource has been deleted.'));
    }


}


