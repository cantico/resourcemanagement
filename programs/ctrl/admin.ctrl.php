<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/admin.ui.php';


/**
 *
 */
class resourcemanagement_CtrlAdmin extends resourcemanagement_Controller
{


    public function edit()
    {
        if (!bab_isUserAdministrator()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->addItemMenu('admin', resourcemanagement_translate('Administration'), resourcemanagement_Controller()->Admin()->edit()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('admin');

        $page->addItem(
            $W->Title(resourcemanagement_translate('Resource application managers'))
        );

        $editor = new resourcemanagement_AdminEditor;
        $page->addItem($editor);

        return $page;
    }


    public function manage()
    {
        if (!(resourcemanagement_canManageResources() || resourcemanagement_isManager())) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->addItem(
            $W->Title(resourcemanagement_translate('Resource management administration'), 1)
        );

        $this->addDefaultItemMenu($page);
// 		$page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
// 		$page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        $page->setCurrentItemMenu('management');


        $frame = $W->Frame();
        $frame->addClass('widget-bordered', 'BabLoginMenuBackground', Func_Icons::ICON_LEFT_24);

        $box = $W->FlowItems();
        $box->setSpacing(5, 'em');

        $frame->addItem($box);

        $parametersSection = $W->Section(
            resourcemanagement_translate('Parameters'),
            $W->VBoxLayout()
                ->setVerticalSpacing(1, 'em')
        );

        $box->addItem($parametersSection);

        if (resourcemanagement_isManager()) {
            $domainLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource domains'), Func_Icons::APPS_ORGCHARTS),
                resourcemanagement_Controller()->Domain()->displayList()
            );
            $parametersSection->addItem($domainLink);

            $reservationTypeLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Reservation types'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                resourcemanagement_Controller()->ReservationType()->displayList()
            );
            $parametersSection->addItem($reservationTypeLink);

            $typeLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Resource types'), Func_Icons::ACTIONS_VIEW_LIST_TEXT),
                resourcemanagement_Controller()->Type()->displayList()
            );
            $parametersSection->addItem($typeLink);

            $typeLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Default time slote display'), Func_Icons::CATEGORIES_PREFERENCES_OTHER),
                resourcemanagement_Controller()->Admin()->editTimeSlot()
            );
            $parametersSection->addItem($typeLink);

            $optionLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Options'), Func_Icons::PLACES_USER_APPLICATIONS),
                resourcemanagement_Controller()->Admin()->editOptions()
            );
            $parametersSection->addItem($optionLink);

        }
        if (resourcemanagement_canManageResources()) {
            $resourceLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Resources'), Func_Icons::OBJECTS_RESOURCE),
                resourcemanagement_Controller()->Resource()->displayList()
            );
            $parametersSection->addItem($resourceLink);
        }
        if (resourcemanagement_canManageEquipment()) {
            $equipmentLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Equipment'), Func_Icons::APPS_CRM),
                resourcemanagement_Controller()->Equipment()->displayList()
                );
            $parametersSection->addItem($equipmentLink);
        }
        if (resourcemanagement_canManageServices()) {
            $serviceLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Services'), Func_Icons::APPS_FILE_MANAGER),
                resourcemanagement_Controller()->Resource()->displayList()
            );
            $parametersSection->addItem($serviceLink);
        }
        if (resourcemanagement_canExportReservations()) {
            $reservationExportLink = $W->Link(
                $W->Icon(resourcemanagement_translate('Reservation export'), Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD),
                resourcemanagement_Controller()->Reservation()->displayList()
            );
            $parametersSection->addItem($reservationExportLink);
        }




        $page->addItem($frame);

        return $page;
    }


    public function save($options = array())
    {
        if (!bab_isUserAdministrator()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }


        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        aclSetRightsString('resourcemanagement_manager_groups', 1,
            $options['manager']);

        return true;
    }



    public function initdb()
    {
        if (!bab_isUserAdministrator()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        require_once dirname(__FILE__).'/../set/resource.class.php';
        require_once dirname(__FILE__).'/../set/type.class.php';
        require_once dirname(__FILE__).'/../set/resourcetype.class.php';

        global $babDB;

        $sql = 'TRUNCATE resourcemanagement_type';
        $babDB->db_query($sql);
        $sql = 'TRUNCATE resourcemanagement_resource';
        $babDB->db_query($sql);
        $sql = 'TRUNCATE resourcemanagement_resourcetype';
        $babDB->db_query($sql);


        $typeSet = new resourcemanagement_TypeSet();

        $typeFacturable = $typeSet->newRecord();
        $typeFacturable->name = 'Facturable';
        $typeFacturable->save();

        $typeVehicule = $typeSet->newRecord();
        $typeVehicule->name = 'Vehicule';
        $typeVehicule->save();



        $resourceSet = new resourcemanagement_ResourceSet();

        $resourceClio = $resourceSet->newRecord();
        $resourceClio->name = 'Clio ABC';
        $resourceClio->description = 'Bla';
        $resourceClio->save();

        $resourceSafrane = $resourceSet->newRecord();
        $resourceSafrane->name = 'Safrane DEF';
        $resourceSafrane->description = 'Hello';
        $resourceSafrane->save();


        $resourceTypeSet = new resourcemanagement_ResourceTypeSet();


        $resourceType = $resourceTypeSet->newRecord();
        $resourceType->resource = $resourceClio->id;
        $resourceType->type = $typeVehicule->id;
        $resourceType->save();

        $resourceType = $resourceTypeSet->newRecord();
        $resourceType->resource = $resourceClio->id;
        $resourceType->type = $typeFacturable->id;
        $resourceType->save();

        $resourceType = $resourceTypeSet->newRecord();
        $resourceType->resource = $resourceSafrane->id;
        $resourceType->type = $typeVehicule->id;
        $resourceType->save();

        $resourceType = $resourceTypeSet->newRecord();
        $resourceType->resource = $resourceSafrane->id;
        $resourceType->type = $typeFacturable->id;
        $resourceType->save();


    }

    public function editTimeSlot()
    {

        if (!bab_isUserAdministrator()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->addItemMenu('admin', resourcemanagement_translate('Administration'), resourcemanagement_Controller()->Admin()->edit()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('admin');

        $page->addItem(
            $W->Title(resourcemanagement_translate('Parameter'))
        );

        $editor = new resourcemanagement_AdminTimeSlotEditor();
        $page->addItem($editor);

        return $page;
    }


    public function saveTimeSlot($timeslot = null)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');
        $registry->setKeyValue('start', $timeslot['start']);
        $registry->setKeyValue('end', $timeslot['end']);

        $GLOBALS['babBody']->addNextPageMessage(resourcemanagement_translate('Record done.'));

        return true;
    }

    public function editOptions()
    {
        if(!bab_isUserAdministrator()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->addItemMenu('admin', resourcemanagement_translate('Administration'), resourcemanagement_Controller()->Admin()->edit()->url());
        $page->addItemMenu('reservations', resourcemanagement_translate('Reservations'), resourcemanagement_Controller()->Reservation()->displayList()->url());
        $page->setCurrentItemMenu('admin');

        $page->addItem(
            $W->Title(resourcemanagement_translate('Options'))
        );

        $editor = new resourcemanagement_AdminOptionEditor();
        $page->addItem($editor);

        return $page;
    }

    public function saveOptions($globalOptions = null)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/resourcemanagement');
        $registry->setKeyValue('linkedEventVisibility', $globalOptions['linkedEventVisibility']);

        $GLOBALS['babBody']->addNextPageMessage(resourcemanagement_translate('Record done.'));

        return true;
    }
}
