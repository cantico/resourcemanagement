<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/equipment.ui.php';
require_once dirname(__FILE__).'/../ui/domain.ui.php';
require_once dirname(__FILE__).'/../set/recurrence.class.php';
require_once dirname(__FILE__).'/../set/equipment.class.php';
require_once dirname(__FILE__).'/../set/reservation.class.php';


/**
 *
 */
class resourcemanagement_CtrlEquipment extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_EquipmentSet	$equipmentSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $equipmentSet)
    {

        // Initial conditions are based on "manage" access rights.
        $conditions = $equipmentSet->isManageable();

        if (isset($filter['domain']) && !empty($filter['domain'])) {
            $conditions = $conditions->_AND_($equipmentSet->isInDomain($filter['domain']));
        }

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($equipmentSet->name->contains($filter['name']));
        }

        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($equipmentSet->description->contains($filter['description']));
        }

        if (isset($filter['allowConditionalBooking'])) {
            if ($filter['allowConditionalBooking'] === '0') {
                $conditions = $conditions->_AND_($equipmentSet->allowConditionalBooking->is(false));
            } elseif ($filter['allowConditionalBooking'] === '1') {
                $conditions = $conditions->_AND_($equipmentSet->allowConditionalBooking->is(true));
            }
        }

        if (isset($filter['type']) && !empty($filter['type'])) {
            $equipmentTypeSet = new resourcemanagement_EquipmentTypeSet();
            $conditions = $conditions->_AND_($equipmentSet->id->in($equipmentTypeSet->type->is($filter['type']), 'equipment'));
        }

        return $conditions;
    }




    /**
     *
     * @param array	$filter
     * @return resourcemanagement_EquipmentTableView
     */
    protected function tableView($filter = null)
    {
        $equipmentSet = new resourcemanagement_EquipmentSet();

        $tableView = new resourcemanagement_EquipmentTableView();

        $tableView->addDefaultColumns($equipmentSet);
        $conditions = $this->getFilterCriteria($filter, $equipmentSet);
        $selection = $equipmentSet->select($conditions);

        $tableView->setDataSource($selection);

        return $tableView;
    }



    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $W->Link(
                resourcemanagement_translate('Create a new equipment'),
                $this->proxy()->edit()
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );

        $box->addItem($toolbar);


        $tableView = $this->tableView($filter);
        if (isset($itemId)) {
            $tableView->setId($itemId);
        }
        $tableView->setAjaxAction();

        $filter = $tableView->getFilterValues();

        $filterPanel = $tableView->filterPanel('equipments', $filter);

        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select($domainSet->isManaged());
        $domainSelect = resourcemanagement_DomainSelect($domains, true);
        $domainSelect->setName('domain');
        $filterForm = $filterPanel->getFilter();
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Domain')),
                $domainSelect
                ),
            0
        );

        $typeSelect = $W->Select();
        $typeSelect->setName('type');
        $typeSelect->addOption('', '');

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select();
        foreach ($types as $type) {
            $typeSelect->addOption($type->id, $type->name);
        }

        $filter = $filterPanel->getFilter();
        $filter->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Type'))
                ->colon(),
                $typeSelect
                ),
            1
        );
        $box->addItem($filterPanel);

        $box->setReloadAction($this->proxy()->filteredView(null, null, $tableView->getId()));

        return $box;
    }


    /**
     * Displays a filterable list of equipments.
     *
     * @param array $equipments		Filter data.
     */
    public function displayList($equipments = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('equipments', resourcemanagement_translate('Equipments'), resourcemanagement_Controller()->Equipment()->displayList()->url());
        $page->setCurrentItemMenu('equipments');


        $filter = isset($equipments['filter']) ? $equipments['filter'] : array();


        $page->setTitle(resourcemanagement_translate('Equipments management'));


        $filteredView = $this->filteredView($filter);

        $page->addItem($filteredView);

        return $page;
    }





    /**
     *
     * @param string $equipment
     * @return Widget_BabPage
     */
    public function display($equipment = null)
    {
        $equipmentFrame = $this->fullFrame($equipment);

        if ($this->isAjaxRequest()) {
            return $equipmentFrame;
        }

        $W = bab_Widgets();

        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('equipments', resourcemanagement_translate('Equipments'), resourcemanagement_Controller()->Equipment()->displayList()->url());
        $page->setCurrentItemMenu('equipments');

        $page->addItem($equipmentFrame);

        return $page;
    }







    /**
     * Displays the page with the equipment editor.
     *
     * @param string $equipment		The equipment id or null to create a new equipment.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($equipment = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('equipments', resourcemanagement_translate('Equipments'), resourcemanagement_Controller()->Equipment()->displayList()->url());

        $editor = new resourcemanagement_EquipmentEditor();
        $editor->addClass('widget-50em');
        $editor->setPersistent(true, Widget_Widget::STORAGE_SESSION);

        if (isset($equipment)) {
            $set = new resourcemanagement_EquipmentSet();
            $record = $set->get($equipment);

            if (!resourcemanagement_canUpdateEquipment($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->addItemMenu('equipment', resourcemanagement_translate('Equipment'), resourcemanagement_Controller()->Equipment()->edit($record->id)->url());
            $page->setCurrentItemMenu('equipment');

            $page->setTitle(resourcemanagement_translate('Edit equipment') . ' ' . $record->name);

            $editor->setEquipment($record);
        } else {
            $editor->setEquipment(null);
            $page->setTitle(resourcemanagement_translate('New equipment'));
        }

        $page->addItem($editor);

        return $page;
    }





    /**
     *
     * @param resourcemanagement_Equipment $equipment
     */
    protected function saveImages(resourcemanagement_Equipment $equipment)
    {
        $W = bab_Widgets();

        $files = $W->ImagePicker()->getTemporaryFiles('mainphoto');

        if (isset($files)) {
            $imagePath = $equipment->mainImagePath();
            try {
                $imagePath->deleteDir();
            } catch (bab_FolderAccessRightsException $e) { }
            $imagePath->createDir();

            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $equipment->importMainImage($filePickerItem);
            }
        }

        $files = $W->ImagePicker()->getTemporaryFiles('photo');

        if (isset($files)) {
            $imagePath = $equipment->imagePath();
            try {
                $imagePath->deleteDir();
            } catch (bab_FolderAccessRightsException $e) { }
            $imagePath->createDir();
            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $equipment->importImage($filePickerItem);
            }
        }
    }





    /**
     *
     * @param array $equipment		The equipment data as posted by the equipment editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($equipment = null)
    {
        if (!isset($equipment)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing equipment information.'));
        }

        $set = new resourcemanagement_EquipmentSet();

        /* @var $record resourcemanagement_Equipment */
        if (!empty($equipment['id'])) {
            $record = $set->get($equipment['id']);

            if (!resourcemanagement_canUpdateEquipment($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }
        } else {
            $record = $set->newRecord();
        }

        $record->setFormInputValues($equipment);

        $record->save();

//        $this->saveImages($record);

        $this->addReloadSelector('.depends-equipments');

        return true;
    }



    public function setEquipmentEditorTypes($editor, $equipment = null)
    {
        $_SESSION['resourcemanagement']['editor'][$editor]['types'] = explode(',', $equipment['types'][0]);
        die;
    }




    public function dynamicEditorFrame($editor)
    {
        $typeIds = $_SESSION['resourcemanagement']['editor'][$editor]['types'];

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select($typeSet->id->in($typeIds));

        $editor = new resourcemanagement_EquipmentEditor();

        $frame = $editor->dynamicSectionsFrame($types);
        $frame->setName('equipment');

        return $frame;
    }






    /**
     *
     * @param int $equipment		The equipment id.
     */
    public function checkDelete($equipment = null)
    {
        $equipmentSet = new resourcemanagement_EquipmentSet();

        /* @var $equipment resourcemanagement_Equipment */
        $equipment = $equipmentSet->get($equipment);
        if (!isset($equipment)) {
            throw new resourcemanagement_Exception('Trying to access a equipment with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteEquipment($equipment)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $W = bab_Widgets();

        $page = $W->BabPage();



        $dialog = $W->Frame();
        $dialog->addClass('BabLoginMenuBackground');
        $dialog->addClass('widget-bordered');

        $dialog->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $dialog->addItem($W->Title(resourcemanagement_translate('Delete equipment?'), 1));
        //$dialog->addItem($W->Title($equipment->name, 2));
        $dialog->addItem(resourcemanagement_equipmentShortPreview($equipment));


        $page->addItem($dialog);

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select(
                $reservationSet->equipment->is($equipment->id)
        );

        $nbReservations = $reservations->count();

        if ($nbReservations > 0) {

            $dialog->addItem(

                $W->Section(
                    sprintf(resourcemanagement_translate('There are %d reservations of this equipment:'), $nbReservations),
                    $reservationsBox = $W->ListLayout()
                        ->setVerticalSpacing(1, 'em')
                )

            );

            $max = 10;
            foreach ($reservations as $reservation) {
                /* @var $reservation resourcemanagement_Reservation */

                if ($max-- <= 0) {
                    $reservationsBox->addItem($W->Label('...'));
                    break;
                }
                $reservationFrame = $W->VBoxItems(
                    $W->Label($reservation->description)->addClass('widget-strong'),
                    $W->Label($reservationSet->start->output($reservation->start))
                );

                $reservationsBox->addItem($reservationFrame);
            }

            $dialog->addItem(
                $W->VboxItems(
                    $W->Label(resourcemanagement_translate('All those reservations will be deleted if you confirm deletion!'))
                        ->addClass('icon', Func_Icons::STATUS_DIALOG_WARNING)
                )->addClass(Func_Icons::ICON_LEFT_32)
            );

            $confirmDeletionLink = $W->Link(
                resourcemanagement_translate('Confirm deletion anyway'),
                resourcemanagement_Controller()->Equipment()->delete($equipment->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        } else {

            $confirmDeletionLink = $W->Link(
                resourcemanagement_translate('Confirm deletion'),
                resourcemanagement_Controller()->Equipment()->delete($equipment->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        }

        $dialog->addItem(
            $W->FlowItems(
                $confirmDeletionLink,
                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->Equipment()->displayList()
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
            )->setHorizontalSpacing(2, 'em')->addClass(Func_Icons::ICON_LEFT_16)
        );

        return $page;
    }





    /**
     *
     * @param int $equipment		The equipment id.
     */
    public function delete($equipment = null)
    {
        $equipmentSet = new resourcemanagement_EquipmentSet();

        /* @var $equipment resourcemanagement_Equipment */
        $equipment = $equipmentSet->get($equipment);
        if (!isset($equipment)) {
            throw new resourcemanagement_Exception('Trying to access a equipment with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteEquipment($equipment)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select($reservationSet->equipment->is($equipment->id));

        foreach ($reservations as $reservation) {
            $reservation->delete();
        }

        $equipment->delete();

        resourcemanagement_redirect(resourcemanagement_Controller()->Equipment()->displayList(), resourcemanagement_translate('The equipment has been deleted.'));
    }
}


