<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/type.ui.php';
require_once dirname(__FILE__).'/../set/typefield.class.php';



/**
 *
 */
class resourcemanagement_CtrlType extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ReTypeSet	$typeSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $typeSet)
    {
        // Initial conditions are base on read access rights.
        $conditions = new ORM_TrueCriterion();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($typeSet->name->contains($filter['name']));
        }

        return $conditions;
    }




    /**
     *
     */
    protected function tableView($filter = null)
    {
        $typeSet = new resourcemanagement_TypeSet();

        $tableView = new resourcemanagement_TypeTableView();

        $tableView->addDefaultColumns($typeSet);
        $conditions = $this->getFilterCriteria($filter, $typeSet);
        $selection = $typeSet->select($conditions);
        $tableView->setDataSource($selection);


        return $tableView;
    }



    /**
     *
     */
    public function descriptionFieldTableView($type = null)
    {
        $W = bab_Widgets();


        $set = new resourcemanagement_TypeFieldSet();

        $tableView = new resourcemanagement_TypeFieldTableView();

        $tableView->addDefaultColumns($set);


        $tableView->addColumn(
            widget_TableModelViewColumn($set->availabilityFilter, resourcemanagement_translate('Availability filter'))
            ->addClass('widget-align-center', 'widget-col-10em')
        );

        $tableView->addColumn(
            widget_TableModelViewColumn('', '')
            ->addClass('widget-align-center', 'widget-col-10em')
        );
        $tableView->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-align-right', 'widget-col-10em')
        );

        $selection = $set->select(
            $set->type->is($type)
                ->_AND_($set->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION))
        );
        $selection->orderDesc($set->order);
        $selection->orderAsc($set->id);
        $tableView->setDataSource($selection);


        $typeFieldEditor = new resourcemanagement_TypeFieldEditor();
        $typeFieldEditor->setHiddenValue('field[type]', $type);
        $typeFieldEditor->setHiddenValue('field[nature]', resourcemanagement_TypeField::NATURE_DESCRIPTION);

        $orderFieldEditor = new resourcemanagement_OrderFieldEditor($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);

        $vbox = $W->VBoxItems($tableView);

        $vbox->addItem(
            $W->FlowItems(
                $W->VBoxItems(
                    $W->Link(resourcemanagement_translate('Add description field...'))
                        ->addClass('widget-instant-button', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD),
                    $typeFieldEditor->addClass('widget-instant-form')
                )->addClass('widget-instant-container'),
                $W->VBoxItems(
                    $W->Link(resourcemanagement_translate('Reorder fields...'))
                        ->addClass('widget-instant-button', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_UP_DOUBLE),
                    $orderFieldEditor->addClass('widget-instant-form')
                )->addClass('widget-instant-container')
            )->addClass('widget-toolbar', Func_Icons::ICON_LEFT_SYMBOLIC)
            ->setHorizontalSpacing(1, 'em')
        );

        $typeFieldEditor->saveButton->setAjaxAction(resourcemanagement_Controller()->Type()->saveField(), $vbox);
        $orderFieldEditor->saveButton->setAjaxAction(resourcemanagement_Controller()->Type()->orderField(), $vbox);

        return $vbox;
    }





    /**
     *
     */
    public function reservationFieldTableView($type = null)
    {
        $W = bab_Widgets();

        $set = new resourcemanagement_TypeFieldSet();

        $tableView = new resourcemanagement_TypeFieldTableView();

        $tableView->addDefaultColumns($set);

        $tableView->addColumn(
            widget_TableModelViewColumn($set->exportFilter, resourcemanagement_translate('Export filter'))
            ->addClass('widget-align-center', 'widget-col-10em')
        );

        $tableView->addColumn(
            widget_TableModelViewColumn($set->exportable, resourcemanagement_translate('Exportable'))
                ->addClass('widget-align-center', 'widget-col-10em')
        );


        $tableView->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-align-right', 'widget-col-10em')
        );
        $selection = $set->select(
            $set->type->is($type)
                ->_AND_($set->nature->is(resourcemanagement_TypeField::NATURE_RESERVATION))
        );
        $selection->orderDesc($set->order);
        $selection->orderAsc($set->id);
        $tableView->setDataSource($selection);


        $typeFieldEditor = new resourcemanagement_TypeFieldEditor();
        $typeFieldEditor->setHiddenValue('field[type]', $type);
        $typeFieldEditor->setHiddenValue('field[nature]', resourcemanagement_TypeField::NATURE_RESERVATION);

        $orderFieldEditor = new resourcemanagement_OrderFieldEditor($type, resourcemanagement_TypeField::NATURE_RESERVATION);

        $vbox = $W->VBoxItems($tableView);

        $vbox->addItem(
            $W->FlowItems(
                $W->VBoxItems(
                    $W->Link(resourcemanagement_translate('Add reservation field...'))
                        ->addClass('widget-instant-button', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD),
                    $typeFieldEditor->addClass('widget-instant-form')
                )->addClass('widget-instant-container'),
                $W->VBoxItems(
                    $W->Link(resourcemanagement_translate('Reorder fields...'))
                        ->addClass('widget-instant-button', 'widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_UP_DOUBLE),
                    $orderFieldEditor->addClass('widget-instant-form')
                )->addClass('widget-instant-container')
            )->addClass('widget-toolbar', Func_Icons::ICON_LEFT_SYMBOLIC)
            ->setHorizontalSpacing(1, 'em')
        );

        $typeFieldEditor->saveButton->setAjaxAction(resourcemanagement_Controller()->Type()->saveField(), $vbox);
        $orderFieldEditor->saveButton->setAjaxAction(resourcemanagement_Controller()->Type()->orderField(), $vbox);

        return $vbox;
    }





    public function editNewType()
    {
        $W = bab_Widgets();

        $page = $W->Page();


        $page->addItem($this->newTypeEditor());

        return $page;
    }





    public function newTypeEditor()
    {
        $W = bab_Widgets();

        $editor = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $editor->setName('type');

        $editor->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('New resource type name'))
                    ->colon(),
                $W->LineEdit()
                    ->addClass('widget-30em')
                    ->setName('name')
            )->setVerticalSpacing(4, 'px')
        );

        $editor->addItem(
            $W->SubmitButton()
                ->setLabel(resourcemanagement_translate('Save'))
                ->setAction(resourcemanagement_Controller()->Type()->save())
        );
        $editor->setHiddenValue('tg', bab_rp('tg'));

        return $editor;
    }





    public function displayList($types = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('types', resourcemanagement_translate('Resource types'), resourcemanagement_Controller()->Type()->displayList()->url());
        $page->setCurrentItemMenu('types');

        $page->addItem($W->Title(resourcemanagement_translate('Resource types'), 1));


        $filter = isset($types['filter']) ? $types['filter'] : array();

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(2, 'em');


        $newTypeEditor = $this->newTypeEditor();

        $instantEditor = $W->VBoxItems(
            $W->Link(
                $W->Icon(resourcemanagement_translate('Create a new type'), Func_Icons::ACTIONS_LIST_ADD),
                $this->proxy()->editNewType()
            )->addClass('bab_toolbarItem icon widget-instant-button widget-actionbutton'),
            $newTypeEditor->addClass('widget-instant-form')
                ->setTitle(resourcemanagement_translate('Create a new type'))
        )->addClass('widget-instant-container');

        $toolbar->addItem($instantEditor);

        $page->addItem($toolbar);


        $tableView = $this->tableView($filter);
        $page->addItem($tableView);

        return $page;
    }





    public function fullFrame($type = null)
    {
        $typeSet = new resourcemanagement_TypeSet();
        $type = $typeSet->get($type);

        if (!isset($type)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $frame = new resourcemanagement_TypeFrame();
        $frame->setType($type);

        $frame->initialize();

        return $frame;
    }









    /**
     *
     * @param string $type
     * @return Widget_BabPage
     */
    public function display($type = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('types', resourcemanagement_translate('Resource types'), resourcemanagement_Controller()->Type()->displayList()->url());


        $page->addItem($this->fullFrame($type));

        return $page;
    }










    /**
     * Displays the page with the type editor.
     *
     * @param string $type		The type id or null to create a new type.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($type = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('types', resourcemanagement_translate('Resource types'), resourcemanagement_Controller()->Type()->displayList()->url());
        $page->addItemMenu('type', resourcemanagement_translate('Type'), resourcemanagement_Controller()->Type()->edit($type)->url());
        $page->setCurrentItemMenu('type');

        $editor = new resourcemanagement_TypeEditor();

        $editor->addClass('BabLoginMenuBackground');
        $editor->addClass('widget-bordered');

        if (isset($type)) {

            $set = new resourcemanagement_TypeSet();
            $record = $set->get($type);

            if (!resourcemanagement_canUpdateType($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->addItem($W->Title(resourcemanagement_translate('Edit type') . ' ' . $record->name, 1));

            $editor->setType($record);

        } else {

            $page->addItem($W->Title(resourcemanagement_translate('New type'), 1));

        }


        $page->addItem($editor);

        return $page;
    }





    /**
     * Displays the page with the description type field editor.
     *
     * @param string $type		The type id or null to create a new type.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function editTypeFieldDescription($typeField = null, $type = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('types', resourcemanagement_translate('Resource types'), resourcemanagement_Controller()->Type()->displayList()->url());
        $page->addItemMenu('type', resourcemanagement_translate('Type'), resourcemanagement_Controller()->Type()->editTypeFieldDescription($typeField, $type)->url());
        $page->setCurrentItemMenu('type');
        $editor = new resourcemanagement_TypeFieldEditor();

        $editor->addClass('BabLoginMenuBackground');
        $editor->addClass('widget-bordered');

        if (isset($typeField)) {

            $set = new resourcemanagement_TypeFieldDescriptionSet();
            $record = $set->get($typeField);
            if (!isset($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Invalid id'));
            }
            if (!resourcemanagement_canUpdateType($record->type)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->addItem($W->Title(resourcemanagement_translate('Edit field')));

            $editor->setTypeField($record);

        } else {

            $page->addItem($W->Title(resourcemanagement_translate('New field')));

            $editor->setHiddenValue('field[type]', $type);
        }

        $page->addItem($editor);

        return $page;
    }





    /**
     *
     * @param array $type		The type data as posted by the type editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($type = null, $fields = array())
    {

        if (!isset($type)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing type information.'));
        }

        $set = new resourcemanagement_TypeSet();

        /* @var $record resourcemanagement_Resource */
        if (!empty($type['id'])) {

            $record = $set->get($type['id']);

            if (!resourcemanagement_canUpdateType($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $creation = false;
        } else {

            if (!resourcemanagement_canCreateType()) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $record = $set->newRecord();
            $creation = true;
        }

        $sameNameCondition = $set->name->like($type['name']);
        if (isset($type['id'])) {
            $sameNameCondition = $sameNameCondition->_AND_($set->id->isNot($type['id']));
        }
        $sameNameTypes = $set->select($sameNameCondition);

        if ($sameNameTypes->count() > 0) {
            resourcemanagement_redirect($this->proxy()->displayList($record->id), 'The resource type name already exists.');
        }


        $record->setFormInputValues($type);

        $record->save();


        $typeFieldSet = new resourcemanagement_TypeFieldSet();

        foreach ($fields as $fieldId => $field) {
            $typeField = $typeFieldSet->get($fieldId);
            $typeField->setFormInputValues($field);
            $typeField->save();
        }

        if ($creation) {
            resourcemanagement_redirect($this->proxy()->edit($record->id));
        }

        return true;
    }





    public function delete($type)
    {
        if (!isset($type)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing type id.'));
        }

        $typeSet = new resourcemanagement_TypeSet();

        $type = $typeSet->get($type);

        if (!resourcemanagement_canUpdateType($type)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $type->delete();

        resourcemanagement_redirect(resourcemanagement_Controller()->Type()->displayList(), resourcemanagement_translate('The resource type has been deleted.'));
    }





    /**
     *
     * @param array $type		The type data as posted by the type editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function orderField($fields = null)
    {

        if (!isset($fields)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing field information.'));
        }

        $set = new resourcemanagement_TypeFieldSet();

        $order = 999;
        foreach ($fields as $field) {
            $fieldType = $set->get($set->id->is($field));
            if($fieldType){
                $fieldType->order = $order;
                $fieldType->save();
                $order--;
            }
        }

        die;
    }





    /**
     *
     * @param array $type		The type data as posted by the type editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function saveField($field = null)
    {

        if (!isset($field)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing field information.'));
        }

        $set = new resourcemanagement_TypeFieldSet();

        /* @var $record resourcemanagement_TypeField */
        if (!empty($field['id'])) {

            $record = $set->get($field['id']);

            if (!resourcemanagement_canUpdateType($record->type)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

        } else {

            if (!resourcemanagement_canUpdateType($field['type'])) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $record = $set->newRecord();
        }

        $record->setFormInputValues($field);

        $record->save();

        die;
    }



    public function deleteField($field = null)
    {
        $set = new resourcemanagement_TypeFieldSet();
        $record = $set->get($field);
        if (!isset($field)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Invalid field id.'));
        }
        $set->delete($set->id->is($record->id));

        die; // To remove when using ovidentia >= 8.2
        return true;
    }
}


