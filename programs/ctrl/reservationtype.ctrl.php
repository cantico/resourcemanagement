<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/reservationtype.ui.php';
require_once dirname(__FILE__).'/../set/recurrence.class.php';
require_once dirname(__FILE__).'/../set/reservationtype.class.php';
require_once dirname(__FILE__).'/../set/reservation.class.php';



/**
 *
 */
class resourcemanagement_CtrlReservationType extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ReReservationTypeSet	$reservationTypeSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $reservationTypeSet)
    {
        // Initial conditions are base on read access rights.
        $conditions = new ORM_TrueCriterion();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($reservationTypeSet->name->contains($filter['name']));
        }

        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($reservationTypeSet->description->contains($filter['description']));
        }

        return $conditions;
    }




    /**
     *
     */
    protected function tableView($filter = null)
    {
        $reservationTypeSet = new resourcemanagement_ReservationTypeSet();

        $tableView = new resourcemanagement_ReservationTypeTableView();

        $tableView->addDefaultColumns($reservationTypeSet);
        $conditions = $this->getFilterCriteria($filter, $reservationTypeSet);
        $selection = $reservationTypeSet->select($conditions);

        $tableView->setDataSource($selection);

        return $tableView;
    }






    public function displayList($reservationTypes = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->addItemMenu('reservationTypes', resourcemanagement_translate('Reservation types'), resourcemanagement_Controller()->ReservationType()->displayList()->url());
        $page->setCurrentItemMenu('reservationTypes');

        $page->addItem($W->Title(resourcemanagement_translate('Reservation types'), 1));

        $filter = isset($reservationTypes['filter']) ? $reservationTypes['filter'] : array();

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $W->Link(
                $W->Icon(resourcemanagement_translate('Create a new reservation type'), Func_Icons::ACTIONS_DOCUMENT_NEW ),
                $this->proxy()->edit()
            )->addClass('bab_toolbarItem')
        );

        $page->addItem($toolbar);



        $tableView = $this->tableView($filter);
        $filterPanel = $tableView->filterPanel('reservationTypes', $filter);
        $page->addItem($filterPanel);

        return $page;
    }
















    /**
     * Displays the page with the reservationType editor.
     *
     * @param string $reservationType		The reservationType id or null to create a new reservationType.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($reservationType = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->addItemMenu('calendarall', resourcemanagement_translate('CSN Calendar'), resourcemanagement_Controller()->Calendar()->displayAll()->url());

        $page->addItemMenu('calendar', resourcemanagement_translate('Resource calendar'), resourcemanagement_Controller()->Calendar()->display()->url());
        if (resourcemanagement_canManageResources()) {
            $page->addItemMenu('management', resourcemanagement_translate('Management'), resourcemanagement_Controller()->Admin()->manage()->url());
        }
        $page->addItemMenu('reservationTypes', resourcemanagement_translate('Reservation types'), resourcemanagement_Controller()->ReservationType()->displayList()->url());
//		$page->setCurrentItemMenu('reservationTypes');

        $editor = new resourcemanagement_ReservationTypeEditor();

        $editor->addClass('BabLoginMenuBackground');
        $editor->addClass('widget-bordered');

        if (isset($reservationType)) {

            $set = new resourcemanagement_ReservationTypeSet();
            $record = $set->get($reservationType);

            if (!resourcemanagement_canUpdateReservationType($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->addItem($W->Title(resourcemanagement_translate('Edit reservation type'), 1));

            $editor->setReservationType($record);

        } else {

            $page->addItem($W->Title(resourcemanagement_translate('New reservation type'), 1));

        }


        $page->addItem($editor);

        return $page;
    }







    /**
     *
     * @param array $reservationType		The reservationType data as posted by the reservationType editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($reservationType = null, $fields = array())
    {

        if (!isset($reservationType)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing reservation type information.'));
        }

        $set = new resourcemanagement_ReservationTypeSet();

        /* @var $record resourcemanagement_Resource */
        if (!empty($reservationType['id'])) {

            $record = $set->get($reservationType['id']);

            if (!resourcemanagement_canUpdateReservationType($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

        } else {

            if (!resourcemanagement_canCreateReservationType()) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $record = $set->newRecord();
        }


        $record->setFormInputValues($reservationType);

        $record->save();

        return true;
    }



    /**
     *
     * @param int $resource		The resource id.
     */
    public function checkDelete($reservationType = null)
    {
        $reservationTypeSet = new resourcemanagement_ReservationTypeSet();

        /* @var $reservationType resourcemanagement_ReservationType */
        $reservationType = $reservationTypeSet->get($reservationType);
        if (!isset($reservationType)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteReservationType($reservationType)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $W = bab_Widgets();

        $page = $W->BabPage();



        $dialog = $W->Frame();
        $dialog->addClass('BabLoginMenuBackground');
        $dialog->addClass('widget-bordered');

        $dialog->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $dialog->addItem($W->Title(resourcemanagement_translate('Delete reservation type?'), 1));
        $dialog->addItem($W->Title($reservationType->name, 2));
        //$dialog->addItem(resourcemanagement_resourceShortPreview($resource));


        $page->addItem($dialog);

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select(
            $reservationSet->type->is($reservationType->id)
        );

        $nbReservations = $reservations->count();

        if ($nbReservations > 0) {

            $dialog->addItem(

                $W->Section(
                    sprintf(resourcemanagement_translate('There are %d reservations of this type:'), $nbReservations),
                    $reservationsBox = $W->ListLayout()
                        ->setVerticalSpacing(1, 'em')
                )

            );

            $max = 10;
            foreach ($reservations as $reservation) {
                /* @var $reservation resourcemanagement_Reservation */

                if ($max-- <= 0) {
                    $reservationsBox->addItem($W->Label('...'));
                    break;
                }

                $reservationFrame = $W->VBoxItems(
                    $W->Label($reservation->description)->addClass('widget-strong'),
                    $W->Label($reservationSet->start->output($reservation->start))
                );

                $reservationsBox->addItem($reservationFrame);
            }

            $confirmDeletionLink = $W->Link(
                resourcemanagement_translate('Confirm deletion anyway'),
                resourcemanagement_Controller()->ReservationType()->delete($reservationType->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        } else {

            $confirmDeletionLink = $W->Link(
                    resourcemanagement_translate('Confirm deletion'),
                    resourcemanagement_Controller()->ReservationType()->delete($reservationType->id)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE);

        }


        $dialog->addItem(

            $W->FlowItems(

                $confirmDeletionLink,

                $W->Link(
                    resourcemanagement_translate('Cancel'),
                    resourcemanagement_Controller()->ReservationType()->displayList()
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_CANCEL)

            )->setHorizontalSpacing(2, 'em')->addClass(Func_Icons::ICON_LEFT_16)

        );

        return $page;
    }





    /**
     *
     * @param int $reservationType		The reservation type id.
     */
    public function delete($reservationType = null)
    {
        $reservationTypeSet = new resourcemanagement_ReservationTypeSet();

        /* @var $reservationType resourcemanagement_ReservationType */
        $reservationType = $reservationTypeSet->get($reservationType);
        if (!isset($reservationType)) {
            throw new resourcemanagement_Exception('Trying to access a resource with a wrong (unknown) id.');
        }

        if (!resourcemanagement_canDeleteReservationType($reservationType)) {
            throw new resourcemanagement_Exception('Access denied.');
        }

        $reservationSet = new resourcemanagement_ReservationSet();

        $reservations = $reservationSet->select($reservationSet->type->is($reservationType->id));

    // 		$nbReservations = $reservations->count();
// 		if ($nbReservations > 0) {
// 			throw new resourcemanagement_Exception(resourcemanagement_translate('There are reservations for this resource.'));
// 		}

        foreach ($reservations as $reservation) {
            $reservation->type = 0;
            $reservation->save();
        }

        $reservationType = $reservationTypeSet->delete($reservationTypeSet->id->is($reservationType->id));

        resourcemanagement_redirect(resourcemanagement_Controller()->ReservationType()->displayList(), resourcemanagement_translate('The reservation type has been deleted.'));
    }

}


