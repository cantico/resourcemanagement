<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/domain.ui.php';
require_once dirname(__FILE__).'/../set/domain.class.php';



/**
 *
 */
class resourcemanagement_CtrlDomain extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ReDomainSet	$domainSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $domainSet)
    {
        // Initial conditions are base on read access rights.
        $conditions = new ORM_TrueCriterion();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($domainSet->name->contains($filter['name']));
        }

        return $conditions;
    }




    /**
     *
     */
    public function treeview($itemId = 'resourcemanagement_domains')
    {
        $W = bab_Widgets();

        $domainSet = new resourcemanagement_DomainSet();

        $treeview = $W->SimpleTreeView($itemId);
        $treeview->setPersistent(true);

        $domains = $domainSet->select();

        foreach ($domains as $domain) {
            $element = $treeview->createElement($domain->id, '', $domain->name, '', '');

            $editLink = $W->Link(
                resourcemanagement_translate('Edit'),
                $this->proxy()->edit($domain->id)
            )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT, 'widget-actionbutton')
            ->setOpenMode(Widget_Link::OPEN_DIALOG);

            $addSubdomainLink = $W->Link(
                resourcemanagement_translate('Add a subdomain'),
                $this->proxy()->addSubDomain($domain->id)
            )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_NEW, 'widget-actionbutton')
            ->setOpenMode(Widget_Link::OPEN_DIALOG);

            $accessLink = $W->Link(
                resourcemanagement_translate('Access rights'),
                $this->proxy()->editAccess($domain->id)
            )->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS, 'widget-actionbutton');

            $deleteLink = $W->Link(
                '',
                $this->proxy()->delete($domain->id)
            )->setConfirmationMessage(resourcemanagement_translate('Are you sure you want to delete this domain?'))
            ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE, 'widget-actionbutton')
            ->setAjaxAction();

            $element->setItem(
                $W->FlowItems(
                    $W->Link(
                        $domain->name,
                        resourcemanagement_Controller()->Resource()->displayList(array('filter' => array('domain' => $domain->id)))
                    )->addClass('icon', Func_Icons::PLACES_FOLDER),
                    $W->FlowItems(
                        $editLink,
                        $addSubdomainLink,
                        $accessLink,
                        $deleteLink
                    )
                    ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC)
                    ->addClass('right_elements')
                    ->setHorizontalSpacing(4, 'px')
                )
                ->addClass(Func_Icons::ICON_LEFT_16)
            );


            $parentId = ($domain->parent == 0) ? null : $domain->parent;
            $treeview->appendElement($element, $parentId);
        }

        $treeview->addClass('depends-domains');
        $treeview->setReloadAction($this->proxy()->treeview($treeview->getId()));

        return $treeview;
    }






    public function displayList()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);

        $page->addItemMenu('domains', resourcemanagement_translate('Resource domains'), resourcemanagement_Controller()->Domain()->displayList()->url());
        $page->setCurrentItemMenu('domains');

        $page->setTitle(resourcemanagement_translate('Resource domains'));


        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $toolbar->addItem(
            $W->Link(
                resourcemanagement_translate('Create a new domain'),
                $this->proxy()->edit()
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG)
        );

        $page->addItem($toolbar);

        $treeview = $this->treeview();
        $page->addItem($treeview);

        return $page;
    }




    public function addSubDomain($parent = null)
    {
        return $this->edit(null, $parent);
    }



    /**
     * Displays the page with the domain editor.
     *
     * @param string $domain		The domain id or null to create a new domain.
     *
     * @throws Exception
     * @return Widget_BabPage
     */
    public function edit($domain = null, $parent = null)
    {
        if (!resourcemanagement_isManager()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);

        $page->addItemMenu('domains', resourcemanagement_translate('Resource domains'), resourcemanagement_Controller()->Domain()->displayList()->url());
        $page->addItemMenu('domain', resourcemanagement_translate('Domain'), resourcemanagement_Controller()->Domain()->edit($domain, $parent)->url());
        $page->setCurrentItemMenu('domain');

        $editor = new resourcemanagement_DomainEditor();

        if (isset($domain)) {
            $set = new resourcemanagement_DomainSet();
            $record = $set->get($domain);

            if (!resourcemanagement_canUpdateDomain($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $page->setTitle(resourcemanagement_translate('Edit domain'));

            $editor->setDomain($record);
        } else {
            $page->setTitle(resourcemanagement_translate('New domain'));

            if (isset($parent)) {
                $editor->setHiddenValue('domain[parent]', $parent);
            }
        }

        $page->addItem($editor);

        return $page;
    }



    /**
     *
     * @param array $domain		The domain data as posted by the domain editor.
     *
     * @throws Exception
     * @return boolean
     */
    public function save($domain = null)
    {

        if (!isset($domain)) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Missing domain information.'));
        }

        $set = new resourcemanagement_DomainSet();

        /* @var $record resourcemanagement_Resource */
        if (!empty($domain['id'])) {

            $record = $set->get($domain['id']);

            if (!resourcemanagement_canUpdateDomain($record)) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

        } else {

            if (!resourcemanagement_canCreateDomain()) {
                throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
            }

            $record = $set->newRecord();
        }


        $record->setFormInputValues($domain);

        $record->save();

        $this->addReloadSelector('.depends-domains');

        return true;
    }




    public function delete($domain = null)
    {
        $domainSet = new resourcemanagement_DomainSet();

        $domain = $domainSet->get($domain);
        /* @var $domain resourcemanagement_Domain */
        if (!isset($domain)) {
            throw new resourcemanagement_Exception('Trying to access a domain with a wrong (unknown) id.');
        }

        $domain->delete();

        $this->addReloadSelector('.depends-domains');

        return true;
    }






    public function saveInheritsAccessRights($domain = null, $ownAccessRights = null)
    {
        $domainSet = new resourcemanagement_DomainSet();

        /* @var $domain resourcemanagement_Domain */
        $domain = $domainSet->get($domain);
        if (!isset($domain)) {
            throw new resourcemanagement_Exception('Trying to access a domain with a wrong (unknown) id.');
        }

        $domain->ownAccessRights = $ownAccessRights;

        $domain->save();

        die;
    }





    /**
     * @param string $domain		The domain id.
     */
    public function editAccess($domain = null)
    {
        if (!resourcemanagement_isManager()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        $W = bab_Widgets();


        $domainSet = new resourcemanagement_DomainSet();

        $domain = $domainSet->get($domain);
        if (!isset($domain)) {
            throw new resourcemanagement_Exception('Trying to access a domain with a wrong (unknown) id.');
        }

        $page = $W->BabPage();
        $this->addDefaultItemMenu($page);

        $page->addItemMenu('domains', resourcemanagement_translate('Resource domains'), resourcemanagement_Controller()->Domain()->displayList()->url());
        $page->addItemMenu('domain', resourcemanagement_translate('Domain'), resourcemanagement_Controller()->Domain()->editAccess($domain->id)->url());
        $page->setCurrentItemMenu('domain');


        $page->addItem($W->Title(resourcemanagement_translate('Edit domain access rights'), 1));
        $page->addItem($W->Title($domain->name, 2));

        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

        $enable_group	= 0;
        $disable_group	= 1;

        $macl = new macl(bab_rp('tg'), 'domain.saveAccess', $domain->id, 'acl');

        $macl->addTable('resourcemanagement_domainresourceviewer_groups', resourcemanagement_translate('Who can view resource calendars of this domain'));
        $macl->addTable('resourcemanagement_domainresourcebooker_groups', resourcemanagement_translate('Who can book resources of this domain'));
        $macl->addTable('resourcemanagement_domainresourceextendedbooker_groups', resourcemanagement_translate('Who can book resources of this domain for other people'));
        $macl->addTable('resourcemanagement_domainresourcemanager_groups', resourcemanagement_translate('Who can manage resources of this domain'));
        $macl->addTable('resourcemanagement_domainresourceresamanager_groups', resourcemanagement_translate('Who can manage resources reservation of this domain'));
        $macl->addTable('resourcemanagement_domainresourcenotified_groups', resourcemanagement_translate('Who is notified when a resource of this domain is booked'));
        $macl->addTable('resourcemanagement_domainresourceexport_groups', resourcemanagement_translate('Who can export reservations of resources of this domain'));

        $macl->filter(
            $enable_group,
            0 /* this parameter is no longer used */,
            $disable_group,
            $enable_group,
            $disable_group
        );

        $maclFrame = $W->Html($macl->getHtml());

        if ($domain->parent) {
            $inheritCheckBox = $W->Select()
                ->addOption(0, resourcemanagement_translate('Inherits from parent domain'))
                ->addOption(1, resourcemanagement_translate('Own access rights'));

            $page->addItem(
                $W->FlowItems(
                    $W->Label(resourcemanagement_translate('Access rights'))
                        ->colon(),
                    $inheritCheckBox
                        ->setName('ownAccessRights')
                )->setSpacing(1, 'em')
            );

            $inheritCheckBox->setValue($domain->ownAccessRights);
            $inheritCheckBox->setAjaxAction(
            	resourcemanagement_Controller()->Domain()->saveInheritsAccessRights($domain->id),
				array(),
				'change'
			);

            $inheritCheckBox->setAssociatedDisplayable($maclFrame, array(1));
        }



        $page->addItem($maclFrame);

        return $page;
    }




    public function saveAccess()
    {
        if (!resourcemanagement_isManager()) {
            throw new resourcemanagement_Exception(resourcemanagement_translate('Access denied'));
        }

        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
        maclGroups();

        resourcemanagement_redirect($this->proxy()->displayList());
    }


}


