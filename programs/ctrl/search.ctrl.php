<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/resource.ui.php';

require_once dirname(__FILE__).'/../set/resource.class.php';
require_once dirname(__FILE__).'/../set/resourceavailability.class.php';


/**
 *
 */
class resourcemanagement_CtrlSearch extends resourcemanagement_Controller
{


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	array							$filter
     * @param	resourcemanagement_ResourceSet	$resourceSet
     *
     * @return ORM_Criteria
     */
    protected function getFilterCriteria($filter, $resourceSet)
    {

        // Initial conditions are base on read access rights.
        $conditions = $resourceSet->isBookable();

        if (isset($filter['domain']) && !empty($filter['domain'])) {
            $conditions = $conditions->_AND_($resourceSet->isInDomain($filter['domain']));
        }

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($resourceSet->name->contains($filter['name']));
        }

        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($resourceSet->description->contains($filter['description']));
        }

        if (isset($filter['allowConditionalBooking'])) {
            if ($filter['allowConditionalBooking'] === '0') {
                $conditions = $conditions->_AND_($resourceSet->allowConditionalBooking->is(false));
            } elseif ($filter['allowConditionalBooking'] === '1') {
                $conditions = $conditions->_AND_($resourceSet->allowConditionalBooking->is(true));
            }
        }

        if (isset($filter['type']) && !empty($filter['type'])) {
            $resourceTypeSet = new resourcemanagement_ResourceTypeSet();
            $resourceTypeSet->type();
            $conditions = $conditions->_AND_($resourceSet->id->in($resourceTypeSet->type->id->is($filter['type']), 'resource'));

            if (isset($filter['typefilter'.$filter['type']])) {
                $typeSet = new resourcemanagement_TypeSet();
                $type = $typeSet->get(
                    $typeSet->id->is($filter['type'])
                );
                $resourceExtSet = resourcemanagement_DynamicRecordSet::newTypeRecordSet($type, resourcemanagement_TypeField::NATURE_DESCRIPTION);
                $typecondition = new ORM_TrueCriterion();

                foreach ($filter['typefilter'.$filter['type']] as $k => $v) {
                    if ($v) {
                        if (is_numeric($v)) {
                            $typecondition = $typecondition->_AND_($resourceExtSet->$k->is($v));
                        } else {
                            $typecondition = $typecondition->_AND_($resourceExtSet->$k->contains($v));
                        }
                    }
                }

                $resources = array();
                $resourceExts = $resourceExtSet->select($typecondition);
                foreach ($resourceExts as $resourceExt) {
                    $resources[] = $resourceExt->id;
                }
                $conditions = $conditions->_AND_($resourceSet->id->in($resources));
            }
        }

        return $conditions;
    }




    /**
     *
     */
    protected function tableView($filter = null)
    {
        $resourceSet = new resourcemanagement_ResourceSet();
        $resourceSet->domain();

        $tableView = new resourcemanagement_ResourceSearchTableView();

        $tableView->addDefaultColumns($resourceSet);
        $conditions = $this->getFilterCriteria($filter, $resourceSet);
        $selection = $resourceSet->select($conditions);

        $tableView->setDataSource($selection);

         $start = isset($filter['start']) ? $filter['start'] : null;
         $end = isset($filter['end']) ? $filter['end'] : null;
        $tableView->setPeriod($start, $end);


        return $tableView;
    }





    /**
     *
     * @param string $resources
     * @return Widget_BabPage
     */
    public function displayList($resources = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $this->addDefaultItemMenu($page);
        $page->setCurrentItemMenu('search');

        $dateField = new ORM_DateField('d');
        $filter = isset($resources['filter']) ? $resources['filter'] : array();

        if (isset($filter['start']))  {
            $filter['start'] = $dateField->input($filter['start']) . ' 00:00:00';
        } else {
            $filter['start'] = date('Y-m-d') . ' 00:00:00';
        }
        if (isset($filter['end']))  {
            $filter['end'] = $dateField->input($filter['end']) . ' 23:59:59';
        } else {
            $filter['end'] = date('Y-m-d') . ' 23:59:59';
        }

        $toolbar = $W->FlowLayout();
        $toolbar->addClass('widget-toolbar', 'widget-100pc', Func_Icons::ICON_LEFT_16);
        $toolbar->setHorizontalSpacing(1, 'ex');

        $page->addItem($W->Title(resourcemanagement_translate('Search availability'), 1));


        $tableView = $this->tableView($filter);
        $filterPanel = $tableView->filterPanel('resources', $filter);
        $filterForm = $filterPanel->getFilter();

        $domainSet = new resourcemanagement_DomainSet();
        $domains = $domainSet->select();
        $domainSelect = resourcemanagement_DomainSelect($domains, true);
        $domainSelect->setName('domain');
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Domain')),
                $domainSelect
            ),
            0
        );

        /* Type field layour */
        $typeSection = $W->FlowItems()->setVerticalAlign('bottom');

        $typeFieldSet = new resourcemanagement_TypeFieldSet();
        $typeFields = $typeFieldSet->select(
            $typeFieldSet->nature->is(resourcemanagement_TypeField::NATURE_DESCRIPTION)
            ->_AND_($typeFieldSet->availabilityFilter->is(true))
        );
        $typeFields->orderAsc($typeFieldSet->type);
        $typeFields->orderDesc($typeFieldSet->order);
        $typeFields->orderAsc($typeFieldSet->id);

        $displayable = array();
        /* @var $typeField resourcemanagement_TypeField  */
        foreach ($typeFields as $typeField) {
            if(!isset($displayable[$typeField->type])){
                $displayable[$typeField->type] = $W->FlowItems()->setVerticalAlign('bottom')->setHorizontalSpacing(1, 'em');
                $typeSection->addItem($displayable[$typeField->type]);
            }
            $item = $W->LabelledWidget(
                $typeField->name,
                $typeField->getFilterInputWidget()
            );
            $displayable[$typeField->type]->addItem($W->NamedContainer('typefilter'.$typeField->type)->addItem($item));
        }

        $typeSelect = $W->Select();
        $typeSelect->setName('type');
        $typeSelect->addOption('', '');

        $typeSet = new resourcemanagement_TypeSet();
        $types = $typeSet->select();
        foreach ($types as $type) {
            $typeSelect->addOption($type->id, $type->name);
            if (isset($displayable[$type->id])) {
                $typeSelect->setAssociatedDisplayable($displayable[$type->id], array($type->id));
            }
        }

//		$filter = $filterPanel->getFilter();
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Type'))
                ->colon(),
                $typeSelect
            ),
            1
        );
        $filterForm->addItem(
            $typeSection,
            2
        );

        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('Start'))
                    ->colon(),
                $W->DatePicker()->setName('start')
            ),
            3
        );
        $filterForm->addItem(
            $W->VBoxItems(
                $W->Label(resourcemanagement_translate('End'))
                    ->colon(),
                $W->DatePicker()->setName('end')
            ),
            4
        );


        $page->addItem($filterPanel);

        return $page;
    }

}


