<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
require_once $GLOBALS['babInstallPath'].'utilit/workinghoursincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.periodcollection.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.eventcalendar.class.php';

require_once dirname(__FILE__).'/controller.class.php';
require_once dirname(__FILE__).'/ui/reservation.ui.php';
require_once dirname(__FILE__).'/set/reservation.class.php';
require_once dirname(__FILE__).'/set/resource.class.php';



class resourcemanagement_ResourceCalendar extends bab_EventCalendar implements bab_ResourceCalendar
{
	
	public $resourceId = null;
	
	
	
	/**
	 *
	 * @param resourcemanagement_Resource $resource
	 */
	public function __construct(resourcemanagement_Resource $resource)
	{
		$this->uid = 'resource' . $resource->id;
		$this->name = $resource->name;
		$this->resourceId = $resource->id;
	}


	
	/**
	 * (non-PHPdoc)
	 * @see bab_EventCalendar::getReferenceType()
	 */
	public function getReferenceType()
	{
		return 'resourcemanagement';
	}



	/**
	 * (non-PHPdoc)
	 * @see bab_EventCalendar::getType()
	 */
	public function getType()
	{
		return resourcemanagement_translate('Resource management calendar');
	}
	


	/**
	 * @see bab_EventCalendar::getBackend()
	 * @return Func_CalendarBackend_ResourceManagement
	 */
	public function getBackend()
	{
		$backend = bab_functionality::get('CalendarBackend/ResourceManagement');
		return $backend;
	}



	/**
	 * Triggered when the calendar has been added as a relation on $event
	 * @param bab_CalendarPeriod $event
	 * @return unknown_type
	 */
	public function onAddRelation(bab_CalendarPeriod $event)
	{

	}



	/**
	 * Triggered when the calendar has been updated as a relation on $event
	 * @param bab_CalendarPeriod $event
	 * @return unknown_type
	*/
	public function onUpdateRelation(bab_CalendarPeriod $event)
	{

	}

}






/**
 * Function called to collect displayable calendars.
 * Add all resources calendars the bab_icalendars collection.
 *
 * @param bab_eventCollectCalendarsBeforeDisplay $event
 * @return unknown_type
 */
function resourcemanagement_onCollectCalendarsBeforeDisplay(bab_eventCollectCalendarsBeforeDisplay $event)
{
	/* @var $backend Func_CalendarBackend_ResourceManagement */
	$backend = bab_functionality::get('CalendarBackend/ResourceManagement');
	if (!isset($backend) || $backend === false) {
		return;
	}

	
	$resourceSet = new resourcemanagement_ResourceSet();
	$resources = $resourceSet->select($resourceSet->isViewable());

	foreach ($resources as $resource) {
		$calendar = new resourcemanagement_ResourceCalendar($resource);
		$event->addCalendar($calendar);
	}
	
}




/**
 * This function is called by Ovidentia's calendar when
 * the planning is about to display the periods.
 *
 * @param bab_eventBeforePeriodsCreated $event
 */
function resourcemanagement_onBeforePeriodsCreated(bab_eventBeforePeriodsCreated $event)
{
	
	

	if (!is_array($event->periods->calendars)) {
		return;
	}
	
	if (!$event->periods->isPeriodCollection('bab_CalendarEventCollection')) {
		return;
	}


	$beginDate = $event->getBeginDate();
	if (!($beginDate instanceof BAB_DateTime)) {
		return;
	}

	$endDate = $event->getEndDate();
	if (!($endDate instanceof BAB_DateTime)) {
		return;
	}
	
	foreach ($event->periods->calendars as $calendar) {
	
		if (!($calendar instanceof resourcemanagement_ResourceCalendar)) {
			continue;
		}
		
		
		// Now we retrieve the beginning and the end of the period displayed by the planning.
		$planningBegin = $beginDate->getIsoDateTime();
		$planningEnd = $endDate->getIsoDateTime();

		$reservationSet = new resourcemanagement_ReservationSet();

		$resourceId = $calendar->resourceId;

		$reservations = $reservationSet->select(
			$reservationSet->resource->is($resourceId)
			->_AND_($reservationSet->end->greaterThanOrEqual($planningBegin))
			->_AND_($reservationSet->start->lessThanOrEqual($planningEnd))
		);
		
		$collection = new bab_CalendarEventCollection();
		$collection->setCalendar($calendar);
		
		// We add them to the list of periods to display.
		foreach ($reservations as $reservation) {
			$start = BAB_DateTime::fromIsoDateTime($reservation->start);
			$end = BAB_DateTime::fromIsoDateTime($reservation->end);
			
			$period = new bab_CalendarPeriod();
			
			$period->setDates($start, $end);
			
	 		$period->setProperty('UID',	$reservation->id);
	 		$period->setProperty('SUMMARY',	$reservation->description);
	 		$period->setProperty('DESCRIPTION',	$reservation->longDescription);
	 		$period->setProperty('LOCATION', '');

	 		$period->setProperty('X-CTO-COLOR', $reservation->getColor());
	 		$periodData = $period->getData();
	
	 		$periodData['viewurl'] = resourcemanagement_Controller()->Reservation()->display($reservation->id)->url();
	 		$periodData['viewinsamewindow'] = true;
	 		$period->setData($periodData);
	
			$collection->addPeriod($period);
	
			$event->periods->addPeriod($period);
		}
	}
	
}

